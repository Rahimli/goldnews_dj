/**
 * Created by rahim on 11/9/2016.
 */

/////////////////////////////////////////////////////////////////
    function password_tester(p1) {
            var input = $(p1);
            var re = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$/;
            var bool = re.test(input.val().trim());
            if (bool && input.val().trim().length > 7) {
                    input.removeClass("invalid").addClass("valid");
                    bool = true;
            }else {
                    input.removeClass("valid").addClass("invalid");
                    bool = false;
            }
            return bool;
    }

////////////////////////////////////////////////////////
     function changePasswordForm(){
         //{#    var csrftoken = getCookie('csrftoken');#}
         var changePass1Form = $('#changePass1Form');
         var changePass1Button = $('#changePass1Button');
         var change_current_password = $('#change-current-password');
         var change_new_password = $('#change-new-password');
         var change_repeat_password = $('#change-repeat-password');
         var change_success_text = $('#change-success-text');
         var change_invalid_text = $('#change-invalid-text');
//if (password_tester(change_new_password)){
//    alert('change_new_password')
//}
//if (password_tester(change_repeat_password)){
//    alert('change_repeat_password')
//}
//if (change_new_password.trim() == change_repeat_password.trim()){
//    alert('tesst')
//}
         if (password_tester(change_new_password) && password_tester(change_repeat_password)) {
         var url = changePass1Form.attr('action');
              //alert('nese');
//This is the Ajax post.Observe carefully. It is nothing but details of where_to_post,what_to_post
            changePass1Button.hide();
             //alert(changePass1Form.serialize());
         $.ajax({
             url: url, // the endpoint,commonly same url
             dataType: "json",
             type: "POST",
             data: changePass1Form.serialize(),
             success: function (json) {
                 //alert('success');
                //$('#loader-div').hide();
            if (json['res'] == 1){
                change_success_text.show();
                change_current_password.removeClass("invalid valid");
                change_new_password.removeClass("invalid valid");
                change_repeat_password.removeClass("invalid valid");
                setTimeout(function() { change_success_text.fadeOut(); changePass1Button.show(1000);}, 10000);
                change_success_text.text(json['message']);
                changePass1Form.each(function(){
                    this.reset();
                });
            }else {
                change_invalid_text.show();
                change_current_password.removeClass("invalid valid");
                change_new_password.removeClass("invalid valid");
                change_repeat_password.removeClass("invalid valid");
                setTimeout(function() { change_invalid_text.fadeOut(); changePass1Button.show(1000);}, 4000);
                change_invalid_text.text(json['message']);
            }

            //$('#newsletterForm input').removeClass("valid");
        },
             // handle a non-successful response
             error: function (xhr, errmsg, err) {
                 changePass1Button.show();
                 alert('error');
                //$('#email1').removeClass("invalid valid");
                //subs_invalid_text.show();
                //setTimeout(function() { subs_invalid_text.fadeOut(); newsletterButton.show(1000);}, 4000);
                //subs_invalid_text.text('Error ! Please, try again ');
            }
         });

         }
     }