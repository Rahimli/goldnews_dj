(function (lib, img, cjs, ss, an) {

var p; // shortcut to reference prototypes
lib.ssMetadata = [
		{name:"xezine_620x100_atlas_", frames: [[0,375,620,411],[622,375,262,89],[0,788,620,376],[0,1512,620,241],[0,1166,620,344],[0,0,960,373],[0,1755,620,189]]}
];


// symbols:



(lib.bina1 = function() {
	this.spriteSheet = ss["xezine_620x100_atlas_"];
	this.gotoAndStop(0);
}).prototype = p = new cjs.Sprite();



(lib.logo620 = function() {
	this.spriteSheet = ss["xezine_620x100_atlas_"];
	this.gotoAndStop(1);
}).prototype = p = new cjs.Sprite();



(lib.masa1 = function() {
	this.spriteSheet = ss["xezine_620x100_atlas_"];
	this.gotoAndStop(2);
}).prototype = p = new cjs.Sprite();



(lib.stol1 = function() {
	this.spriteSheet = ss["xezine_620x100_atlas_"];
	this.gotoAndStop(3);
}).prototype = p = new cjs.Sprite();



(lib.zal1 = function() {
	this.spriteSheet = ss["xezine_620x100_atlas_"];
	this.gotoAndStop(4);
}).prototype = p = new cjs.Sprite();



(lib.zal2 = function() {
	this.spriteSheet = ss["xezine_620x100_atlas_"];
	this.gotoAndStop(5);
}).prototype = p = new cjs.Sprite();



(lib.zal3 = function() {
	this.spriteSheet = ss["xezine_620x100_atlas_"];
	this.gotoAndStop(6);
}).prototype = p = new cjs.Sprite();
// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.Tween20 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#B17805").s().p("AALDOIAAlOIg/AiIgbhAIBKgmQAJgFAKgCQAJgDAJABQAVgBANAOQAOANAAAeIAAFjg");
	this.shape.setTransform(209,2.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#B17805").s().p("AAMDOIAAlOIhAAiIgbhAIBLgmQAIgFAKgCQAJgDAJABQAVgBANAOQANANABAeIAAFjg");
	this.shape_1.setTransform(178.7,2.8);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#B17805").s().p("AALDOIAAlOIg/AiIgbhAIBKgmQAJgFAKgCQAJgDAJABQAVgBANAOQAOANAAAeIAAFjg");
	this.shape_2.setTransform(129.5,2.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#B17805").s().p("Ag8DDQgcgOgTgcQgSgcgBgqIAAilQABgqASgcQATgcAcgOQAcgOAgAAQAhAAAcAOQAdAOASAcQASAcAAAqIAAClQAAAqgSAcQgSAcgdAOQgcAOghAAQggAAgcgOgAgbiIQgNAHgIAOQgJANAAAVIAACkQAAAUAJAOQAIANANAHQAOAGANABQAPgBAMgGQAOgHAJgNQAIgOAAgUIAAikQAAgVgIgNQgJgOgOgHQgMgGgPAAQgNAAgOAGg");
	this.shape_3.setTransform(101.2,3);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#B17805").s().p("Ag6DEQgcgNgRgbQgSgagBgnIAAgXQABhAAQg0QAQg1AcgoQAdgoAlgbIBMARQghAZgXAcQgYAdgOAbQgPAbgHAUQAPgMAQgFQAPgGAPAAQAaAAAWAOQAWAOANAaQAOAaAAAnIAAAeQgBAngRAaQgSAagbAOQgcANgfAAQgfAAgcgNgAgZAMQgMAHgIANQgIAOAAASIAAAVQAAASAIAOQAIANAMAGQANAIANAAQANgBAMgGQAMgGAIgNQAIgNAAgUIAAgVQgBgcgPgPQgQgOgVAAQgNAAgNAFg");
	this.shape_4.setTransform(51.7,3);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#B17805").s().p("Ag6DEQgcgNgRgbQgSgagBgnIAAgXQABhAAQg0QAQg1AcgoQAdgoAlgbIBMARQghAZgXAcQgYAdgOAbQgPAbgHAUQAPgMAQgFQAPgGAPAAQAaAAAWAOQAWAOANAaQAOAaAAAnIAAAeQgBAngRAaQgSAagbAOQgcANgfAAQgfAAgcgNgAgZAMQgMAHgIANQgIAOAAASIAAAVQAAASAIAOQAIANAMAGQANAIANAAQANgBAMgGQAMgGAIgNQAIgNAAgUIAAgVQgBgcgPgPQgQgOgVAAQgNAAgNAFg");
	this.shape_5.setTransform(21.4,3);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#B17805").s().p("AhODPQgVgDgIgOQgIgOABgSQAAgMAFgLQAEgKAKgKQAmglAdgkQAcglAOgfQAQggAAgYIAAgMQAAgXgLgLQgLgMgQAAQgUAAgVANQgUAMgPARIgRhFQATgUAYgJQAYgKAYAAQAbAAAZANQAZAMAPAYQAPAZABAjIAAAQQgBAjgSAoQgSAngdAnQgdAmggAfICQAAIAABCg");
	this.shape_6.setTransform(-9.2,2.8);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#B17805").s().p("Ag8DDQgdgOgSgcQgRgcgCgqIAAilQACgqARgcQASgcAdgOQAdgOAfAAQAgAAAdAOQAdAOASAcQASAcAAAqIAAClQAAAqgSAcQgSAcgdAOQgdAOggAAQgfAAgdgOgAgbiIQgMAHgJAOQgJANAAAVIAACkQAAAUAJAOQAJANAMAHQAOAGANABQAPgBAMgGQAOgHAJgNQAIgOAAgUIAAikQAAgVgIgNQgJgOgOgHQgMgGgPAAQgNAAgOAGg");
	this.shape_7.setTransform(-58,3);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#B17805").s().p("AhEDIQgbgGgZgLIAShCQAQAKAaAHQAZAHAZAAQAZABASgNQASgMABgfIAAgNQABgVgMgPQgNgQghgBIg2AAQgVAAgLgIQgLgJgFgLQgEgNAAgOIAAgIIABgHIAKhOQACgTALgRQALgSASgMQASgLAZgBIByAAIAABCIhpAAQgNAAgJAJQgIAKgCARIgFA8IArAAQAuAAAbAQQAaAQALAZQALAagBAfIAAASQAAArgTAaQgTAageAMQgeAMgjAAQgcAAgbgHg");
	this.shape_8.setTransform(-88.6,3.2);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#B17805").s().p("AAGDPIAAhWIhJAAQgdAAgNgPQgOgPAAgXIACgNIADgPIBLjSQAHgSAQgJQAPgJASAAQAPAAAOAHQAOAGAJAMQAJANAAASIAADOIAyAAIAABBIgyAAIAABWgAg4A4IA+AAIAAi9g");
	this.shape_9.setTransform(-119.1,2.8);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#B17805").s().p("AhQDAQAhgZAYgdQAXgcAOgbQAPgbAHgUQgPALgQAHQgPAFgQAAQgZAAgWgOQgWgOgNgbQgNgagBgmIAAgeQABgoARgaQASgaAcgNQAbgNAfAAQAfAAAcANQAcANARAbQASAaABAnIAAAXQAABAgQAzQgRA1gcApQgdApglAagAgZiIQgMAGgIANQgIANAAATIAAAVQABAdAPAPQAQAOAVAAQANAAANgGQAMgHAIgNQAIgMAAgUIAAgUQAAgTgIgNQgIgNgMgGQgNgIgNABQgNAAgMAGg");
	this.shape_10.setTransform(-149.2,3);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#B17805").s().p("AhQDAQAhgZAYgdQAXgcAOgbQAPgbAHgUQgPALgQAHQgPAFgQAAQgZAAgWgOQgWgOgNgbQgNgagBgmIAAgeQABgoARgaQASgaAcgNQAbgNAfAAQAfAAAcANQAcANARAbQASAaABAnIAAAXQAABAgQAzQgRA1gcApQgdApglAagAgZiIQgMAGgIANQgIANAAATIAAAVQABAdAPAPQAQAOAVAAQANAAANgGQAMgHAIgNQAIgMAAgUIAAgUQAAgTgIgNQgIgNgMgGQgNgIgNABQgNAAgMAGg");
	this.shape_11.setTransform(-179.5,3);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#B17805").s().p("AgiCNIAAhrIhqAAIAAhBIBqAAIAAhtIBEAAIAABtIBrAAIAABBIhrAAIAABrg");
	this.shape_12.setTransform(-210.4,2.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-227.6,-39,455.3,78.1);


(lib.Tween18 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.logo620();
	this.instance.parent = this;
	this.instance.setTransform(-131,-44.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-131,-44.5,262,89);


(lib.Tween16 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#987D22").s().p("AgYBSQgNAAgIgHQgHgIAAgLQAAgFACgGIAFgLIA4hRIg4AAIAAgiIBDAAQAOAAAFAGQAHAHAAAKQAAAGgCAHQgCAHgFAHIg2BPIBEAAIAAAig");
	this.shape.setTransform(223.7,21.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#987D22").s().p("AgRBSIAAijIAjAAIAACjg");
	this.shape_1.setTransform(214.2,21.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#987D22").s().p("AATBUIAAhvQAAgLgFgFQgGgFgHAAQgHgBgFAHQgHAFAAAMIAABtIgkAAIAAikIAeAAIAFAOQAGgIAJgEQAIgFAIAAQATABAMAOQAMAPAAAXIAAByg");
	this.shape_2.setTransform(203.9,21.5);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#987D22").s().p("AgRBSIAAijIAjAAIAACjg");
	this.shape_3.setTransform(193.7,21.6);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#987D22").s().p("AgaBTIgUgEIACgjIAQAEQAJACAJAAQAJAAAGgEQAFgEAAgJQAAgGgFgFQgGgFgIgEIgRgLQgIgFgFgIQgHgIAAgNIAAgFQAAgSAIgKQAGgKAMgEQALgFAMAAQAKAAALACQAKACAJADIgGAgIgNgDIgQgBQgIgBgEAEQgFAEgBAIQABAGAFAFQAFAFAIAEIARAKQAIAFAHAIQAFAIAAAMIAAAGQAAATgHALQgIALgLAEQgMAFgNAAQgKAAgLgCg");
	this.shape_4.setTransform(184.8,21.6);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#987D22").s().p("AAUB4IAAhZQgIAJgIAFQgIAFgJAAQgLAAgJgGQgJgGgHgMQgFgMgBgQIAAhAQABgSAFgMQAHgMAJgFQAJgGALAAQAKAAAIAFQAJAFAHAIIADgPIAgAAIAADsgAAAhUQgHAAgGAEQgFAFgBAKIAAA8QABAJAFAGQAGAFAHAAQAHAAAGgGQAGgFABgLIAAg3QgBgMgGgFQgGgFgGAAIgBAAg");
	this.shape_5.setTransform(172.5,25.1);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#987D22").s().p("AgqBHQgMgOAAgYIAAg/QAAgRAHgMQAGgMAJgGQAKgHAMAAQAIAAAIAEQAHAFAIAIIADgOIAfAAIAACjIgfAAIgEgRQgGAKgIAFQgIAFgIAAQgUgBgMgNgAAAgxQgIAAgFAFQgFAGAAAKIAAA6QAAAKAFAFQAGAFAHAAQAHAAAGgFQAGgGAAgLIAAg2QAAgMgGgGQgGgFgGAAIgBAAg");
	this.shape_6.setTransform(158.8,21.6);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#987D22").s().p("AgZBPQgMgGgHgLQgIgMAAgQIAAhCQAAgSAIgLQAHgMAMgFQAMgGANAAQAOAAAMAFQAMAFAHALQAIALAAARIAAAOIgkAFIAAgPQAAgJgFgFQgFgEgHAAQgGgBgFAFQgFAEAAAKIAAA/QAAAJAFAFQAFAEAGAAQAHAAAFgEQAFgFAAgJIAAgMIAkAAIAAAOQAAAQgIALQgHAMgMAGQgMAGgOAAQgMAAgNgGg");
	this.shape_7.setTransform(145.9,21.6);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#987D22").s().p("AgpBHQgMgOgBgYIAAg/QAAgRAHgMQAFgMALgGQAJgHALAAQAJAAAIAEQAHAFAIAIIADgOIAfAAIAACjIgfAAIgDgRQgHAKgIAFQgHAFgKAAQgTgBgLgNgAAAgxQgHAAgGAFQgFAGAAAKIAAA6QAAAKAGAFQAFAFAHAAQAIAAAFgFQAGgGAAgLIAAg2QAAgMgGgGQgGgFgGAAIgBAAg");
	this.shape_8.setTransform(132.6,21.6);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#987D22").s().p("AgRB/IAAj9IAjAAIAAD9g");
	this.shape_9.setTransform(122.5,17.2);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#987D22").s().p("AgqBHQgLgOgBgYIAAg/QAAgRAHgMQAFgMALgGQAJgHAMAAQAIAAAIAEQAHAFAIAIIADgOIAfAAIAACjIgfAAIgEgRQgGAKgIAFQgHAFgJAAQgUgBgMgNgAAAgxQgHAAgGAFQgFAGAAAKIAAA6QAAAKAGAFQAFAFAHAAQAIAAAFgFQAGgGAAgLIAAg2QAAgMgGgGQgGgFgGAAIgBAAg");
	this.shape_10.setTransform(112.2,21.6);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#987D22").s().p("AATB4IAAhZQgGAJgJAFQgIAFgJAAQgLAAgKgGQgIgGgGgMQgGgMAAgQIAAhAQAAgSAGgMQAGgMAIgFQAKgGALAAQAKAAAIAFQAKAFAGAIIAEgPIAeAAIAADsgAAAhUQgHAAgFAEQgGAFgBAKIAAA8QABAJAGAGQAFAFAHAAQAHAAAGgGQAGgFAAgLIAAg3QAAgMgGgFQgFgFgHAAIgBAAg");
	this.shape_11.setTransform(98.7,25.1);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#987D22").s().p("AgRBSIAAijIAjAAIAACjg");
	this.shape_12.setTransform(78.6,21.6);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#987D22").s().p("AgZBSQgMAAgIgHQgHgIAAgLQAAgFACgGIAFgLIA4hRIg4AAIAAgiIBDAAQANAAAHAGQAGAHAAAKQAAAGgCAHQgDAHgEAHIg2BPIBEAAIAAAig");
	this.shape_13.setTransform(69.1,21.6);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#987D22").s().p("AgqBHQgMgOAAgYIAAg/QAAgRAHgMQAGgMAJgGQAKgHAMAAQAIAAAIAEQAHAFAIAIIADgOIAfAAIAACjIgfAAIgEgRQgGAKgIAFQgIAFgIAAQgUgBgMgNgAAAgxQgIAAgFAFQgFAGAAAKIAAA6QAAAKAFAFQAGAFAHAAQAHAAAGgFQAGgGAAgLIAAg2QgBgMgFgGQgGgFgGAAIgBAAg");
	this.shape_14.setTransform(56.2,21.6);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#987D22").s().p("AgqBUIAAikIAeAAIAEARQAGgKAHgEQAIgGAJAAIALACQAEACAGADIAAAkQgGgEgGgCQgGgCgHAAQgLAAgHAGQgFAHgBANIAABqg");
	this.shape_15.setTransform(44.7,21.5);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#987D22").s().p("AAUBUIAAhvQgBgLgFgFQgFgFgIAAQgHgBgGAHQgFAFgBAMIAABtIgkAAIAAikIAfAAIADAOQAIgIAIgEQAIgFAIAAQATABAMAOQAMAPAAAXIAAByg");
	this.shape_16.setTransform(22.4,21.5);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#987D22").s().p("AgYBQQgMgFgIgLQgIgLAAgRIAAgOQAAgNAEgIQAGgGAHgEQAIgDAKAAIAiAAIAAgTQAAgJgFgFQgFgFgHABQgGgBgFAFQgFAFAAAJIAAAFIgkAAIAAgHQAAgQAIgMQAIgLAMgGQAMgGAMAAQAOAAAMAGQALAGAIALQAIAMAAAQIAABCQAAASgHALQgIAMgMAFQgMAGgOAAQgMAAgMgFgAgQAgQAAAJAFAFQAFAEAGAAQAHAAAFgEQAFgFAAgJIAAgLIghAAg");
	this.shape_17.setTransform(9.2,21.6);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#987D22").s().p("AgpByQgMgNgBgZIAAhBQAAgPAGgMQAHgMAKgGQAJgGALgBQAJAAAHAFQAJAEAHAIIAAhnIAiAAIAAD9IgfAAIgDgRQgHAKgIAEQgHAFgKAAQgSgBgMgNgAAAgGQgHAAgGAFQgFAEAAALIAAA8QAAAKAFAFQAGAEAHAAQAIAAAFgFQAGgFAAgMIAAg3QAAgMgGgEQgGgGgGAAIgBAAg");
	this.shape_18.setTransform(-4,17.3);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#987D22").s().p("AgZBSQgNAAgGgHQgIgIAAgLQAAgFACgGIAGgLIA3hRIg4AAIAAgiIBCAAQAOAAAHAGQAGAHAAAKQAAAGgCAHQgDAHgEAHIg2BPIBEAAIAAAig");
	this.shape_19.setTransform(-16.7,21.6);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#987D22").s().p("AgRB8IAAikIAjAAIAACkgAgPhQQgGgIAAgJQAAgLAGgIQAGgHAJAAQAKAAAGAHQAGAIAAALQAAAJgGAIQgGAGgKABQgJgBgGgGg");
	this.shape_20.setTransform(-26.2,17.5);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#987D22").s().p("AAUBUIAAhvQgBgLgFgFQgFgFgIAAQgGgBgHAHQgFAFgBAMIAABtIgkAAIAAikIAfAAIADAOQAIgIAIgEQAIgFAIAAQATABAMAOQAMAPAAAXIAAByg");
	this.shape_21.setTransform(-36.4,21.5);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#987D22").s().p("AgRB8IAAikIAjAAIAACkgAgPhQQgGgIAAgJQAAgLAGgIQAGgHAJAAQAKAAAGAHQAGAIAAALQAAAJgGAIQgGAGgKABQgJgBgGgGg");
	this.shape_22.setTransform(-46.7,17.5);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#987D22").s().p("AgqBUIAAikIAeAAIAEARQAGgKAHgEQAIgGAJAAIAKACQAFACAGADIAAAkQgGgEgGgCQgGgCgHAAQgKAAgIAGQgFAHgBANIAABqg");
	this.shape_23.setTransform(-55,21.5);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#987D22").s().p("AgZBQQgMgFgHgLQgIgLAAgRIAAgOQAAgNAFgIQAEgGAIgEQAIgDAJAAIAjAAIAAgTQAAgJgFgFQgFgFgHABQgGgBgFAFQgFAFAAAJIAAAFIgkAAIAAgHQAAgQAIgMQAHgLAMgGQANgGAMAAQAOAAAMAGQAMAGAHALQAIAMAAAQIAABCQAAASgIALQgHAMgMAFQgMAGgOAAQgMAAgNgFgAgQAgQAAAJAFAFQAFAEAGAAQAHAAAFgEQAFgFAAgJIAAgLIghAAg");
	this.shape_24.setTransform(-67,21.6);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#987D22").s().p("AgRB/IAAj9IAjAAIAAD9g");
	this.shape_25.setTransform(-76.9,17.2);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#987D22").s().p("AgqBUIAAikIAeAAIAEARQAGgKAHgEQAIgGAJAAIALACQAEACAGADIAAAkQgGgEgGgCQgGgCgHAAQgKAAgIAGQgFAHgBANIAABqg");
	this.shape_26.setTransform(-85.2,21.5);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#987D22").s().p("AgRB8IAAikIAjAAIAACkgAgPhQQgGgIAAgJQAAgLAGgIQAGgHAJAAQAKAAAGAHQAGAIAAALQAAAJgGAIQgGAGgKABQgJgBgGgGg");
	this.shape_27.setTransform(-94.3,17.5);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#987D22").s().p("AgFB7QgIgEgGgKIgEARIgfAAIAAj9IAjAAIAABnQAHgIAHgEQAIgFAIAAQAMABAKAGQAKAGAGAMQAGAMAAAPIAABBQAAAZgMANQgMANgUABQgIAAgIgFgAgLAAQgHAEAAAMIAAA3QABAMAFAFQAGAFAHAAQAHAAAFgEQAGgFAAgKIAAg8QAAgLgFgEQgFgFgIAAIgBAAQgGAAgFAGg");
	this.shape_28.setTransform(-104.4,17.3);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#987D22").s().p("AgqByQgMgNAAgZIAAhBQAAgPAGgMQAGgMAKgGQAKgGAMgBQAIAAAHAFQAIAEAIAIIAAhnIAiAAIAAD9IgfAAIgEgRQgGAKgIAEQgIAFgIAAQgUgBgMgNgAAAgGQgIAAgFAFQgFAEAAALIAAA8QAAAKAFAFQAGAEAHAAQAHAAAGgFQAGgFAAgMIAAg3QgBgMgFgEQgGgGgGAAIgBAAg");
	this.shape_29.setTransform(-118,17.3);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#987D22").s().p("AgZBQQgLgFgIgLQgIgLAAgRIAAgOQAAgNAEgIQAGgGAHgEQAIgDAKAAIAiAAIAAgTQAAgJgFgFQgFgFgHABQgGgBgFAFQgFAFAAAJIAAAFIgkAAIAAgHQAAgQAIgMQAIgLALgGQAMgGANAAQANAAANAGQAMAGAHALQAIAMAAAQIAABCQAAASgHALQgIAMgMAFQgMAGgOAAQgMAAgNgFgAgQAgQAAAJAFAFQAFAEAGAAQAHAAAFgEQAFgFAAgJIAAgLIghAAg");
	this.shape_30.setTransform(-131.1,21.6);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#987D22").s().p("AgBBoQgJgFgFgKQgEgLAAgRIAAhVIgVAAIAAgiIAVAAIAAgnIAigKIAAAxIAaAAIAAAiIgaAAIAABVQAAAHADADQADADAFAAIAGgBIAFgBIAAAhIgLACIgKABQgKAAgHgEg");
	this.shape_31.setTransform(-142,19.3);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#987D22").s().p("AATBUIAAhvQAAgLgFgFQgGgFgHAAQgHgBgFAHQgHAFAAAMIAABtIgkAAIAAikIAeAAIAFAOQAGgIAJgEQAIgFAIAAQATABAMAOQAMAPAAAXIAAByg");
	this.shape_32.setTransform(-163.3,21.5);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#987D22").s().p("AgYBQQgNgFgHgLQgIgLAAgRIAAgOQAAgNAEgIQAFgGAIgEQAIgDAJAAIAjAAIAAgTQAAgJgFgFQgFgFgHABQgGgBgFAFQgFAFAAAJIAAAFIgkAAIAAgHQAAgQAIgMQAHgLANgGQALgGANAAQANAAAMAGQAMAGAIALQAIAMAAAQIAABCQAAASgIALQgHAMgMAFQgMAGgOAAQgMAAgMgFgAgQAgQAAAJAFAFQAFAEAGAAQAHAAAFgEQAFgFAAgJIAAgLIghAAg");
	this.shape_33.setTransform(-176.5,21.6);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#987D22").s().p("AgRB/IAAj9IAjAAIAAD9g");
	this.shape_34.setTransform(-186.4,17.2);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#987D22").s().p("AgRB8IAAikIAjAAIAACkgAgPhQQgGgIAAgJQAAgLAGgIQAGgHAJAAQAKAAAGAHQAGAIAAALQAAAJgGAIQgGAGgKABQgJgBgGgGg");
	this.shape_35.setTransform(-193.3,17.5);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#987D22").s().p("AAUBUIAAhvQgBgLgFgFQgFgFgIAAQgHgBgGAHQgFAFgBAMIAABtIgkAAIAAikIAfAAIADAOQAIgIAIgEQAIgFAIAAQATABAMAOQAMAPAAAXIAAByg");
	this.shape_36.setTransform(-203.5,21.5);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("#987D22").s().p("AgYBQQgMgFgIgLQgIgLAAgRIAAgOQAAgNAEgIQAGgGAHgEQAIgDAKAAIAiAAIAAgTQAAgJgFgFQgFgFgHABQgGgBgFAFQgFAFAAAJIAAAFIgkAAIAAgHQAAgQAIgMQAIgLAMgGQAMgGAMAAQAOAAAMAGQALAGAIALQAIAMAAAQIAABCQAAASgHALQgIAMgMAFQgMAGgOAAQgMAAgMgFgAgQAgQAAAJAFAFQAFAEAGAAQAHAAAFgEQAFgFAAgJIAAgLIghAAg");
	this.shape_37.setTransform(-216.7,21.6);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("#987D22").s().p("AgBBoQgJgFgFgKQgFgLABgRIAAhVIgVAAIAAgiIAVAAIAAgnIAigKIAAAxIAaAAIAAAiIgaAAIAABVQAAAHADADQADADAFAAIAGgBIAFgBIAAAhIgLACIgKABQgKAAgHgEg");
	this.shape_38.setTransform(-227.6,19.3);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("#987D22").s().p("AgaBTIgTgEIABgjIAQAEQAJACAJAAQAKAAAEgEQAGgEAAgJQAAgGgGgFQgFgFgHgEIgSgLQgIgFgGgIQgFgIAAgNIAAgFQgBgSAIgKQAGgKAMgEQAKgFAMAAQALAAALACQAJACAKADIgGAgIgNgDIgQgBQgIgBgEAEQgGAEAAAIQAAAGAGAFQAFAFAIAEIARAKQAJAFAFAIQAGAIAAAMIAAAGQAAATgIALQgGALgMAEQgMAFgNAAQgLAAgKgCg");
	this.shape_39.setTransform(-237.6,21.6);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f("#987D22").s().p("AgRB8IAAikIAjAAIAACkgAgPhQQgGgIAAgJQAAgLAGgIQAGgHAJAAQAKAAAGAHQAGAIAAALQAAAJgGAIQgGAGgKABQgJgBgGgGg");
	this.shape_40.setTransform(-246.6,17.5);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f("#987D22").s().p("AgqBTIAAijIAdAAIAGARQAFgKAHgFQAIgEAIAAQAGgBAFACQAGACAFADIAAAkQgGgEgGgCQgHgCgFAAQgMgBgGAHQgHAGAAAOIAABpg");
	this.shape_41.setTransform(220.8,-17.8);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f("#987D22").s().p("AgYBQQgMgFgIgLQgIgLAAgRIAAgOQAAgNAEgIQAFgGAIgEQAIgDAJAAIAjAAIAAgTQAAgJgFgFQgFgFgHABQgGgBgFAFQgFAFAAAJIAAAFIgkAAIAAgHQAAgQAIgMQAIgLAMgGQAMgGAMAAQANAAAMAGQAMAGAIALQAIAMAAAQIAABCQAAASgHALQgIAMgMAFQgMAGgOAAQgMAAgMgFgAgQAgQAAAJAFAFQAFAEAGAAQAHAAAFgEQAFgFAAgJIAAgLIghAAg");
	this.shape_42.setTransform(208.8,-17.7);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f("#987D22").s().p("AgYB0QgMgGgHgLQgIgLAAgRIAAgIIAkAAIAAAEQAAAKAFAFQAFAFAGAAQAHAAAFgFQAGgFAAgKIAAglQgGAKgIAEQgHAFgJAAQgSgBgNgNQgMgOAAgXIAAhAQAAgRAGgMQAGgMAKgGQAKgHALAAQAJAAAHAEQAJAFAHAIIACgOIAgAAIAAC8QgBARgHALQgIALgMAFQgMAGgOAAQgNAAgMgFgAAAhVQgIAAgFAFQgFAGAAAKIAAA7QAAAJAFAFQAGAFAHAAQAHAAAGgFQAGgGAAgKIAAg3QgBgMgFgGQgGgFgGAAIgBAAg");
	this.shape_43.setTransform(195.5,-14.1);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f("#987D22").s().p("AgRB7IAAijIAjAAIAACjgAgPhQQgGgHAAgKQAAgMAGgGQAGgIAJAAQAKAAAGAIQAGAGAAAMQAAAKgGAHQgGAGgKABQgJgBgGgGg");
	this.shape_44.setTransform(185.4,-21.8);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f("#987D22").s().p("AgpByQgNgNAAgZIAAhBQAAgPAGgMQAHgLAJgHQAKgGALAAQAJAAAHADQAJAFAHAJIAAhoIAiAAIAAD8IgfAAIgDgQQgHAJgIAFQgIAFgJAAQgSAAgMgOgAAAgGQgIAAgFAFQgFAEAAALIAAA8QAAAKAFAFQAGAEAHABQAHgBAGgFQAGgGAAgLIAAg3QgBgMgFgFQgGgFgGAAIgBAAg");
	this.shape_45.setTransform(175.2,-22);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f("#987D22").s().p("AgYBQQgMgFgIgLQgIgLAAgRIAAgOQAAgNAEgIQAGgGAHgEQAIgDAKAAIAiAAIAAgTQAAgJgFgFQgFgFgHABQgGgBgFAFQgFAFAAAJIAAAFIgkAAIAAgHQAAgQAIgMQAIgLAMgGQAMgGAMAAQAOAAALAGQANAGAHALQAIAMAAAQIAABCQAAASgHALQgIAMgMAFQgMAGgOAAQgMAAgMgFgAgQAgQAAAJAFAFQAFAEAGAAQAHAAAFgEQAFgFAAgJIAAgLIghAAg");
	this.shape_46.setTransform(152.2,-17.7);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f("#987D22").s().p("AgOBQQgGgFgDgJIgkiUIAmAAIAVB/IAWh/IAmAAIgkCUQgDAJgGAFQgHADgIAAQgHAAgHgDg");
	this.shape_47.setTransform(139.5,-17.6);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f("#987D22").s().p("AgRB7IAAijIAjAAIAACjgAgPhQQgGgHAAgKQAAgMAGgGQAGgIAJAAQAKAAAGAIQAGAGAAAMQAAAKgGAHQgGAGgKABQgJgBgGgGg");
	this.shape_48.setTransform(120,-21.8);

	this.shape_49 = new cjs.Shape();
	this.shape_49.graphics.f("#987D22").s().p("AgqBTIAAijIAeAAIAEARQAGgKAHgFQAIgEAIAAQAGgBAGACQAFACAFADIAAAkQgGgEgGgCQgGgCgHAAQgKgBgIAHQgFAGgBAOIAABpg");
	this.shape_49.setTransform(111.7,-17.8);

	this.shape_50 = new cjs.Shape();
	this.shape_50.graphics.f("#987D22").s().p("AgYBQQgMgFgIgLQgIgLAAgRIAAgOQAAgNAEgIQAGgGAHgEQAIgDAKAAIAiAAIAAgTQAAgJgFgFQgFgFgHABQgGgBgFAFQgFAFAAAJIAAAFIgkAAIAAgHQAAgQAIgMQAIgLAMgGQAMgGAMAAQAOAAAMAGQALAGAIALQAIAMAAAQIAABCQAAASgHALQgIAMgMAFQgMAGgOAAQgMAAgMgFgAgQAgQAAAJAFAFQAFAEAGAAQAHAAAFgEQAFgFAAgJIAAgLIghAAg");
	this.shape_50.setTransform(99.7,-17.7);

	this.shape_51 = new cjs.Shape();
	this.shape_51.graphics.f("#987D22").s().p("AgRB/IAAj9IAjAAIAAD9g");
	this.shape_51.setTransform(89.8,-22.2);

	this.shape_52 = new cjs.Shape();
	this.shape_52.graphics.f("#987D22").s().p("AATBTIAAhuQAAgLgFgFQgGgFgHAAQgGAAgHAFQgGAGAAAMIAABsIgkAAIAAijIAeAAIAFAPQAGgJAJgFQAHgDAJAAQATAAAMAOQAMAOAAAYIAABxg");
	this.shape_52.setTransform(79.5,-17.8);

	this.shape_53 = new cjs.Shape();
	this.shape_53.graphics.f("#987D22").s().p("AggB3QgKgGgGgMQgGgLAAgQIAAhzIAkAAIAABwQAAAJAFAFQAGAFAHAAQAHAAAGgGQAGgGABgNIAAhqIAkAAIAACjIgeAAIgFgRQgFAKgJAFQgIAFgKAAQgKAAgLgGgAAMhSQgFgHgBgKQABgLAFgHQAHgHAKAAQAKAAAGAHQAFAHAAALQAAAKgFAHQgGAHgKABQgKgBgHgHgAgrhSQgHgHAAgKQAAgLAHgHQAFgHALAAQAJAAAGAHQAGAHAAALQAAAKgGAHQgGAHgJABQgLgBgFgHg");
	this.shape_53.setTransform(65.9,-21.7);

	this.shape_54 = new cjs.Shape();
	this.shape_54.graphics.f("#987D22").s().p("AgYB0QgMgGgHgLQgIgLAAgRIAAgIIAkAAIAAAEQAAAKAFAFQAFAFAGAAQAIAAAEgFQAGgFAAgKIAAglQgGAKgIAEQgGAFgKAAQgSgBgMgNQgMgOgBgXIAAhAQAAgRAGgMQAHgMAKgGQAJgHALAAQAJAAAHAEQAJAFAHAIIACgOIAgAAIAAC8QAAARgIALQgIALgNAFQgLAGgOAAQgNAAgMgFgAAAhVQgHAAgGAFQgFAGAAAKIAAA7QAAAJAFAFQAGAFAHAAQAIAAAFgFQAGgGAAgKIAAg3QAAgMgGgGQgGgFgGAAIgBAAg");
	this.shape_54.setTransform(52.2,-14.1);

	this.shape_55 = new cjs.Shape();
	this.shape_55.graphics.f("#987D22").s().p("AgqByQgMgNAAgZIAAhBQAAgPAHgMQAGgLAJgHQAKgGAMAAQAIAAAIADQAHAFAIAJIAAhoIAiAAIAAD8IgfAAIgEgQQgGAJgIAFQgIAFgIAAQgUAAgMgOgAAAgGQgIAAgFAFQgFAEAAALIAAA8QAAAKAGAFQAFAEAHABQAIgBAFgFQAGgGAAgLIAAg3QgBgMgFgFQgGgFgGAAIgBAAg");
	this.shape_55.setTransform(28.9,-22);

	this.shape_56 = new cjs.Shape();
	this.shape_56.graphics.f("#987D22").s().p("AgpBHQgNgOAAgYIAAg/QAAgRAGgMQAHgMAJgGQAKgHALAAQAJAAAHAEQAJAFAHAIIACgOIAgAAIAACjIgfAAIgDgRQgHAKgIAFQgIAFgJAAQgSgBgMgNgAAAgxQgIAAgFAFQgFAGAAAKIAAA6QAAAKAFAFQAGAFAHAAQAHAAAGgFQAGgGAAgLIAAg2QgBgMgFgGQgGgFgGAAIgBAAg");
	this.shape_56.setTransform(15.4,-17.7);

	this.shape_57 = new cjs.Shape();
	this.shape_57.graphics.f("#987D22").s().p("AgaAdQAKgJAEgKQADgJAAgNIAAgSIAjgEIAAAVQAAAQgDAMQgEALgIAKg");
	this.shape_57.setTransform(-4.8,-10);

	this.shape_58 = new cjs.Shape();
	this.shape_58.graphics.f("#987D22").s().p("AgRBSIAAijIAjAAIAACjg");
	this.shape_58.setTransform(-11.6,-17.7);

	this.shape_59 = new cjs.Shape();
	this.shape_59.graphics.f("#987D22").s().p("AgqByQgMgNAAgZIAAhBQAAgPAGgMQAGgLAKgHQAKgGALAAQAJAAAHADQAJAFAHAJIAAhoIAiAAIAAD8IgfAAIgEgQQgGAJgIAFQgIAFgJAAQgSAAgNgOgAAAgGQgIAAgFAFQgFAEAAALIAAA8QAAAKAFAFQAGAEAHABQAHgBAGgFQAGgGAAgLIAAg3QgBgMgFgFQgGgFgGAAIgBAAg");
	this.shape_59.setTransform(-21.8,-22);

	this.shape_60 = new cjs.Shape();
	this.shape_60.graphics.f("#987D22").s().p("AATBSIAAgiQABgQgGgHQgFgIgJAAQgIAAgFAIQgFAHAAAQIAAAiIgkAAIAAgjQAAgQAGgMQAHgMAOgHQgOgEgHgMQgGgLAAgTIAAgjIAkAAIAAAjQAAAPAFAHQAFAIAIAAQAJAAAFgIQAGgHgBgPIAAgjIAkAAIAAAjQAAATgGALQgGAMgPAEQAPAHAGAMQAGAMAAAQIAAAjg");
	this.shape_60.setTransform(-35,-17.7);

	this.shape_61 = new cjs.Shape();
	this.shape_61.graphics.f("#987D22").s().p("AgqBHQgMgOAAgYIAAg/QAAgRAGgMQAGgMAKgGQAKgHALAAQAJAAAHAEQAJAFAHAIIACgOIAgAAIAACjIgfAAIgEgRQgGAKgIAFQgIAFgJAAQgSgBgNgNgAAAgxQgIAAgFAFQgFAGAAAKIAAA6QAAAKAFAFQAGAFAHAAQAHAAAGgFQAGgGAAgLIAAg2QgBgMgFgGQgGgFgGAAIgBAAg");
	this.shape_61.setTransform(-48.4,-17.7);

	this.shape_62 = new cjs.Shape();
	this.shape_62.graphics.f("#987D22").s().p("AgYBzQgLgHgIgKQgIgMAAgQIAAgIIAkAAIAAAEQAAAKAFAFQAFAEAGAAQAIAAAFgEQAFgGABgJIAAgnQgIALgIAFQgHAEgJAAQgMAAgJgGQgKgHgGgKQgFgMAAgOIAAh0IAkAAIAABwQAAAJAFAFQAGAFAHAAQAHAAAGgGQAGgGABgMIAAhrIAkAAIAAC8QgBAQgIAMQgHALgNAFQgMAGgOgBQgNAAgMgEg");
	this.shape_62.setTransform(-61.9,-14);

	this.shape_63 = new cjs.Shape();
	this.shape_63.graphics.f("#987D22").s().p("AgqBHQgMgOAAgYIAAg/QAAgRAHgMQAGgMAJgGQAKgHAMAAQAIAAAIAEQAHAFAIAIIADgOIAfAAIAACjIgfAAIgEgRQgGAKgIAFQgIAFgIAAQgUgBgMgNgAAAgxQgIAAgFAFQgFAGAAAKIAAA6QAAAKAGAFQAFAFAHAAQAIAAAFgFQAGgGAAgLIAAg2QgBgMgFgGQgGgFgGAAIgBAAg");
	this.shape_63.setTransform(-75.5,-17.7);

	this.shape_64 = new cjs.Shape();
	this.shape_64.graphics.f("#987D22").s().p("AAUBTIAAhuQgBgLgFgFQgGgFgHAAQgGAAgHAFQgFAGgBAMIAABsIgkAAIAAijIAfAAIADAPQAIgJAIgFQAHgDAJAAQATAAAMAOQAMAOAAAYIAABxg");
	this.shape_64.setTransform(-89,-17.8);

	this.shape_65 = new cjs.Shape();
	this.shape_65.graphics.f("#987D22").s().p("AgRBSIAAijIAjAAIAACjg");
	this.shape_65.setTransform(-99.2,-17.7);

	this.shape_66 = new cjs.Shape();
	this.shape_66.graphics.f("#987D22").s().p("AATBSIAAgiQABgQgGgHQgFgIgJAAQgIAAgFAIQgFAHgBAQIAAAiIgkAAIAAgjQABgQAGgMQAHgMANgHQgNgEgHgMQgGgLgBgTIAAgjIAkAAIAAAjQABAPAFAHQAFAIAIAAQAJAAAFgIQAGgHgBgPIAAgjIAkAAIAAAjQABATgHALQgGAMgPAEQAPAHAGAMQAHAMgBAQIAAAjg");
	this.shape_66.setTransform(-109.2,-17.7);

	this.shape_67 = new cjs.Shape();
	this.shape_67.graphics.f("#987D22").s().p("AgZAdQAJgJAEgKQADgJABgNIAAgSIAjgEIAAAVQAAAQgFAMQgDALgIAKg");
	this.shape_67.setTransform(-129.3,-10);

	this.shape_68 = new cjs.Shape();
	this.shape_68.graphics.f("#987D22").s().p("AAUBTIAAhuQgBgLgFgFQgGgFgHAAQgGAAgHAFQgFAGgBAMIAABsIgkAAIAAijIAfAAIADAPQAIgJAIgFQAHgDAJAAQATAAAMAOQAMAOAAAYIAABxg");
	this.shape_68.setTransform(-139.4,-17.8);

	this.shape_69 = new cjs.Shape();
	this.shape_69.graphics.f("#987D22").s().p("AgqBHQgMgOAAgYIAAg/QAAgRAHgMQAGgMAJgGQAKgHAMAAQAIAAAIAEQAHAFAIAIIADgOIAfAAIAACjIgfAAIgEgRQgGAKgIAFQgIAFgIAAQgUgBgMgNgAAAgxQgIAAgFAFQgFAGAAAKIAAA6QAAAKAFAFQAGAFAHAAQAHAAAGgFQAGgGAAgLIAAg2QgBgMgFgGQgGgFgGAAIgBAAg");
	this.shape_69.setTransform(-153,-17.7);

	this.shape_70 = new cjs.Shape();
	this.shape_70.graphics.f("#987D22").s().p("AgUB4IgOgEIADgaIALADQAGACAHAAIAHgBQADgCAAgDQAAgDgEgDQgFgDgMAAIAAggIgOgCIgOgDIADgjIAPAEQAJACAJAAQAJAAAFgEQAGgEABgIQgBgGgGgFQgEgFgJgEIgQgLQgJgGgGgIQgFgIgBgNIAAgFQABgSAGgKQAIgKAKgEQAMgFAMAAQAKAAAKACQALACAIADIgEAgIgOgDIgQgBQgIgBgEAEQgFAEgBAIQABAGAFAFQAFAFAJAEIAQAKQAJAGAFAIQAGAIABAMIAAAGQgBAWgKALQgKALgOADIAAAMQALAFAFAHQAFAHAAAJQAAARgLAIQgMAJgQAAIgOgBg");
	this.shape_70.setTransform(-165.1,-14.1);

	this.shape_71 = new cjs.Shape();
	this.shape_71.graphics.f("#987D22").s().p("AgRB7IAAijIAjAAIAACjgAgPhQQgGgHAAgKQAAgMAGgGQAGgIAJAAQAKAAAGAIQAGAGAAAMQAAAKgGAHQgGAGgKABQgJgBgGgGg");
	this.shape_71.setTransform(-174.1,-21.8);

	this.shape_72 = new cjs.Shape();
	this.shape_72.graphics.f("#987D22").s().p("AATBTIAAhuQAAgLgFgFQgGgFgHAAQgHAAgFAFQgHAGAAAMIAABsIgkAAIAAijIAfAAIADAPQAIgJAIgFQAHgDAJAAQATAAAMAOQAMAOAAAYIAABxg");
	this.shape_72.setTransform(-184.2,-17.8);

	this.shape_73 = new cjs.Shape();
	this.shape_73.graphics.f("#987D22").s().p("AgaAdQAKgJAEgKQADgJAAgNIAAgSIAjgEIAAAVQAAAQgDAMQgEALgIAKg");
	this.shape_73.setTransform(-204.6,-10);

	this.shape_74 = new cjs.Shape();
	this.shape_74.graphics.f("#987D22").s().p("AgXBzQgMgHgIgKQgIgMAAgQIAAgIIAkAAIAAAEQAAAKAFAFQAFAEAGAAQAIAAAFgEQAGgGAAgJIAAgnQgIALgHAFQgIAEgJAAQgMAAgJgGQgKgHgFgKQgGgMgBgOIAAh0IAkAAIAABwQABAJAGAFQAFAFAHAAQAHAAAGgGQAGgGABgMIAAhrIAkAAIAAC8QgBAQgIAMQgHALgNAFQgMAGgOgBQgMAAgMgEg");
	this.shape_74.setTransform(-214.7,-14);

	this.shape_75 = new cjs.Shape();
	this.shape_75.graphics.f("#987D22").s().p("AgaBOQgMgGgJgNQgIgMAAgUIAAg0QAAgVAIgNQAJgMAMgHQANgGANAAQAOAAANAGQANAHAIAMQAHANABAVIAAAzQgBAVgHAMQgIANgNAGQgNAHgOAAQgNAAgNgHgAAOAtQAGgFAAgNIAAg1QAAgNgGgFQgHgGgHABQgHgBgGAGQgGAFAAANIAAA1QAAANAGAFQAGAGAHgBQAHABAHgGg");
	this.shape_75.setTransform(-228.3,-17.7);

	this.shape_76 = new cjs.Shape();
	this.shape_76.graphics.f("#987D22").s().p("AgQB2IAAjJIgtAAIAAgjIB7AAIAAAjIgsAAIAADJg");
	this.shape_76.setTransform(-239.4,-21.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_76},{t:this.shape_75},{t:this.shape_74},{t:this.shape_73},{t:this.shape_72},{t:this.shape_71},{t:this.shape_70},{t:this.shape_69},{t:this.shape_68},{t:this.shape_67},{t:this.shape_66},{t:this.shape_65},{t:this.shape_64},{t:this.shape_63},{t:this.shape_62},{t:this.shape_61},{t:this.shape_60},{t:this.shape_59},{t:this.shape_58},{t:this.shape_57},{t:this.shape_56},{t:this.shape_55},{t:this.shape_54},{t:this.shape_53},{t:this.shape_52},{t:this.shape_51},{t:this.shape_50},{t:this.shape_49},{t:this.shape_48},{t:this.shape_47},{t:this.shape_46},{t:this.shape_45},{t:this.shape_44},{t:this.shape_43},{t:this.shape_42},{t:this.shape_41},{t:this.shape_40},{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-254.3,-45.5,488.3,87);


(lib.Tween14 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("EgwbAH0IAAvnMBg3AAAIAAPng");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-310,-50,620,100);


(lib.Tween12 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.zal1();
	this.instance.parent = this;
	this.instance.setTransform(-310,-172);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-310,-172,620,344);


(lib.Tween10 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.masa1();
	this.instance.parent = this;
	this.instance.setTransform(-310,-188);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-310,-188,620,376);


(lib.Tween8 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.zal2();
	this.instance.parent = this;
	this.instance.setTransform(-480,-55,0.648,0.648);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-480,-55,622.4,241.8);


(lib.Tween6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.zal3();
	this.instance.parent = this;
	this.instance.setTransform(-310,-94.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-310,-94.5,620,189);


(lib.Tween4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.stol1();
	this.instance.parent = this;
	this.instance.setTransform(-310,-120.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-310,-120.5,620,241);


(lib.Tween2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.bina1();
	this.instance.parent = this;
	this.instance.setTransform(-310,-205.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-310,-205.5,620,411);


(lib.Symbol2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(210,145,14,0.439)").s().p("AgDAFIgBgCIgBgBQgBgCABgDQACgCADAAQADAAACACQACADgBACQgBADgDABIgCAAQAAAAAAAAQAAAAgBAAQAAAAgBgBQAAAAgBAAg");
	this.shape.setTransform(132.2,34.4,2.881,2.881,90);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("rgba(210,145,14,0.439)").s().p("AgBAGQgDgBgBgDIAAgCIAAAAQAAgDADgCQACgCADACQADACAAADQAAADgCACQgBAAAAABQgBAAAAAAQgBAAAAABQgBAAAAAAIgBgBg");
	this.shape_1.setTransform(237.3,-31.9,2.881,2.881,90);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("rgba(210,145,14,0.439)").s().p("AgEAFQgDgEADgCIAAgCQACgDADABQAEAAABAEQABACgCACQgBADgDAAIgBAAQAAAAAAAAQgBAAgBAAQAAAAgBAAQAAgBgBAAg");
	this.shape_2.setTransform(361.5,-36.8,2.881,2.881,90);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("rgba(210,145,14,0.439)").s().p("AgBAGQgDgBgBgDQgBgCACgDIACgBIABgBQACgBADABQACACAAADQAAADgCACIgEABIgBAAg");
	this.shape_3.setTransform(471.4,20.9,2.881,2.881,90);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("rgba(210,145,14,0.439)").s().p("AgCAFIAAAAIgCgCQgCgCABgCQABgEADAAQACgBADACQACACAAACQAAADgCACIgDABIgDgBg");
	this.shape_4.setTransform(145.7,20.9,2.881,2.881,90);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("rgba(210,145,14,0.439)").s().p("AgEAEIAAgCQgDgCADgEQACgCADABQADAAABADQACACgCADQgBADgDAAIgBAAQAAAAgBAAQgBAAAAAAQAAgBgBAAQAAgBgBAAg");
	this.shape_5.setTransform(255.7,-36.9,2.881,2.881,90);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("rgba(210,145,14,0.439)").s().p("AgCAGQgEgCABgEIAAgBIAEgEQACgBADACQACACAAACQAAAEgDABIgDABIgCAAg");
	this.shape_6.setTransform(379.9,-31.9,2.881,2.881,90);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("rgba(210,145,14,0.439)").s().p("AgEAEQgCgEACgCIAAAAIACgCQACgCACABQAEABAAADQABACgCADQgCACgDAAQgCAAgCgCg");
	this.shape_7.setTransform(484.9,34.4,2.881,2.881,90);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("rgba(210,145,14,0.439)").s().p("AgEANQgJgCAAgHQAAgHAHgBIACAAIAAgDQABgGAHAAQAHAAACAJIABAOIgBADIgDABIgOgBg");
	this.shape_8.setTransform(479,26.9,2.881,2.881,90);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("rgba(210,145,14,0.439)").s().p("AgNAKQgCgDABgDQABgEADAAIACAAIgCgCQgBgEABgCQABgEAEgBQAFgCAGAGIAIAKQAAABABAAQAAABAAAAQAAAAAAABQAAAAgBAAQAAABAAABQAAAAAAABQAAAAgBAAQAAAAgBABQgJAFgDABIgEABQgGAAgDgFg");
	this.shape_9.setTransform(371,-34.7,2.881,2.881,90);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("rgba(210,145,14,0.439)").s().p("AgFAOQgEgBgBgDQgBgEABgDIABgBIgBgBQgGgDADgHQAEgHAJADQADABAJAGQABAAAAAAQABAAAAABQAAAAAAAAQAAABAAAAQABABAAAAQAAABAAAAQAAAAAAABQgBAAAAAAIgIALQgEAFgDAAIgEgBg");
	this.shape_10.setTransform(246.1,-34.7,2.881,2.881,90);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("rgba(210,145,14,0.439)").s().p("AgEAHIAAgCIgDAAQgGgBAAgHQAAgHAJgCIAOgBIADABIABADIgBAOQgCAJgHAAQgHAAgBgHg");
	this.shape_11.setTransform(138.2,26.9,2.881,2.881,90);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("rgba(210,145,14,0.439)").s().p("AA8BZQgNgBgJgHQgQgMgKgYQgEAGgGAEQgVAOgZgQQgUgNgMgZQgJgQgCgPQgCgJABgKQAAgIAEgLQAHgQAPgKQAWgNAeAHQAZAFAWASQAYATAAAZQAAAUgQALQAaALAKAQQAHAJABANQACAPgIAHQgGAHgLAAIgGgBgAAQARQACAFAAAGIAAABQAGAZALALQAHAIALADQAMADAHgHQAHgHgDgMQgDgLgIgHQgLgLgZgGIgBAAQgGAAgHgDgAhBhBQgSASAFAbQAEAWASAUQAWAZAUgBQAOgBAEgMQAFgLgIgKIgFgDQgFAAgFABIgDACIACABQAFABACAFQABAEgDAFIgCADQgLAHgIgJQgJgJAFgLQAEgJALgCQAIgCALADQgDgLACgIQACgLAJgEQALgFAJAJQAJAIgHALIgCABQgFAEgGgCQgFgCgBgGIgBABQAAABAAAAQgBAAAAABQAAAAAAABQAAABAAAAQgCAGADAGIABABQAKAHAMgEQALgEABgOQABgUgZgWQgUgSgWgEIgLgBQgTAAgPAOg");
	this.shape_12.setTransform(471.1,34.8,2.881,2.881,90);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("rgba(210,145,14,0.439)").s().p("AhCA2QgNgJgMgNQgFgGgEgHQgFgJgBgLQgDgRAIgQQANgXAegJQAagIAcAFQAbAEANAUQAMATgJASQAdgDARAJQAKAEAHALQAJAMgEAKQgEALgNAFQgMAEgMgBQgTgCgUgQIgDALQgKAXgbACIgFAAQgXAAgZgSgAg/g2QgaAJgGAZQgHAXARAVQAOARAaAJQAeALAQgJQANgHgBgNQAAgOgNgFIgFABQgFABgDAFIgCADIABgBQAGgCAEAEQADADAAAGIgBADQgFAMgMgEQgMgDgBgMQgDgUAbgHQgIgGgDgJQgDgKAGgJQAIgJAKADQAMADgBAMIAAADQgCAFgHABQgFABgDgFIAAAEQAAAGAEAEQAAAAABABQAAAAABABQAAAAABAAQAAABABAAQAOABAHgLQAHgLgHgMQgKgQgdgGQgMgDgKAAQgPAAgMAFgAAcAEIgBAAQgHAEgGAAIACACQADADAEAGIABABQASASAQAEQAJACAKgCQALgDAEgIQADgJgJgKQgHgIgKgDQgGgBgIAAQgLAAgQAEg");
	this.shape_13.setTransform(369,-22.8,2.881,2.881,90);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("rgba(210,145,14,0.439)").s().p("AghBHQgPAAgRgHQgJgDgHgFQgIgEgGgJQgKgNgCgQQgBgaAVgYQATgUAbgLQAdgKAUANQARALACASQAVgQATgCQAMgBALAFQANAFAEAKQAEALgJAMQgHALgLAEQgSAIgbgDQADAHAAAEQADAagZAPQgQALgZAAIgMgBgAgtgxQgaAJgOARQgRAVAHAXQAGAZAaAJQAWAIAbgGQAcgGALgQQAHgMgHgLQgHgMgOACQgEADgCAEQgDAEABAGIACgCQADgEAFACQAEABACAFQAAAAAAABQAAAAABAAQAAABAAAAQAAAAAAABQABAMgMADQgKADgIgJQgGgJADgKQADgJAIgGQgbgHADgUQABgMAMgDQAMgEAFAMIABACQABAGgFAEQgFAEgFgCIABABIACADQAEAEAHABIABAAQANgFAAgNQABgOgNgHQgHgEgLAAQgMAAgQAGgAA4gpQgOAEgSASIgBABQgDAHgGADIACABQAGAAAFAEIABAAQAbAGAPgEQAJgCAIgJQAIgJgDgJQgEgIgMgDIgJgBQgGAAgFABg");
	this.shape_14.setTransform(248.3,-22.8,2.881,2.881,90);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("rgba(210,145,14,0.439)").s().p("AgjBZQgIAAgLgEQgQgHgKgPQgNgWAHgeQAFgZASgWQATgYAZAAQAUAAALAQQALgaAQgKQAJgHANgBQAPgCAHAIQAIAIgCAPQgBANgHAJQgMAQgYAKQAGAEAEAGQAOAVgQAZQgNAUgZAMQgQAJgPACQgHABgIAAIgEAAgAg4gVQgSAUgEAWQgFAbASASQASASAbgFQAWgEAUgSQAZgWgBgUQgBgOgMgEQgLgFgKAIIgDAFQAAAFABAFIACADIABgCQABgFAFgCQAEgBAFADIADACQAHALgJAIQgJAJgLgFQgSgIAIgaQgaAIgIgSQgFgLAJgJQAIgJALAHIABACQAEAFgCAGQgCAFgGABIABABQABAAAAAAQAAABABAAQAAAAABAAQABAAAAAAQAGACAGgDIABgBQAHgKgEgLQgEgMgOgBIgBAAQgUAAgVAYgAA1hLQgLADgHAIQgLALgGAZIAAABQAAAHgDAGIACgBQAFgCAGAAIABAAQAZgGALgLQAIgHADgLQADgMgHgHQgFgFgIAAIgGABg");
	this.shape_15.setTransform(146.1,34.8,2.881,2.881,90);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("rgba(210,145,14,0.439)").s().p("AAMAbIgNgOQgKgIgEgFQgEgGAAgHQAAgJAGgEQAGgDAHADQAFADAEAGQADAFACAMIAGAWQABADgDACIgDABQgBAAAAAAQAAAAgBAAQAAAAAAAAQgBgBAAAAg");
	this.shape_16.setTransform(538.9,63.8,2.881,2.881,90);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("rgba(210,145,14,0.439)").s().p("AAWAUIgWgGQgMgCgFgDQgGgEgDgFQgDgHADgGQAEgGAJAAQAHAAAGAEQAFAEAIAKIAOANQADACgDAEQAAAAAAABQgBAAAAAAQgBABAAAAQgBAAAAAAIgCAAg");
	this.shape_17.setTransform(442.1,-33.1,2.881,2.881,90);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("rgba(210,145,14,0.439)").s().p("AgLAPQgHAAgGgEQgGgEAAgHQAAgGAGgEQAGgEAHAAQAFAAALAEIAXAHQADAAAAADQAAABAAABQAAABgBAAQAAABAAAAQgBAAgBAAIgXAHQgKAEgFAAIgBAAg");
	this.shape_18.setTransform(308.6,-68.6,2.881,2.881,90);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("rgba(210,145,14,0.439)").s().p("AgaAOQgDgGADgHQADgFAGgEQAFgDAMgCIAWgGQADgBACADQADAEgDACIgOANQgIAKgFAEQgGAEgHAAQgJAAgEgGg");
	this.shape_19.setTransform(175,-33.1,2.881,2.881,90);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("rgba(210,145,14,0.439)").s().p("AgNAbQgGgEAAgIQAAgIAEgGQAEgFAKgIIANgOQACgDAEADQADACgBADIgGAWQgCAMgDAFQgEAGgFADQgDABgEAAQgDAAgDgBg");
	this.shape_20.setTransform(78.2,63.8,2.881,2.881,90);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("rgba(210,145,14,0.439)").s().p("AAiAnQgGgFgIgDQgYgEgMgDQgVgEgDgSQgCgMAIgLQAIgMANgEQALgEAJACQAKACAHAJQAJAMgHALIgCABQgDAGgHACQgHACgGgFQgDgCgBgFQgBgGAEgEQAFgFAJACIgBgBIgCgDQgDgEgGgCQgHgCgHADQgKAGgFAIQgDAJAIAIQAIAIALAEIAQAEIAKADQAKADAHAHQAEAEgEADIgEACQAAAAAAgBQgBAAAAAAQAAAAgBAAQAAgBAAAAg");
	this.shape_21.setTransform(89.2,88.4,2.881,2.881,90);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("rgba(210,145,14,0.439)").s().p("AgOAVQgFAAABgGQACgPAKgKQALgNAKAFQAFADgBAGQgCAFgFACIgNAGQgGAFgCAJQAAAAgBABQAAABAAAAQgBAAAAABQgBAAAAAAIgCAAg");
	this.shape_22.setTransform(116,60.4,2.881,2.881,90);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("rgba(210,145,14,0.439)").s().p("AgVAMQgFgDAEgEQALgMAMgEQARgFAGAJQACAFgCADQgDAEgFAAIgHAAIgHgBQgJAAgIAIIgEABIgCgBg");
	this.shape_23.setTransform(210.7,-17.9,2.881,2.881,90);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("rgba(210,145,14,0.439)").s().p("AAMALQgIgHgEgCQgJgCgJABQgEABgBgDQgCgFAFgBQARgGAMAEQARAEgBAKQAAAEgEACIgFABIgEgBg");
	this.shape_24.setTransform(331,-38.3,2.881,2.881,90);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("rgba(210,145,14,0.439)").s().p("AAKATQgFgCgCgFIgGgNQgFgGgJgCQgEgBABgEQAAgFAGABQAPACAKAKQANALgFAKQgCAEgEAAIgDAAg");
	this.shape_25.setTransform(445.5,4.6,2.881,2.881,90);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("rgba(210,145,14,0.439)").s().p("AAAAYQgEgDAAgFQACgKgBgEQAAgJgIgIQgCgDACgDQADgFAEAEQAMALAEAMQAFARgJAGIgFABIgDgBg");
	this.shape_26.setTransform(523.7,99.4,2.881,2.881,90);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("rgba(210,145,14,0.439)").s().p("AgMAWQgCgDADgDQAIgIAAgJIgBgHIgBgHQABgFAEgDQADgCAEACQAKAGgGARQgEAMgMALQAAAAgBABQAAAAAAAAQgBAAAAABQgBAAAAAAQgBAAAAAAQAAgBgBAAQAAAAgBgBQAAAAgBgBg");
	this.shape_27.setTransform(93.4,99.4,2.881,2.881,90);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("rgba(210,145,14,0.439)").s().p("AgUAOQgBgDAEgBQAJgDAFgFIAGgNQACgGAFgBQAGgCADAGQAFAJgNALQgKAKgPADIgCAAQgEAAAAgFg");
	this.shape_28.setTransform(171.7,4.6,2.881,2.881,90);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("rgba(210,145,14,0.439)").s().p("AgVAIIgDgCQAAgBAAAAQAAAAAAgBQAAAAAAgBQAAAAAAgBQABgDAFABQAKACAIgEQAHgGAEgCQAFgCAEACQAEACABAEQAAAJgQAFQgFACgFAAQgJAAgLgEg");
	this.shape_29.setTransform(286.2,-38.3,2.881,2.881,90);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("rgba(210,145,14,0.439)").s().p("AAAALQgLgEgLgMQgEgEAFgDQADgCADADQAIAIAJAAQAFAAAJgCQAFABADAEQACADgCAEQgEAGgIAAQgFAAgHgCg");
	this.shape_30.setTransform(406.4,-17.9,2.881,2.881,90);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("rgba(210,145,14,0.439)").s().p("AgGALQgKgKgCgPQgBgGAFAAQAEgBABAEQACAJAGAFIANAGQAFACACAFQABAGgFADIgGACQgHAAgIgKg");
	this.shape_31.setTransform(501.2,60.4,2.881,2.881,90);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("rgba(210,145,14,0.439)").s().p("AgGAGQgGgGAGgGQAFgGAHAFIABABIAAABQAGAGgGAGQgDADgDAAQgDAAgEgEg");
	this.shape_32.setTransform(109,21.8,2.881,2.881,90);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("rgba(210,145,14,0.439)").s().p("AgIACQgDgIAJgDQAHgBAEAHIAAACIAAABQABADgCADQgDADgEABIgBAAQgGAAgCgIg");
	this.shape_33.setTransform(223.5,-54.5,2.881,2.881,90);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("rgba(210,145,14,0.439)").s().p("AgCAJQgIgCACgIQADgJAHABQAIACgBAIIAAABIAAACQgDAGgFAAIgDgBg");
	this.shape_34.setTransform(360.7,-63.4,2.881,2.881,90);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("rgba(210,145,14,0.439)").s().p("AgGAHQgGgHAHgGQAGgGAGAGQAGAFgFAHIgBABIgBAAQgEADgCAAQgDAAgDgDg");
	this.shape_35.setTransform(484.1,-2.4,2.881,2.881,90);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("rgba(210,145,14,0.439)").s().p("AgCABQgBgFAGgEIAAARQgEgDgBgFg");
	this.shape_36.setTransform(560.3,110.1,2.881,2.881,90);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("rgba(210,145,14,0.439)").s().p("AgFAHQgHgHAGgGQAGgGAGAGIABAAIABABQAFAGgGAHQgDACgDAAQgDAAgDgDg");
	this.shape_37.setTransform(133.1,-2.4,2.881,2.881,90);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("rgba(210,145,14,0.439)").s().p("AgIACQgCgIAIgDQAHgBAEAHIAAACIAAABQABADgCADQgDADgEABIgBAAQgGAAgCgIg");
	this.shape_38.setTransform(256.4,-63.4,2.881,2.881,90);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("rgba(210,145,14,0.439)").s().p("AgCAJQgIgCACgIQABgEADgCQADgCADAAQAEABACADQACADgBADIAAABIAAACQgDAGgFAAIgDgBg");
	this.shape_39.setTransform(393.7,-54.5,2.881,2.881,90);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f("rgba(210,145,14,0.439)").s().p("AgHAHQgFgHAGgFQAHgHAGAGQAGAGgGAGIAAABIgBABQgDACgDAAQgDAAgEgDg");
	this.shape_40.setTransform(508.2,21.8,2.881,2.881,90);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f("rgba(210,145,14,0.439)").s().p("AgGAHQgGgHAGgGQAGgGAGAGIABAAIABABQAFAGgGAHQgDACgDAAQgDAAgEgDg");
	this.shape_41.setTransform(111.8,14,2.881,2.881,90);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f("rgba(210,145,14,0.439)").s().p("AgIACQgDgIAJgDQAHgBAEAHIAAACIAAABQABADgCADQgCADgFABIgBAAQgGAAgCgIg");
	this.shape_42.setTransform(229.8,-59.9,2.881,2.881,90);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f("rgba(210,145,14,0.439)").s().p("AgCAKQgIgDACgIQADgJAHABQAIADgBAHIAAACIAAABQgDAGgFAAIgDAAg");
	this.shape_43.setTransform(368.9,-64.8,2.881,2.881,90);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f("rgba(210,145,14,0.439)").s().p("AgHAHQgFgGAGgHQAHgGAGAGQAGAGgGAGIAAABIgBABQgEACgCAAQgDAAgEgDg");
	this.shape_44.setTransform(491.9,0.5,2.881,2.881,90);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f("rgba(210,145,14,0.439)").s().p("AgFAHQgHgHAGgGQAGgGAGAGIABAAIAAABQAFAGgFAHQgDACgDAAQgDAAgDgDg");
	this.shape_45.setTransform(125.3,0.4,2.881,2.881,90);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f("rgba(210,145,14,0.439)").s().p("AgIACQgCgIAIgDQAHgCAEAIIAAABIAAACQABAIgJACIgBAAQgFAAgDgIg");
	this.shape_46.setTransform(248.3,-64.8,2.881,2.881,90);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f("rgba(210,145,14,0.439)").s().p("AgCAJQgIgCACgIQACgJAIABQAEABACADQACADgBADIAAABIAAACQgDAGgFAAIgDgBg");
	this.shape_47.setTransform(387.4,-59.8,2.881,2.881,90);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f("rgba(210,145,14,0.439)").s().p("AgHAHQgFgHAGgFQAHgHAGAGQAGAGgGAGIAAABIgBAAQgEADgCAAQgDAAgEgDg");
	this.shape_48.setTransform(505.4,14,2.881,2.881,90);

	this.shape_49 = new cjs.Shape();
	this.shape_49.graphics.f("rgba(210,145,14,0.439)").s().p("AgFAGQgIgGAHgGQAFgFAHAEIACACQAFAHgGAFQgDADgDAAQgDAAgDgEg");
	this.shape_49.setTransform(147.6,55.7,2.881,2.881,90);

	this.shape_50 = new cjs.Shape();
	this.shape_50.graphics.f("rgba(210,145,14,0.439)").s().p("AgIACQgBgDABgDQACgEAEgBQAIgBACAHIABABIAAACQABAIgJACIgBAAQgGAAgCgIg");
	this.shape_50.setTransform(240,-5.7,2.881,2.881,90);

	this.shape_51 = new cjs.Shape();
	this.shape_51.graphics.f("rgba(210,145,14,0.439)").s().p("AgCAKQgJgDADgIQADgJAHABQAJADgBAIIgBACQgDAGgGAAIgCAAg");
	this.shape_51.setTransform(350.7,-12.9,2.881,2.881,90);

	this.shape_52 = new cjs.Shape();
	this.shape_52.graphics.f("rgba(210,145,14,0.439)").s().p("AgGAHQgHgHAIgFQAFgIAHAHQAGAFgFAHIgCACQgDACgDAAQgDAAgDgDg");
	this.shape_52.setTransform(450.2,36.3,2.881,2.881,90);

	this.shape_53 = new cjs.Shape();
	this.shape_53.graphics.f("rgba(210,145,14,0.439)").s().p("AgFAGQgDgCgBgEQAAgDADgDQAFgGAHAFIACACQAFAHgGAFQgEADgDAAQgCAAgDgEg");
	this.shape_53.setTransform(167,36.4,2.881,2.881,90);

	this.shape_54 = new cjs.Shape();
	this.shape_54.graphics.f("rgba(210,145,14,0.439)").s().p("AgIACQgBgDABgDQACgEAEgBQADgBADACQADACABADIABACQABAJgJACIAAAAQgGAAgDgIg");
	this.shape_54.setTransform(266.4,-12.8,2.881,2.881,90);

	this.shape_55 = new cjs.Shape();
	this.shape_55.graphics.f("rgba(210,145,14,0.439)").s().p("AgCAKQgIgDACgIQACgJAIABQAIACgBAIIAAACIgBABQgBADgDACIgEABIgCAAg");
	this.shape_55.setTransform(377.1,-5.7,2.881,2.881,90);

	this.shape_56 = new cjs.Shape();
	this.shape_56.graphics.f("rgba(210,145,14,0.439)").s().p("AgGAHQgGgHAHgFQACgDADgBQAEAAADADQAGAFgFAHIgCACQgEACgCAAQgDAAgDgDg");
	this.shape_56.setTransform(469.4,55.7,2.881,2.881,90);

	this.shape_57 = new cjs.Shape();
	this.shape_57.graphics.f("rgba(210,145,14,0.439)").s().p("AAAAKQgEAAgDgDQgFgGAGgGQAGgHAHAGQAGAGgGAGIAAABIgBABQgDACgDAAIAAAAg");
	this.shape_57.setTransform(499.9,6.1,2.881,2.881,90);

	this.shape_58 = new cjs.Shape();
	this.shape_58.graphics.f("rgba(210,145,14,0.439)").s().p("AgCAKQgEgBgBgEQgCgDABgDQABgEADgCQADgCADAAQAIACgBAIIAAACIAAABQgDAGgGAAIgCAAg");
	this.shape_58.setTransform(378.5,-63.9,2.881,2.881,90);

	this.shape_59 = new cjs.Shape();
	this.shape_59.graphics.f("rgba(210,145,14,0.439)").s().p("AgIACQgDgIAJgDQAHgCAEAIIAAABIAAACQABAIgJACIgBAAQgGAAgCgIg");
	this.shape_59.setTransform(238.6,-63.9,2.881,2.881,90);

	this.shape_60 = new cjs.Shape();
	this.shape_60.graphics.f("rgba(210,145,14,0.439)").s().p("AgGAHQgDgDAAgEQAAgDADgDQAGgGAGAGIABAAIAAABQAGAHgHAGQgCACgDAAQgDAAgEgDg");
	this.shape_60.setTransform(117.4,6.1,2.881,2.881,90);

	this.shape_61 = new cjs.Shape();
	this.shape_61.graphics.f("rgba(210,145,14,0.439)").s().p("AgBAJQgEgCgCgDQgCgEABgCQABgEAEgBQADgCADAAQADABACAEQACADgBADQgBAEgDACIgEACIgCgBg");
	this.shape_61.setTransform(526,71.7,2.881,2.881,90);

	this.shape_62 = new cjs.Shape();
	this.shape_62.graphics.f("rgba(210,145,14,0.439)").s().p("AgEAIQgEgCgBgDQAAgDACgDQABgEAEgBQACgBAEACQADACACAEQABACgCAEQgCADgEABIgCAAIgEgBg");
	this.shape_62.setTransform(434.2,-20.2,2.881,2.881,90);

	this.shape_63 = new cjs.Shape();
	this.shape_63.graphics.f("rgba(210,145,14,0.439)").s().p("AAAAJQgEAAgCgCQgDgDABgEQgBgDADgDQADgCADAAQAEgBACADQADADAAADQAAAEgDADQgCACgEAAIAAAAg");
	this.shape_63.setTransform(308.6,-53.8,2.881,2.881,90);

	this.shape_64 = new cjs.Shape();
	this.shape_64.graphics.f("rgba(210,145,14,0.439)").s().p("AgCAJQgEgBgBgEQgCgDAAgDQABgDAEgCQAHgFAFAIQACAEgBADQgCADgDACIgEACIgCgBg");
	this.shape_64.setTransform(183.1,-20.2,2.881,2.881,90);

	this.shape_65 = new cjs.Shape();
	this.shape_65.graphics.f("rgba(210,145,14,0.439)").s().p("AgDAIQgEgCgBgDQgBgDACgDQACgEAEgBQACgCAEACQADACABAEQABADgCAEQgCADgDABIgDABQgBAAgCgCg");
	this.shape_65.setTransform(91.1,71.7,2.881,2.881,90);

	this.shape_66 = new cjs.Shape();
	this.shape_66.graphics.f("rgba(210,145,14,0.439)").s().p("AgJACQgBgJAJgBQACgBAEACQADACABAEQACAGgIAEIgBAAIgBAAIgBAAQgHAAgCgHg");
	this.shape_66.setTransform(88.1,61.5,2.881,2.881,90);

	this.shape_67 = new cjs.Shape();
	this.shape_67.graphics.f("rgba(210,145,14,0.439)").s().p("AAAAJIgBAAIgBgBQgEAAgBgEQgCgDABgDQABgEADgBQAEgCACABQAEABACADQADAEgBACQgBADgDADIgFABIgBAAg");
	this.shape_67.setTransform(185.5,-30.6,2.881,2.881,90);

	this.shape_68 = new cjs.Shape();
	this.shape_68.graphics.f("rgba(210,145,14,0.439)").s().p("AgFAHIgBAAIAAgBQgDgDABgDQAAgEADgCQAFgFAHAFQACADAAADQABAEgDACQgCADgEAAIgBAAQgCAAgDgCg");
	this.shape_68.setTransform(316,-61.6,2.881,2.881,90);

	this.shape_69 = new cjs.Shape();
	this.shape_69.graphics.f("rgba(210,145,14,0.439)").s().p("AgIADIAAgBIAAgBQgBgIAIgCQAJgBABAJQABACgCAEQgCADgEABIgCABQgFAAgDgHg");
	this.shape_69.setTransform(444.3,-23.2,2.881,2.881,90);

	this.shape_70 = new cjs.Shape();
	this.shape_70.graphics.f("rgba(210,145,14,0.439)").s().p("AgBAKQgEgBgCgDQgCgDABgDIAAgBIAAgBQABgEAEgBQADgCACABQAEABACADQACAEgBACQgBAEgDACQgDACgCAAIgBAAg");
	this.shape_70.setTransform(536.4,74.2,2.881,2.881,90);

	this.shape_71 = new cjs.Shape();
	this.shape_71.graphics.f("rgba(210,145,14,0.439)").s().p("AgDAIQgEgBgBgEIAAgBIAAgBQgBgCACgEQACgDAEgBQAIgBACAJQABACgCAEQgCADgEABIgCABIgDgCg");
	this.shape_71.setTransform(80.8,74.2,2.881,2.881,90);

	this.shape_72 = new cjs.Shape();
	this.shape_72.graphics.f("rgba(210,145,14,0.439)").s().p("AgBAKQgEgBgCgDQgCgDABgDIAAgBIAAgBQAEgIAHACQADABACADQACAEgBACQAAAEgEACQgCACgDAAIgBAAg");
	this.shape_72.setTransform(172.8,-23.2,2.881,2.881,90);

	this.shape_73 = new cjs.Shape();
	this.shape_73.graphics.f("rgba(210,145,14,0.439)").s().p("AgFAHQgHgGAGgGIAAgBIABgBQADgCADAAQAEAAADAEQAFAFgGAGQgEAEgDAAQgCAAgDgDg");
	this.shape_73.setTransform(301.1,-61.6,2.881,2.881,90);

	this.shape_74 = new cjs.Shape();
	this.shape_74.graphics.f("rgba(210,145,14,0.439)").s().p("AgEAIQgDgCgBgEQgCgHAIgDIABAAIABAAQACgBAEACQADACABAEQABAIgJACIgCAAIgEgBg");
	this.shape_74.setTransform(431.6,-30.6,2.881,2.881,90);

	this.shape_75 = new cjs.Shape();
	this.shape_75.graphics.f("rgba(210,145,14,0.439)").s().p("AgBAJQgEAAgCgEQgCgDAAgDQACgIAIABIABAAIABAAQAIAEgCAHQgBADgDACIgFABIgBAAg");
	this.shape_75.setTransform(529.1,61.5,2.881,2.881,90);

	this.shape_76 = new cjs.Shape();
	this.shape_76.graphics.f("rgba(210,145,14,0.439)").s().p("AgGAYQgNgSgIgHQgIgGgIgDQgFgBACgEQABgFAEACQALACAKAJIAFAFIAMAMQAHAIALACQAMADAGgHQAGgIgBgKIAAgBQgCgLgIgFQgGgDgEAAIgBAAQAGAHgCAIQgBAFgFACQgFABgFgBQgGgDgCgHQgBgHADgGIABgDQAFgLAPACQALABAHAJQAHAIABALQADAOgHANQgIAOgNACIgEAAQgNAAgKgNg");
	this.shape_76.setTransform(104.3,59.8,2.881,2.881,90);

	this.shape_77 = new cjs.Shape();
	this.shape_77.graphics.f("rgba(210,145,14,0.439)").s().p("AgNAmQgPgFgIgPQgJgPAIgMQAIgMARgDIAcgEQAMgEAGgGQACgDAEADQAEADgEAEQgHAHgKADIgLAEIgPADQgMAEgHAIQgIAIADAJQAFAJAJAEQAIAEAIgDQAHgDACgDIACgDQgKADgEgHQgEgEABgFQABgFADgCQAGgFAHACQAHACADAGIACACQAHALgJALQgHAJgKACIgGABQgGAAgIgDg");
	this.shape_77.setTransform(527.9,88.4,2.881,2.881,90);

	this.shape_78 = new cjs.Shape();
	this.shape_78.graphics.f("rgba(210,145,14,0.439)").s().p("AgmAoQgDgEADgCQAFgGADgIIAHgkQAEgVASgDQAMgCAMAIQALAIAEANQAEALgCAJQgCAKgJAHQgMAJgLgHIgBgCQgGgDgCgHQgCgHAFgGQACgDAFgBQAGgBAEAEQAFAFgCAJIABgBIADgCQAEgDACgGQACgHgDgHQgGgKgIgFQgJgDgIAIQgIAIgEALIgEAQIgDAKQgDAKgHAHIgDACQgBAAAAAAQgBgBAAAAQgBAAAAAAQAAgBgBAAg");
	this.shape_78.setTransform(417.4,-22.1,2.881,2.881,90);

	this.shape_79 = new cjs.Shape();
	this.shape_79.graphics.f("rgba(210,145,14,0.439)").s().p("AAAAbIgBgCQgDgGABgHQACgHAGgDQAFgCAFACQAFACABAFQACAIgGAGIACAAQAFAAAFgDQAIgGABgKQABgKgGgJQgGgHgMADQgLADgIAHIgLANIgFAEQgKAKgLACQgEABgBgEQgCgFAFgBQAIgCAIgHIALgNQAHgKAEgEQAGgHAJgCQAKgDAJAEQAKAGAFAMQAFAMgDALQgBALgHAIQgIAJgLABIgCAAQgNAAgEgKg");
	this.shape_79.setTransform(512.9,59.8,2.881,2.881,90);

	this.shape_80 = new cjs.Shape();
	this.shape_80.graphics.f("rgba(210,145,14,0.439)").s().p("AgJAmQgMgIgDgRQgBgSgDgKQgEgMgGgGQgDgCADgEQADgEAEAEQAHAHADAKIAEALIADAPQAEAMAIAHQAIAIAJgDQAJgFAEgJQAEgIgDgIQgDgHgDgCIgDgCQADAKgHAEQgEAEgFgBQgFgBgCgDQgFgGACgHQACgHAGgDIACgCQALgHALAJQAJAHACALQACAIgEALQgFAPgPAIQgIAFgHAAQgGAAgGgEg");
	this.shape_80.setTransform(199.7,-22.1,2.881,2.881,90);

	this.shape_81 = new cjs.Shape();
	this.shape_81.graphics.f("rgba(210,145,14,0.439)").s().p("AgdAmQgKgNAHgTQAKgUACgKQABgKgBgHQgCgEAFgBQAEgCABAFQAEAKgEANIgBAHIgGAPQgCAMADALQAEAMAKABQAJAAAIgFIABgBQAJgGAAgLQAAgEgCgEIgBgCQgDAJgHACQgGABgEgDQgDgDgBgGQgBgHAFgFQAFgFAHABIACAAQANgBAGANQAIAUgTAQQgLAKgPABIgBAAQgPAAgJgKg");
	this.shape_81.setTransform(325.8,-46,2.881,2.881,90);

	this.shape_82 = new cjs.Shape();
	this.shape_82.graphics.f("rgba(210,145,14,0.439)").s().p("AgUAqQgOgIgCgNQgCgQAPgMQASgNAHgHQAGgIADgIQABgFAEACQAFABgCAEQgCALgJAKIgFAFIgMAMQgIAHgCALQgDAMAHAGQAIAGAKgBIABAAQALgCAFgIQADgGAAgEIAAgBQgHAGgIgCQgFgBgCgFQgBgFABgFQADgGAHgCQAHgBAGADIADABQALAFgCAPQgBALgJAHQgIAHgLABIgHABQgKAAgKgFg");
	this.shape_82.setTransform(446.1,-7.1,2.881,2.881,90);

	this.shape_83 = new cjs.Shape();
	this.shape_83.graphics.f("rgba(210,145,14,0.439)").s().p("AALAsQgCgIgHgIIgNgLIgOgLQgQgQAIgSQAGgKAMgFQAMgFALADQALABAIAHQAJAIABALQABAPgLAEIgCABQgGADgHgBQgHgCgDgGQgCgFACgFQACgFAFgBQAIgCAGAGIAAgCQAAgFgDgFQgGgIgKgBQgKAAgJAFQgHAGADAMQADALAHAIIANALIAEAFQAKAKACALQABAEgEABIgDAAQAAAAgBAAQAAAAgBgBQAAAAAAgBQgBAAAAgBg");
	this.shape_83.setTransform(171.1,-7.1,2.881,2.881,90);

	this.shape_84 = new cjs.Shape();
	this.shape_84.graphics.f("rgba(210,145,14,0.439)").s().p("AgRAvQgFgBACgEIAAgMIgNgmQgGgYAQgKQAKgGANABQANACAJAJQAJAHADAJQADALgEAJQgHAOgMgCIgCAAQgHABgFgFQgFgFABgHQABgGADgDQAFgDAFABQAHADADAIIACgDIABgJQgBgJgIgGIgBgBQgIgFgJAAQgKABgEAMQgDALACAMIAGAPIABAHQAEANgEAKQAAABAAABQAAABgBAAQAAAAgBABQAAAAgBAAIgCgBg");
	this.shape_84.setTransform(291.3,-45.9,2.881,2.881,90);

	this.shape_85 = new cjs.Shape();
	this.shape_85.graphics.f("rgba(210,145,14,0.439)").s().p("AgKAbQgDgFABgGQAHgMABgGQADgJgCgKIgBgDIAAgBQgBgEAEgBQAEgBABAEIABADQAHARABAPQABAWgOACIgBAAQgGAAgDgFg");
	this.shape_85.setTransform(92.3,52.6,2.881,2.881,90);

	this.shape_86 = new cjs.Shape();
	this.shape_86.graphics.f("rgba(210,145,14,0.439)").s().p("AgOAaQgGgEABgGQAAgGAGgFIAOgKQAGgGADgJIAAgBIABgCIAAgBQABgEAFABQAEACgBAEIgBADQgCAUgJAOQgEAHgFADQgDACgDAAQgEAAgDgCg");
	this.shape_86.setTransform(193.6,-36.9,2.881,2.881,90);

	this.shape_87 = new cjs.Shape();
	this.shape_87.graphics.f("rgba(210,145,14,0.439)").s().p("AgZAOQgFgGADgFQADgFAGgCIAKgBIAJgBQAJgCAIgHIADgCIAAgBQADgDADAEQADADgDADIgCACQgLAOgNAJQgIAGgHAAQgHAAgEgGg");
	this.shape_87.setTransform(325.3,-62.6,2.881,2.881,90);

	this.shape_88 = new cjs.Shape();
	this.shape_88.graphics.f("rgba(210,145,14,0.439)").s().p("AgfAAQAAgHAFgDQAFgDAGABIAJAEIAJAEQAJADAKgCIADgBIABAAQAEgBABAEQABAEgEABIgDABQgRAHgPABIgDAAQgTAAgCgNg");
	this.shape_88.setTransform(453.2,-19,2.881,2.881,90);

	this.shape_89 = new cjs.Shape();
	this.shape_89.graphics.f("rgba(210,145,14,0.439)").s().p("AAWAUIgDgBQgUgCgOgJQgHgEgDgFQgEgHAEgGQAEgGAGABQAGAAAFAGIAKAOQAGAGAJADIABAAIACABIABAAQAEABgBAFQgBADgDAAIgCAAg");
	this.shape_89.setTransform(542.7,82.3,2.881,2.881,90);

	this.shape_90 = new cjs.Shape();
	this.shape_90.graphics.f("rgba(210,145,14,0.439)").s().p("AgZAPQgEgGAEgHQADgFAHgEQAOgJAUgCIADgBQAEgBACAEQABAFgEABIgBAAIgCABIgBAAQgJADgGAGIgKAOQgFAGgGAAIgCAAQgFAAgDgFg");
	this.shape_90.setTransform(74.5,82.3,2.881,2.881,90);

	this.shape_91 = new cjs.Shape();
	this.shape_91.graphics.f("rgba(210,145,14,0.439)").s().p("AgaALQgFgEAAgGQACgOAWABQAPABARAHIADABQAEABgBAEQgBAEgEgBIgBAAIgDgBQgKgCgJADIgJAEIgJAEIgDAAQgEAAgEgCg");
	this.shape_91.setTransform(164,-19,2.881,2.881,90);

	this.shape_92 = new cjs.Shape();
	this.shape_92.graphics.f("rgba(210,145,14,0.439)").s().p("AAVATIAAgBIgDgCQgHgHgLgDQgNAAgGgCQgGgCgCgEQgCgGAEgFQAJgMARAMQANAJALAOIACACQADADgDAEIgDABIgDgBg");
	this.shape_92.setTransform(291.8,-62.6,2.881,2.881,90);

	this.shape_93 = new cjs.Shape();
	this.shape_93.graphics.f("rgba(210,145,14,0.439)").s().p("AALAZIAAgBIgBgCIAAgBQgDgJgGgGQgBgDgFgDIgIgEQgGgFAAgGQgBgGAGgEQAGgEAHAEQAFADAEAHQAJAOACAUIABADQABAEgEACIgCAAQgDAAgBgDg");
	this.shape_93.setTransform(423.5,-36.9,2.881,2.881,90);

	this.shape_94 = new cjs.Shape();
	this.shape_94.graphics.f("rgba(210,145,14,0.439)").s().p("AgBAfQgEgBABgEIAAgBIABgDQACgKgDgJIgEgJIgEgJQgBgGADgFQAEgFAGAAQAOACgBAWQgBAPgHARIgBADQgBABAAABQAAAAgBABQAAAAgBAAQAAABgBAAIgBgBg");
	this.shape_94.setTransform(524.8,52.7,2.881,2.881,90);

	this.shape_95 = new cjs.Shape();
	this.shape_95.graphics.f("rgba(210,145,14,0.439)").s().p("AgVAeQgCgKAJgGIAOgGQAGgEADgFQAFgLgHgLQgDgEgEgCQgFgEAFgEQACgEADADQAGAFACADQANANgFAVQgCAOgHAKQgIANgLAAQgLgBgDgKg");
	this.shape_95.setTransform(96.8,43,2.881,2.881,90);

	this.shape_96 = new cjs.Shape();
	this.shape_96.graphics.f("rgba(210,145,14,0.439)").s().p("AgVAhQgKgFABgKQABgLALgCIAOABQAHABAFgCQAOgFAAgQQgBgFgDgEQAAgBAAgBQAAAAAAgBQAAAAAAgBQAAAAAAgBQABAAAAgBQAAAAABAAQAAgBAAAAQABAAAAAAQAFgDABAFQADAFAAAGQAEARgOAQQgIAKgKAGQgIAEgHAAQgEAAgEgBg");
	this.shape_96.setTransform(202.4,-43.2,2.881,2.881,90);

	this.shape_97 = new cjs.Shape();
	this.shape_97.graphics.f("rgba(210,145,14,0.439)").s().p("AgjANQgGgJAGgHQAGgJAKAEQAEABAJAHQAGAEAGAAQAOAAAHgLQACgGgBgEQAAgGAHACIADABQAAAAAAAAQABABAAAAQAAABAAAAQAAABAAAAIgDAMQgGAQgVAHQgMAEgKAAQgPAAgHgJg");
	this.shape_97.setTransform(335.6,-63.9,2.881,2.881,90);

	this.shape_98 = new cjs.Shape();
	this.shape_98.graphics.f("rgba(210,145,14,0.439)").s().p("AgDAVQgOgCgKgHQgNgIAAgLQABgLAKgDQAKgCAGAJQADAFADAJQAEAGAFADQALAFALgHQAEgDACgEQAEgFAEAFQAEACgDADQgFAGgDACQgKAJgOAAIgKgBg");
	this.shape_98.setTransform(462.8,-14.5,2.881,2.881,90);

	this.shape_99 = new cjs.Shape();
	this.shape_99.graphics.f("rgba(210,145,14,0.439)").s().p("AgNAUQgKgIgGgKQgHgNAEgKQAFgKAKABQALABACALIgBAOQgBAHACAFQAFAOAQAAQAFgBAEgDQABAAABAAQAAAAABAAQAAAAABAAQAAAAABAAQAAABABAAQAAAAAAABQABAAAAAAQAAABAAAAQADAFgFABQgFADgGAAIgHABQgNAAgNgLg");
	this.shape_99.setTransform(549,91.1,2.881,2.881,90);

	this.shape_100 = new cjs.Shape();
	this.shape_100.graphics.f("rgba(210,145,14,0.439)").s().p("AggAWQgEgKAHgMQAGgKAKgJQAQgOARAEQAGAAAFADQAFABgDAFQAAAAAAABQAAAAgBAAQAAABAAAAQgBAAAAABQgBAAAAAAQgBAAAAAAQgBAAAAAAQgBAAgBAAQgEgDgFgBQgRAAgFAQQgBAEABAHQABAKAAADQgCALgLABIgBAAQgJAAgFgJg");
	this.shape_100.setTransform(68.2,91.1,2.881,2.881,90);

	this.shape_101 = new cjs.Shape();
	this.shape_101.graphics.f("rgba(210,145,14,0.439)").s().p("AgeAWQgJgDgBgLQAAgLAOgJQALgGAOgDQATgDANAMQAFADADAFQADAEgEABQAAAAgBABQAAAAAAAAQgBABAAAAQgBAAgBAAQAAAAgBAAQAAAAgBgBQAAAAgBAAQAAgBgBAAQgCgEgEgDQgLgHgMAGQgFADgEAHIgGANQgEAHgHAAIgFgBg");
	this.shape_101.setTransform(154.3,-14.5,2.881,2.881,90);

	this.shape_102 = new cjs.Shape();
	this.shape_102.graphics.f("rgba(210,145,14,0.439)").s().p("AAcASQABgEgCgGQgHgMgPABQgFAAgIAFQgIAGgEABQgKAEgFgJQgGgHAGgJQAHgJAPAAQAMgBAMAFQATAHAGARIADALQAAAAAAABQAAAAAAABQAAAAgBAAQAAABAAAAIgDABIgDAAQgEAAAAgEg");
	this.shape_102.setTransform(281.5,-63.9,2.881,2.881,90);

	this.shape_103 = new cjs.Shape();
	this.shape_103.graphics.f("rgba(210,145,14,0.439)").s().p("AAVAhQAAAAgBAAQAAAAAAgBQgBAAAAAAQAAgBgBAAQAAgBAAAAQAAgBAAAAQAAgBAAAAQAAgBAAgBQADgEABgFQAAgRgQgFQgEgBgHABQgKABgDAAQgLgCgBgLQgBgKAKgFQAKgEANAHQAKAGAIAKQAOAQgEARQAAAGgDAFQAAABAAABQAAAAgBABQAAAAgBAAQAAAAAAAAIgEgBg");
	this.shape_103.setTransform(414.8,-43.2,2.881,2.881,90);

	this.shape_104 = new cjs.Shape();
	this.shape_104.graphics.f("rgba(210,145,14,0.439)").s().p("AAAAmQAAAAgBgBQAAAAAAAAQgBgBAAAAQAAgBAAgBQAAAAAAgBQAAAAABgBQAAAAAAgBQABAAAAgBQAEgCADgEQAHgLgGgMQgDgFgHgEQgIgDgFgDQgJgGADgKQADgJALgBQALAAAJAOQAGALADAOQADATgMANQgDAFgFADIgCACQgBAAAAgBQgBAAAAAAQgBAAAAgBQAAAAAAgBg");
	this.shape_104.setTransform(520.4,43,2.881,2.881,90);

	this.shape_105 = new cjs.Shape();
	this.shape_105.graphics.f("rgba(210,145,14,0.439)").s().p("AgkBAQgTgGgCgPQgCgTASgMQAFgDAdgKQATgIAOgRQAPgQAGgUQACgEAFABQAFABgBAEQgOAkgeAYIghASQgQAJgDALQgDAJAHAGQAGAFAGgFIgCgDQgCgGAFgEQAGgEAFADQAGACAAAHQABAGgEAEQgGAIgLAAQgFAAgHgCg");
	this.shape_105.setTransform(114.6,45.5,2.881,2.881,90);

	this.shape_106 = new cjs.Shape();
	this.shape_106.graphics.f("rgba(210,145,14,0.439)").s().p("AhBAgQgPgMAEgPQAEgSAVgDQANgCAXAEQAvAJAlggQADgEADAEQAFAEgEADQggAagpAEIgXgBQgPgBgIACQgHABgFAEQgGAFAAAGQAAAFAEADQADAEAEgBIAAgCQAAgGAGgCQAGgCAEAEQAFADgBAHQgCAGgEADQgEACgFAAQgJAAgLgIg");
	this.shape_106.setTransform(214.2,-29.9,2.881,2.881,90);

	this.shape_107 = new cjs.Shape();
	this.shape_107.graphics.f("rgba(210,145,14,0.439)").s().p("Ag7AcQgPgDgEgUQgEgSANgJQANgKATAIQAJAEATAQQAkAeAvgKQAFgBAAAFQADAFgFABQgpAHgkgRQgGgDgLgHIgSgKQgOgIgLAGQgGADgBAFQgBAIAHADIABgDQAEgFAHACQAGACABAGQAAAHgFAEQgEADgEAAIgEgBg");
	this.shape_107.setTransform(344.7,-45.3,2.881,2.881,90);

	this.shape_108 = new cjs.Shape();
	this.shape_108.graphics.f("rgba(210,145,14,0.439)").s().p("AA9A6QgkgOgYgeIgSghQgJgQgLgDQgJgDgGAHQgFAGAFAGIADgCQAGgCAEAFQAEAGgDAFQgCAGgHAAQgGABgEgEQgNgKAHgTQAGgTAPgCQATgCAMASQADAFAKAdQAJATAQAOQAQAPAUAGQAEACgBAFQgBAEgCAAIgCAAg");
	this.shape_108.setTransform(460.3,3.2,2.881,2.881,90);

	this.shape_109 = new cjs.Shape();
	this.shape_109.graphics.f("rgba(210,145,14,0.439)").s().p("AAIAHIAAgXQABgPgCgJQgBgHgFgEQgEgGgGAAQgEAAgEADQgDAEAAADIACAAQAHAAACAGQACAGgEAFQgEAEgGgBQgGgBgDgFQgIgLAOgRQAMgPAOADQASAEAEAWQABAMgEAXQgHAmAUAfIAAASQgXgggDgkg");
	this.shape_109.setTransform(536.7,101.1,2.881,2.881,90);

	this.shape_110 = new cjs.Shape();
	this.shape_110.graphics.f("rgba(210,145,14,0.439)").s().p("AgFBKQgOgBgKgRQgLgSAOgJQAGgDAFADQAGACAAAGQABAFgEADQgEAEgEgBQAAAEAEADQAEAEAFgBQALgCADgRQACgIgBgPIAAgWQADgkAXggIAAASQgJAOgEARQgDARAGAiQADAXgIAMQgJAOgMAAIgDgBg");
	this.shape_110.setTransform(80.5,101,2.881,2.881,90);

	this.shape_111 = new cjs.Shape();
	this.shape_111.graphics.f("rgba(210,145,14,0.439)").s().p("AgyA3QgMgGgDgUQgCgVAPgEQAGgBAFAEQAFADgBAGQAAAGgGACQgFACgEgDQgFAGAFAGQAGAHALgEQAKgEAIgOIASgiQAYgeAkgNQAEgCABAGQABAFgEABQguAPgSA0QgIAXgMAJQgJAGgIAAQgGAAgGgDg");
	this.shape_111.setTransform(156.8,3.3,2.881,2.881,90);

	this.shape_112 = new cjs.Shape();
	this.shape_112.graphics.f("rgba(210,145,14,0.439)").s().p("AhLASQgIgMAIgRQAHgUAQAEQAGABADAGQADAGgFAFQgDAEgGAAQgGgBgBgFQgHACABAIQABAGAGACQALAGAOgIIASgJQALgIAGgDQAkgQApAHQAFABgDAFQAAAFgFgBQgagGgaAIQgRAGgXATQgOAMgOADIgJABQgMAAgIgLg");
	this.shape_112.setTransform(272.6,-45.3,2.881,2.881,90);

	this.shape_113 = new cjs.Shape();
	this.shape_113.graphics.f("rgba(210,145,14,0.439)").s().p("ABFAnQgVgSgZgGQgSgDghAGQgYADgMgIQgOgKABgOQACgOARgKQASgLAIAOQADAGgCAFQgDAGgGAAQgEABgEgEQgDgEABgEQgEAAgEAEQgDAEABAFQACAMAQACQAIACAPgBIAWAAQApAEAgAaQAEADgFADQAAABgBAAQAAABgBAAQAAAAAAAAQgBABAAAAQgBAAAAgBQAAAAgBAAQAAAAgBAAQAAgBAAAAg");
	this.shape_113.setTransform(403.1,-29.9,2.881,2.881,90);

	this.shape_114 = new cjs.Shape();
	this.shape_114.graphics.f("rgba(210,145,14,0.439)").s().p("AAwA/QgPgug0gSQgXgIgJgMQgLgPAIgOQAGgMAUgDQAVgCAEAPQABAGgDAFQgEAFgGgBQgGAAgCgGQgCgFADgEQgGgFgGAFQgHAGAEALQAEAKAOAIIAiASQAeAYANAkQACAEgGABIgCAAQgDAAgBgDg");
	this.shape_114.setTransform(502.6,45.5,2.881,2.881,90);

	this.shape_115 = new cjs.Shape();
	this.shape_115.graphics.f("rgba(210,145,14,0.439)").s().p("AgBgGQACABABAEQABACgCADIgCADg");
	this.shape_115.setTransform(619.8,47.4,0.58,0.58,180);

	this.shape_116 = new cjs.Shape();
	this.shape_116.graphics.f("rgba(210,145,14,0.439)").s().p("AgBgGIACADQACADgBADQgBADgCABg");
	this.shape_116.setTransform(619.8,54.9,0.58,0.58,180);

	this.shape_117 = new cjs.Shape();
	this.shape_117.graphics.f("rgba(210,145,14,0.439)").s().p("AgHAaQgIgBgGgGIAAgTQABAEACAEQAJAOANgHQAOgJgIgOQgIgOgOAIQgIAEgBAHIAAgQIAFgDQAJgFAKADQALADAFAJQAFAJgDAJQgDAKgIAGIgCABQgGADgGAAIgDAAg");
	this.shape_117.setTransform(618.7,19.3,0.58,0.58,180);

	this.shape_118 = new cjs.Shape();
	this.shape_118.graphics.f("rgba(210,145,14,0.439)").s().p("AgMAXQgJgGgDgKQgDgKAGgJQAFgJALgDQAKgDAJAGQAJAFACAKQADAJgFAJIgBACQgGAJgKACIgGABQgGAAgGgDgAgOgHQgIANAPAIQANAJAJgOQAIgOgPgIQgFgEgEAAQgIAAgFAKg");
	this.shape_118.setTransform(605.4,32.8,0.58,0.58,180);

	this.shape_119 = new cjs.Shape();
	this.shape_119.graphics.f("rgba(210,145,14,0.439)").s().p("AgSATQgHgIAAgLQAAgKAIgIQAIgIAKABQAKAAAHAIQAHAHABAKIAAAAIAAABQgBALgHAHQgIAHgKAAQgKAAgIgHgAgQAAQAAARAQAAQARgBAAgQQAAgPgRAAIAAAAQgQAAAAAPg");
	this.shape_119.setTransform(600.5,51.2,0.58,0.58,180);

	this.shape_120 = new cjs.Shape();
	this.shape_120.graphics.f("rgba(210,145,14,0.439)").s().p("AgGAZQgKgDgFgJQgGgKADgJQADgLAKgFQAJgFAJADQAKACAFAJIABACQAFAJgDAJQgDALgJAFQgGADgHAAIgGgBgAgHgNQgPAIAIANQAIAPAOgJQAPgIgIgOQgGgIgHAAQgEAAgFADg");
	this.shape_120.setTransform(605.4,69.6,0.58,0.58,180);

	this.shape_121 = new cjs.Shape();
	this.shape_121.graphics.f("rgba(210,145,14,0.439)").s().p("AgQAWIgFgDIAAgPQABAGAIAFQAOAIAIgPQAIgNgOgJQgNgIgJAPQgCAEgBADIAAgTQAGgFAIgBQAHgBAIAEIACABQAIAGADAKQADAJgGAJQgFAJgLADIgFABQgHAAgGgEg");
	this.shape_121.setTransform(618.7,83.1,0.58,0.58,180);

	this.shape_122 = new cjs.Shape();
	this.shape_122.graphics.f("rgba(210,145,14,0.439)").s().p("AgMAQQgDgEACgFQACgFAFAAQAGgBACAHQAFgCABgEQABgDgCgDQgDgDgDAAIgBAAQgLAAgLgFQgEgCADgEQACgEAEACQALAEAHAAIAOgBQAGABACAEQADAEgCAFQgDAJgNAKQgHAFgEAAQgFAAgEgFg");
	this.shape_122.setTransform(614.4,82.4,0.58,0.58,180);

	this.shape_123 = new cjs.Shape();
	this.shape_123.graphics.f("rgba(210,145,14,0.439)").s().p("AgWAOQgBgEADgEQADgEAFABQAEABACADQACADgBADQAFABADgEQACgCgBgEQgBgDgCgBIgBAAQgIgGgIgKQgCgDADgDQAEgCADAEQAGAIAHAEIANAGQAEACABAFQABAFgEAEQgIAHgOACIgGABQgMAAgCgKg");
	this.shape_123.setTransform(601.6,67,0.58,0.58,180);

	this.shape_124 = new cjs.Shape();
	this.shape_124.graphics.f("rgba(210,145,14,0.439)").s().p("AgJAVQgQgHAEgLQABgEAFgBQAFgCADAEQADABAAAEQAAAEgDACQAEADADgBQAEgBABgDQACgEgCgDIAAAAQgFgJgBgNQAAgEADAAQAFAAAAAEQACAKAFAJIAIAKQACAFgCAFQgDAEgFABIgHABQgHAAgJgEg");
	this.shape_124.setTransform(598.7,47.2,0.58,0.58,180);

	this.shape_125 = new cjs.Shape();
	this.shape_125.graphics.f("rgba(210,145,14,0.439)").s().p("AAEAYQgKgDgJgNQgLgNALgHQAEgDAFACQAEACABAFQABAGgHACQACAFAEABQACABADgCQADgDAAgDIABgBQAAgLAFgLQABgEAEADQAEACgCAEQgEALABAHIABAOQgBAGgEACQgCACgDAAIgEgBg");
	this.shape_125.setTransform(606.2,28.2,0.58,0.58,180);

	this.shape_126 = new cjs.Shape();
	this.shape_126.graphics.f("rgba(210,145,14,0.439)").s().p("AANAXQgFgLAAgMIgBAAQAAgDgDgCQgDgDgCABQgEABgCAGQADAAACACQACADgBADQgBAFgFABQgEACgEgDQgLgHALgNQAKgNAJgDQAFgCAEADQAEADABAGQAAAEgCAJQABALADAHQACAEgEACIgCABQgBAAAAAAQgBAAAAgBQAAAAgBAAQAAgBAAAAg");
	this.shape_126.setTransform(606.2,74.2,0.58,0.58,180);

	this.shape_127 = new cjs.Shape();
	this.shape_127.graphics.f("rgba(210,145,14,0.439)").s().p("AgBAVQABgNAFgJIAAAAQACgDgCgEQgBgDgEgBQgDgBgEADQADADAAADQAAAEgDABQgEADgEgBQgFgCgBgDQgEgLAOgGQATgHAHADQAFABACAFQACAFgDAEIgIALQgFAJgBAJQAAAEgFAAQgDAAAAgEg");
	this.shape_127.setTransform(598.7,55.2,0.58,0.58,180);

	this.shape_128 = new cjs.Shape();
	this.shape_128.graphics.f("rgba(210,145,14,0.439)").s().p("AgPAXQgDgCACgEQAIgKAIgGIABAAQACgBABgDQABgEgCgCQgDgEgFABQAEAJgMABQgLABACgMQABgFAGgDQAEgCAGABQATACAGAIQAEADgBAFQgBAEgEADIgOAGQgGAEgGAIQgBABAAAAQAAABgBAAQAAAAgBAAQAAABgBAAIgDgBg");
	this.shape_128.setTransform(601.6,35.3,0.58,0.58,180);

	this.shape_129 = new cjs.Shape();
	this.shape_129.graphics.f("rgba(210,145,14,0.439)").s().p("AgXASQgDgEAEgBQALgFALAAIABgBQADAAADgDQACgDgBgCQgBgEgGgCQAAADgCACQgDACgDgBQgFgBgBgFQgCgEADgEQAHgLAOALQAMAKADAJQACAFgDAEQgDAEgGABIgOgCQgKABgHADIgDABQAAAAgBAAQAAAAgBgBQAAAAAAAAQgBgBAAgBg");
	this.shape_129.setTransform(614.4,20,0.58,0.58,180);

	this.shape_130 = new cjs.Shape();
	this.shape_130.graphics.f("rgba(210,145,14,0.439)").s().p("AgLAOQgIAAgFgDIgBAAIgBAAQgFgDAEgEQAAAAABgBQAAAAABgBQABAAAAAAQABAAAAAAIAOADQAIgBAEgDIAGgGIAGgGQAFgDAFACQAFACAAAGQgBAJgSAGQgIADgIAAIgGAAg");
	this.shape_130.setTransform(614.1,84.5,0.58,0.58,180);

	this.shape_131 = new cjs.Shape();
	this.shape_131.graphics.f("rgba(210,145,14,0.439)").s().p("AACAMQgIgCgKgGQgHgEgEgFIAAAAIgBgBQAAgBgBAAQAAgBAAAAQAAAAAAgBQAAAAABgBQAAAAAAgBQAAAAAAAAQABgBAAAAQABAAAAAAQADgCACADQAGAFAFAEQAFACAGAAQADAAAGgCQAGgDADAAQAGAAADAEQAEAEgEAFQgEAGgJAAQgGAAgHgCg");
	this.shape_131.setTransform(600.5,68.3,0.58,0.58,180);

	this.shape_132 = new cjs.Shape();
	this.shape_132.graphics.f("rgba(210,145,14,0.439)").s().p("AgHALQgHgHgEgHQgDgGgBgHIAAgBIgBgBQAAgGAFABQAEAAAAAEQACAHADAGQADAEAEACQADADAGAAIAJACQAGADABAGQABAGgGADIgEABQgJAAgMgNg");
	this.shape_132.setTransform(596.8,47.6,0.58,0.58,180);

	this.shape_133 = new cjs.Shape();
	this.shape_133.graphics.f("rgba(210,145,14,0.439)").s().p("AAFAeQgJgBgGgSQgEgKABgMQAAgIADgFIAAgBIAAgBQADgFAEAEQAAAAABABQAAAAAAABQABABAAAAQAAABAAAAIgDAOQABAIADAEIAGAGQAFAEABACQADAFgCAFQgCAFgFAAIgBAAg");
	this.shape_133.setTransform(604,27.9,0.58,0.58,180);

	this.shape_134 = new cjs.Shape();
	this.shape_134.graphics.f("rgba(210,145,14,0.439)").s().p("AgDgFIABgBQABAAAAAAQABAAAAAAQAAAAAAAAQABAAAAAAQAAAAABAAQAAABAAAAQABAAAAABQAAAAAAAAQACADgDABIgFAHg");
	this.shape_134.setTransform(619.7,15.6,0.58,0.58,180);

	this.shape_135 = new cjs.Shape();
	this.shape_135.graphics.f("rgba(210,145,14,0.439)").s().p("AgCAHIgBgBIAAgMIAFAGQADACgCADQAAAAAAAAQAAABgBAAQAAAAAAABQgBAAAAAAIgBAAIgCAAg");
	this.shape_135.setTransform(619.7,86.8,0.58,0.58,180);

	this.shape_136 = new cjs.Shape();
	this.shape_136.graphics.f("rgba(210,145,14,0.439)").s().p("AgKAbIAAgBIAAgBQgDgFAAgIQgBgLAEgLQAGgSAJgBQAGAAACAFQACAFgDAFIgGAGIgGAGQgDAEgBAIIADAOQAAABAAAAQAAABAAABQAAAAgBAAQAAABgBAAIgDACQgBAAAAAAQgBAAAAgBQgBAAAAgBQAAAAgBgBg");
	this.shape_136.setTransform(604,74.5,0.58,0.58,180);

	this.shape_137 = new cjs.Shape();
	this.shape_137.graphics.f("rgba(210,145,14,0.439)").s().p("AgXATIABgBIAAgBQABgHADgGQAEgHAHgHQAPgQAKAEQAGADgBAGQgBAGgGADIgJACQgGABgDABQgEADgDAEQgDAGgCAHQAAAEgEAAIgBAAQgEAAAAgFg");
	this.shape_137.setTransform(596.8,54.8,0.58,0.58,180);

	this.shape_138 = new cjs.Shape();
	this.shape_138.graphics.f("rgba(210,145,14,0.439)").s().p("AgaANQAAAAgBAAQAAAAgBgBQAAAAAAAAQAAAAAAgBQgBAAAAgBQAAAAAAgBQAAAAAAgBQABAAAAgBIABgBIAAAAQAEgFAHgEQAJgGAJgCQAUgFAGAJQAEAFgEAEQgDAEgGAAQgDAAgGgDQgGgCgDAAQgFAAgGACQgGAEgFAFQAAABgBAAQAAAAAAAAQgBABAAAAQAAAAgBAAIgCgBg");
	this.shape_138.setTransform(600.5,34.1,0.58,0.58,180);

	this.shape_139 = new cjs.Shape();
	this.shape_139.graphics.f("rgba(210,145,14,0.439)").s().p("AAPAMQgCgBgEgFIgGgGQgEgDgIgBIgOADQgBAAAAAAQgBAAgBAAQAAAAAAgBQgBAAAAgBQgEgEAFgDIABAAIABAAQAFgDAIAAQALgBALAEQASAGABAJQAAAGgFACIgEABQgDAAgDgCg");
	this.shape_139.setTransform(614.1,17.9,0.58,0.58,180);

	this.shape_140 = new cjs.Shape();
	this.shape_140.graphics.f("rgba(210,145,14,0.439)").s().p("AgEAIQgFgDgBgIIABgCIACgCQAIgFAGAFQACADABADQABADgCADQgDAFgFAAQgCAAgDgCg");
	this.shape_140.setTransform(618.3,40.2,0.58,0.58,180);

	this.shape_141 = new cjs.Shape();
	this.shape_141.graphics.f("rgba(210,145,14,0.439)").s().p("AgJADIgBgDIABgCQAFgHAGAAQAEAAACADQADADAAADQAAAEgDADQgDADgEAAQgEAAgGgHg");
	this.shape_141.setTransform(615.4,51.2,0.58,0.58,180);

	this.shape_142 = new cjs.Shape();
	this.shape_142.graphics.f("rgba(210,145,14,0.439)").s().p("AgHAIIgCgCIgBgCQABgJAGgDQADgCADABQADABADADQAEAGgGAHQgDACgEAAQgCAAgFgCg");
	this.shape_142.setTransform(618.3,62.2,0.58,0.58,180);

	this.shape_143 = new cjs.Shape();
	this.shape_143.graphics.f("rgba(210,145,14,0.439)").s().p("AgYAWQgIgKAAgMQABgLAIgKIACgCQAKgIALAAQANgBAJAKQALAJgBANQAAAMgKALQgKAKgMgBQgOABgKgLgAgPgPQgOAOANAMQAOANANgNQANgNgNgNQgGgHgGAAQgHAAgHAHg");
	this.shape_143.setTransform(616.7,30.6,0.58,0.58,180);

	this.shape_144 = new cjs.Shape();
	this.shape_144.graphics.f("rgba(210,145,14,0.439)").s().p("AgIAfQgMgEgHgLQgGgKACgMIABgBIAAgCQAFgMALgHQALgGAMAEQAOADAFAMQAGALgDAMQgEAOgLAHQgHAEgIAAIgJgCgAgVgFQgFASATAFQARAFAFgTQAFgSgSgFIgGgBQgNAAgEAPg");
	this.shape_144.setTransform(609.2,43.6,0.58,0.58,180);

	this.shape_145 = new cjs.Shape();
	this.shape_145.graphics.f("rgba(210,145,14,0.439)").s().p("AgPAcQgKgGgFgMIAAgCIgBgBQgCgMAHgLQAGgLANgEQANgDALAHQAKAGAEAOQAEAMgHAMQgGALgOAEIgIABQgHAAgIgFgAgHgRQgTAFAFASQAFATASgFQASgFgFgSQgEgPgLAAIgHABg");
	this.shape_145.setTransform(609.2,58.8,0.58,0.58,180);

	this.shape_146 = new cjs.Shape();
	this.shape_146.graphics.f("rgba(210,145,14,0.439)").s().p("AgVAYIgCgCQgIgKAAgMQgBgMAKgJQAJgLANABQAMAAAKAKQALAKgBAMQABAOgLAKQgKAIgMAAQgLgBgKgIgAgPgKQgOAMAOAOQAOAOAMgNQANgNgNgOQgHgHgGAAQgHAAgGAHg");
	this.shape_146.setTransform(616.7,71.8,0.58,0.58,180);

	this.shape_147 = new cjs.Shape();
	this.shape_147.graphics.f("rgba(210,145,14,0.439)").s().p("AgEAGQgDgCAAgEQAAgCADgCQACgDADAAQADABACACIACAEIgBAEIgBACIgCABIgEABQgCAAgCgCg");
	this.shape_147.setTransform(616.9,30.7,0.58,0.58,180);

	this.shape_148 = new cjs.Shape();
	this.shape_148.graphics.f("rgba(210,145,14,0.439)").s().p("AgBAIQgGgCAAgGQAAgDAEgCQADgCACAAQAGACAAAFIAAACQgDAGgFAAIgBAAg");
	this.shape_148.setTransform(609.4,43.7,0.58,0.58,180);

	this.shape_149 = new cjs.Shape();
	this.shape_149.graphics.f("rgba(210,145,14,0.439)").s().p("AgDAGQgDgCgBgDQAAgCACgDQACgCADgBQAFAAADAGIAAABQAAAGgGACIgCAAQAAAAAAAAQgBAAgBgBQAAAAgBAAQAAAAAAgBg");
	this.shape_149.setTransform(609.4,58.7,0.58,0.58,180);

	this.shape_150 = new cjs.Shape();
	this.shape_150.graphics.f("rgba(210,145,14,0.439)").s().p("AgEAFQgDgCAAgDQABgDACgCIAEgCIAEABIACABIABACIABADQAAADgCACQgCADgEAAQgCAAgCgDg");
	this.shape_150.setTransform(616.9,71.7,0.58,0.58,180);

	this.shape_151 = new cjs.Shape();
	this.shape_151.graphics.f("rgba(210,145,14,0.439)").s().p("AgZAAIAGAAQAUgCASgHQAEgCACAFQABADgDACQgZAKgXABg");
	this.shape_151.setTransform(618.4,76,0.58,0.58,180);

	this.shape_152 = new cjs.Shape();
	this.shape_152.graphics.f("rgba(210,145,14,0.439)").s().p("AAjAeQgrgFgXgVQgLgJAGgNQAGgNAPADQAGABAFAEQAEAEgBAGQgDAHgIgEQgJgFgCABIgDAGQAAAEADAEIAIAEIAOAHQASAIASACQAFABgBAFQAAAAAAABQAAABgBAAQAAAAgBABQAAAAgBAAIgBAAg");
	this.shape_152.setTransform(609.4,64.4,0.58,0.58,180);

	this.shape_153 = new cjs.Shape();
	this.shape_153.graphics.f("rgba(210,145,14,0.439)").s().p("AAUAoQgigZgKgcQgEgOAKgJQALgJAMAKQAFAEACAFQADAGgEAEQgDAEgGgCQgBAAAAAAQgBgBAAAAQAAAAAAgBQAAAAAAgBIgDgFQgDgFgEABQgFACAAAGQAAAFAEAGIAJAMQAMARAOALQAEADgDAEIgDABIgCgBg");
	this.shape_153.setTransform(606.8,48.5,0.58,0.58,180);

	this.shape_154 = new cjs.Shape();
	this.shape_154.graphics.f("rgba(210,145,14,0.439)").s().p("AgGAqQgTgpAIgdQAEgOANgCQANgBAFAOQADAGgBAFQgBAHgFACQgJADgBgNQACgHgKgCIgDAEQgCADAAAGIABARQACAUAHASQABAEgDACIgCABQAAAAgBAAQAAgBgBAAQAAAAAAgBQgBAAAAgBg");
	this.shape_154.setTransform(612,33.4,0.58,0.58,180);

	this.shape_155 = new cjs.Shape();
	this.shape_155.graphics.f("rgba(210,145,14,0.439)").s().p("AAAAtQgNgCgEgOQgIgdATgpQABgDAEABQADACgBAEQgHASgCAVIgBAQQAAAGACADQADAIAEgHQAGgBgCgFQABgGABgCQADgEAFACQAFACABAHQABAFgDAGQgFANgLAAIgCAAg");
	this.shape_155.setTransform(612,69,0.58,0.58,180);

	this.shape_156 = new cjs.Shape();
	this.shape_156.graphics.f("rgba(210,145,14,0.439)").s().p("AgSAmQgLgIAGgRQALgdAggXQADgCACACQADAEgEADQgOALgMARIgJAMQgFAIACAGQACAGADgBQAEAAADgFQAHgMAGAHQADAEgCAHQgDAEgEAEQgFAFgGAAIgCAAQgFAAgFgDg");
	this.shape_156.setTransform(606.8,53.9,0.58,0.58,180);

	this.shape_157 = new cjs.Shape();
	this.shape_157.graphics.f("rgba(210,145,14,0.439)").s().p("AgbAcQgHgDgDgGQgFgNANgKQAXgUApgFQAEgBAAAEQABAFgFABQgSACgSAIIgOAHQgHADgBADQgDACAAAEQABAIAFgDQADAAAIgEQAGgCACAGQABAGgEAEQgEAEgHABIgEABQgEAAgEgCg");
	this.shape_157.setTransform(609.4,38,0.58,0.58,180);

	this.shape_158 = new cjs.Shape();
	this.shape_158.graphics.f("rgba(210,145,14,0.439)").s().p("AATAKQgSgHgVgDIgFAAIAAgJQAXABAZAKQADACgBADQgBADgDAAIgCAAg");
	this.shape_158.setTransform(618.4,26.4,0.58,0.58,180);

	this.shape_159 = new cjs.Shape();
	this.shape_159.graphics.f("rgba(210,145,14,0.439)").s().p("AgEAGQgGgGAFgFQAFgFAGAFQACACAAADQABAEgDACQgDADgCAAQgCAAgDgDg");
	this.shape_159.setTransform(619.2,77,0.58,0.58,180);

	this.shape_160 = new cjs.Shape();
	this.shape_160.graphics.f("rgba(210,145,14,0.439)").s().p("AgHACQgCgHAIgCQACgBADACQADACABADQABACgCADQgCAEgDAAIgCAAQgFAAgCgGg");
	this.shape_160.setTransform(608.7,64.5,0.58,0.58,180);

	this.shape_161 = new cjs.Shape();
	this.shape_161.graphics.f("rgba(210,145,14,0.439)").s().p("AgBAIQgIgCADgHQACgIAGACQAEABABADQACADgBACQgBADgCACIgFACIgBgBg");
	this.shape_161.setTransform(605.9,48.4,0.58,0.58,180);

	this.shape_162 = new cjs.Shape();
	this.shape_162.graphics.f("rgba(210,145,14,0.439)").s().p("AgFAGQgFgGAFgEQAFgHAGAGQAFAFgFAGQgDADgDAAQgCAAgDgDg");
	this.shape_162.setTransform(611.5,33,0.58,0.58,180);

	this.shape_163 = new cjs.Shape();
	this.shape_163.graphics.f("rgba(210,145,14,0.439)").s().p("AgFAFQgFgFAFgFQACgDADAAQADABADACQACADAAACQAAAEgCACQgDADgCAAQgCAAgEgEg");
	this.shape_163.setTransform(611.5,69.3,0.58,0.58,180);

	this.shape_164 = new cjs.Shape();
	this.shape_164.graphics.f("rgba(210,145,14,0.439)").s().p("AgGACQgDgHAIgCQADgBADACQACACABADQABACgCADQgBAEgEAAIgCAAQgFAAgBgGg");
	this.shape_164.setTransform(605.9,54,0.58,0.58,180);

	this.shape_165 = new cjs.Shape();
	this.shape_165.graphics.f("rgba(210,145,14,0.439)").s().p("AgBAIQgIgCACgHQACgIAHACQADABACADQACADgBACQgBADgDACIgEACIgBgBg");
	this.shape_165.setTransform(608.7,37.9,0.58,0.58,180);

	this.shape_166 = new cjs.Shape();
	this.shape_166.graphics.f("rgba(210,145,14,0.439)").s().p("AgFAGQgFgGAGgFQAFgFAFAFQADACAAADQgBADgCADQgDADgDAAQgCAAgDgDg");
	this.shape_166.setTransform(619.2,25.4,0.58,0.58,180);

	this.shape_167 = new cjs.Shape();
	this.shape_167.graphics.f("rgba(210,145,14,0.439)").s().p("AgEAGQgGgGAFgFQAGgFAFAGQACACAAACQAAAEgCACQgDADgDAAQgBAAgDgDg");
	this.shape_167.setTransform(615.9,81,0.58,0.58,180);

	this.shape_168 = new cjs.Shape();
	this.shape_168.graphics.f("rgba(210,145,14,0.439)").s().p("AgGACQgCgIAHgBQAHgBACAHQABACgCADQgBADgEABIgCAAQgFAAgBgGg");
	this.shape_168.setTransform(603.9,66.2,0.58,0.58,180);

	this.shape_169 = new cjs.Shape();
	this.shape_169.graphics.f("rgba(210,145,14,0.439)").s().p("AgBAIQgDgBgCgDQgBgDAAgCQACgHAHABQADABACADQABADgBACQAAADgDACIgDABIgCAAg");
	this.shape_169.setTransform(600.8,47.4,0.58,0.58,180);

	this.shape_170 = new cjs.Shape();
	this.shape_170.graphics.f("rgba(210,145,14,0.439)").s().p("AgFAFQgCgCAAgDQAAgCACgDQAFgFAGAFQAFAFgGAFQgDADgCAAQgCAAgDgDg");
	this.shape_170.setTransform(607.6,29.7,0.58,0.58,180);

	this.shape_171 = new cjs.Shape();
	this.shape_171.graphics.f("rgba(210,145,14,0.439)").s().p("AgFAFQgCgCAAgDQAAgDACgCQACgCADAAQADAAACACQADACAAADQAAADgCADQgDACgDAAQgCAAgDgDg");
	this.shape_171.setTransform(607.6,72.7,0.58,0.58,180);

	this.shape_172 = new cjs.Shape();
	this.shape_172.graphics.f("rgba(210,145,14,0.439)").s().p("AgHACQAAgCABgDQACgDADgBQACgBADACQADACAAADQABACgBADQgCADgDABIgCAAQgFAAgCgGg");
	this.shape_172.setTransform(600.8,54.9,0.58,0.58,180);

	this.shape_173 = new cjs.Shape();
	this.shape_173.graphics.f("rgba(210,145,14,0.439)").s().p("AgBAIQgHgBACgIQACgHAGABQAEABABADQACADgBACQgCAGgGAAIgBAAg");
	this.shape_173.setTransform(603.9,36.2,0.58,0.58,180);

	this.shape_174 = new cjs.Shape();
	this.shape_174.graphics.f("rgba(210,145,14,0.439)").s().p("AgFAGQgFgGAGgFQACgCACAAQAEAAACACQACACAAADQAAADgCACQgCADgEAAIAAAAQgDAAgCgCg");
	this.shape_174.setTransform(615.9,21.4,0.58,0.58,180);

	this.shape_175 = new cjs.Shape();
	this.shape_175.graphics.f("rgba(210,145,14,0.439)").s().p("AgGAHQgHgHAHgGQADgDAEAAQAEgBADADQADAEAAADQgBAEgDADQgDADgEAAQgDAAgDgDg");
	this.shape_175.setTransform(610.6,24.4,0.58,0.58,180);

	this.shape_176 = new cjs.Shape();
	this.shape_176.graphics.f("rgba(210,145,14,0.439)").s().p("AgCAKQgEgBgCgDQgCgEABgDQADgKAIACQAFABACAEQACAEgCADQAAAEgEACIgFACIgCgBg");
	this.shape_176.setTransform(600.7,41.4,0.58,0.58,180);

	this.shape_177 = new cjs.Shape();
	this.shape_177.graphics.f("rgba(210,145,14,0.439)").s().p("AgJACQgBgDACgDQACgEAEgBQADgBAEACQAEACAAAEQACADgCAEQgCAFgFAAIgCABQgGAAgDgJg");
	this.shape_177.setTransform(600.7,61,0.58,0.58,180);

	this.shape_178 = new cjs.Shape();
	this.shape_178.graphics.f("rgba(210,145,14,0.439)").s().p("AgGAHQgDgDAAgEQgBgEADgDQAEgDADABQAEAAADADQADADAAADQAAAEgDADQgDADgEAAQgDAAgDgDg");
	this.shape_178.setTransform(610.6,78,0.58,0.58,180);

	this.shape_179 = new cjs.Shape();
	this.shape_179.graphics.f("rgba(210,145,14,0.439)").s().p("ABABUQgcgNg1gKQgigHgTgCIAAgJQAYABAcAEQAuAHAXAKQgKgXgHguQgGgtAAgXQgxAWgmAFQgDATgIAaIAAg9QAvgCA2gVQABAAABAAQAAAAABAAQAAAAABABQAAAAABABIABACIAAAGQAAAXAKAwQAKA1ANAcQACADgDACQAAABgBAAQAAAAAAABQgBAAAAAAQAAAAgBAAIgCgBg");
	this.shape_179.setTransform(615.8,30.5,0.58,0.58,180);

	this.shape_180 = new cjs.Shape();
	this.shape_180.graphics.f("rgba(210,145,14,0.439)").s().p("AhBBYQgKg7gWgpQgCgDAEgDQAogZAmguQAAAAABgBQABAAAAAAQABAAAAAAQABAAAAAAIADACIAAABIACADQAMAVAfAlQAnArAXAPQACABgBAEQgBACgDABQgcACg2ASQgxASgUAMIgCABIgCAAQgBAAgBAAQAAgBgBAAQAAgBAAAAQgBgBAAAAgAhNgKQAPAlAFA0QAWgMAogRQArgRAZgDQgUgQgdgjQgYgfgOgZQgfAqggAZg");
	this.shape_180.setTransform(608.2,44,0.58,0.58,180);

	this.shape_181 = new cjs.Shape();
	this.shape_181.graphics.f("rgba(210,145,14,0.439)").s().p("AgRBaQgmgugogZQgEgDACgDQAWgpAKg7QAAAAABgBQAAAAAAgBQABAAAAgBQABAAABAAIACAAIACABQAUAMAxASQA2ASAcACQADABABACQABAEgCABQgXAPgnArQgfAlgMAVIgCADIAAABIgDACIgBAAIgDgBgAhNALQAgAZAfAqQAOgZAYgfQAdgjAUgQQgZgDgrgRQgogRgWgMQgFA0gPAlg");
	this.shape_181.setTransform(608.2,58.4,0.58,0.58,180);

	this.shape_182 = new cjs.Shape();
	this.shape_182.graphics.f("rgba(210,145,14,0.439)").s().p("AAfBVQg2gVgvgCIAAg+QAIAbADATQAmAFAxAWQAAgXAGgtQAHguAKgXQgXAKguAHQgcAEgYABIAAgJQATgCAigHQA1gKAcgNQADgCACADQADACgCADQgNAcgKA1QgKAwAAAXIAAAGIgBACQgBABAAAAQAAAAgBABQAAAAAAAAQgBAAAAAAIgCAAg");
	this.shape_182.setTransform(615.8,71.9,0.58,0.58,180);

	this.shape_183 = new cjs.Shape();
	this.shape_183.graphics.f("rgba(210,145,14,0.439)").s().p("AheEEQAQAAAagDQAXgEAugMIADgBIgBgDIgVhFQgUg2gegSQgDgBABgEQABgDAEAAQAjgBArgkIA1gxIACgCIgCgBIg1gxQgrgkgjgBQgEAAgBgDQgBgEADgBQAegSAUg2IALgiIAKgjIABgDIgDgBQgugMgXgEQgagDgQAAIAAgOQAJABAfAGQAnAHAjALIAHABQAEABgBAFIgCAHQgIAggPAtQgOAqgNAQQAOAGAOALIAaAVQAfAbAZAaIAGAGQAAAAABAAQAAABAAAAQAAABAAAAQAAAAAAAAQAAABAAAAQAAABAAAAQAAAAAAABQgBAAAAAAIgGAGQgZAagfAbIgaAVQgOAMgOAFQANAQAOAqQAOAmAJAnIACAHQABAFgEABIgHABQgjALgnAHQgfAGgJABg");
	this.shape_183.setTransform(614.4,51.2,0.58,0.58,180);

	this.shape_184 = new cjs.Shape();
	this.shape_184.graphics.f("rgba(210,145,14,0.439)").s().p("AgNgLIACAAQAAABABAAQAAAAABAAQAAAAABAAQAAAAABgBQgFgCgBgGQgBgJAIgEQAHgDAHAFQAGAFAAAIQAAAIgGAGQgGAGgKABQAEAHgBAIQgBAIgFAFIgCACg");
	this.shape_184.setTransform(619.1,41.2,0.58,0.58,180);

	this.shape_185 = new cjs.Shape();
	this.shape_185.graphics.f("rgba(210,145,14,0.439)").s().p("AgtBFIAAgKIAaAAIATgDIAkgJIgBgEQgEgSgFgOIgHgRQgMgXgLgOQgXgWgQAQIgCACIAAgSQATgJAVATQAJAHAJAMQALAPAKAcQAFAOAFATIACAJQACAEgFABIgJADQgVAHgRACQgRADgNAAIgLAAg");
	this.shape_185.setTransform(617.3,41.1,0.58,0.58,180);

	this.shape_186 = new cjs.Shape();
	this.shape_186.graphics.f("rgba(210,145,14,0.439)").s().p("Ag1BCQgQgIgDgTQgCgLAFgLQAFgLAIgGQgLgGgEgPQgEgPAGgNQALgZAiAHQANABAMAGQASAIAWATQALAKAOAPIAGAGQAAAAABAAQAAABAAAAQAAABAAAAQAAAAAAAAQAAABAAAAQAAABAAAAQAAAAAAABQgBAAAAAAIgGAGQgMAOgOALQgYAWgXAJIgNADQgIACgHAAQgKAAgIgEgAg1AUQgGAJAEAPQAGAWAhgIQARgHAWgPIAMgJIAYgYIADgDIgDgCIgYgXIgNgKQgVgQgSgGQgggIgGAWQgEAPAGAJQAFAJAIgBQADAAAEgCQgHABgDgGQgFgIAGgHQAGgHAIACQAIACADAHQAEAHgCAHQgDAKgIAFQANAIgCAPQgBAIgGAFQgHAFgHgDQgHgCgCgHQgCgHAGgFQAEgDAEAAIgHgCIAAAAQgIAAgFAIg");
	this.shape_186.setTransform(615.3,51.2,0.58,0.58,180);

	this.shape_187 = new cjs.Shape();
	this.shape_187.graphics.f("rgba(210,145,14,0.439)").s().p("AgtBDIAAgTIACADQAQAQAXgXQANgPAKgXIAHgPQAFgPAEgRIABgEIgEgBIgggIIgtgEIAAgJQASgCAbAFQAPADATAGIAJACQAFABgCAFIgCAIQgEATgHAQQgLAggPARIgHAIQgJAKgNAEQgGACgGAAQgGAAgFgCg");
	this.shape_187.setTransform(617.3,61.3,0.58,0.58,180);

	this.shape_188 = new cjs.Shape();
	this.shape_188.graphics.f("rgba(210,145,14,0.439)").s().p("AgKAdQgFgGADgHQACgDADgCQAAAAgBAAQgBAAAAAAQgBAAAAAAQAAAAgBAAIgCABIAAgtQAEADACAFQAFAKgGALQAPABAGANQADAIgEAHQgDAIgJABIAAAAQgGAAgEgFg");
	this.shape_188.setTransform(619.1,61.1,0.58,0.58,180);

	this.shape_189 = new cjs.Shape();
	this.shape_189.graphics.f("rgba(210,145,14,0.439)").s().p("AgEAEQgCgDABgCQABgDADgBQACgBADACIABACIABABQABACgBADQgCACgDAAQgDAAgCgCg");
	this.shape_189.setTransform(604.6,86.7,0.58,0.58,180);

	this.shape_190 = new cjs.Shape();
	this.shape_190.graphics.f("rgba(210,145,14,0.439)").s().p("AgCAGQgDgCAAgEQAAgCACgCQADgCACABQADABABADIAAABIAAABQAAADgDACIgDABIgCgBg");
	this.shape_190.setTransform(591.2,65.5,0.58,0.58,180);

	this.shape_191 = new cjs.Shape();
	this.shape_191.graphics.f("rgba(210,145,14,0.439)").s().p("AAAAGQgEAAgBgEQgBgCACgCQABgDADAAQACgBADACQADAEgDACIAAACQgCACgDAAIAAAAg");
	this.shape_191.setTransform(590.2,40.5,0.58,0.58,180);

	this.shape_192 = new cjs.Shape();
	this.shape_192.graphics.f("rgba(210,145,14,0.439)").s().p("AgDAGQgCgCAAgDQAAgDACgCQADgCACABQADABABADQABACgCADIgCABIgBABIgCAAIgDAAg");
	this.shape_192.setTransform(601.8,18.4,0.58,0.58,180);

	this.shape_193 = new cjs.Shape();
	this.shape_193.graphics.f("rgba(210,145,14,0.439)").s().p("AgDAFQgCgCAAgDQAAgCACgCQADgCADACIAAAAIACACQACACgBACQgBAEgDAAIgCAAIgDgBg");
	this.shape_193.setTransform(601.8,84,0.58,0.58,180);

	this.shape_194 = new cjs.Shape();
	this.shape_194.graphics.f("rgba(210,145,14,0.439)").s().p("AAAAGQgDAAgBgDQgCgDACgCQABgDADAAQADgBACADIAAACQADACgDAEQgBAAAAABQgBAAAAAAQgBAAAAAAQgBAAgBAAIAAAAg");
	this.shape_194.setTransform(590.2,61.8,0.58,0.58,180);

	this.shape_195 = new cjs.Shape();
	this.shape_195.graphics.f("rgba(210,145,14,0.439)").s().p("AgDAFQgCgCAAgDQAAgDADgBQACgCADABQAEACgBADIAAACIgEAEIgCAAQAAAAAAAAQAAAAgBAAQAAAAgBgBQAAAAgBAAg");
	this.shape_195.setTransform(591.2,36.8,0.58,0.58,180);

	this.shape_196 = new cjs.Shape();
	this.shape_196.graphics.f("rgba(210,145,14,0.439)").s().p("AgBAGQgEgBAAgDQgBgCACgDQACgCACAAQADAAACACQACADgCADIAAAAIgCACQAAAAgBAAQAAABAAAAQgBAAAAAAQgBAAAAAAIgBAAg");
	this.shape_196.setTransform(604.5,15.7,0.58,0.58,180);

	this.shape_197 = new cjs.Shape();
	this.shape_197.graphics.f("rgba(210,145,14,0.439)").s().p("AgMAFIgBgOIABgDIADgBIAOABQAJACAAAHQAAAHgHABIgCAAIAAADQgBAGgHAAQgHAAgCgJg");
	this.shape_197.setTransform(603,16.9,0.58,0.58,180);

	this.shape_198 = new cjs.Shape();
	this.shape_198.graphics.f("rgba(210,145,14,0.439)").s().p("AgFAKQgCgCgGgJQAAAAgBAAQAAgBAAAAQAAAAAAgBQAAAAABgBQAAAAAAgBQAAAAAAAAQAAgBABAAQAAAAABAAQAJgGADgBQAJgDAEAHQACADgBADQgBADgDABIgCABIACABQABADgBAEQgBADgEABIgDABQgEAAgEgFg");
	this.shape_198.setTransform(590.6,38.6,0.58,0.58,180);

	this.shape_199 = new cjs.Shape();
	this.shape_199.graphics.f("rgba(210,145,14,0.439)").s().p("AABAOQgDgBgJgFQgBgBAAAAQgBAAAAAAQAAgBAAAAQAAgBAAgBQgBAAAAAAQAAgBAAAAQAAAAAAgBQABAAAAgBQAGgJACgBQAFgGAGACQAEABABAEQABADgBADIgBACIABAAQAGADgDAHQgDAFgGAAIgEgBg");
	this.shape_199.setTransform(590.6,63.8,0.58,0.58,180);

	this.shape_200 = new cjs.Shape();
	this.shape_200.graphics.f("rgba(210,145,14,0.439)").s().p("AgMANIgBgDIABgOQACgJAHAAQAHAAABAHIAAACIADAAQAGABAAAHQAAAHgJACIgOABg");
	this.shape_200.setTransform(603,85.5,0.58,0.58,180);

	this.shape_201 = new cjs.Shape();
	this.shape_201.graphics.f("rgba(210,145,14,0.439)").s().p("AALBXQgZgFgWgSQgYgTAAgZQAAgUAQgLQgagLgKgQQgHgJgBgNQgCgPAIgHQAIgIAPACQANABAJAHQAQAMAKAYQAEgGAGgEQAVgOAZAQQAUANAMAZQAJAQACAPQACAJgBAKQAAAIgEALQgHAQgPAKQgOAJgSAAQgKAAgKgDgAgggCQgMADgBAOQgBAUAZAWQAUASAWAEQAbAFASgSQASgSgFgbQgEgWgSgUQgWgZgUABQgOABgEAMQgFALAIAKIAFADQAFABAFgCIADgCIgCgBQgFgBgCgFQgBgEADgFIACgDQALgHAIAJQAJAJgFALQgIASgagIQAIAagSAIQgLAFgJgJQgJgIAHgLIACgBQAFgEAGACQAFADABAFIABgBQAAgBAAAAQABAAAAgBQAAAAAAgBQAAgBAAAAQACgGgDgGIgBgBQgGgEgGAAQgFAAgEACgAgOgOIgBgCQgCgFAAgGIAAgBQgGgZgLgLQgHgIgLgDQgMgDgHAHQgHAHADAMQADALAIAHQALALAZAGIABAAQAHAAAGADg");
	this.shape_201.setTransform(604.6,18.5,0.58,0.58,180);

	this.shape_202 = new cjs.Shape();
	this.shape_202.graphics.f("rgba(210,145,14,0.439)").s().p("AAFBGQgbgEgNgUQgMgTAJgSQgdADgRgJQgKgEgHgLQgJgMAEgKQAEgLAOgFQALgEAMABQATACAUAQIADgLQAKgXAbgCQAZgCAcAUQAOAJALANQAFAGAEAHQAFAJABALQADARgIAQQgNAXgeAJQgQAFgQAAQgLAAgLgCgAgYAMQgHALAHAMQAKAQAdAGQAbAGAWgIQAagJAGgZQAHgXgRgVQgOgRgagJQgegLgQAJQgNAHABAOQAAANAMAFIAGgBQAFgBADgFIACgDIgBABQgGACgEgEQgDgDAAgGIABgDQAFgMAMAEQAMADABAMQADAUgbAHQAUARgMARQgIAJgKgDQgMgDABgMIAAgDQADgFAGgBQAFgBADAFIAAgEQAAgGgFgEQAAAAAAgBQAAAAgBgBQAAAAgBAAQAAgBgBAAIgEAAQgLAAgGAKgAhMgpQgLADgEAIQgDAJAJAKQAHAIAKADQAPADAagGIABAAQAHgEAGAAIgCgCQgDgDgEgGIgBgBQgSgSgQgEIgIgBIgLABg");
	this.shape_202.setTransform(593,39,0.58,0.58,180);

	this.shape_203 = new cjs.Shape();
	this.shape_203.graphics.f("rgba(210,145,14,0.439)").s().p("AgHBBQgRgLgCgSQgVAQgTACQgMABgLgFQgNgFgEgKQgEgLAJgMQAHgLALgEQASgIAbADQgDgHAAgEQgCgaAYgPQATgNAiADQAPABARAGQAJADAHAFQAIAEAGAJQAKANACAQQACAagWAYQgSAUgcALQgNAEgLAAQgOAAgLgHgAgMAgQgBANANAHQAQAJAegLQAagIAOgSQARgVgHgXQgGgZgagJQgWgIgbAGQgcAGgLAQQgHAMAHALQAHALAOgBQAEgDACgEQADgEgBgGIgCACQgDAEgFgCQgEgBgCgFQAAAAAAAAQgBgBAAAAQAAAAAAgBQAAAAAAgBQgBgMAMgDQAKgDAIAJQAMARgUARQAbAHgDAUQgBAMgMADQgMAEgFgMIgBgCQgBgGAFgEQAFgEAFACIgBgBIgCgDQgEgEgHgBIgCAAQgMAFAAAOgAhFACQgJACgIAJQgIAJADAJQAEAIAMADQAKADAKgDQAOgEASgSIABgBQADgHAGgDIgCgBQgGAAgFgEIgBAAQgQgEgLAAQgJAAgGACg");
	this.shape_203.setTransform(593,63.3,0.58,0.58,180);

	this.shape_204 = new cjs.Shape();
	this.shape_204.graphics.f("rgba(210,145,14,0.439)").s().p("AhSBTQgIgIACgPQABgNAHgJQAMgQAYgKQgGgEgEgGQgOgVAQgZQANgUAZgMQAQgJAPgCQAJgCAKABQAIAAALAEQAQAHAKAPQANAWgHAeQgFAZgSAWQgTAYgZAAQgUAAgLgQQgLAagQAKQgJAHgNABIgFAAQgLAAgGgGgAgQAQQgFACgGAAIgBAAQgZAGgLALQgIAHgDALQgDAMAHAHQAHAHAMgDQALgDAHgIQALgLAGgZIAAgBQAAgGADgHgAABALIgBABQgHAKAFAMQADALAOABQAUABAWgZQASgUAEgWQAFgbgSgSQgSgSgbAFQgWAEgUASQgZAWABAUQABAOAMAEQALAFAKgIIADgFQABgFgCgFIgCgDIgBACQgBAFgFACQgEABgFgDIgDgCQgHgLAJgIQAJgJALAFQASAIgIAaQALgDAIACQALACAEAJQAFALgJAJQgIAJgLgHIgBgCQgEgFACgGQADgFAFgBIgBgBQgBAAAAAAQAAgBgBAAQAAAAgBAAQgBAAAAAAIgFgBQgDAAgEACg");
	this.shape_204.setTransform(604.6,83.9,0.58,0.58,180);

	this.shape_205 = new cjs.Shape();
	this.shape_205.graphics.f("rgba(210,145,14,0.439)").s().p("AABAbQgFgDgEgGQgDgFgCgMIgGgWQgBgDADgCQAEgDACADIANAOQAKAIAEAFQAEAGAAAIQAAAIgGAEQgDABgEAAQgDAAgDgBg");
	this.shape_205.setTransform(610.5,4.8,0.58,0.58,180);

	this.shape_206 = new cjs.Shape();
	this.shape_206.graphics.f("rgba(210,145,14,0.439)").s().p("AABAQQgFgEgIgKIgOgNQgDgCADgEQACgDADABIAWAGQAMACAFADQAGAEADAFQADAHgDAGQgEAGgIAAQgIAAgGgEg");
	this.shape_206.setTransform(591,24.3,0.58,0.58,180);

	this.shape_207 = new cjs.Shape();
	this.shape_207.graphics.f("rgba(210,145,14,0.439)").s().p("AgEALIgXgHQgDAAAAgEQAAAAAAgBQAAAAAAgBQABAAAAgBQABAAABAAIAXgHQALgEAFAAQAHAAAGAEQAGAEAAAGQAAAHgGAEQgGAEgHAAIgBAAQgFAAgKgEg");
	this.shape_207.setTransform(583.8,51.2,0.58,0.58,180);

	this.shape_208 = new cjs.Shape();
	this.shape_208.graphics.f("rgba(210,145,14,0.439)").s().p("AgaASQgDgEADgCIAOgNQAIgKAFgEQAGgEAIAAQAIAAAEAGQADAGgDAHQgDAFgGAEQgFADgMACIgWAGIgCAAQAAAAgBAAQAAAAgBgBQAAAAgBAAQAAgBAAAAg");
	this.shape_208.setTransform(591,78.1,0.58,0.58,180);

	this.shape_209 = new cjs.Shape();
	this.shape_209.graphics.f("rgba(210,145,14,0.439)").s().p("AgRAbQgDgCABgDIAGgWQACgMADgFQAEgGAFgDQAHgDAGADQAGAEAAAJQAAAHgEAGQgEAFgKAIIgNAOQgBAAAAABQAAAAgBAAQAAAAAAAAQgBAAAAAAIgDgBg");
	this.shape_209.setTransform(610.5,97.6,0.58,0.58,180);

	this.shape_210 = new cjs.Shape();
	this.shape_210.graphics.f("rgba(210,145,14,0.439)").s().p("AgFAoQgLgCgHgJQgJgMAHgLIACgBQADgGAHgCQAHgCAGAFQADACABAFQABAGgEAEQgFAFgJgCIABABIACADQADAEAGACQAHACAHgDQAKgGAFgIQADgJgIgIQgIgIgLgEIgQgEIgKgDQgKgDgHgHQgEgEAEgDQAEgDACADQAGAFAIADQALAEAZADQAVAEADASQACAMgIAMQgIALgNAEQgIADgGAAIgFgBg");
	this.shape_210.setTransform(615.4,95.4,0.58,0.58,180);

	this.shape_211 = new cjs.Shape();
	this.shape_211.graphics.f("rgba(210,145,14,0.439)").s().p("AgOATQgGgDACgGQABgFAGgCIANgGQAFgFADgJQABgEADABQAGAAgBAGQgDAPgKAKQgHAKgIAAIgFgCg");
	this.shape_211.setTransform(609.8,90,0.58,0.58,180);

	this.shape_212 = new cjs.Shape();
	this.shape_212.graphics.f("rgba(210,145,14,0.439)").s().p("AgXAIQgCgFADgDQACgEAFAAIAHABIAHAAQAJAAAIgIQADgCADACQAFADgEAEQgLAMgMAEQgGABgFAAQgIAAgEgFg");
	this.shape_212.setTransform(594,70.9,0.58,0.58,180);

	this.shape_213 = new cjs.Shape();
	this.shape_213.graphics.f("rgba(210,145,14,0.439)").s().p("AgIAKQgRgEABgKQAAgEAEgCQAFgCAEACIAGAEIAGAFQAJADAJgCQAEgBABADQACAFgFABQgKAEgJAAQgFAAgFgCg");
	this.shape_213.setTransform(589.9,46.7,0.58,0.58,180);

	this.shape_214 = new cjs.Shape();
	this.shape_214.graphics.f("rgba(210,145,14,0.439)").s().p("AAPATQgPgDgKgKQgNgLAFgJQADgGAGACQAFABACAGIAGANQAFAFAJADQAEABgBADQAAAFgEAAIgCAAg");
	this.shape_214.setTransform(598.6,23.6,0.58,0.58,180);

	this.shape_215 = new cjs.Shape();
	this.shape_215.graphics.f("rgba(210,145,14,0.439)").s().p("AAFAXQgMgLgEgMQgFgRAJgGQAFgCADADQAEACAAAFQABAFgCAJQAAAJAIAIQACADgCADQAAABAAAAQgBABAAAAQgBAAAAABQgBAAAAAAQAAAAgBAAQAAAAgBgBQAAAAgBAAQAAgBgBAAg");
	this.shape_215.setTransform(617.6,7.8,0.58,0.58,180);

	this.shape_216 = new cjs.Shape();
	this.shape_216.graphics.f("rgba(210,145,14,0.439)").s().p("AgHAYQgFgDAAgIQgBgHADgGQADgLAMgLQAEgEADAFQACADgCADQgIAIAAAJIABAHIAAAHQgBAFgDADIgDABIgFgBg");
	this.shape_216.setTransform(617.6,94.5,0.58,0.58,180);

	this.shape_217 = new cjs.Shape();
	this.shape_217.graphics.f("rgba(210,145,14,0.439)").s().p("AgSAPQgFgKANgLQAKgKAPgCQAGgBAAAFQABAEgEABQgJACgFAGIgGANQgCAFgFACIgDAAQgEAAgCgEg");
	this.shape_217.setTransform(598.6,78.8,0.58,0.58,180);

	this.shape_218 = new cjs.Shape();
	this.shape_218.graphics.f("rgba(210,145,14,0.439)").s().p("AgTALQgEgCgBgEQAAgJAQgFQAMgEASAGIADACQAAABAAAAQAAAAAAABQAAAAAAABQAAAAAAABQgBADgFgBQgKgCgJAEQgGAGgEACIgFABIgEgBg");
	this.shape_218.setTransform(589.9,55.7,0.58,0.58,180);

	this.shape_219 = new cjs.Shape();
	this.shape_219.graphics.f("rgba(210,145,14,0.439)").s().p("AAQAMQgIgIgJAAQgJACgFgBQgFgBgDgDQgCgDACgFQADgFAIAAQAHgBAFADQAMADALAMQAEAEgFADIgDABIgDgBg");
	this.shape_219.setTransform(594,31.5,0.58,0.58,180);

	this.shape_220 = new cjs.Shape();
	this.shape_220.graphics.f("rgba(210,145,14,0.439)").s().p("AAKASQgDgJgFgFIgNgGQgGgCgBgFQgCgGAGgDQAJgFALANQAKAKADAPQABAGgGAAIgBAAQAAAAgBAAQAAgBgBAAQAAAAgBgBQAAgBAAAAg");
	this.shape_220.setTransform(609.8,12.4,0.58,0.58,180);

	this.shape_221 = new cjs.Shape();
	this.shape_221.graphics.f("rgba(210,145,14,0.439)").s().p("AAAAdQgMgBgIgIQgIgJAAgLIAAAAQAAgLAJgIQAIgJALAAQAMAAAHAIQAJAIAAAMIABAAIgBABQAAAMgJAIQgIAIgKAAIgBAAgAgTgBIAAADQABAQARgBQAQAAABgRQgBgPgQgBIgCAAQgPAAgBAPg");
	this.shape_221.setTransform(594.1,51.2,0.58,0.58,180);

	this.shape_222 = new cjs.Shape();
	this.shape_222.graphics.f("rgba(210,145,14,0.439)").s().p("AgGAcQgLgDgGgJIAAgBIgBgBIAAAAIAAgBQgFgKADgKQADgLAKgGQAKgGAKADQALADAGAJIABABIABACQAFALgEAKQgDALgLAGQgGADgGAAIgHgBgAgJgNQgPAHAHAOQAIARAPgIQAQgJgIgPQgGgJgIAAQgEAAgFADg");
	this.shape_222.setTransform(599.9,72.8,0.58,0.58,180);

	this.shape_223 = new cjs.Shape();
	this.shape_223.graphics.f("rgba(210,145,14,0.439)").s().p("AgIAbQgLgDgGgLQgFgJADgKQADgLAJgGIABAAIABgBIAAAAIABAAQAKgFAKADQALADAGAKQAGAKgDAKQgDALgJAGIgBABIgCABQgGADgHAAIgIgCgAgHgRQgRAIAIAPQAJAQAPgIQAOgJgIgOQgFgKgJAAQgDAAgEACg");
	this.shape_223.setTransform(615.7,13.7,0.58,0.58,180);

	this.shape_224 = new cjs.Shape();
	this.shape_224.graphics.f("rgba(210,145,14,0.439)").s().p("AgOAYQgKgGgDgLQgCgKAFgJIAAgBIABgCQAGgJALgDQALgCAKAFQAKAGADALQADAKgFAKIgBACIgBABQgGAJgMADIgGABQgHAAgHgFgAgRgHQgHAOAPAHQAOAIAJgOQAIgPgQgIQgFgDgEAAQgJAAgFALg");
	this.shape_224.setTransform(599.9,29.5,0.58,0.58,180);

	this.shape_225 = new cjs.Shape();
	this.shape_225.graphics.f("rgba(210,145,14,0.439)").s().p("AgMAZIgBAAIgCgBQgJgGgDgLQgCgLAFgKQAGgKALgDQAKgDAKAFIACABIABABQAJAGADAMQADAKgHAKQgGAKgLADIgHABQgGAAgGgEgAgPgFQgIAPAQAIQAOAHAHgPQAIgOgOgJQgGgDgEAAQgIAAgFALg");
	this.shape_225.setTransform(615.7,88.7,0.58,0.58,180);

	this.shape_226 = new cjs.Shape();
	this.shape_226.graphics.f("rgba(210,145,14,0.439)").s().p("AgFAIIgBgBIAAgBQgGgGAGgGQAGgGAGAHQAHAGgGAGQgDADgEAAQgCAAgDgCg");
	this.shape_226.setTransform(602,91.4,0.58,0.58,180);

	this.shape_227 = new cjs.Shape();
	this.shape_227.graphics.f("rgba(210,145,14,0.439)").s().p("AgIAEIAAgCIAAgBQgBgDACgDQADgDAEgBQAHgBACAJQADAIgJADIgDAAQgFAAgDgGg");
	this.shape_227.setTransform(586.6,68.3,0.58,0.58,180);

	this.shape_228 = new cjs.Shape();
	this.shape_228.graphics.f("rgba(210,145,14,0.439)").s().p("AgBAKQgIgCABgIIAAgBIAAgCQAEgIAHADQAIACgCAIQgCAIgHAAIgBAAg");
	this.shape_228.setTransform(584.9,40.7,0.58,0.58,180);

	this.shape_229 = new cjs.Shape();
	this.shape_229.graphics.f("rgba(210,145,14,0.439)").s().p("AgGAHQgGgFAFgHIABgBIABAAQAGgGAGAGQAGAGgHAGQgDAEgDAAQgCAAgEgDg");
	this.shape_229.setTransform(597.1,15.8,0.58,0.58,180);

	this.shape_230 = new cjs.Shape();
	this.shape_230.graphics.f("rgba(210,145,14,0.439)").s().p("AgCgHQAEACAAAFQACAFgGAEg");
	this.shape_230.setTransform(619.8,0.5,0.58,0.58,180);

	this.shape_231 = new cjs.Shape();
	this.shape_231.graphics.f("rgba(210,145,14,0.439)").s().p("AgCgIQAGAEgCAFQAAAFgEADg");
	this.shape_231.setTransform(619.8,101.9,0.58,0.58,180);

	this.shape_232 = new cjs.Shape();
	this.shape_232.graphics.f("rgba(210,145,14,0.439)").s().p("AgFAHIgBAAIgBgBQgFgGAGgHQAGgFAGAGQAIAHgHAGQgDADgDAAQgCAAgEgDg");
	this.shape_232.setTransform(597.1,86.6,0.58,0.58,180);

	this.shape_233 = new cjs.Shape();
	this.shape_233.graphics.f("rgba(210,145,14,0.439)").s().p("AgIAEIAAgCIAAgBQgBgDADgDQACgDAEgBQAHgBACAJQACAIgIADIgDAAQgFAAgDgGg");
	this.shape_233.setTransform(584.9,61.7,0.58,0.58,180);

	this.shape_234 = new cjs.Shape();
	this.shape_234.graphics.f("rgba(210,145,14,0.439)").s().p("AgBAKQgEgBgCgDQgCgDABgDIAAgBIAAgCQAEgIAHADQAJACgDAIQgCAIgGAAIgCAAg");
	this.shape_234.setTransform(586.6,34,0.58,0.58,180);

	this.shape_235 = new cjs.Shape();
	this.shape_235.graphics.f("rgba(210,145,14,0.439)").s().p("AgGAHQgGgGAGgGIAAgBIABgBQAGgFAHAGQAFAGgGAGQgEAEgDAAQgDAAgDgDg");
	this.shape_235.setTransform(602,11,0.58,0.58,180);

	this.shape_236 = new cjs.Shape();
	this.shape_236.graphics.f("rgba(210,145,14,0.439)").s().p("AgFAHIgBAAIAAgBQgFgGAFgHQAGgFAGAGQAHAHgGAGQgDADgEAAQgCAAgDgDg");
	this.shape_236.setTransform(600.4,90.8,0.58,0.58,180);

	this.shape_237 = new cjs.Shape();
	this.shape_237.graphics.f("rgba(210,145,14,0.439)").s().p("AgIAEIAAgCIAAgBQgBgDACgDQACgDAFgBQAHgBACAJQADAIgJADIgDAAQgFAAgDgGg");
	this.shape_237.setTransform(585.6,67.1,0.58,0.58,180);

	this.shape_238 = new cjs.Shape();
	this.shape_238.graphics.f("rgba(210,145,14,0.439)").s().p("AgBAKQgIgDABgHIAAgCIAAgBQAEgIAHACQAIADgCAIQgCAIgHAAIgBAAg");
	this.shape_238.setTransform(584.6,39,0.58,0.58,180);

	this.shape_239 = new cjs.Shape();
	this.shape_239.graphics.f("rgba(210,145,14,0.439)").s().p("AgGAHQgGgGAGgGIAAgBIABAAQAGgFAHAFQAFAGgGAGQgEAEgDAAQgDAAgDgDg");
	this.shape_239.setTransform(597.7,14.3,0.58,0.58,180);

	this.shape_240 = new cjs.Shape();
	this.shape_240.graphics.f("rgba(210,145,14,0.439)").s().p("AgFAHIgBAAIgBgBQgFgGAGgHQAGgFAHAGQAGAHgGAGQgDADgEAAQgCAAgDgDg");
	this.shape_240.setTransform(597.7,88.1,0.58,0.58,180);

	this.shape_241 = new cjs.Shape();
	this.shape_241.graphics.f("rgba(210,145,14,0.439)").s().p("AgIAEIAAgBIAAgCQgBgIAJgCQAHgBACAJQACAIgIADIgDAAQgFAAgDgGg");
	this.shape_241.setTransform(584.6,63.3,0.58,0.58,180);

	this.shape_242 = new cjs.Shape();
	this.shape_242.graphics.f("rgba(210,145,14,0.439)").s().p("AgBAKQgEgBgCgDQgCgDABgDIAAgBIAAgCQAEgIAHADQAJACgDAIQgCAIgHAAIgBAAg");
	this.shape_242.setTransform(585.6,35.3,0.58,0.58,180);

	this.shape_243 = new cjs.Shape();
	this.shape_243.graphics.f("rgba(210,145,14,0.439)").s().p("AgGAHQgGgGAGgGIAAgBIABgBQAGgFAHAGQAFAGgGAHQgEADgDAAQgDAAgDgDg");
	this.shape_243.setTransform(600.4,11.5,0.58,0.58,180);

	this.shape_244 = new cjs.Shape();
	this.shape_244.graphics.f("rgba(210,145,14,0.439)").s().p("AgFAIIgCgCQgFgHAGgFQAGgHAGAIQAIAFgHAHQgDADgDAAQgCAAgEgCg");
	this.shape_244.setTransform(608.8,83.6,0.58,0.58,180);

	this.shape_245 = new cjs.Shape();
	this.shape_245.graphics.f("rgba(210,145,14,0.439)").s().p("AgHAEIgBgBIAAgCQgBgIAJgCQAHgBACAJQABADgBADQgCAEgEABIgDAAQgFAAgCgGg");
	this.shape_245.setTransform(596.5,65,0.58,0.58,180);

	this.shape_246 = new cjs.Shape();
	this.shape_246.graphics.f("rgba(210,145,14,0.439)").s().p("AgBAKQgJgDABgIIABgCQAEgIAHACQAJADgDAIQgCAIgHAAIgBAAg");
	this.shape_246.setTransform(595,42.7,0.58,0.58,180);

	this.shape_247 = new cjs.Shape();
	this.shape_247.graphics.f("rgba(210,145,14,0.439)").s().p("AgGAHQgFgFAEgHIACgCQAHgFAFAGQAGAGgHAGQgDAEgDAAQgDAAgDgDg");
	this.shape_247.setTransform(604.9,22.7,0.58,0.58,180);

	this.shape_248 = new cjs.Shape();
	this.shape_248.graphics.f("rgba(210,145,14,0.439)").s().p("AgFAIIgCgCQgFgHAGgFQAGgGAGAHQADACABADQAAAEgDADQgDADgDAAQgCAAgEgCg");
	this.shape_248.setTransform(605,79.7,0.58,0.58,180);

	this.shape_249 = new cjs.Shape();
	this.shape_249.graphics.f("rgba(210,145,14,0.439)").s().p("AgDAJQgDgCgBgDIgBgCQgBgJAJgCQAHAAACAIQABADgBADQgCAEgEABIgDAAIgDgBg");
	this.shape_249.setTransform(595,59.7,0.58,0.58,180);

	this.shape_250 = new cjs.Shape();
	this.shape_250.graphics.f("rgba(210,145,14,0.439)").s().p("AgBAKQgIgCABgIIAAgCIABgBQABgDADgCQADgCADABQAIADgCAIQgCAIgHAAIgBAAg");
	this.shape_250.setTransform(596.5,37.4,0.58,0.58,180);

	this.shape_251 = new cjs.Shape();
	this.shape_251.graphics.f("rgba(210,145,14,0.439)").s().p("AgGAHQgGgFAFgHIACgCQAHgFAFAGQAGAGgHAGQgCADgEABIAAAAQgDAAgDgDg");
	this.shape_251.setTransform(608.8,18.8,0.58,0.58,180);

	this.shape_252 = new cjs.Shape();
	this.shape_252.graphics.f("rgba(210,145,14,0.439)").s().p("AgGAHQgGgGAGgGIAAgBIABAAQADgDADABQAEAAADADQAFAFgGAHQgDADgEAAIAAAAQgDAAgDgDg");
	this.shape_252.setTransform(598.8,12.7,0.58,0.58,180);

	this.shape_253 = new cjs.Shape();
	this.shape_253.graphics.f("rgba(210,145,14,0.439)").s().p("AgBAKQgIgCABgIIAAgCIAAgBQAEgIAHACQAJADgDAIQgBAEgDACQgCACgDAAIgBAAg");
	this.shape_253.setTransform(584.7,37.1,0.58,0.58,180);

	this.shape_254 = new cjs.Shape();
	this.shape_254.graphics.f("rgba(210,145,14,0.439)").s().p("AgIAEIAAgBIAAgCQgBgIAJgCQAHgBACAJQADAIgJADIgDAAQgFAAgDgGg");
	this.shape_254.setTransform(584.7,65.3,0.58,0.58,180);

	this.shape_255 = new cjs.Shape();
	this.shape_255.graphics.f("rgba(210,145,14,0.439)").s().p("AgFAHIgBAAIAAgBQgDgDABgDQAAgEADgDQAFgFAHAGQADADAAADQAAAEgDADQgDADgEAAQgCAAgDgDg");
	this.shape_255.setTransform(598.8,89.7,0.58,0.58,180);

	this.shape_256 = new cjs.Shape();
	this.shape_256.graphics.f("rgba(210,145,14,0.439)").s().p("AgCAJQgDgBgCgDQgFgIAIgFQAEgCADACQADABACAEQACADgBADQgBADgEACQgCACgCAAIgCgBg");
	this.shape_256.setTransform(612.1,7.4,0.58,0.58,180);

	this.shape_257 = new cjs.Shape();
	this.shape_257.graphics.f("rgba(210,145,14,0.439)").s().p("AgDAIQgEgCgBgDQgCgDACgEQACgDAEgBQADgBAEACQADACABADQABADgCADQgCAEgDABIgDAAIgDgBg");
	this.shape_257.setTransform(593.6,25.9,0.58,0.58,180);

	this.shape_258 = new cjs.Shape();
	this.shape_258.graphics.f("rgba(210,145,14,0.439)").s().p("AgFAHQgDgDAAgEQAAgDADgDQADgDADABQAEAAACACQADADgBADQABAEgDADQgDACgEAAIAAAAQgDAAgCgCg");
	this.shape_258.setTransform(586.8,51.2,0.58,0.58,180);

	this.shape_259 = new cjs.Shape();
	this.shape_259.graphics.f("rgba(210,145,14,0.439)").s().p("AgIAEQgCgDACgDQABgEAEgCQADgCADACQADABACADQACADgBADQgBAEgDACQgDACgCAAQgEAAgEgGg");
	this.shape_259.setTransform(593.6,76.5,0.58,0.58,180);

	this.shape_260 = new cjs.Shape();
	this.shape_260.graphics.f("rgba(210,145,14,0.439)").s().p("AgEAIQgDgCgBgEQgBgDACgDQACgEADgBQADAAADACQAEABABAEQABACgCAEQgCADgDACIgDABIgEgCg");
	this.shape_260.setTransform(612.1,95,0.58,0.58,180);

	this.shape_261 = new cjs.Shape();
	this.shape_261.graphics.f("rgba(210,145,14,0.439)").s().p("AgEAIQgDgCgBgEQgCgGAIgEIABAAIABAAQAIgBACAIQABAJgJABIgCAAIgEgBg");
	this.shape_261.setTransform(610,95.6,0.58,0.58,180);

	this.shape_262 = new cjs.Shape();
	this.shape_262.graphics.f("rgba(210,145,14,0.439)").s().p("AgBAJQgEgBgCgDQgCgEAAgCQABgEADgCQADgCADABIABAAIABAAQAEABACAEQABADgBACQgBAEgDACIgFABIgBAAg");
	this.shape_262.setTransform(591.5,76,0.58,0.58,180);

	this.shape_263 = new cjs.Shape();
	this.shape_263.graphics.f("rgba(210,145,14,0.439)").s().p("AgGAHQgCgDAAgEQgBgDADgCQAFgHAHAGIABAAIAAABQADADgBADQAAAEgDACQgDADgDAAQgCAAgEgDg");
	this.shape_263.setTransform(585.2,49.7,0.58,0.58,180);

	this.shape_264 = new cjs.Shape();
	this.shape_264.graphics.f("rgba(210,145,14,0.439)").s().p("AgEAIQgEgCAAgEQgBgCACgEQACgDAEgBQAGgCAEAIIAAABIAAABQABAIgIACIgCAAQgCAAgCgCg");
	this.shape_264.setTransform(592.9,23.8,0.58,0.58,180);

	this.shape_265 = new cjs.Shape();
	this.shape_265.graphics.f("rgba(210,145,14,0.439)").s().p("AgCAJQgEgBgBgDQgCgEABgCQABgEADgCQAEgCACAAQADABADADQACADgBADIAAABIgBABQAAAEgEACIgEABIgCgBg");
	this.shape_265.setTransform(612.6,5.3,0.58,0.58,180);

	this.shape_266 = new cjs.Shape();
	this.shape_266.graphics.f("rgba(210,145,14,0.439)").s().p("AgIACQgBgCACgEQACgDAEgBQAHgCADAIIAAABIAAABQABACgCAEQgCADgEABIgCAAQgHAAgBgIg");
	this.shape_266.setTransform(612.6,97.1,0.58,0.58,180);

	this.shape_267 = new cjs.Shape();
	this.shape_267.graphics.f("rgba(210,145,14,0.439)").s().p("AgCAJQgDgBgCgDQgCgEABgCQABgEADgCQADgCADAAQAIACgBAIIAAABIAAABQgDAHgGAAIgCgBg");
	this.shape_267.setTransform(592.9,78.5,0.58,0.58,180);

	this.shape_268 = new cjs.Shape();
	this.shape_268.graphics.f("rgba(210,145,14,0.439)").s().p("AgHAGQgFgGAGgGQAGgGAGAGQAHAFgGAHIAAABIgBAAQgDADgDAAQgEgBgDgDg");
	this.shape_268.setTransform(585.2,52.7,0.58,0.58,180);

	this.shape_269 = new cjs.Shape();
	this.shape_269.graphics.f("rgba(210,145,14,0.439)").s().p("AgFAIQgDgCgBgEQgBgIAJgCQACgBAEACQADACABAEQACAHgIADIgBAAIgBAAIgBAAQgCAAgDgBg");
	this.shape_269.setTransform(591.5,26.4,0.58,0.58,180);

	this.shape_270 = new cjs.Shape();
	this.shape_270.graphics.f("rgba(210,145,14,0.439)").s().p("AAAAJIgBAAIgBAAQgIgEACgHQABgDADgCQAEgCACABQAEABACADQADADgBADQgBAEgDACQgDABgDAAIAAAAg");
	this.shape_270.setTransform(610,6.8,0.58,0.58,180);

	this.shape_271 = new cjs.Shape();
	this.shape_271.graphics.f("rgba(210,145,14,0.439)").s().p("AgTAkQgLgBgHgJQgHgIgBgLQgDgOAHgNQAIgOANgCQAQgCAMAPIAUAZQAIAGAIADQAFABgCAEQgBAFgEgCQgLgCgKgJIgFgFIgMgMQgHgIgLgCQgLgDgHAHQgGAIABAKIAAABQACALAIAFQAGADAEAAIABAAQgGgHACgIQABgFAFgCQAFgBAFABQAGADACAHQABAHgDAGIgBADQgFAKgLAAIgEgBg");
	this.shape_271.setTransform(609.7,92.4,0.58,0.58,180);

	this.shape_272 = new cjs.Shape();
	this.shape_272.graphics.f("rgba(210,145,14,0.439)").s().p("AgnAnQgEgDAEgEQAHgHAKgDIALgEIAPgDQAMgEAHgIQAIgIgDgJQgFgJgJgEQgIgEgIADQgHADgCADIgCADQAKgDAEAHQAEAEgBAFQgBAFgDACQgGAFgHgCQgHgCgDgGIgCgCQgHgLAJgLQAHgJALgCQAIgCALAEQAPAFAIAPQAJAPgIAMQgIAMgRADIgcAEQgMAEgGAGQAAAAAAABQgBAAAAAAQAAAAgBAAQAAABAAAAIgEgCg");
	this.shape_272.setTransform(615.4,7,0.58,0.58,180);

	this.shape_273 = new cjs.Shape();
	this.shape_273.graphics.f("rgba(210,145,14,0.439)").s().p("AgVAjQgMgIgEgNQgEgLACgIQACgLAJgHQAFgEAGAAQAHgBAFADIABACQAGADACAHQACAHgFAGQgCADgFABQgGABgEgEQgFgFACgJIgBABIgDACQgEADgCAGQgCAHADAHQAGAKAIAFQAJADAIgIQAIgIAEgLIAEgQIADgKQADgKAHgHQAEgEADAEQADAEgDACQgFAGgDAIQgEALgDAZQgEAVgSADIgEABQgKAAgJgHg");
	this.shape_273.setTransform(593.2,29.3,0.58,0.58,180);

	this.shape_274 = new cjs.Shape();
	this.shape_274.graphics.f("rgba(210,145,14,0.439)").s().p("AgcAiQgKgGgFgMQgFgMADgLQABgLAHgIQAIgJALgBQAPgBAEALIABACQADAGgBAHQgCAHgGADQgFACgFgCQgFgCgBgFQgCgIAGgGIgCAAQgFAAgFADQgIAGgBAKQAAAKAFAJQAGAHAMgDQALgDAIgHIALgNIAFgEQAKgKALgCQAEgBABAEQACAFgFABQgIACgIAHIgLANQgHAKgEAEQgLALgLAAQgGAAgGgDg");
	this.shape_274.setTransform(609.7,10,0.58,0.58,180);

	this.shape_275 = new cjs.Shape();
	this.shape_275.graphics.f("rgba(210,145,14,0.439)").s().p("AAgAoQgHgHgDgKIgEgLIgDgPQgEgMgIgHQgIgIgJADQgJAFgEAJQgEAIADAIQADAHADACIADACQgDgKAHgEQAEgEAFABQAFABACADQAFAGgCAHQgCAHgGADIgCACQgLAHgLgJQgJgHgCgKQgCgJAEgLQAFgPAPgIQAPgJAMAIQAMAIADARIAEAcQAEAMAGAGQADACgDAEQgBAAAAABQAAAAgBAAQAAAAgBABQAAAAgBAAIgDgCg");
	this.shape_275.setTransform(593.2,73.1,0.58,0.58,180);

	this.shape_276 = new cjs.Shape();
	this.shape_276.graphics.f("rgba(210,145,14,0.439)").s().p("AANAsQgEgKAEgNIABgHIAGgPQACgMgDgLQgEgMgKgBQgJAAgIAFIgBABQgJAGAAALQAAAEACAEIABACQADgJAHgCQAGgBADADQAEADABAGQABAHgFAFQgFAFgHgBIgCAAQgNABgGgNQgIgUATgQQALgKAPgBQAQgBAJALQAKANgHATIgMAeQgBAKABAHQACAEgFABIgDABQAAAAgBAAQAAgBAAAAQgBgBAAAAQAAgBAAgBg");
	this.shape_276.setTransform(588.4,47.7,0.58,0.58,180);

	this.shape_277 = new cjs.Shape();
	this.shape_277.graphics.f("rgba(210,145,14,0.439)").s().p("AgPAvQgFgBACgEQACgLAJgKIAFgFIAMgMQAIgHACgLQADgLgHgHQgIgGgKABIgBAAQgLACgFAIQgDAGAAAEIAAABQAHgGAIACQAFABACAFQABAFgBAFQgDAGgHACQgHABgGgDIgDgBQgLgFACgPQABgLAJgHQAIgHALgBQAOgDANAHQAOAIACANQACAQgPAMQgSANgHAHQgGAIgDAIQAAABAAAAQgBABAAAAQAAABgBAAQAAAAgBAAIgCAAg");
	this.shape_277.setTransform(596.2,23.5,0.58,0.58,180);

	this.shape_278 = new cjs.Shape();
	this.shape_278.graphics.f("rgba(210,145,14,0.439)").s().p("AgHAuQgLgBgIgHQgJgIgBgLQgBgPALgEIACgBQAGgDAHABQAHACADAGQACAFgCAFQgCAFgFABQgIACgGgGIAAACQAAAFADAFQAGAIAKABQAKABAJgGQAHgGgDgMQgDgLgHgIIgNgLIgEgFQgKgKgCgLQgBgEAEgBQAFgCABAFQACAIAHAIIANALIAOALQAQAQgIASQgGAKgMAFQgIADgIAAIgHgBg");
	this.shape_278.setTransform(596.2,78.9,0.58,0.58,180);

	this.shape_279 = new cjs.Shape();
	this.shape_279.graphics.f("rgba(210,145,14,0.439)").s().p("AAAAwQgMgCgJgJQgJgHgDgJQgEgLAFgKQAHgNAMACIACAAQAHgBAEAFQAFAFAAAHQgBAGgEADQgEADgGgBQgHgDgCgIIgCADIgBAJQABAJAIAGIAAABQAJAFAJAAQAJgBAEgMQADgLgCgMIgGgPIgBgHQgDgNADgKQABgFAEACQAFABgBAEIgBAMQACAMALAaQAHAYgRAKQgHAFgKAAIgGAAg");
	this.shape_279.setTransform(588.4,54.7,0.58,0.58,180);

	this.shape_280 = new cjs.Shape();
	this.shape_280.graphics.f("rgba(210,145,14,0.439)").s().p("AgDAcIgBgDQgHgRgBgPQgBgWAOgCQAHAAADAFQADAFgBAGIgEAJIgEAJQgDAJACAKIABADIAAABQABAEgEABIgCABQAAAAAAgBQgBAAAAAAQgBgBAAAAQgBgBAAgBg");
	this.shape_280.setTransform(608.2,94.8,0.58,0.58,180);

	this.shape_281 = new cjs.Shape();
	this.shape_281.graphics.f("rgba(210,145,14,0.439)").s().p("AgQAcQgEgCABgEIABgDQACgUAJgOQAEgHAFgDQAHgEAGAEQAGAEgBAGQAAAGgGAFIgOAKQgGAGgDAJIAAABIgBACIAAABQgBADgDAAIgCAAg");
	this.shape_281.setTransform(590.2,74.4,0.58,0.58,180);

	this.shape_282 = new cjs.Shape();
	this.shape_282.graphics.f("rgba(210,145,14,0.439)").s().p("AgaATQgDgEADgDIACgCQALgOANgJQARgMAJAMQAFAGgDAFQgDAFgGACQgNAAgGACQgJACgIAHIgDACIAAABIgDABIgDgBg");
	this.shape_282.setTransform(585,47.8,0.58,0.58,180);

	this.shape_283 = new cjs.Shape();
	this.shape_283.graphics.f("rgba(210,145,14,0.439)").s().p("AAQANQgMgHgGgBQgJgDgKACIgDABIgBAAQgEABgBgEQgBgEAEgBIADgBQARgHAPgBQAWgBACAOQAAAHgFADQgDACgEAAIgEAAg");
	this.shape_283.setTransform(593.8,22.1,0.58,0.58,180);

	this.shape_284 = new cjs.Shape();
	this.shape_284.graphics.f("rgba(210,145,14,0.439)").s().p("AAQAUQgGAAgFgGIgKgOQgGgGgJgDIgBAAIgCgBIgBAAQgEgBABgFQACgEAEABIADABQAUACAOAJQAHAEADAFQAEAHgEAGQgDAFgGAAIgBAAg");
	this.shape_284.setTransform(614.2,4,0.58,0.58,180);

	this.shape_285 = new cjs.Shape();
	this.shape_285.graphics.f("rgba(210,145,14,0.439)").s().p("AgbARQgBgFAEgBIABAAIACgBIABAAQAJgDAGgGQADgBACgFIAFgIQAFgGAGAAQAGgBAEAGQAEAGgEAHQgDAFgHAEQgOAJgUACIgDABIgCAAQgDAAgBgDg");
	this.shape_285.setTransform(614.2,98.4,0.58,0.58,180);

	this.shape_286 = new cjs.Shape();
	this.shape_286.graphics.f("rgba(210,145,14,0.439)").s().p("AAIANQgPgBgRgHIgDgBQgEgBABgEQABgEAEABIABAAIADABQAKACAJgDQAGgBAMgHQAGgBAFADQAFAEAAAGQgCANgTAAIgDAAg");
	this.shape_286.setTransform(593.8,80.3,0.58,0.58,180);

	this.shape_287 = new cjs.Shape();
	this.shape_287.graphics.f("rgba(210,145,14,0.439)").s().p("AAAAOQgNgJgLgOIgCgCQgDgDADgDQADgEADADIAAABIADACQAHAHALADIAJABIAKABQAGACACAEQACAGgEAFQgEAGgHAAQgHAAgIgGg");
	this.shape_287.setTransform(585,54.6,0.58,0.58,180);

	this.shape_288 = new cjs.Shape();
	this.shape_288.graphics.f("rgba(210,145,14,0.439)").s().p("AACAaQgFgDgEgHQgJgOgCgUIgBgDQgBgEAEgCQAFgBABAEIAAABIABACIAAABQADAJAGAGQABADAFACIAIAFQAGAFAAAGQABAGgGAEQgDACgEAAQgDAAgDgCg");
	this.shape_288.setTransform(590.2,28,0.58,0.58,180);

	this.shape_289 = new cjs.Shape();
	this.shape_289.graphics.f("rgba(210,145,14,0.439)").s().p("AABAgQgOgCABgWQABgPAHgRIABgDQABgEAEABQAEABgBAEIAAABIgBADQgCAKADAJQABAGAHAMQABAGgDAFQgDAFgGAAIgBAAg");
	this.shape_289.setTransform(608.2,7.6,0.58,0.58,180);

	this.shape_290 = new cjs.Shape();
	this.shape_290.graphics.f("rgba(210,145,14,0.439)").s().p("AgEAnQgGgFgCgDQgNgNAFgVQACgOAHgKQAIgNALAAQALABADAKQACAKgJAGIgNAGQgHAEgDAFQgFALAHALQADAEAEACQAFAEgFAEQAAABgBAAQAAABAAAAQAAAAAAAAQgBABAAAAIgDgCg");
	this.shape_290.setTransform(606.3,93.9,0.58,0.58,180);

	this.shape_291 = new cjs.Shape();
	this.shape_291.graphics.f("rgba(210,145,14,0.439)").s().p("AgaAfQgDgFAAgGQgEgRAOgQQAJgKAKgGQAMgHAKAEQAKAFgBAKQgBALgLACIgNgBQgIgBgFACQgOAFAAAQQABAFADAEQAAABAAABQAAAAAAABQAAAAAAABQAAAAAAABQgBAAAAABQAAAAgBAAQAAABAAAAQgBAAAAAAIgEABQAAAAAAAAQgBAAAAAAQgBgBAAAAQAAgBAAgBg");
	this.shape_291.setTransform(588.9,72.6,0.58,0.58,180);

	this.shape_292 = new cjs.Shape();
	this.shape_292.graphics.f("rgba(210,145,14,0.439)").s().p("AgiAWIgDgBQAAAAgBAAQAAgBAAAAQAAgBAAAAQAAgBAAAAIADgMQAGgQAVgHQAggLAMAQQAGAJgGAHQgGAJgKgEQgEgBgJgHQgHgEgFAAQgOAAgHALQgCAGABAEQAAAEgEAAIgDAAg");
	this.shape_292.setTransform(584.7,45.7,0.58,0.58,180);

	this.shape_293 = new cjs.Shape();
	this.shape_293.graphics.f("rgba(210,145,14,0.439)").s().p("AAOAPIgGgNQgEgHgFgDQgLgFgLAHQgEADgCAEQgEAFgEgFQgEgBADgEQAFgGADgCQANgNAVAFQAOACAKAHQANAIAAALQgBALgKADIgEAAQgIAAgEgHg");
	this.shape_293.setTransform(594.7,20.1,0.58,0.58,180);

	this.shape_294 = new cjs.Shape();
	this.shape_294.graphics.f("rgba(210,145,14,0.439)").s().p("AASAfQgLgBgCgLIABgNQABgIgCgFQgFgOgQAAQgFABgEADQgBAAgBAAQAAAAgBAAQAAAAgBAAQAAAAgBAAQAAgBgBAAQAAAAAAgBQgBAAAAAAQAAgBAAAAQgDgFAFgBQAFgDAGAAQARgEAQAOQAKAJAGAKQAHAMgEAKQgFAJgJAAIgBAAg");
	this.shape_294.setTransform(616,2.8,0.58,0.58,180);

	this.shape_295 = new cjs.Shape();
	this.shape_295.graphics.f("rgba(210,145,14,0.439)").s().p("AgUAeIgKgDQgFgBADgFQAAAAAAgBQAAAAABAAQAAgBAAAAQABAAAAgBQABAAAAAAQABAAAAAAQABAAAAAAQABAAABAAQAEADAFABQAQAAAGgQQABgEgBgHQgBgKAAgDQACgLALgBQAKgBAFAKQAEAKgHANQgGAKgKAIQgNALgNAAIgIgBg");
	this.shape_295.setTransform(616,99.6,0.58,0.58,180);

	this.shape_296 = new cjs.Shape();
	this.shape_296.graphics.f("rgba(210,145,14,0.439)").s().p("AgeANQgFgDgDgFQgDgDAEgCQAAAAABgBQAAAAAAAAQABgBAAAAQABAAABAAQAAAAABAAQAAAAABABQAAAAABAAQAAABABAAQACAEAEADQALAHAMgGQAFgDAEgHIAGgNQAGgJAKADQAJADABALQAAALgOAJQgLAGgOADIgIABQgOAAgKgKg");
	this.shape_296.setTransform(594.7,82.3,0.58,0.58,180);

	this.shape_297 = new cjs.Shape();
	this.shape_297.graphics.f("rgba(210,145,14,0.439)").s().p("AgKASQgTgHgGgRIgDgLQAAAAAAgBQAAAAAAgBQAAAAABAAQAAgBAAAAIADgBQAHgCAAAGQgBAEACAGQAHAMAPgBQAFAAAIgFQAIgGAEgBQAKgEAGAJQAFAHgGAJQgHAJgPAAIgCAAQgLAAgLgEg");
	this.shape_297.setTransform(584.7,56.6,0.58,0.58,180);

	this.shape_298 = new cjs.Shape();
	this.shape_298.graphics.f("rgba(210,145,14,0.439)").s().p("AAAAeQgKgGgJgKQgOgQAEgRQAAgGADgFQABgFAFADQAAAAABAAQAAAAAAABQABAAAAAAQAAABABAAQAAABAAAAQAAABAAAAQAAABAAAAQAAABAAABQgDAEgBAFQAAAQAQAGQAEABAHgBQAKgBADAAQALACABALQABAKgKAFQgEABgEAAQgHAAgHgEg");
	this.shape_298.setTransform(588.9,29.8,0.58,0.58,180);

	this.shape_299 = new cjs.Shape();
	this.shape_299.graphics.f("rgba(210,145,14,0.439)").s().p("AgMAbQgGgLgDgOQgDgTAMgNQADgFAFgDQAEgDABAEQAAAAABABQAAAAAAAAQABABAAAAQAAABAAABQAAAAAAABQAAAAgBABQAAAAAAABQgBAAAAABQgEACgDAEQgHALAGAMQADAFAHAEIANAGQAJAGgDAKQgDAJgLABQgLAAgJgOg");
	this.shape_299.setTransform(606.3,8.5,0.58,0.58,180);

	this.shape_300 = new cjs.Shape();
	this.shape_300.graphics.f("rgba(210,145,14,0.439)").s().p("Ag1BCQgFgBABgEQAOgkAegYIAhgSQAQgJADgLQADgJgHgGQgGgFgGAFIACADQACAGgFAEQgFAEgGgDQgGgCAAgHQgBgGAEgEQAKgNATAHQATAGACAPQACATgSAMQgFADgdAKQgTAJgOAQQgPAQgGAUQgCADgDAAIgCAAg");
	this.shape_300.setTransform(606.8,90.3,0.58,0.58,180);

	this.shape_301 = new cjs.Shape();
	this.shape_301.graphics.f("rgba(210,145,14,0.439)").s().p("AhKAmQgFgEAEgDQAggaApgEIAXABQAPABAIgCQASgEAAgMQAAgFgEgDQgDgEgEABIAAACQAAAGgGACQgGACgEgEQgFgDACgHQABgGAEgDQAMgHARANQAPAMgEAPQgEASgVADQgMACgYgEQgvgJglAgQAAABAAAAQgBABAAAAQgBAAAAAAQAAAAgBAAQAAAAgBAAQAAAAAAAAQgBgBAAAAQgBgBAAAAg");
	this.shape_301.setTransform(591.6,70.2,0.58,0.58,180);

	this.shape_302 = new cjs.Shape();
	this.shape_302.graphics.f("rgba(210,145,14,0.439)").s().p("AAmAZQgJgEgTgQQgkgegvAKQgFABAAgFQgDgFAFgBQApgHAjARQAHADALAHIASAKQAOAIALgGQAGgDABgFQABgIgHgDIgBADQgEAFgGgCQgHgCgBgGQAAgHAFgEQAGgEAGACQAPAEAEATQAEASgNAJQgHAGgJAAQgHAAgJgEg");
	this.shape_302.setTransform(588.5,43.9,0.58,0.58,180);

	this.shape_303 = new cjs.Shape();
	this.shape_303.graphics.f("rgba(210,145,14,0.439)").s().p("AAMAqQgDgFgKgdQgIgTgRgOQgQgPgUgGQgEgCABgFQABgFAEABQAkAOAYAeIASAhQAJAQALADQAJADAGgHQAFgGgFgGIgDACQgGACgEgFQgEgFADgGQACgGAHAAQAGgBAEAEQANAKgHATQgGATgPACIgEAAQgQAAgLgQg");
	this.shape_303.setTransform(598.3,20.6,0.58,0.58,180);

	this.shape_304 = new cjs.Shape();
	this.shape_304.graphics.f("rgba(210,145,14,0.439)").s().p("AgBBKQgSgEgDgVQgCgNAEgXQAHglgUggIAAgSQAXAgADAkIAAAXQgBAPACAJQABAHAFAFQAEAFAGAAQAEAAAEgDQADgEAAgDIgCAAQgHAAgCgGQgCgGAEgFQAEgEAGABQAGABADAFQAIALgOARQgKANgLAAIgFgBg");
	this.shape_304.setTransform(618,5.2,0.58,0.58,180);

	this.shape_305 = new cjs.Shape();
	this.shape_305.graphics.f("rgba(210,145,14,0.439)").s().p("AghA5QAJgPADgQQAEgRgGgiQgDgXAIgMQAJgPAOACQAOABAKARQALASgOAJQgFADgGgDQgFgCgBgGQAAgFAEgDQADgEAEABQABgEgEgDQgFgEgEABQgMACgDARQgBAIAAAPIABAWQgDAkgXAgg");
	this.shape_305.setTransform(618,97.2,0.58,0.58,180);

	this.shape_306 = new cjs.Shape();
	this.shape_306.graphics.f("rgba(210,145,14,0.439)").s().p("AhBA2QgBgFAEgBQAugPASg0QAIgXAMgJQAPgLAOAIQAMAGADAUQACAVgPAEQgGABgFgDQgFgEABgGQABgGAFgCQAFgCAEADQAFgGgFgGQgGgHgLAEQgKAEgIAOIgSAiQgYAegkANIgBAAQgDAAgBgEg");
	this.shape_306.setTransform(598.3,81.8,0.58,0.58,180);

	this.shape_307 = new cjs.Shape();
	this.shape_307.graphics.f("rgba(210,145,14,0.439)").s().p("AA1AcQgGgBgDgGQgDgGAFgFQAEgEAFABQAGAAABAFQAHgCgBgIQgBgGgGgCQgLgGgOAIIgSAJQgLAIgHADQgjAQgpgHQgFgBADgFQAAgFAFABQAaAGAagIQARgGAXgTQAOgMAPgDQASgEAKAOQAIAMgHASQgHAQgMAAIgFgBg");
	this.shape_307.setTransform(588.5,58.4,0.58,0.58,180);

	this.shape_308 = new cjs.Shape();
	this.shape_308.graphics.f("rgba(210,145,14,0.439)").s().p("AAhAhQgDgGACgFQADgGAGAAQAFgBADAEQADAEgBAEQAEAAAEgEQADgEgBgFQgCgMgQgCQgIgCgPABIgWAAQgpgEgggaQgEgDAFgDQADgFADAEQAVASAZAGQASADAhgGQAYgDAMAIQAOAKgBAOQgCAOgRAKQgIAFgGAAQgHAAgFgIg");
	this.shape_308.setTransform(591.6,32.2,0.58,0.58,180);

	this.shape_309 = new cjs.Shape();
	this.shape_309.graphics.f("rgba(210,145,14,0.439)").s().p("AAEA1QgBgGAEgFQADgFAGABQAGABACAFQACAFgDAEQAGAFAGgFQAHgGgEgLQgEgKgOgIIgigSQgegYgNgkQgCgEAGgBQAFgBABAEQAPAuA0ASQAXAIAJAMQALAPgIAOQgGAMgUADIgFAAQgQAAgEgNg");
	this.shape_309.setTransform(606.8,12.1,0.58,0.58,180);

	this.shape_310 = new cjs.Shape();
	this.shape_310.graphics.f("rgba(210,145,14,0.439)").s().p("Ag0BoQgfgMgRgbQgRgbADggQAEgiAUgfQAWggAfgOQAegMAgAKQAfAKAVAaQAlAuABBaIAAAFIAAACIgBABIAAAAIgBAAIgHAEIgCABIgDACIgCABQg6AfgvAAQgYAAgWgIgAhmAYQACAfAZAXQAYAUAgAEQAfADAfgKQgNAAgMgFIgJgFQgPgIgIgPQgJgPACgRIACgKQgWAPgYgJQgNgEgJgLQgJgKgDgMQgEAQABATgAAggVQgSAHgMASQgLAUACAUQADAWASAMIACABQAkARA2gdQgCg7gdgWIgFgEQgKgGgLAAQgIAAgJADgAg7g/QgSAFgJAQQgIAPAGASQAFAQAQAIQAPAIAQgFQARgFAIgNQAJgPgFgSQgEgRgPgJQgJgFgLAAQgHAAgGABgABKgaQAKAFAIAKIAHAKQgJgrgZgaQgXgYgegEQgfgFgbARQgJAGgJAJQAQgFAPAFQAQAEAKANQALAQgBATQAJgIAKgEQALgFAKAAQAQAAAPAKg");
	this.shape_310.setTransform(602.6,31.4,0.58,0.58,180);

	this.shape_311 = new cjs.Shape();
	this.shape_311.graphics.f("rgba(210,145,14,0.439)").s().p("AAcBzQg/gBgogSIAAgKQASAJATAEQgIgFgJgKQgFgFgCgFQgJgOAAgRQAAgRAJgOQADgEAFgFQgLABgKgEIAAgJQAVAJASgLQAPgIAEgRQAFgSgIgPQgIgOgRgFQgQgFgOAGIAAgOQAggQAhADQAeACAYAYQAWAWAHAgQANA5gqBOIAAAAIgCAEIgBABIgEAHIAAABIAAAAIgBAAIgCABgAgfACQgTAMgHASQgHATAKASIAEAGQAXAdA6ACQAdg2gRgkIgBgCQgMgSgWgDIgHAAQgQAAgQAJgAAGgVQAcADAQAbQAGAKACAKIABAOQAOgsgLglQgJgegYgSQgZgTgfADQgOACgGACQAQADALAMQALAMACAPQADAUgLARQAKgCAGAAIAFAAg");
	this.shape_311.setTransform(615.5,16.4,0.58,0.58,180);

	this.shape_312 = new cjs.Shape();
	this.shape_312.graphics.f("rgba(210,145,14,0.439)").s().p("Ag/BoQgegOgPgeQgPghACgkQADgnAVgcQATgaAigIQAggGAeAMQAhAMAbAeQAWAXAWAmIAAABIAAABIAAABQgZArgZAZQghAhgnAIQgLACgLAAQgVAAgUgJgAhYBFQASAaAgAGQAfAFAcgNQAbgLAYgZQgKAFgMACIgMABQgRABgOgIQgOgIgIgPIgEgMQgMAXgZAFQgNADgNgFQgNgEgJgKQAFAUALAOgAADglQgLAQAAAVQAAAXALAQQANARAWAAIADAAQAngDAgg1Qggg0gngDIgEAAQgVAAgNASgAhCgqQgSAAgMANQgNANAAAQQABASAMANQANAMASAAQARAAANgNQAMgNAAgRQAAgRgNgMQgMgNgRAAIgBAAgAg5gyQAaAFAMAXQADgKAHgLQAQgWAfABQAKAAANAEIALAFQgeghgkgKQgegIgcALQgdALgPAcQgEAHgEAPIAAAAQAQgRAVAAIAKABg");
	this.shape_312.setTransform(596.6,51.2,0.58,0.58,180);

	this.shape_313 = new cjs.Shape();
	this.shape_313.graphics.f("rgba(210,145,14,0.439)").s().p("AgzBiQgegUgSghQgSgjADgiQACgfAYgYQAWgWAggHQA5gNBOAqIAAAAIAEACIABABIAHAEIABAAIAAAAIAAABIABACIAAAGQgBBmgxAuQgZAWggAEIgLABQgaAAgWgOgAgiBeQAdAOAggMQAegLATgcQAQgXAHgfQgFAIgKAJQgFAFgFACQgOAJgRAAQgRAAgOgJQgEgDgFgFQABAagSARQgWASgcgIQAPAOAPAIgAhFgOQgPAJgFAQQgFARAIAPQAJAQASAFQATAFAPgKQAPgJAEgRQAEgRgJgPQgIgOgRgEQgHgCgGAAQgKAAgKAFgAAQhOIgCABQgSAMgDAWQgCAUALAUQAMASASAHQATAHASgKIAGgEQAdgWACg7QgigSgbAAQgQAAgNAGgAgmhhQgeAJgSAYQgTAZADAgQACANACAGQADgPAMgLQAMgLAPgCQAUgDARALQgDgOABgIQADgcAbgQQAKgGAKgCIAOgBQgZgIgWAAQgRAAgRAFg");
	this.shape_313.setTransform(602.6,71,0.58,0.58,180);

	this.shape_314 = new cjs.Shape();
	this.shape_314.graphics.f("rgba(210,145,14,0.439)").s().p("AgdBzQgZgDgVgKIAAgOQAPAHAQgGQAQgGAIgPQAIgPgFgQQgFgRgOgIQgTgLgUAJIAAgKQAKgCAKAAQgIgJgEgKQgMgaARgaQAFgKAKgIIAKgHQgWAFgQAIIAAgKQAmgSBCgBIAFAAIACAAIABABIAAAAIAAABIAEAHIABACIADAFQAwBZgZA+QgMAfgbARQgXAOgaAAIgJAAgAgJBCQgEANgLAJQgKAJgNADQARAEATgBQAegCAXgZQAUgYAEggQADgdgKghQAAANgFAMIgFAJQgIAPgPAIQgPAJgQgCIgKgCQAPAWgJAYgAgyhKIgEAFQgKASAHAUQAHASATAMQAUALATgCQAWgDAMgSIABgCQARgkgdg2Qg6ACgXAdg");
	this.shape_314.setTransform(615.5,85.9,0.58,0.58,180);

	this.shape_315 = new cjs.Shape();
	this.shape_315.graphics.f("rgba(210,145,14,0.439)").s().p("AgpKaQgegMgZgeIgIgLQgBAAAAAAQAAAAAAgBQAAAAgBAAQAAAAAAgBIAAgBIgBAAQgIgNgJgTIgPgiIgCgFQgQgggVgSQgZgVgqACQgaACgYAKIAAgQQARgGASgDQAcgDAYAGQAXAGAVAXQARARALAXIABACIABACIAVAwIACADIAAABQAQAgAcAXQAjAbAigCQAWgBASgOQARgOAHgWQAJgggSgiQgQgdgegVIgBgBIgVgMIgggOQgUgIgLgHIgBgBQgZgOgQgTQghglANhAQAHgeASgaQASgaAagRQATgNAUgFQAXgGAeAKQAJADANAHQAJADAIAGQALAGARANIAbAUIASALIABAAIAAAAQAaAMAaACQAdADAXgKQAcgMANgfQAGgPgDgSQgDgRgJgNQgOgWgcgOQgYgMgegDIgCAAIgZAAQgsAHgWAAIgBAAQgfAAgZgJQgqgPgUg3QgLgdABgfQACghANgcQALgYAPgPQAPgQAegHQATgFAUAAIAEAAQAWAAAqAGQAMACAPgBIABAAQA9gGAeglQAVgYgDghQgCgQgKgOQgJgOgOgIQgXgOgeABQgbABgbALIgGADIAAAAIgBAAIgRAKIgsAgQgHAGgJAFQgYAOgZAFQg0AMgvgtQgrgpgHg4QgEgdAHgYQAFgWAXgWQANgMASgLQAUgLArgSIAKgEQAggTAUgcQAXghgDghQgDgWgPgRQgPgSgVgFQgigJgkAYQgaAQgRAZIgBABQgGAJgGANQgRArgMAUQgPAagTAQQglAhg/gOQgMgCgJgEIAAgQQAVAJAVACQAqAFAZgRQARgMAOgUQAKgPANgeQAOgfAIgOIAEgHIABgBIAEgFQAYgeAdgPQAigRAfAIQAoALAPAlQANAggPAjQgMAfgdAZIgPAMIAAAAQgNAJgTAJIgiAOIgBABQghAQgTAVQgaAdAGAuQAFAoAYAjQAWAeAgAQQAfAPAigKQATgGASgMIAjgZQAWgPAPgIIAEgCIASgHQAbgIAaABQAdABAWAOQAZAQAJAcQAKAbgPAgQgLAYgaAQQgXAPgdAGIgSACIgCABIgCAAQgOABgsgFQgugFgdAIQggAJgRAeQgWAhgBAqQgBAmAQAhQAQAmAhAMQATAHATAAQAQABAkgEQAhgEASABIACAAIACABIANABQAdAFAXAOQAaAPANAXQAPAYgFAgQgEAcgbAVQgoAeg/gRQgNgEgMgFIgDgCQgOgHgQgLIgbgVQgZgRgTgHQgkgOgfANQgfAOgXAbQgfAmgEAuQgEAqARAYQAMAQAVAPQAOAJAgAOQAgANAOAKIALAIQAdAYAOAeQARAigJAgQgMAngmAOQgOAGgOAAQgSAAgUgJg");
	this.shape_315.setTransform(602.7,51.2,0.58,0.58,180);

	this.shape_316 = new cjs.Shape();
	this.shape_316.graphics.f("rgba(210,145,14,0.439)").s().p("AgBACQgBgCACgDIABgCIACgBIAAANQgDgBgBgEg");
	this.shape_316.setTransform(0.2,54.9,0.58,0.58,180);

	this.shape_317 = new cjs.Shape();
	this.shape_317.graphics.f("rgba(210,145,14,0.439)").s().p("AABAGIgBgCQgCgDABgDQABgDADgBIAAANg");
	this.shape_317.setTransform(0.2,47.4,0.58,0.58,180);

	this.shape_318 = new cjs.Shape();
	this.shape_318.graphics.f("rgba(210,145,14,0.439)").s().p("AgCAZQgLgDgFgJQgFgJADgJQADgKAIgGIACgBQAQgIANAMIAAAmIgFADQgGADgHAAIgGgBgAgDgOQgOAJAIAOQAJAPANgJQAOgIgIgOQgGgJgIAAQgEAAgEACg");
	this.shape_318.setTransform(1.3,83.1,0.58,0.58,180);

	this.shape_319 = new cjs.Shape();
	this.shape_319.graphics.f("rgba(210,145,14,0.439)").s().p("AgNAWQgJgFgCgKQgDgJAFgJIABgCQAGgJAKgCQAJgDAJAFQAJAGADAKQADAKgGAJQgFAJgLADIgGABQgHAAgGgEgAgOgIQgIAOAPAIQAOAJAIgPQAIgNgPgIQgFgDgEAAQgIAAgFAIg");
	this.shape_319.setTransform(14.5,69.6,0.58,0.58,180);

	this.shape_320 = new cjs.Shape();
	this.shape_320.graphics.f("rgba(210,145,14,0.439)").s().p("AAAAaQgKAAgHgIQgHgHgBgKIAAgBIAAAAQABgLAHgHQAIgHAKAAQAKAAAIAIQAHAHAAAKQAAALgIAIQgHAHgKAAIgBAAgAAAgPQgPAAgBAPQABAQAPABQARAAAAgRQAAgPgQAAIgBAAg");
	this.shape_320.setTransform(19.5,51.2,0.58,0.58,180);

	this.shape_321 = new cjs.Shape();
	this.shape_321.graphics.f("rgba(210,145,14,0.439)").s().p("AgGAZQgKgCgFgJIgBgCQgFgJADgJQADgLAJgFQAJgFAKADQAKADAFAJQAGAKgDAKQgDAKgKAFQgFADgGAAIgHgBgAgHgNQgPAIAIAOQAJAOANgJQAPgIgIgNQgFgKgIAAQgEAAgFAEg");
	this.shape_321.setTransform(14.5,32.8,0.58,0.58,180);

	this.shape_322 = new cjs.Shape();
	this.shape_322.graphics.f("rgba(210,145,14,0.439)").s().p("AgHAXIgCgBQgIgGgDgKQgDgJAGgJQAFgJALgDQAJgDAJAGIAFADIAAAmQgGAFgIABIgDAAQgGAAgGgDgAgJgHQgIANAOAJQANAIAJgPQAIgOgOgIQgFgDgFAAQgGAAgGAKg");
	this.shape_322.setTransform(1.3,19.3,0.58,0.58,180);

	this.shape_323 = new cjs.Shape();
	this.shape_323.graphics.f("rgba(210,145,14,0.439)").s().p("AASAUQgLgEgHABQgKABgEAAQgGgBgCgEQgDgEACgFQADgKANgJQANgLAHALQADAEgCAFQgCAEgFABQgHABgBgHQgFACgBAEQgBACADADQACADADAAIAAABQAMAAALAFQAEABgDAEQAAABgBABQAAAAAAAAQgBABAAAAQgBAAAAAAIgDgBg");
	this.shape_323.setTransform(5.6,20,0.58,0.58,180);

	this.shape_324 = new cjs.Shape();
	this.shape_324.graphics.f("rgba(210,145,14,0.439)").s().p("AAJAVQgGgIgHgEIgNgGQgEgDgBgEQAAgFADgEQAIgHAOgCQARgDADAMQABAEgDAEQgDAEgFgBQgEgBgCgDQgCgDABgDQgFgBgDAEQgCACABAEQABADACABIAAAAQAJAGAIAKQACAEgDACIgDABQgBAAAAAAQgBgBAAAAQgBAAAAgBQAAAAgBgBg");
	this.shape_324.setTransform(18.4,35.3,0.58,0.58,180);

	this.shape_325 = new cjs.Shape();
	this.shape_325.graphics.f("rgba(210,145,14,0.439)").s().p("AgGAVQgCgLgFgIIgIgKQgCgFACgEQADgFAFgBQAKgDANAGQAQAHgEALQgBADgFACQgFACgDgEQgDgBAAgEQAAgDADgCQgEgEgEABQgDABgBAEQgCADACADIAAABQAEAIACANQAAAEgDAAQgFAAAAgEg");
	this.shape_325.setTransform(21.2,55.2,0.58,0.58,180);

	this.shape_326 = new cjs.Shape();
	this.shape_326.graphics.f("rgba(210,145,14,0.439)").s().p("AgSAYQgEgCACgEQAEgLAAgHQgCgKABgEQABgGAEgCQAEgDAFACQAJADAKANQAKANgKAHQgEADgFgCQgFgCAAgFQgBgHAHgBQgCgFgEgBQgDgBgDADQgDACAAADIAAAAQAAAMgFALQAAAAgBABQAAAAAAABQgBAAAAAAQAAAAgBAAIgDgBg");
	this.shape_326.setTransform(13.8,74.2,0.58,0.58,180);

	this.shape_327 = new cjs.Shape();
	this.shape_327.graphics.f("rgba(210,145,14,0.439)").s().p("AgMAXQgEgDgBgGIACgOQgBgKgDgHQgCgEAEgCQAEgDABAEQAFALAAALIABABQAAADADADQADACACgBQAFgBABgGQgDAAgCgCQgBgDAAgDQABgFAFgBQAEgCAEADQALAHgLAOQgKAMgJADIgEABQgDAAgCgCg");
	this.shape_327.setTransform(13.8,28.2,0.58,0.58,180);

	this.shape_328 = new cjs.Shape();
	this.shape_328.graphics.f("rgba(210,145,14,0.439)").s().p("AgOAYQgFgBgCgFQgCgFADgEIAIgLQAFgJABgJQAAgEAFAAQADAAAAAEQgCANgEAJIAAAAQgCADACAEQABADADABQAEABAEgDQgDgDAAgDQAAgEADgBQAEgDAEABQAFACABADQAEALgOAGQgOAFgHAAIgFgBg");
	this.shape_328.setTransform(21.2,47.2,0.58,0.58,180);

	this.shape_329 = new cjs.Shape();
	this.shape_329.graphics.f("rgba(210,145,14,0.439)").s().p("AAGAXQgTgCgGgIQgEgDABgFQABgFAEgCIAOgGQAGgEAGgIQADgEAEACQADADgCADQgIAKgJAGIAAAAQgCABgBADQgBAEACACQADAEAFgBQgEgJAMgBQALgBgCAMQgBAGgGACQgDACgEAAIgDgBg");
	this.shape_329.setTransform(18.4,67,0.58,0.58,180);

	this.shape_330 = new cjs.Shape();
	this.shape_330.graphics.f("rgba(210,145,14,0.439)").s().p("AgHAQQgNgKgDgJQgCgFADgEQADgEAGgBQAEAAAJACQALgBAHgDQAEgCACAEQADAEgEABQgLAFgMAAIAAABQgDAAgCADQgDADABACQABAFAGABQAAgDACgCQADgBADAAQAFABABAFQACAEgDAEQgEAFgFAAQgEAAgHgFg");
	this.shape_330.setTransform(5.6,82.4,0.58,0.58,180);

	this.shape_331 = new cjs.Shape();
	this.shape_331.graphics.f("rgba(210,145,14,0.439)").s().p("AgYANQgFgCAAgGQABgJASgGQAKgEAMABQAIAAAFADIABAAIABAAQAFADgEAEQAAAAgBABQAAAAgBAAQgBABAAAAQgBAAAAAAIgOgDQgIABgEADIgGAGIgGAGQgDACgDAAIgEgBg");
	this.shape_331.setTransform(5.9,17.8,0.58,0.58,180);

	this.shape_332 = new cjs.Shape();
	this.shape_332.graphics.f("rgba(210,145,14,0.439)").s().p("AAWAMQgGgFgFgEQgFgCgGAAQgDAAgGACQgGADgDAAQgGAAgDgEQgEgEAEgFQAGgJAUAFQAIACAKAGQAHAEAEAFIAAAAIABABQAAABABAAQAAABAAAAQAAABAAAAQAAABgBAAQAAABAAAAQAAAAAAAAQgBABAAAAQgBAAAAAAIgCABQgBAAAAAAQAAAAgBgBQAAAAAAAAQgBAAAAgBg");
	this.shape_332.setTransform(19.5,34.1,0.58,0.58,180);

	this.shape_333 = new cjs.Shape();
	this.shape_333.graphics.f("rgba(210,145,14,0.439)").s().p("AATAYQgEAAAAgEQgCgHgDgGQgDgEgEgDQgDgBgGgBIgJgCQgGgDgBgGQgBgGAGgDQAKgEAPAQQAHAHAEAHQADAGABAHIAAABIABABQAAAFgEAAIgBAAg");
	this.shape_333.setTransform(23.2,54.8,0.58,0.58,180);

	this.shape_334 = new cjs.Shape();
	this.shape_334.graphics.f("rgba(210,145,14,0.439)").s().p("AAEAcQAAAAgBgBQAAAAgBgBQAAgBAAAAQAAgBAAAAIADgOQgBgIgDgEQgBgCgFgEIgGgGQgDgFACgFQACgFAGAAQAJABAGASQAEAKgBAMQAAAIgDAFIAAABIAAABQgBABAAAAQAAABgBAAQAAABgBAAQAAAAgBAAIgDgCg");
	this.shape_334.setTransform(16,74.5,0.58,0.58,180);

	this.shape_335 = new cjs.Shape();
	this.shape_335.graphics.f("rgba(210,145,14,0.439)").s().p("AAAAHQAAAAgBAAQAAgBAAAAQgBAAAAgBQAAAAAAAAQgCgDADgCIAFgGIAAAMIAAAAIgBABIgDAAIAAAAg");
	this.shape_335.setTransform(0.3,86.8,0.58,0.58,180);

	this.shape_336 = new cjs.Shape();
	this.shape_336.graphics.f("rgba(210,145,14,0.439)").s().p("AgBAAQgDgBACgDQAAAAAAAAQAAgBABAAQAAAAAAgBQABAAAAAAQAAAAAAAAQAAAAABAAQAAAAABAAQAAAAABAAIABABIAAAAIAAAMg");
	this.shape_336.setTransform(0.3,15.6,0.58,0.58,180);

	this.shape_337 = new cjs.Shape();
	this.shape_337.graphics.f("rgba(210,145,14,0.439)").s().p("AgMAZQgCgFADgFIAGgGIAGgGQADgEABgIIgDgOQAAgBAAAAQAAgBAAgBQAAAAABAAQAAgBABAAQAEgEADAFIAAABIAAABQADAFAAAIQABALgEALQgGASgJABIgBAAQgFAAgCgFg");
	this.shape_337.setTransform(16,27.9,0.58,0.58,180);

	this.shape_338 = new cjs.Shape();
	this.shape_338.graphics.f("rgba(210,145,14,0.439)").s().p("AgRAXQgGgDABgGQABgGAGgDIAJgCQAGAAADgDQAEgCADgEQADgGACgHQAAgEAEAAQAFgBAAAGIgBABIAAABQgBAHgDAGQgEAHgHAHQgMANgIAAIgFgBg");
	this.shape_338.setTransform(23.2,47.6,0.58,0.58,180);

	this.shape_339 = new cjs.Shape();
	this.shape_339.graphics.f("rgba(210,145,14,0.439)").s().p("AgbAIQgEgFAEgEQADgEAGAAQADAAAGADQAGACADAAQAFAAAGgCQAGgEAFgFQACgDADACQAAAAABAAQAAAAABABQAAAAAAAAQAAABAAAAQABABAAAAQAAABAAAAQAAAAAAABQgBAAAAABIgBABIAAAAQgEAFgHAEQgKAGgIACQgHACgGAAQgJAAgEgGg");
	this.shape_339.setTransform(19.5,68.3,0.58,0.58,180);

	this.shape_340 = new cjs.Shape();
	this.shape_340.graphics.f("rgba(210,145,14,0.439)").s().p("AgKALQgSgGgBgJQAAgGAFgCQAFgCAFADIAGAGQAEAFACABQAEADAIABIAOgDQABAAAAAAQABAAABAAQAAAAAAABQABAAAAABQAEAEgFADIgBAAIgBAAQgFADgIAAIgGAAQgHAAgJgDg");
	this.shape_340.setTransform(5.9,84.5,0.58,0.58,180);

	this.shape_341 = new cjs.Shape();
	this.shape_341.graphics.f("rgba(210,145,14,0.439)").s().p("AgGAHQgDgCAAgEQgBgDACgDQACgDAEgBQADgBADACQAFAEABAIIAAACIgCACQgFACgDAAQgDAAgDgDg");
	this.shape_341.setTransform(1.6,62.2,0.58,0.58,180);

	this.shape_342 = new cjs.Shape();
	this.shape_342.graphics.f("rgba(210,145,14,0.439)").s().p("AgBAKQgDAAgDgDQgDgDAAgEQAAgDADgDQADgDAEAAQAEAAAGAHIABACIgBADQgFAHgFAAIgBAAg");
	this.shape_342.setTransform(4.6,51.2,0.58,0.58,180);

	this.shape_343 = new cjs.Shape();
	this.shape_343.graphics.f("rgba(210,145,14,0.439)").s().p("AgCAKQgDgBgDgDQgEgGAGgHQAGgEAIAEIACACIABACQgBAJgGADIgEABIgCAAg");
	this.shape_343.setTransform(1.6,40.2,0.58,0.58,180);

	this.shape_344 = new cjs.Shape();
	this.shape_344.graphics.f("rgba(210,145,14,0.439)").s().p("AgVAYQgLgKABgOQAAgLAKgKQAKgLAMAAQAOAAAKAMQAIAJAAALQgBAMgIAKIgCACQgKAJgMAAQgMgBgJgIgAgKgKQgNANANAOQAMANAOgOQAOgOgNgMQgHgHgHAAQgGAAgHAHg");
	this.shape_344.setTransform(3.2,71.8,0.58,0.58,180);

	this.shape_345 = new cjs.Shape();
	this.shape_345.graphics.f("rgba(210,145,14,0.439)").s().p("AgIAfQgNgDgGgMQgGgLADgMQAEgOALgHQAMgGAMAEQAMAEAHALQAGAKgCAMIgBABIAAACQgFAMgLAHQgHAEgIAAIgIgCgAgOgDQgFASASAFQASAFAFgTQAFgSgSgFIgHgBQgMAAgEAPg");
	this.shape_345.setTransform(10.8,58.7,0.58,0.58,180);

	this.shape_346 = new cjs.Shape();
	this.shape_346.graphics.f("rgba(210,145,14,0.439)").s().p("AgQAcQgKgGgEgOQgEgMAHgMQAGgLAOgEQAMgCALAGQAKAGAFAMIAAACIABABQACAMgHALQgGALgNAEIgIABQgIAAgIgFgAgBgTQgSAFAFASQAFATASgFQASgFgFgSQgEgPgNAAIgGABg");
	this.shape_346.setTransform(10.8,43.6,0.58,0.58,180);

	this.shape_347 = new cjs.Shape();
	this.shape_347.graphics.f("rgba(210,145,14,0.439)").s().p("AAAAgQgLAAgKgKQgLgKAAgMQAAgOAMgKQAJgIALAAQAMABAKAIIACACQAJAKAAALQgBANgIAJQgKALgMAAIgCgBgAgKgQQgOAOAOANQANANAOgNQANgMgOgOQgHgHgHAAQgGAAgGAGg");
	this.shape_347.setTransform(3.2,30.6,0.58,0.58,180);

	this.shape_348 = new cjs.Shape();
	this.shape_348.graphics.f("rgba(210,145,14,0.439)").s().p("AgFAFQgCgCAAgDIABgDIABgCIACgBIADgBQADAAACACQADACAAADQAAADgDACQgCADgDAAQgDgBgCgCg");
	this.shape_348.setTransform(3.1,71.7,0.58,0.58,180);

	this.shape_349 = new cjs.Shape();
	this.shape_349.graphics.f("rgba(210,145,14,0.439)").s().p("AgBAIQgGgCAAgGIABgBQACgHAGABQAGACAAAGQgBADgCACQgBABgBAAQAAAAgBABQAAAAgBAAQAAAAgBAAIgBAAg");
	this.shape_349.setTransform(10.6,58.7,0.58,0.58,180);

	this.shape_350 = new cjs.Shape();
	this.shape_350.graphics.f("rgba(210,145,14,0.439)").s().p("AgHACIAAgCQAAgFAGgCQADAAADACQACACABADQAAACgCADQgCACgDABIgBAAQgEAAgDgGg");
	this.shape_350.setTransform(10.6,43.7,0.58,0.58,180);

	this.shape_351 = new cjs.Shape();
	this.shape_351.graphics.f("rgba(210,145,14,0.439)").s().p("AgDAHIgCgBIgBgCIgBgEQAAgCACgCQACgDADAAQADAAACADQADACAAADQgBADgCACQgCACgDAAIgDgBg");
	this.shape_351.setTransform(3.1,30.7,0.58,0.58,180);

	this.shape_352 = new cjs.Shape();
	this.shape_352.graphics.f("rgba(210,145,14,0.439)").s().p("AgZAHQgBgDADgCQAZgKAYgBIAAAJIgHAAQgUADgRAHIgCAAQgDAAgCgDg");
	this.shape_352.setTransform(1.5,26.4,0.58,0.58,180);

	this.shape_353 = new cjs.Shape();
	this.shape_353.graphics.f("rgba(210,145,14,0.439)").s().p("AAQAdQgGgBgEgEQgGgEACgGQADgHAIAEQAJAFACgBIADgGQAAgEgDgEIgIgEIgOgHQgSgIgSgCQgFgBABgFQAAgEAEABQArAFAXAVQALAJgGANQgFALgLAAIgFgBg");
	this.shape_353.setTransform(10.6,38,0.58,0.58,180);

	this.shape_354 = new cjs.Shape();
	this.shape_354.graphics.f("rgba(210,145,14,0.439)").s().p("AgEAkQgFgEgCgEQgDgHAEgEQADgEAGACIADADIACAFQADAFAEgBQAFgCAAgGQAAgFgEgGIgJgMQgMgRgOgLQgEgDADgEQACgCADACQAiAZAKAcQAEAPgKAIQgFAEgGAAQgGAAgGgFg");
	this.shape_354.setTransform(13.2,53.9,0.58,0.58,180);

	this.shape_355 = new cjs.Shape();
	this.shape_355.graphics.f("rgba(210,145,14,0.439)").s().p("AgRAgQgDgGABgFQABgHAFgCQAJgDABANQgCAHAKACIADgEQACgDAAgGIgBgRQgCgUgHgSQgBgEADgCQAEgBABADQATApgIAdQgEAOgNACIgCAAQgLAAgFgNg");
	this.shape_355.setTransform(8,69,0.58,0.58,180);

	this.shape_356 = new cjs.Shape();
	this.shape_356.graphics.f("rgba(210,145,14,0.439)").s().p("AACAsQgDgCABgEQAHgSACgVIABgQQAAgGgCgDQgDgIgFAHQgFABACAFQgBAGgBACQgCAEgGgCQgFgCgBgHQgBgFADgGQAFgOANABQANACAEAOQAIAdgTApQAAABgBAAQAAABAAAAQgBAAAAABQgBAAAAAAIgCgBg");
	this.shape_356.setTransform(8,33.4,0.58,0.58,180);

	this.shape_357 = new cjs.Shape();
	this.shape_357.graphics.f("rgba(210,145,14,0.439)").s().p("AgYAoQgDgEAEgDQAOgLAMgQIAJgNQAFgIgCgFQgCgGgDAAQgEABgDAFQgHALgGgHQgDgEACgGQADgFAEgEQAMgKALAJQALAIgGARQgLAcggAYIgCAAIgDgBg");
	this.shape_357.setTransform(13.2,48.5,0.58,0.58,180);

	this.shape_358 = new cjs.Shape();
	this.shape_358.graphics.f("rgba(210,145,14,0.439)").s().p("AgmAbQgBgFAFgBQASgCASgIIAOgHQAHgEABgCQADgCAAgEQgBgIgFADIgKAEQgHACgCgGQgCgGAFgEQAEgEAHgBQAGgBAGACQAHADADAGQAFANgNAKQgXAUgpAFIgCAAQAAAAgBAAQAAgBAAAAQgBAAAAgBQAAgBAAAAg");
	this.shape_358.setTransform(10.6,64.4,0.58,0.58,180);

	this.shape_359 = new cjs.Shape();
	this.shape_359.graphics.f("rgba(210,145,14,0.439)").s().p("AgXgBQgDgCABgDQACgFAFACQASAHAUACIAGAAIAAAKQgYgBgZgKg");
	this.shape_359.setTransform(1.5,76,0.58,0.58,180);

	this.shape_360 = new cjs.Shape();
	this.shape_360.graphics.f("rgba(210,145,14,0.439)").s().p("AgFAGQgCgCAAgEQgBgDADgCQAFgFAFAFQAGAFgFAGQgDADgDAAQgCAAgDgDg");
	this.shape_360.setTransform(0.8,25.4,0.58,0.58,180);

	this.shape_361 = new cjs.Shape();
	this.shape_361.graphics.f("rgba(210,145,14,0.439)").s().p("AgDAHQgDgCgBgDQgBgCACgDQACgDADgBQAHgCACAIQACAHgIACIgCABIgDgCg");
	this.shape_361.setTransform(11.3,37.9,0.58,0.58,180);

	this.shape_362 = new cjs.Shape();
	this.shape_362.graphics.f("rgba(210,145,14,0.439)").s().p("AgBAIQgEAAgBgEQgCgDABgCQABgDADgCQACgCADABQAIACgDAHQgBAGgGAAIgBAAg");
	this.shape_362.setTransform(14.1,54,0.58,0.58,180);

	this.shape_363 = new cjs.Shape();
	this.shape_363.graphics.f("rgba(210,145,14,0.439)").s().p("AgFAGQgFgGAFgEQAFgHAGAGQAFAFgFAGQgDADgDAAQgCAAgDgDg");
	this.shape_363.setTransform(8.4,69.3,0.58,0.58,180);

	this.shape_364 = new cjs.Shape();
	this.shape_364.graphics.f("rgba(210,145,14,0.439)").s().p("AgFAGQgGgGAGgFQAFgFAGAGQAGAFgGAFQgCADgEAAQgCgBgDgCg");
	this.shape_364.setTransform(8.4,33,0.58,0.58,180);

	this.shape_365 = new cjs.Shape();
	this.shape_365.graphics.f("rgba(210,145,14,0.439)").s().p("AgDAHQgDgCgBgDQgBgCACgDQABgDAEgBQAGgCACAIQADAHgIACIgCABIgDgCg");
	this.shape_365.setTransform(14.1,48.4,0.58,0.58,180);

	this.shape_366 = new cjs.Shape();
	this.shape_366.graphics.f("rgba(210,145,14,0.439)").s().p("AgBAIQgDAAgCgEQgCgDABgCQABgDADgCQADgCACABQAIACgCAHQgCAGgGAAIgBAAg");
	this.shape_366.setTransform(11.3,64.5,0.58,0.58,180);

	this.shape_367 = new cjs.Shape();
	this.shape_367.graphics.f("rgba(210,145,14,0.439)").s().p("AgFAFQgDgCAAgDQABgCACgDQADgCACAAQAEAAACACQAFAFgGAFQgCAEgDAAQgCAAgDgEg");
	this.shape_367.setTransform(0.8,77,0.58,0.58,180);

	this.shape_368 = new cjs.Shape();
	this.shape_368.graphics.f("rgba(210,145,14,0.439)").s().p("AgFAFQgCgCAAgDQAAgDACgCQACgCADAAQADAAACACQAGAFgFAGQgDACgDAAQgCAAgDgDg");
	this.shape_368.setTransform(4.1,21.4,0.58,0.58,180);

	this.shape_369 = new cjs.Shape();
	this.shape_369.graphics.f("rgba(210,145,14,0.439)").s().p("AgHACQgBgCACgDQABgDAEgBQAGgBACAHQACAIgHABIgCAAQgFAAgCgGg");
	this.shape_369.setTransform(16.1,36.2,0.58,0.58,180);

	this.shape_370 = new cjs.Shape();
	this.shape_370.graphics.f("rgba(210,145,14,0.439)").s().p("AgBAIQgDgBgCgDQgBgDABgCQAAgDADgCQADgCACABQADABACADQACADgBACQgCAGgFAAIgCAAg");
	this.shape_370.setTransform(19.1,54.9,0.58,0.58,180);

	this.shape_371 = new cjs.Shape();
	this.shape_371.graphics.f("rgba(210,145,14,0.439)").s().p("AgFAGQgFgGAGgFQACgCACAAQAEAAACACQACACAAADQAAADgCACQgDADgDAAQgCAAgDgCg");
	this.shape_371.setTransform(12.4,72.7,0.58,0.58,180);

	this.shape_372 = new cjs.Shape();
	this.shape_372.graphics.f("rgba(210,145,14,0.439)").s().p("AgEAFQgDgCAAgDQAAgDACgCQAFgFAGAFQACADAAACQAAADgCACQgDADgDAAQgBAAgDgDg");
	this.shape_372.setTransform(12.4,29.7,0.58,0.58,180);

	this.shape_373 = new cjs.Shape();
	this.shape_373.graphics.f("rgba(210,145,14,0.439)").s().p("AgDAHQgDgCAAgDQgBgCABgDQACgDADgBQAHgBACAHQABACgCADQgCADgDABIgCAAIgDgBg");
	this.shape_373.setTransform(19.1,47.4,0.58,0.58,180);

	this.shape_374 = new cjs.Shape();
	this.shape_374.graphics.f("rgba(210,145,14,0.439)").s().p("AgBAIQgEgBgBgDQgCgDABgCQACgHAHABQAHABgCAIQgBAGgGAAIgBAAg");
	this.shape_374.setTransform(16.1,66.2,0.58,0.58,180);

	this.shape_375 = new cjs.Shape();
	this.shape_375.graphics.f("rgba(210,145,14,0.439)").s().p("AgFAFQgCgCAAgDQAAgCACgDQACgCADAAQADgBADADQAFAFgGAFQgDADgCAAQgCAAgDgDg");
	this.shape_375.setTransform(4.1,80.9,0.58,0.58,180);

	this.shape_376 = new cjs.Shape();
	this.shape_376.graphics.f("rgba(210,145,14,0.439)").s().p("AgHAIQgDgEABgEQAAgDADgDQADgDADAAQAEAAADADQAHAGgHAHQgDADgEABQgEAAgDgDg");
	this.shape_376.setTransform(9.4,78,0.58,0.58,180);

	this.shape_377 = new cjs.Shape();
	this.shape_377.graphics.f("rgba(210,145,14,0.439)").s().p("AgBAKQgFAAgCgFQgCgEACgDQABgEADgCQAEgCADABQAEABACAEQACADgBADQgDAJgHAAIgBgBg");
	this.shape_377.setTransform(19.2,61,0.58,0.58,180);

	this.shape_378 = new cjs.Shape();
	this.shape_378.graphics.f("rgba(210,145,14,0.439)").s().p("AgEAJQgDgCgBgEQgCgDACgEQACgEAFgBQAIgCADAKQABADgCAEQgCADgEABIgDABIgEgCg");
	this.shape_378.setTransform(19.2,41.4,0.58,0.58,180);

	this.shape_379 = new cjs.Shape();
	this.shape_379.graphics.f("rgba(210,145,14,0.439)").s().p("AgGAHQgDgDAAgEQAAgDADgDQAGgHAHAHQADADABAEQAAAEgDADQgEADgEAAQgDgBgDgDg");
	this.shape_379.setTransform(9.4,24.4,0.58,0.58,180);

	this.shape_380 = new cjs.Shape();
	this.shape_380.graphics.f("rgba(210,145,14,0.439)").s().p("AgjBTIgBgCIAAgGQAAgXgKgwQgKg1gNgcQgCgDADgCQACgDADACQAcANA1AKQAiAHATACIAAAJQgYgBgcgEQgugHgXgKQAKAXAHAuQAGAtAAAXQAxgWAmgFQADgVAIgZIAAA+QgvACg2AVIgCAAQAAAAgBAAQAAAAgBAAQAAgBAAAAQgBAAAAgBg");
	this.shape_380.setTransform(4.1,71.9,0.58,0.58,180);

	this.shape_381 = new cjs.Shape();
	this.shape_381.graphics.f("rgba(210,145,14,0.439)").s().p("AAOBbIgDgCIAAgBIgCgDQgMgVgfglQgngrgXgPQgCgBABgEQABgCADgBQAcgCA2gSQAxgSAUgMIACgBIACAAQABAAABAAQAAABABAAQAAABAAAAQABABAAAAQAKA7AWApQACADgEADQgoAZgmAuIgDABIgBAAgAgEgxQgrARgZADQAUAQAdAjQAXAfAPAZQAfgqAggZQgPglgFg0QgWAMgoARg");
	this.shape_381.setTransform(11.8,58.4,0.58,0.58,180);

	this.shape_382 = new cjs.Shape();
	this.shape_382.graphics.f("rgba(210,145,14,0.439)").s().p("AA8BbIgCgBQgUgMgxgSQg2gSgcgCQgDgBgBgCQgBgEACgBQAXgPAngrQAfglAMgVIACgDIAAgBIADgCQAAAAABAAQAAAAABAAQAAAAABAAQABABAAAAQAmAuAoAZQAEADgCADQgWApgKA7QAAAAgBABQAAAAAAABQgBAAAAABQgBAAgBAAgAgXgVQgdAjgUAQQAZADArARQAoARAWAMQAFg0APglQgggZgfgqQgPAZgXAfg");
	this.shape_382.setTransform(11.8,44,0.58,0.58,180);

	this.shape_383 = new cjs.Shape();
	this.shape_383.graphics.f("rgba(210,145,14,0.439)").s().p("AhEBTQgDgCACgDQANgcAKg1QAKgwAAgXIAAgGIABgCQABgBAAAAQAAgBABAAQABAAAAAAQABAAABAAQA2AVAvACIAAA9QgIgYgDgVQgmgFgxgWQAAAXgGAtQgHAugKAXQAXgKAugHQAcgEAYgBIAAAJQgTACgiAHQg1AKgcANIgCABQgBAAAAAAQgBAAAAAAQAAgBgBAAQAAAAAAgBg");
	this.shape_383.setTransform(4.1,30.5,0.58,0.58,180);

	this.shape_384 = new cjs.Shape();
	this.shape_384.graphics.f("rgba(210,145,14,0.439)").s().p("AA3ELQgngHgjgLIgHgBQgEgBABgFIACgHQAIggAPgtQANgqANgQQgOgFgOgMIgagVQgegbgZgaIgGgGQgBAAAAAAQAAgBAAAAQgBAAAAgBQAAAAAAgBQAAAAAAAAQAAAAABgBQAAAAAAgBQAAAAABAAIAGgGQAZgaAegbIAagVQAOgLAOgGQgNgQgNgqQgOgmgJgnIgCgHQgBgFAEgBIAHgBQAjgLAngHQAfgGAKgBIAAAOQgSAAgZADIhFAQIgDABIABADIAUBFQAVA2AeASQADABgBAEQgBADgEAAQgjABgrAkIg1AxIgCABIACACIA1AxQArAkAjABQAEAAABADQABAEgDABQgeASgVA2IgUBFIgBADIADABIAiAIIAjAIQAZADASAAIAAAOQgKgBgfgGg");
	this.shape_384.setTransform(5.6,51.2,0.58,0.58,180);

	this.shape_385 = new cjs.Shape();
	this.shape_385.graphics.f("rgba(210,145,14,0.439)").s().p("AgHAfQgGgFAAgIQAAgIAGgGQAGgGAKgBQgEgHABgIQABgIAFgFIACgCIAAAtIgCAAQgBgBAAAAQgBAAAAAAQgBAAAAAAQgBAAAAABQAFACABAGQABAJgIAEIgGABQgEAAgEgDg");
	this.shape_385.setTransform(0.8,61.2,0.58,0.58,180);

	this.shape_386 = new cjs.Shape();
	this.shape_386.graphics.f("rgba(210,145,14,0.439)").s().p("AAGA5QgJgHgJgMQgLgPgKgcQgFgOgFgUIgCgIQgCgFAFgBIAJgCQAVgHARgDQAYgEASABIAAAKIgbABIgUACIgjAKIABAEQAEARAFAPIAHAQQAMAYALANQAXAXAQgQIADgDIAAATQgHACgGAAQgOAAgOgMg");
	this.shape_386.setTransform(2.7,61.3,0.58,0.58,180);

	this.shape_387 = new cjs.Shape();
	this.shape_387.graphics.f("rgba(210,145,14,0.439)").s().p("AAZBEQgNgBgMgGQgSgIgWgTQgLgKgOgPIgGgGQAAAAgBAAQAAgBAAAAQAAAAAAgBQAAAAAAgBQAAAAAAAAQAAAAAAgBQAAAAAAgBQABAAAAAAIAGgGQAMgOAOgLQAYgWAXgJIANgDQATgEAOAGQAQAIADATQACALgFALQgFALgIAFQALAHAEAPQAEAPgGANQgJAUgXAAIgNgCgAARg5QgRAHgWAPIgMAJIgYAYIgDACIADADIAYAXIANAKQAVAQASAGQAgAIAGgWQAEgPgGgJQgFgJgIABQgDAAgEACQAHgBADAGQAFAIgGAHQgGAHgIgCQgIgCgDgHQgEgHACgHQADgKAIgGQgNgHACgPQABgIAGgFQAHgFAHADQAHACACAHQACAHgGAFQgDADgFAAIAHACQAIABAFgJQAGgJgEgPQgEgQgTAAQgHAAgJACg");
	this.shape_387.setTransform(4.6,51.2,0.58,0.58,180);

	this.shape_388 = new cjs.Shape();
	this.shape_388.graphics.f("rgba(210,145,14,0.439)").s().p("AAABBQgOgDgTgGIgJgCQgFgBACgFIACgIQAEgTAHgQQALggAPgRIAHgIQAKgKAMgEQANgEALAEIAAATIgDgDQgQgQgXAXQgNAPgKAXIgHAPQgFAPgEARIgBAEIAjAKIAvADIAAAJIgLABQgQAAgUgEg");
	this.shape_388.setTransform(2.7,41.1,0.58,0.58,180);

	this.shape_389 = new cjs.Shape();
	this.shape_389.graphics.f("rgba(210,145,14,0.439)").s().p("AAIAaQgFgKAGgLQgPgBgGgNQgDgIAEgHQADgIAIgBQAHAAAEAFQAFAGgDAHQgCADgDACQAAAAABAAQAAAAABAAQAAAAABAAQAAAAABAAIACgBIAAAtQgEgDgCgFg");
	this.shape_389.setTransform(0.8,41.2,0.58,0.58,180);

	this.shape_390 = new cjs.Shape();
	this.shape_390.graphics.f("rgba(210,145,14,0.439)").s().p("AgDAFIgBgCIgBgBQgBgCABgDQACgCADAAQADAAACACQACADgBACQgBADgDABIgCAAQAAAAAAAAQAAAAgBAAQAAAAgBgBQAAAAgBAAg");
	this.shape_390.setTransform(15.4,15.7,0.58,0.58,180);

	this.shape_391 = new cjs.Shape();
	this.shape_391.graphics.f("rgba(210,145,14,0.439)").s().p("AgBAGQgDgBgBgDIAAgCIAAAAQAAgDADgCQACgCADACQADACAAADQAAADgCACQgBAAAAABQgBAAAAAAQgBAAAAABQgBAAAAAAIgBgBg");
	this.shape_391.setTransform(28.8,36.8,0.58,0.58,180);

	this.shape_392 = new cjs.Shape();
	this.shape_392.graphics.f("rgba(210,145,14,0.439)").s().p("AgEAFQgDgEADgCIAAgCQACgDADABQAEAAABAEQABACgCACQgBADgDAAIgBAAQAAAAAAAAQgBAAgBAAQAAAAgBAAQAAgBgBAAg");
	this.shape_392.setTransform(29.8,61.8,0.58,0.58,180);

	this.shape_393 = new cjs.Shape();
	this.shape_393.graphics.f("rgba(210,145,14,0.439)").s().p("AgBAGQgDgBgBgDQgBgCACgDIACgBIABgBQACgBADABQACACAAADQAAADgCACIgEABIgBAAg");
	this.shape_393.setTransform(18.1,84,0.58,0.58,180);

	this.shape_394 = new cjs.Shape();
	this.shape_394.graphics.f("rgba(210,145,14,0.439)").s().p("AgCAFIAAAAIgCgCQgCgCABgCQABgEADAAQACgBADACQACACAAACQAAADgCACIgDABIgDgBg");
	this.shape_394.setTransform(18.1,18.4,0.58,0.58,180);

	this.shape_395 = new cjs.Shape();
	this.shape_395.graphics.f("rgba(210,145,14,0.439)").s().p("AgEAEIAAgCQgDgCADgEQACgCADABQADAAABADQACACgCADQgBADgDAAIgBAAQAAAAgBAAQgBAAAAAAQAAgBgBAAQAAgBgBAAg");
	this.shape_395.setTransform(29.8,40.5,0.58,0.58,180);

	this.shape_396 = new cjs.Shape();
	this.shape_396.graphics.f("rgba(210,145,14,0.439)").s().p("AgCAGQgEgCABgEIAAgBIAEgEQACgBADACQACACAAACQAAAEgDABIgDABIgCAAg");
	this.shape_396.setTransform(28.8,65.6,0.58,0.58,180);

	this.shape_397 = new cjs.Shape();
	this.shape_397.graphics.f("rgba(210,145,14,0.439)").s().p("AgEAEQgCgEACgCIAAAAIACgCQACgCACABQAEABAAADQABACgCADQgCACgDAAQgCAAgCgCg");
	this.shape_397.setTransform(15.4,86.7,0.58,0.58,180);

	this.shape_398 = new cjs.Shape();
	this.shape_398.graphics.f("rgba(210,145,14,0.439)").s().p("AgEANQgJgCAAgHQAAgHAHgBIACAAIAAgDQABgGAHAAQAHAAACAJIABAOIgBADIgDABIgOgBg");
	this.shape_398.setTransform(16.9,85.5,0.58,0.58,180);

	this.shape_399 = new cjs.Shape();
	this.shape_399.graphics.f("rgba(210,145,14,0.439)").s().p("AgNAKQgCgDABgDQABgEADAAIACAAIgCgCQgBgEABgCQABgEAEgBQAFgCAGAGIAIAKQAAABABAAQAAABAAAAQAAAAAAABQAAAAgBAAQAAABAAABQAAAAAAABQAAAAgBAAQAAAAgBABQgJAFgDABIgEABQgGAAgDgFg");
	this.shape_399.setTransform(29.3,63.8,0.58,0.58,180);

	this.shape_400 = new cjs.Shape();
	this.shape_400.graphics.f("rgba(210,145,14,0.439)").s().p("AgFAOQgEgBgBgDQgBgEABgDIABgBIgBgBQgGgDADgHQAEgHAJADQADABAJAGQABAAAAAAQABAAAAABQAAAAAAAAQAAABAAAAQABABAAAAQAAABAAAAQAAAAAAABQgBAAAAAAIgIALQgEAFgDAAIgEgBg");
	this.shape_400.setTransform(29.3,38.6,0.58,0.58,180);

	this.shape_401 = new cjs.Shape();
	this.shape_401.graphics.f("rgba(210,145,14,0.439)").s().p("AgEAHIAAgCIgDAAQgGgBAAgHQAAgHAJgCIAOgBIADABIABADIgBAOQgCAJgHAAQgHAAgBgHg");
	this.shape_401.setTransform(16.9,16.9,0.58,0.58,180);

	this.shape_402 = new cjs.Shape();
	this.shape_402.graphics.f("rgba(210,145,14,0.439)").s().p("AA8BZQgNgBgJgHQgQgMgKgYQgEAGgGAEQgVAOgZgQQgUgNgMgZQgJgQgCgPQgCgJABgKQAAgIAEgLQAHgQAPgKQAWgNAeAHQAZAFAWASQAYATAAAZQAAAUgQALQAaALAKAQQAHAJABANQACAPgIAHQgGAHgLAAIgGgBgAAQARQACAFAAAGIAAABQAGAZALALQAHAIALADQAMADAHgHQAHgHgDgMQgDgLgIgHQgLgLgZgGIgBAAQgGAAgHgDgAhBhBQgSASAFAbQAEAWASAUQAWAZAUgBQAOgBAEgMQAFgLgIgKIgFgDQgFAAgFABIgDACIACABQAFABACAFQABAEgDAFIgCADQgLAHgIgJQgJgJAFgLQAEgJALgCQAIgCALADQgDgLACgIQACgLAJgEQALgFAJAJQAJAIgHALIgCABQgFAEgGgCQgFgCgBgGIgBABQAAABAAAAQgBAAAAABQAAAAAAABQAAABAAAAQgCAGADAGIABABQAKAHAMgEQALgEABgOQABgUgZgWQgUgSgWgEIgLgBQgTAAgPAOg");
	this.shape_402.setTransform(15.3,83.9,0.58,0.58,180);

	this.shape_403 = new cjs.Shape();
	this.shape_403.graphics.f("rgba(210,145,14,0.439)").s().p("AhCA2QgNgJgMgNQgFgGgEgHQgFgJgBgLQgDgRAIgQQANgXAegJQAagIAcAFQAbAEANAUQAMATgJASQAdgDARAJQAKAEAHALQAJAMgEAKQgEALgNAFQgMAEgMgBQgTgCgUgQIgDALQgKAXgbACIgFAAQgXAAgZgSgAg/g2QgaAJgGAZQgHAXARAVQAOARAaAJQAeALAQgJQANgHgBgNQAAgOgNgFIgFABQgFABgDAFIgCADIABgBQAGgCAEAEQADADAAAGIgBADQgFAMgMgEQgMgDgBgMQgDgUAbgHQgIgGgDgJQgDgKAGgJQAIgJAKADQAMADgBAMIAAADQgCAFgHABQgFABgDgFIAAAEQAAAGAEAEQAAAAABABQAAAAABABQAAAAABAAQAAABABAAQAOABAHgLQAHgLgHgMQgKgQgdgGQgMgDgKAAQgPAAgMAFgAAcAEIgBAAQgHAEgGAAIACACQADADAEAGIABABQASASAQAEQAJACAKgCQALgDAEgIQADgJgJgKQgHgIgKgDQgGgBgIAAQgLAAgQAEg");
	this.shape_403.setTransform(26.9,63.4,0.58,0.58,180);

	this.shape_404 = new cjs.Shape();
	this.shape_404.graphics.f("rgba(210,145,14,0.439)").s().p("AghBHQgPAAgRgHQgJgDgHgFQgIgEgGgJQgKgNgCgQQgBgaAVgYQATgUAbgLQAdgKAUANQARALACASQAVgQATgCQAMgBALAFQANAFAEAKQAEALgJAMQgHALgLAEQgSAIgbgDQADAHAAAEQADAagZAPQgQALgZAAIgMgBgAgtgxQgaAJgOARQgRAVAHAXQAGAZAaAJQAWAIAbgGQAcgGALgQQAHgMgHgLQgHgMgOACQgEADgCAEQgDAEABAGIACgCQADgEAFACQAEABACAFQAAAAAAABQAAAAABAAQAAABAAAAQAAAAAAABQABAMgMADQgKADgIgJQgGgJADgKQADgJAIgGQgbgHADgUQABgMAMgDQAMgEAFAMIABACQABAGgFAEQgFAEgFgCIABABIACADQAEAEAHABIABAAQANgFAAgNQABgOgNgHQgHgEgLAAQgMAAgQAGgAA4gpQgOAEgSASIgBABQgDAHgGADIACABQAGAAAFAEIABAAQAbAGAPgEQAJgCAIgJQAIgJgDgJQgEgIgMgDIgJgBQgGAAgFABg");
	this.shape_404.setTransform(26.9,39,0.58,0.58,180);

	this.shape_405 = new cjs.Shape();
	this.shape_405.graphics.f("rgba(210,145,14,0.439)").s().p("AgjBZQgIAAgLgEQgQgHgKgPQgNgWAHgeQAFgZASgWQATgYAZAAQAUAAALAQQALgaAQgKQAJgHANgBQAPgCAHAIQAIAIgCAPQgBANgHAJQgMAQgYAKQAGAEAEAGQAOAVgQAZQgNAUgZAMQgQAJgPACQgHABgIAAIgEAAgAg4gVQgSAUgEAWQgFAbASASQASASAbgFQAWgEAUgSQAZgWgBgUQgBgOgMgEQgLgFgKAIIgDAFQAAAFABAFIACADIABgCQABgFAFgCQAEgBAFADIADACQAHALgJAIQgJAJgLgFQgSgIAIgaQgaAIgIgSQgFgLAJgJQAIgJALAHIABACQAEAFgCAGQgCAFgGABIABABQABAAAAAAQAAABABAAQAAAAABAAQABAAAAAAQAGACAGgDIABgBQAHgKgEgLQgEgMgOgBIgBAAQgUAAgVAYgAA1hLQgLADgHAIQgLALgGAZIAAABQAAAHgDAGIACgBQAFgCAGAAIABAAQAZgGALgLQAIgHADgLQADgMgHgHQgFgFgIAAIgGABg");
	this.shape_405.setTransform(15.3,18.5,0.58,0.58,180);

	this.shape_406 = new cjs.Shape();
	this.shape_406.graphics.f("rgba(210,145,14,0.439)").s().p("AAMAbIgNgOQgKgIgEgFQgEgGAAgHQAAgJAGgEQAGgDAHADQAFADAEAGQADAFACAMIAGAWQABADgDACIgDABQgBAAAAAAQAAAAgBAAQAAAAAAAAQgBgBAAAAg");
	this.shape_406.setTransform(9.5,97.6,0.58,0.58,180);

	this.shape_407 = new cjs.Shape();
	this.shape_407.graphics.f("rgba(210,145,14,0.439)").s().p("AAWAUIgWgGQgMgCgFgDQgGgEgDgFQgDgHADgGQAEgGAJAAQAHAAAGAEQAFAEAIAKIAOANQADACgDAEQAAAAAAABQgBAAAAAAQgBABAAAAQgBAAAAAAIgCAAg");
	this.shape_407.setTransform(29,78.1,0.58,0.58,180);

	this.shape_408 = new cjs.Shape();
	this.shape_408.graphics.f("rgba(210,145,14,0.439)").s().p("AgLAPQgHAAgGgEQgGgEAAgHQAAgGAGgEQAGgEAHAAQAFAAALAEIAXAHQADAAAAADQAAABAAABQAAABgBAAQAAABAAAAQgBAAgBAAIgXAHQgKAEgFAAIgBAAg");
	this.shape_408.setTransform(36.2,51.2,0.58,0.58,180);

	this.shape_409 = new cjs.Shape();
	this.shape_409.graphics.f("rgba(210,145,14,0.439)").s().p("AgaAOQgDgGADgHQADgFAGgEQAFgDAMgCIAWgGQADgBACADQADAEgDACIgOANQgIAKgFAEQgGAEgHAAQgJAAgEgGg");
	this.shape_409.setTransform(29,24.3,0.58,0.58,180);

	this.shape_410 = new cjs.Shape();
	this.shape_410.graphics.f("rgba(210,145,14,0.439)").s().p("AgNAbQgGgEAAgIQAAgIAEgGQAEgFAKgIIANgOQACgDAEADQADACgBADIgGAWQgCAMgDAFQgEAGgFADQgDABgEAAQgDAAgDgBg");
	this.shape_410.setTransform(9.5,4.8,0.58,0.58,180);

	this.shape_411 = new cjs.Shape();
	this.shape_411.graphics.f("rgba(210,145,14,0.439)").s().p("AAiAnQgGgFgIgDQgYgEgMgDQgVgEgDgSQgCgMAIgLQAIgMANgEQALgEAJACQAKACAHAJQAJAMgHALIgCABQgDAGgHACQgHACgGgFQgDgCgBgFQgBgGAEgEQAFgFAJACIgBgBIgCgDQgDgEgGgCQgHgCgHADQgKAGgFAIQgDAJAIAIQAIAIALAEIAQAEIAKADQAKADAHAHQAEAEgEADIgEACQAAAAAAgBQgBAAAAAAQAAAAgBAAQAAgBAAAAg");
	this.shape_411.setTransform(4.5,7,0.58,0.58,180);

	this.shape_412 = new cjs.Shape();
	this.shape_412.graphics.f("rgba(210,145,14,0.439)").s().p("AgOAVQgFAAABgGQACgPAKgKQALgNAKAFQAFADgBAGQgCAFgFACIgNAGQgGAFgCAJQAAAAgBABQAAABAAAAQgBAAAAABQgBAAAAAAIgCAAg");
	this.shape_412.setTransform(10.2,12.4,0.58,0.58,180);

	this.shape_413 = new cjs.Shape();
	this.shape_413.graphics.f("rgba(210,145,14,0.439)").s().p("AgVAMQgFgDAEgEQALgMAMgEQARgFAGAJQACAFgCADQgDAEgFAAIgHAAIgHgBQgJAAgIAIIgEABIgCgBg");
	this.shape_413.setTransform(26,31.5,0.58,0.58,180);

	this.shape_414 = new cjs.Shape();
	this.shape_414.graphics.f("rgba(210,145,14,0.439)").s().p("AAMALQgIgHgEgCQgJgCgJABQgEABgBgDQgCgFAFgBQARgGAMAEQARAEgBAKQAAAEgEACIgFABIgEgBg");
	this.shape_414.setTransform(30.1,55.7,0.58,0.58,180);

	this.shape_415 = new cjs.Shape();
	this.shape_415.graphics.f("rgba(210,145,14,0.439)").s().p("AAKATQgFgCgCgFIgGgNQgFgGgJgCQgEgBABgEQAAgFAGABQAPACAKAKQANALgFAKQgCAEgEAAIgDAAg");
	this.shape_415.setTransform(21.4,78.8,0.58,0.58,180);

	this.shape_416 = new cjs.Shape();
	this.shape_416.graphics.f("rgba(210,145,14,0.439)").s().p("AAAAYQgEgDAAgFQACgKgBgEQAAgJgIgIQgCgDACgDQADgFAEAEQAMALAEAMQAFARgJAGIgFABIgDgBg");
	this.shape_416.setTransform(2.3,94.5,0.58,0.58,180);

	this.shape_417 = new cjs.Shape();
	this.shape_417.graphics.f("rgba(210,145,14,0.439)").s().p("AgMAWQgCgDADgDQAIgIAAgJIgBgHIgBgHQABgFAEgDQADgCAEACQAKAGgGARQgEAMgMALQAAAAgBABQAAAAAAAAQgBAAAAABQgBAAAAAAQgBAAAAAAQAAgBgBAAQAAAAgBgBQAAAAgBgBg");
	this.shape_417.setTransform(2.3,7.8,0.58,0.58,180);

	this.shape_418 = new cjs.Shape();
	this.shape_418.graphics.f("rgba(210,145,14,0.439)").s().p("AgUAOQgBgDAEgBQAJgDAFgFIAGgNQACgGAFgBQAGgCADAGQAFAJgNALQgKAKgPADIgCAAQgEAAAAgFg");
	this.shape_418.setTransform(21.4,23.6,0.58,0.58,180);

	this.shape_419 = new cjs.Shape();
	this.shape_419.graphics.f("rgba(210,145,14,0.439)").s().p("AgVAIIgDgCQAAgBAAAAQAAAAAAgBQAAAAAAgBQAAAAAAgBQABgDAFABQAKACAIgEQAHgGAEgCQAFgCAEACQAEACABAEQAAAJgQAFQgFACgFAAQgJAAgLgEg");
	this.shape_419.setTransform(30.1,46.7,0.58,0.58,180);

	this.shape_420 = new cjs.Shape();
	this.shape_420.graphics.f("rgba(210,145,14,0.439)").s().p("AAAALQgLgEgLgMQgEgEAFgDQADgCADADQAIAIAJAAQAFAAAJgCQAFABADAEQACADgCAEQgEAGgIAAQgFAAgHgCg");
	this.shape_420.setTransform(26,70.9,0.58,0.58,180);

	this.shape_421 = new cjs.Shape();
	this.shape_421.graphics.f("rgba(210,145,14,0.439)").s().p("AgGALQgKgKgCgPQgBgGAFAAQAEgBABAEQACAJAGAFIANAGQAFACACAFQABAGgFADIgGACQgHAAgIgKg");
	this.shape_421.setTransform(10.2,90,0.58,0.58,180);

	this.shape_422 = new cjs.Shape();
	this.shape_422.graphics.f("rgba(210,145,14,0.439)").s().p("AgTAVQgHgIgCgMIAAgBIAAAAQACgMAIgIQAJgIALAAQALABAIAIQAIAJAAAKIAAAAQAAAMgIAIQgJAJgMAAIAAAAQgLAAgIgIgAACgQQgPABgBAPQABARAPAAQARABABgQIAAgDQgBgPgPAAIgCAAg");
	this.shape_422.setTransform(25.9,51.2,0.58,0.58,180);

	this.shape_423 = new cjs.Shape();
	this.shape_423.graphics.f("rgba(210,145,14,0.439)").s().p("AgGAcQgLgDgGgJIgBgBIgBgCQgFgKAEgLQADgLALgGQAJgFAKADQALADAGAJIAAABIABABIAAAAIAAABQAFAKgDAKQgDALgKAGQgGAEgHAAIgHgBgAgFgQQgQAJAIAPQAJAOAOgIQAPgHgHgOQgFgMgJAAQgEAAgFADg");
	this.shape_423.setTransform(20.1,29.5,0.58,0.58,180);

	this.shape_424 = new cjs.Shape();
	this.shape_424.graphics.f("rgba(210,145,14,0.439)").s().p("AgHAbQgLgDgGgKQgGgKADgKQADgLAJgGIABgBIACgBQALgFAKAEQALADAGALQAFAJgDAKQgDALgJAGIgBAAIgBABIAAAAIgBAAQgGAEgHAAIgHgCgAgHgNQgOAJAIAOQAHAPAOgHQARgIgIgPQgGgKgJAAQgDAAgGACg");
	this.shape_424.setTransform(4.3,88.7,0.58,0.58,180);

	this.shape_425 = new cjs.Shape();
	this.shape_425.graphics.f("rgba(210,145,14,0.439)").s().p("AgOAZQgKgGgDgLQgDgKAFgKIABgCIABgBQAGgJAMgDQAKgDAKAHQAKAGADALQADAKgGAJIAAABIgBACQgGAJgLADIgHABQgHAAgHgEgAgNgHQgIAPAQAIQAPAIAIgQQAHgOgPgHQgFgDgFAAQgHAAgGAJg");
	this.shape_425.setTransform(20.1,72.9,0.58,0.58,180);

	this.shape_426 = new cjs.Shape();
	this.shape_426.graphics.f("rgba(210,145,14,0.439)").s().p("AgMAaIgCgBIgBgBQgJgGgDgMQgDgKAHgKQAGgKALgDQAKgCAJAFIABAAIACABQAJAGADALQACALgFAKQgGAKgLADIgIABQgFAAgHgDgAgNgJQgIAOAOAJQAPAIAIgQQAIgPgQgIQgEgCgEAAQgIAAgFAKg");
	this.shape_426.setTransform(4.3,13.7,0.58,0.58,180);

	this.shape_427 = new cjs.Shape();
	this.shape_427.graphics.f("rgba(210,145,14,0.439)").s().p("AgGAGQgGgGAGgGQAFgGAHAFIABABIAAABQAGAGgGAGQgDADgDAAQgDAAgEgEg");
	this.shape_427.setTransform(18,11,0.58,0.58,180);

	this.shape_428 = new cjs.Shape();
	this.shape_428.graphics.f("rgba(210,145,14,0.439)").s().p("AgIACQgDgIAJgDQAHgBAEAHIAAACIAAABQABADgCADQgDADgEABIgBAAQgGAAgCgIg");
	this.shape_428.setTransform(33.3,34,0.58,0.58,180);

	this.shape_429 = new cjs.Shape();
	this.shape_429.graphics.f("rgba(210,145,14,0.439)").s().p("AgCAJQgIgCACgIQADgJAHABQAIACgBAIIAAABIAAACQgDAGgFAAIgDgBg");
	this.shape_429.setTransform(35.1,61.7,0.58,0.58,180);

	this.shape_430 = new cjs.Shape();
	this.shape_430.graphics.f("rgba(210,145,14,0.439)").s().p("AgGAHQgGgHAHgGQAGgGAGAGQAGAFgFAHIgBABIgBAAQgEADgCAAQgDAAgDgDg");
	this.shape_430.setTransform(22.8,86.5,0.58,0.58,180);

	this.shape_431 = new cjs.Shape();
	this.shape_431.graphics.f("rgba(210,145,14,0.439)").s().p("AgCABQgBgFAGgEIAAARQgEgDgBgFg");
	this.shape_431.setTransform(0.2,101.9,0.58,0.58,180);

	this.shape_432 = new cjs.Shape();
	this.shape_432.graphics.f("rgba(210,145,14,0.439)").s().p("AgCgBQABgEAEgCIAAAQQgGgEABgGg");
	this.shape_432.setTransform(0.2,0.5,0.58,0.58,180);

	this.shape_433 = new cjs.Shape();
	this.shape_433.graphics.f("rgba(210,145,14,0.439)").s().p("AgFAHQgHgHAGgGQAGgGAGAGIABAAIABABQAFAGgGAHQgDACgDAAQgDAAgDgDg");
	this.shape_433.setTransform(22.8,15.8,0.58,0.58,180);

	this.shape_434 = new cjs.Shape();
	this.shape_434.graphics.f("rgba(210,145,14,0.439)").s().p("AgIACQgCgIAIgDQAHgBAEAHIAAACIAAABQABADgCADQgDADgEABIgBAAQgGAAgCgIg");
	this.shape_434.setTransform(35.1,40.7,0.58,0.58,180);

	this.shape_435 = new cjs.Shape();
	this.shape_435.graphics.f("rgba(210,145,14,0.439)").s().p("AgCAJQgIgCACgIQABgEADgCQADgCADAAQAEABACADQACADgBADIAAABIAAACQgDAGgFAAIgDgBg");
	this.shape_435.setTransform(33.3,68.3,0.58,0.58,180);

	this.shape_436 = new cjs.Shape();
	this.shape_436.graphics.f("rgba(210,145,14,0.439)").s().p("AgHAHQgFgHAGgFQAHgHAGAGQAGAGgGAGIAAABIgBABQgDACgDAAQgDAAgEgDg");
	this.shape_436.setTransform(18,91.4,0.58,0.58,180);

	this.shape_437 = new cjs.Shape();
	this.shape_437.graphics.f("rgba(210,145,14,0.439)").s().p("AgGAHQgGgHAGgGQAGgGAGAGIABAAIABABQAFAGgGAHQgDACgDAAQgDAAgEgDg");
	this.shape_437.setTransform(19.5,11.5,0.58,0.58,180);

	this.shape_438 = new cjs.Shape();
	this.shape_438.graphics.f("rgba(210,145,14,0.439)").s().p("AgIACQgDgIAJgDQAHgBAEAHIAAACIAAABQABADgCADQgCADgFABIgBAAQgGAAgCgIg");
	this.shape_438.setTransform(34.4,35.3,0.58,0.58,180);

	this.shape_439 = new cjs.Shape();
	this.shape_439.graphics.f("rgba(210,145,14,0.439)").s().p("AgCAKQgIgDACgIQADgJAHABQAIADgBAHIAAACIAAABQgDAGgFAAIgDAAg");
	this.shape_439.setTransform(35.4,63.3,0.58,0.58,180);

	this.shape_440 = new cjs.Shape();
	this.shape_440.graphics.f("rgba(210,145,14,0.439)").s().p("AgHAHQgFgGAGgHQAHgGAGAGQAGAGgGAGIAAABIgBABQgEACgCAAQgDAAgEgDg");
	this.shape_440.setTransform(22.3,88.1,0.58,0.58,180);

	this.shape_441 = new cjs.Shape();
	this.shape_441.graphics.f("rgba(210,145,14,0.439)").s().p("AgFAHQgHgHAGgGQAGgGAGAGIABAAIAAABQAFAGgFAHQgDACgDAAQgDAAgDgDg");
	this.shape_441.setTransform(22.3,14.3,0.58,0.58,180);

	this.shape_442 = new cjs.Shape();
	this.shape_442.graphics.f("rgba(210,145,14,0.439)").s().p("AgIACQgCgIAIgDQAHgCAEAIIAAABIAAACQABAIgJACIgBAAQgFAAgDgIg");
	this.shape_442.setTransform(35.4,39,0.58,0.58,180);

	this.shape_443 = new cjs.Shape();
	this.shape_443.graphics.f("rgba(210,145,14,0.439)").s().p("AgCAJQgIgCACgIQACgJAIABQAEABACADQACADgBADIAAABIAAACQgDAGgFAAIgDgBg");
	this.shape_443.setTransform(34.4,67.1,0.58,0.58,180);

	this.shape_444 = new cjs.Shape();
	this.shape_444.graphics.f("rgba(210,145,14,0.439)").s().p("AgHAHQgFgHAGgFQAHgHAGAGQAGAGgGAGIAAABIgBAAQgEADgCAAQgDAAgEgDg");
	this.shape_444.setTransform(19.5,90.8,0.58,0.58,180);

	this.shape_445 = new cjs.Shape();
	this.shape_445.graphics.f("rgba(210,145,14,0.439)").s().p("AgFAGQgIgGAHgGQAFgFAHAEIACACQAFAHgGAFQgDADgDAAQgDAAgDgEg");
	this.shape_445.setTransform(11.1,18.8,0.58,0.58,180);

	this.shape_446 = new cjs.Shape();
	this.shape_446.graphics.f("rgba(210,145,14,0.439)").s().p("AgIACQgBgDABgDQACgEAEgBQAIgBACAHIABABIAAACQABAIgJACIgBAAQgGAAgCgIg");
	this.shape_446.setTransform(23.5,37.4,0.58,0.58,180);

	this.shape_447 = new cjs.Shape();
	this.shape_447.graphics.f("rgba(210,145,14,0.439)").s().p("AgCAKQgJgDADgIQADgJAHABQAJADgBAIIgBACQgDAGgGAAIgCAAg");
	this.shape_447.setTransform(24.9,59.7,0.58,0.58,180);

	this.shape_448 = new cjs.Shape();
	this.shape_448.graphics.f("rgba(210,145,14,0.439)").s().p("AgGAHQgHgHAIgFQAFgIAHAHQAGAFgFAHIgCACQgDACgDAAQgDAAgDgDg");
	this.shape_448.setTransform(15,79.7,0.58,0.58,180);

	this.shape_449 = new cjs.Shape();
	this.shape_449.graphics.f("rgba(210,145,14,0.439)").s().p("AgFAGQgDgCgBgEQAAgDADgDQAFgGAHAFIACACQAFAHgGAFQgEADgDAAQgCAAgDgEg");
	this.shape_449.setTransform(15,22.7,0.58,0.58,180);

	this.shape_450 = new cjs.Shape();
	this.shape_450.graphics.f("rgba(210,145,14,0.439)").s().p("AgIACQgBgDABgDQACgEAEgBQADgBADACQADACABADIABACQABAJgJACIAAAAQgGAAgDgIg");
	this.shape_450.setTransform(24.9,42.7,0.58,0.58,180);

	this.shape_451 = new cjs.Shape();
	this.shape_451.graphics.f("rgba(210,145,14,0.439)").s().p("AgCAKQgIgDACgIQACgJAIABQAIACgBAIIAAACIgBABQgBADgDACIgEABIgCAAg");
	this.shape_451.setTransform(23.5,65,0.58,0.58,180);

	this.shape_452 = new cjs.Shape();
	this.shape_452.graphics.f("rgba(210,145,14,0.439)").s().p("AgGAHQgGgHAHgFQACgDADgBQAEAAADADQAGAFgFAHIgCACQgEACgCAAQgDAAgDgDg");
	this.shape_452.setTransform(11.1,83.6,0.58,0.58,180);

	this.shape_453 = new cjs.Shape();
	this.shape_453.graphics.f("rgba(210,145,14,0.439)").s().p("AAAAKQgEAAgDgDQgFgGAGgGQAGgHAHAGQAGAGgGAGIAAABIgBABQgDACgDAAIAAAAg");
	this.shape_453.setTransform(21.1,89.7,0.58,0.58,180);

	this.shape_454 = new cjs.Shape();
	this.shape_454.graphics.f("rgba(210,145,14,0.439)").s().p("AgCAKQgEgBgBgEQgCgDABgDQABgEADgCQADgCADAAQAIACgBAIIAAACIAAABQgDAGgGAAIgCAAg");
	this.shape_454.setTransform(35.2,65.3,0.58,0.58,180);

	this.shape_455 = new cjs.Shape();
	this.shape_455.graphics.f("rgba(210,145,14,0.439)").s().p("AgIACQgDgIAJgDQAHgCAEAIIAAABIAAACQABAIgJACIgBAAQgGAAgCgIg");
	this.shape_455.setTransform(35.2,37.1,0.58,0.58,180);

	this.shape_456 = new cjs.Shape();
	this.shape_456.graphics.f("rgba(210,145,14,0.439)").s().p("AgGAHQgDgDAAgEQAAgDADgDQAGgGAGAGIABAAIAAABQAGAHgHAGQgCACgDAAQgDAAgEgDg");
	this.shape_456.setTransform(21.1,12.7,0.58,0.58,180);

	this.shape_457 = new cjs.Shape();
	this.shape_457.graphics.f("rgba(210,145,14,0.439)").s().p("AgBAJQgEgCgCgDQgCgEABgCQABgEAEgBQADgCADAAQADABACAEQACADgBADQgBAEgDACIgEACIgCgBg");
	this.shape_457.setTransform(7.9,95,0.58,0.58,180);

	this.shape_458 = new cjs.Shape();
	this.shape_458.graphics.f("rgba(210,145,14,0.439)").s().p("AgEAIQgEgCgBgDQAAgDACgDQABgEAEgBQACgBAEACQADACACAEQABACgCAEQgCADgEABIgCAAIgEgBg");
	this.shape_458.setTransform(26.4,76.5,0.58,0.58,180);

	this.shape_459 = new cjs.Shape();
	this.shape_459.graphics.f("rgba(210,145,14,0.439)").s().p("AAAAJQgEAAgCgCQgDgDABgEQgBgDADgDQADgCADAAQAEgBACADQADADAAADQAAAEgDADQgCACgEAAIAAAAg");
	this.shape_459.setTransform(33.2,51.2,0.58,0.58,180);

	this.shape_460 = new cjs.Shape();
	this.shape_460.graphics.f("rgba(210,145,14,0.439)").s().p("AgCAJQgEgBgBgEQgCgDAAgDQABgDAEgCQAHgFAFAIQACAEgBADQgCADgDACIgEACIgCgBg");
	this.shape_460.setTransform(26.4,25.9,0.58,0.58,180);

	this.shape_461 = new cjs.Shape();
	this.shape_461.graphics.f("rgba(210,145,14,0.439)").s().p("AgDAIQgEgCgBgDQgBgDACgDQACgEAEgBQACgCAEACQADACABAEQABADgCAEQgCADgDABIgDABQgBAAgCgCg");
	this.shape_461.setTransform(7.9,7.4,0.58,0.58,180);

	this.shape_462 = new cjs.Shape();
	this.shape_462.graphics.f("rgba(210,145,14,0.439)").s().p("AgJACQgBgJAJgBQACgBAEACQADACABAEQACAGgIAEIgBAAIgBAAIgBAAQgHAAgCgHg");
	this.shape_462.setTransform(10,6.8,0.58,0.58,180);

	this.shape_463 = new cjs.Shape();
	this.shape_463.graphics.f("rgba(210,145,14,0.439)").s().p("AAAAJIgBAAIgBgBQgEAAgBgEQgCgDABgDQABgEADgBQAEgCACABQAEABACADQADAEgBACQgBADgDADIgFABIgBAAg");
	this.shape_463.setTransform(28.5,26.4,0.58,0.58,180);

	this.shape_464 = new cjs.Shape();
	this.shape_464.graphics.f("rgba(210,145,14,0.439)").s().p("AgFAHIgBAAIAAgBQgDgDABgDQAAgEADgCQAFgFAHAFQACADAAADQABAEgDACQgCADgEAAIgBAAQgCAAgDgCg");
	this.shape_464.setTransform(34.8,52.7,0.58,0.58,180);

	this.shape_465 = new cjs.Shape();
	this.shape_465.graphics.f("rgba(210,145,14,0.439)").s().p("AgIADIAAgBIAAgBQgBgIAIgCQAJgBABAJQABACgCAEQgCADgEABIgCABQgFAAgDgHg");
	this.shape_465.setTransform(27,78.5,0.58,0.58,180);

	this.shape_466 = new cjs.Shape();
	this.shape_466.graphics.f("rgba(210,145,14,0.439)").s().p("AgBAKQgEgBgCgDQgCgDABgDIAAgBIAAgBQABgEAEgBQADgCACABQAEABACADQACAEgBACQgBAEgDACQgDACgCAAIgBAAg");
	this.shape_466.setTransform(7.4,97.1,0.58,0.58,180);

	this.shape_467 = new cjs.Shape();
	this.shape_467.graphics.f("rgba(210,145,14,0.439)").s().p("AgDAIQgEgBgBgEIAAgBIAAgBQgBgCACgEQACgDAEgBQAIgBACAJQABACgCAEQgCADgEABIgCABIgDgCg");
	this.shape_467.setTransform(7.4,5.3,0.58,0.58,180);

	this.shape_468 = new cjs.Shape();
	this.shape_468.graphics.f("rgba(210,145,14,0.439)").s().p("AgBAKQgEgBgCgDQgCgDABgDIAAgBIAAgBQAEgIAHACQADABACADQACAEgBACQAAAEgEACQgCACgDAAIgBAAg");
	this.shape_468.setTransform(27,23.8,0.58,0.58,180);

	this.shape_469 = new cjs.Shape();
	this.shape_469.graphics.f("rgba(210,145,14,0.439)").s().p("AgFAHQgHgGAGgGIAAgBIABgBQADgCADAAQAEAAADAEQAFAFgGAGQgEAEgDAAQgCAAgDgDg");
	this.shape_469.setTransform(34.8,49.7,0.58,0.58,180);

	this.shape_470 = new cjs.Shape();
	this.shape_470.graphics.f("rgba(210,145,14,0.439)").s().p("AgEAIQgDgCgBgEQgCgHAIgDIABAAIABAAQACgBAEACQADACABAEQABAIgJACIgCAAIgEgBg");
	this.shape_470.setTransform(28.5,76,0.58,0.58,180);

	this.shape_471 = new cjs.Shape();
	this.shape_471.graphics.f("rgba(210,145,14,0.439)").s().p("AgBAJQgEAAgCgEQgCgDAAgDQACgIAIABIABAAIABAAQAIAEgCAHQgBADgDACIgFABIgBAAg");
	this.shape_471.setTransform(10,95.6,0.58,0.58,180);

	this.shape_472 = new cjs.Shape();
	this.shape_472.graphics.f("rgba(210,145,14,0.439)").s().p("AgGAYQgNgSgIgHQgIgGgIgDQgFgBACgEQABgFAEACQALACAKAJIAFAFIAMAMQAHAIALACQAMADAGgHQAGgIgBgKIAAgBQgCgLgIgFQgGgDgEAAIgBAAQAGAHgCAIQgBAFgFACQgFABgFgBQgGgDgCgHQgBgHADgGIABgDQAFgLAPACQALABAHAJQAHAIABALQADAOgHANQgIAOgNACIgEAAQgNAAgKgNg");
	this.shape_472.setTransform(10.3,10,0.58,0.58,180);

	this.shape_473 = new cjs.Shape();
	this.shape_473.graphics.f("rgba(210,145,14,0.439)").s().p("AgNAmQgPgFgIgPQgJgPAIgMQAIgMARgDIAcgEQAMgEAGgGQACgDAEADQAEADgEAEQgHAHgKADIgLAEIgPADQgMAEgHAIQgIAIADAJQAFAJAJAEQAIAEAIgDQAHgDACgDIACgDQgKADgEgHQgEgEABgFQABgFADgCQAGgFAHACQAHACADAGIACACQAHALgJALQgHAJgKACIgGABQgGAAgIgDg");
	this.shape_473.setTransform(4.5,95.4,0.58,0.58,180);

	this.shape_474 = new cjs.Shape();
	this.shape_474.graphics.f("rgba(210,145,14,0.439)").s().p("AgmAoQgDgEADgCQAFgGADgIIAHgkQAEgVASgDQAMgCAMAIQALAIAEANQAEALgCAJQgCAKgJAHQgMAJgLgHIgBgCQgGgDgCgHQgCgHAFgGQACgDAFgBQAGgBAEAEQAFAFgCAJIABgBIADgCQAEgDACgGQACgHgDgHQgGgKgIgFQgJgDgIAIQgIAIgEALIgEAQIgDAKQgDAKgHAHIgDACQgBAAAAAAQgBgBAAAAQgBAAAAAAQAAgBgBAAg");
	this.shape_474.setTransform(26.8,73.1,0.58,0.58,180);

	this.shape_475 = new cjs.Shape();
	this.shape_475.graphics.f("rgba(210,145,14,0.439)").s().p("AAAAbIgBgCQgDgGABgHQACgHAGgDQAFgCAFACQAFACABAFQACAIgGAGIACAAQAFAAAFgDQAIgGABgKQABgKgGgJQgGgHgMADQgLADgIAHIgLANIgFAEQgKAKgLACQgEABgBgEQgCgFAFgBQAIgCAIgHIALgNQAHgKAEgEQAGgHAJgCQAKgDAJAEQAKAGAFAMQAFAMgDALQgBALgHAIQgIAJgLABIgCAAQgNAAgEgKg");
	this.shape_475.setTransform(10.3,92.4,0.58,0.58,180);

	this.shape_476 = new cjs.Shape();
	this.shape_476.graphics.f("rgba(210,145,14,0.439)").s().p("AgJAmQgMgIgDgRQgBgSgDgKQgEgMgGgGQgDgCADgEQADgEAEAEQAHAHADAKIAEALIADAPQAEAMAIAHQAIAIAJgDQAJgFAEgJQAEgIgDgIQgDgHgDgCIgDgCQADAKgHAEQgEAEgFgBQgFgBgCgDQgFgGACgHQACgHAGgDIACgCQALgHALAJQAJAHACALQACAIgEALQgFAPgPAIQgIAFgHAAQgGAAgGgEg");
	this.shape_476.setTransform(26.8,29.3,0.58,0.58,180);

	this.shape_477 = new cjs.Shape();
	this.shape_477.graphics.f("rgba(210,145,14,0.439)").s().p("AgdAmQgKgNAHgTQAKgUACgKQABgKgBgHQgCgEAFgBQAEgCABAFQAEAKgEANIgBAHIgGAPQgCAMADALQAEAMAKABQAJAAAIgFIABgBQAJgGAAgLQAAgEgCgEIgBgCQgDAJgHACQgGABgEgDQgDgDgBgGQgBgHAFgFQAFgFAHABIACAAQANgBAGANQAIAUgTAQQgLAKgPABIgBAAQgPAAgJgKg");
	this.shape_477.setTransform(31.6,54.7,0.58,0.58,180);

	this.shape_478 = new cjs.Shape();
	this.shape_478.graphics.f("rgba(210,145,14,0.439)").s().p("AgUAqQgOgIgCgNQgCgQAPgMQASgNAHgHQAGgIADgIQABgFAEACQAFABgCAEQgCALgJAKIgFAFIgMAMQgIAHgCALQgDAMAHAGQAIAGAKgBIABAAQALgCAFgIQADgGAAgEIAAgBQgHAGgIgCQgFgBgCgFQgBgFABgFQADgGAHgCQAHgBAGADIADABQALAFgCAPQgBALgJAHQgIAHgLABIgHABQgKAAgKgFg");
	this.shape_478.setTransform(23.8,78.9,0.58,0.58,180);

	this.shape_479 = new cjs.Shape();
	this.shape_479.graphics.f("rgba(210,145,14,0.439)").s().p("AALAsQgCgIgHgIIgNgLIgOgLQgQgQAIgSQAGgKAMgFQAMgFALADQALABAIAHQAJAIABALQABAPgLAEIgCABQgGADgHgBQgHgCgDgGQgCgFACgFQACgFAFgBQAIgCAGAGIAAgCQAAgFgDgFQgGgIgKgBQgKAAgJAFQgHAGADAMQADALAHAIIANALIAEAFQAKAKACALQABAEgEABIgDAAQAAAAgBAAQAAAAgBgBQAAAAAAgBQgBAAAAgBg");
	this.shape_479.setTransform(23.8,23.5,0.58,0.58,180);

	this.shape_480 = new cjs.Shape();
	this.shape_480.graphics.f("rgba(210,145,14,0.439)").s().p("AgRAvQgFgBACgEIAAgMIgNgmQgGgYAQgKQAKgGANABQANACAJAJQAJAHADAJQADALgEAJQgHAOgMgCIgCAAQgHABgFgFQgFgFABgHQABgGADgDQAFgDAFABQAHADADAIIACgDIABgJQgBgJgIgGIgBgBQgIgFgJAAQgKABgEAMQgDALACAMIAGAPIABAHQAEANgEAKQAAABAAABQAAABgBAAQAAAAgBABQAAAAgBAAIgCgBg");
	this.shape_480.setTransform(31.6,47.7,0.58,0.58,180);

	this.shape_481 = new cjs.Shape();
	this.shape_481.graphics.f("rgba(210,145,14,0.439)").s().p("AgKAbQgDgFABgGQAHgMABgGQADgJgCgKIgBgDIAAgBQgBgEAEgBQAEgBABAEIABADQAHARABAPQABAWgOACIgBAAQgGAAgDgFg");
	this.shape_481.setTransform(11.7,7.6,0.58,0.58,180);

	this.shape_482 = new cjs.Shape();
	this.shape_482.graphics.f("rgba(210,145,14,0.439)").s().p("AgOAaQgGgEABgGQAAgGAGgFIAOgKQAGgGADgJIAAgBIABgCIAAgBQABgEAFABQAEACgBAEIgBADQgCAUgJAOQgEAHgFADQgDACgDAAQgEAAgDgCg");
	this.shape_482.setTransform(29.8,28,0.58,0.58,180);

	this.shape_483 = new cjs.Shape();
	this.shape_483.graphics.f("rgba(210,145,14,0.439)").s().p("AgZAOQgFgGADgFQADgFAGgCIAKgBIAJgBQAJgCAIgHIADgCIAAgBQADgDADAEQADADgDADIgCACQgLAOgNAJQgIAGgHAAQgHAAgEgGg");
	this.shape_483.setTransform(35,54.6,0.58,0.58,180);

	this.shape_484 = new cjs.Shape();
	this.shape_484.graphics.f("rgba(210,145,14,0.439)").s().p("AgfAAQAAgHAFgDQAFgDAGABIAJAEIAJAEQAJADAKgCIADgBIABAAQAEgBABAEQABAEgEABIgDABQgRAHgPABIgDAAQgTAAgCgNg");
	this.shape_484.setTransform(26.2,80.3,0.58,0.58,180);

	this.shape_485 = new cjs.Shape();
	this.shape_485.graphics.f("rgba(210,145,14,0.439)").s().p("AAWAUIgDgBQgUgCgOgJQgHgEgDgFQgEgHAEgGQAEgGAGABQAGAAAFAGIAKAOQAGAGAJADIABAAIACABIABAAQAEABgBAFQgBADgDAAIgCAAg");
	this.shape_485.setTransform(5.8,98.4,0.58,0.58,180);

	this.shape_486 = new cjs.Shape();
	this.shape_486.graphics.f("rgba(210,145,14,0.439)").s().p("AgZAPQgEgGAEgHQADgFAHgEQAOgJAUgCIADgBQAEgBACAEQABAFgEABIgBAAIgCABIgBAAQgJADgGAGIgKAOQgFAGgGAAIgCAAQgFAAgDgFg");
	this.shape_486.setTransform(5.8,4,0.58,0.58,180);

	this.shape_487 = new cjs.Shape();
	this.shape_487.graphics.f("rgba(210,145,14,0.439)").s().p("AgaALQgFgEAAgGQACgOAWABQAPABARAHIADABQAEABgBAEQgBAEgEgBIgBAAIgDgBQgKgCgJADIgJAEIgJAEIgDAAQgEAAgEgCg");
	this.shape_487.setTransform(26.2,22.1,0.58,0.58,180);

	this.shape_488 = new cjs.Shape();
	this.shape_488.graphics.f("rgba(210,145,14,0.439)").s().p("AAVATIAAgBIgDgCQgHgHgLgDQgNAAgGgCQgGgCgCgEQgCgGAEgFQAJgMARAMQANAJALAOIACACQADADgDAEIgDABIgDgBg");
	this.shape_488.setTransform(35,47.8,0.58,0.58,180);

	this.shape_489 = new cjs.Shape();
	this.shape_489.graphics.f("rgba(210,145,14,0.439)").s().p("AALAZIAAgBIgBgCIAAgBQgDgJgGgGQgBgDgFgDIgIgEQgGgFAAgGQgBgGAGgEQAGgEAHAEQAFADAEAHQAJAOACAUIABADQABAEgEACIgCAAQgDAAgBgDg");
	this.shape_489.setTransform(29.8,74.4,0.58,0.58,180);

	this.shape_490 = new cjs.Shape();
	this.shape_490.graphics.f("rgba(210,145,14,0.439)").s().p("AgBAfQgEgBABgEIAAgBIABgDQACgKgDgJIgEgJIgEgJQgBgGADgFQAEgFAGAAQAOACgBAWQgBAPgHARIgBADQgBABAAABQAAAAgBABQAAAAgBAAQAAABgBAAIgBgBg");
	this.shape_490.setTransform(11.7,94.8,0.58,0.58,180);

	this.shape_491 = new cjs.Shape();
	this.shape_491.graphics.f("rgba(210,145,14,0.439)").s().p("AgVAeQgCgKAJgGIAOgGQAGgEADgFQAFgLgHgLQgDgEgEgCQgFgEAFgEQACgEADADQAGAFACADQANANgFAVQgCAOgHAKQgIANgLAAQgLgBgDgKg");
	this.shape_491.setTransform(13.7,8.5,0.58,0.58,180);

	this.shape_492 = new cjs.Shape();
	this.shape_492.graphics.f("rgba(210,145,14,0.439)").s().p("AgVAhQgKgFABgKQABgLALgCIAOABQAHABAFgCQAOgFAAgQQgBgFgDgEQAAgBAAgBQAAAAAAgBQAAAAAAgBQAAAAAAgBQABAAAAgBQAAAAABAAQAAgBAAAAQABAAAAAAQAFgDABAFQADAFAAAGQAEARgOAQQgIAKgKAGQgIAEgHAAQgEAAgEgBg");
	this.shape_492.setTransform(31.1,29.8,0.58,0.58,180);

	this.shape_493 = new cjs.Shape();
	this.shape_493.graphics.f("rgba(210,145,14,0.439)").s().p("AgjANQgGgJAGgHQAGgJAKAEQAEABAJAHQAGAEAGAAQAOAAAHgLQACgGgBgEQAAgGAHACIADABQAAAAAAAAQABABAAAAQAAABAAAAQAAABAAAAIgDAMQgGAQgVAHQgMAEgKAAQgPAAgHgJg");
	this.shape_493.setTransform(35.2,56.6,0.58,0.58,180);

	this.shape_494 = new cjs.Shape();
	this.shape_494.graphics.f("rgba(210,145,14,0.439)").s().p("AgDAVQgOgCgKgHQgNgIAAgLQABgLAKgDQAKgCAGAJQADAFADAJQAEAGAFADQALAFALgHQAEgDACgEQAEgFAEAFQAEACgDADQgFAGgDACQgKAJgOAAIgKgBg");
	this.shape_494.setTransform(25.3,82.3,0.58,0.58,180);

	this.shape_495 = new cjs.Shape();
	this.shape_495.graphics.f("rgba(210,145,14,0.439)").s().p("AgNAUQgKgIgGgKQgHgNAEgKQAFgKAKABQALABACALIgBAOQgBAHACAFQAFAOAQAAQAFgBAEgDQABAAABAAQAAAAABAAQAAAAABAAQAAAAABAAQAAABABAAQAAAAAAABQABAAAAAAQAAABAAAAQADAFgFABQgFADgGAAIgHABQgNAAgNgLg");
	this.shape_495.setTransform(4,99.6,0.58,0.58,180);

	this.shape_496 = new cjs.Shape();
	this.shape_496.graphics.f("rgba(210,145,14,0.439)").s().p("AggAWQgEgKAHgMQAGgKAKgJQAQgOARAEQAGAAAFADQAFABgDAFQAAAAAAABQAAAAgBAAQAAABAAAAQgBAAAAABQgBAAAAAAQgBAAAAAAQgBAAAAAAQgBAAgBAAQgEgDgFgBQgRAAgFAQQgBAEABAHQABAKAAADQgCALgLABIgBAAQgJAAgFgJg");
	this.shape_496.setTransform(4,2.8,0.58,0.58,180);

	this.shape_497 = new cjs.Shape();
	this.shape_497.graphics.f("rgba(210,145,14,0.439)").s().p("AgeAWQgJgDgBgLQAAgLAOgJQALgGAOgDQATgDANAMQAFADADAFQADAEgEABQAAAAgBABQAAAAAAAAQgBABAAAAQgBAAgBAAQAAAAgBAAQAAAAgBgBQAAAAgBAAQAAgBgBAAQgCgEgEgDQgLgHgMAGQgFADgEAHIgGANQgEAHgHAAIgFgBg");
	this.shape_497.setTransform(25.3,20.1,0.58,0.58,180);

	this.shape_498 = new cjs.Shape();
	this.shape_498.graphics.f("rgba(210,145,14,0.439)").s().p("AAcASQABgEgCgGQgHgMgPABQgFAAgIAFQgIAGgEABQgKAEgFgJQgGgHAGgJQAHgJAPAAQAMgBAMAFQATAHAGARIADALQAAAAAAABQAAAAAAABQAAAAgBAAQAAABAAAAIgDABIgDAAQgEAAAAgEg");
	this.shape_498.setTransform(35.2,45.7,0.58,0.58,180);

	this.shape_499 = new cjs.Shape();
	this.shape_499.graphics.f("rgba(210,145,14,0.439)").s().p("AAVAhQAAAAgBAAQAAAAAAgBQgBAAAAAAQAAgBgBAAQAAgBAAAAQAAgBAAAAQAAgBAAAAQAAgBAAgBQADgEABgFQAAgRgQgFQgEgBgHABQgKABgDAAQgLgCgBgLQgBgKAKgFQAKgEANAHQAKAGAIAKQAOAQgEARQAAAGgDAFQAAABAAABQAAAAgBABQAAAAgBAAQAAAAAAAAIgEgBg");
	this.shape_499.setTransform(31.1,72.6,0.58,0.58,180);

	this.shape_500 = new cjs.Shape();
	this.shape_500.graphics.f("rgba(210,145,14,0.439)").s().p("AAAAmQAAAAgBgBQAAAAAAAAQgBgBAAAAQAAgBAAgBQAAAAAAgBQAAAAABgBQAAAAAAgBQABAAAAgBQAEgCADgEQAHgLgGgMQgDgFgHgEQgIgDgFgDQgJgGADgKQADgJALgBQALAAAJAOQAGALADAOQADATgMANQgDAFgFADIgCACQgBAAAAgBQgBAAAAAAQgBAAAAgBQAAAAAAgBg");
	this.shape_500.setTransform(13.7,93.9,0.58,0.58,180);

	this.shape_501 = new cjs.Shape();
	this.shape_501.graphics.f("rgba(210,145,14,0.439)").s().p("AgkBAQgTgGgCgPQgCgTASgMQAFgDAdgKQATgIAOgRQAPgQAGgUQACgEAFABQAFABgBAEQgOAkgeAYIghASQgQAJgDALQgDAJAHAGQAGAFAGgFIgCgDQgCgGAFgEQAGgEAFADQAGACAAAHQABAGgEAEQgGAIgLAAQgFAAgHgCg");
	this.shape_501.setTransform(13.2,12.1,0.58,0.58,180);

	this.shape_502 = new cjs.Shape();
	this.shape_502.graphics.f("rgba(210,145,14,0.439)").s().p("AhBAgQgPgMAEgPQAEgSAVgDQANgCAXAEQAvAJAlggQADgEADAEQAFAEgEADQggAagpAEIgXgBQgPgBgIACQgHABgFAEQgGAFAAAGQAAAFAEADQADAEAEgBIAAgCQAAgGAGgCQAGgCAEAEQAFADgBAHQgCAGgEADQgEACgFAAQgJAAgLgIg");
	this.shape_502.setTransform(28.4,32.2,0.58,0.58,180);

	this.shape_503 = new cjs.Shape();
	this.shape_503.graphics.f("rgba(210,145,14,0.439)").s().p("Ag7AcQgPgDgEgUQgEgSANgJQANgKATAIQAJAEATAQQAkAeAvgKQAFgBAAAFQADAFgFABQgpAHgkgRQgGgDgLgHIgSgKQgOgIgLAGQgGADgBAFQgBAIAHADIABgDQAEgFAHACQAGACABAGQAAAHgFAEQgEADgEAAIgEgBg");
	this.shape_503.setTransform(31.5,58.5,0.58,0.58,180);

	this.shape_504 = new cjs.Shape();
	this.shape_504.graphics.f("rgba(210,145,14,0.439)").s().p("AA9A6QgkgOgYgeIgSghQgJgQgLgDQgJgDgGAHQgFAGAFAGIADgCQAGgCAEAFQAEAGgDAFQgCAGgHAAQgGABgEgEQgNgKAHgTQAGgTAPgCQATgCAMASQADAFAKAdQAJATAQAOQAQAPAUAGQAEACgBAFQgBAEgCAAIgCAAg");
	this.shape_504.setTransform(21.7,81.8,0.58,0.58,180);

	this.shape_505 = new cjs.Shape();
	this.shape_505.graphics.f("rgba(210,145,14,0.439)").s().p("AAIAHIAAgXQABgPgCgJQgBgHgFgEQgEgGgGAAQgEAAgEADQgDAEAAADIACAAQAHAAACAGQACAGgEAFQgEAEgGgBQgGgBgDgFQgIgLAOgRQAMgPAOADQASAEAEAWQABAMgEAXQgHAmAUAfIAAASQgXgggDgkg");
	this.shape_505.setTransform(2,97.2,0.58,0.58,180);

	this.shape_506 = new cjs.Shape();
	this.shape_506.graphics.f("rgba(210,145,14,0.439)").s().p("AgFBKQgOgBgKgRQgLgSAOgJQAGgDAFADQAGACAAAGQABAFgEADQgEAEgEgBQAAAEAEADQAEAEAFgBQALgCADgRQACgIgBgPIAAgWQADgkAXggIAAASQgJAOgEARQgDARAGAiQADAXgIAMQgJAOgMAAIgDgBg");
	this.shape_506.setTransform(2,5.2,0.58,0.58,180);

	this.shape_507 = new cjs.Shape();
	this.shape_507.graphics.f("rgba(210,145,14,0.439)").s().p("AgyA3QgMgGgDgUQgCgVAPgEQAGgBAFAEQAFADgBAGQAAAGgGACQgFACgEgDQgFAGAFAGQAGAHALgEQAKgEAIgOIASgiQAYgeAkgNQAEgCABAGQABAFgEABQguAPgSA0QgIAXgMAJQgJAGgIAAQgGAAgGgDg");
	this.shape_507.setTransform(21.7,20.6,0.58,0.58,180);

	this.shape_508 = new cjs.Shape();
	this.shape_508.graphics.f("rgba(210,145,14,0.439)").s().p("AhLASQgIgMAIgRQAHgUAQAEQAGABADAGQADAGgFAFQgDAEgGAAQgGgBgBgFQgHACABAIQABAGAGACQALAGAOgIIASgJQALgIAGgDQAkgQApAHQAFABgDAFQAAAFgFgBQgagGgaAIQgRAGgXATQgOAMgOADIgJABQgMAAgIgLg");
	this.shape_508.setTransform(31.5,43.9,0.58,0.58,180);

	this.shape_509 = new cjs.Shape();
	this.shape_509.graphics.f("rgba(210,145,14,0.439)").s().p("ABFAnQgVgSgZgGQgSgDghAGQgYADgMgIQgOgKABgOQACgOARgKQASgLAIAOQADAGgCAFQgDAGgGAAQgEABgEgEQgDgEABgEQgEAAgEAEQgDAEABAFQACAMAQACQAIACAPgBIAWAAQApAEAgAaQAEADgFADQAAABgBAAQAAABgBAAQAAAAAAAAQgBABAAAAQgBAAAAgBQAAAAgBAAQAAAAgBAAQAAgBAAAAg");
	this.shape_509.setTransform(28.4,70.2,0.58,0.58,180);

	this.shape_510 = new cjs.Shape();
	this.shape_510.graphics.f("rgba(210,145,14,0.439)").s().p("AAwA/QgPgug0gSQgXgIgJgMQgLgPAIgOQAGgMAUgDQAVgCAEAPQABAGgDAFQgEAFgGgBQgGAAgCgGQgCgFADgEQgGgFgGAFQgHAGAEALQAEAKAOAIIAiASQAeAYANAkQACAEgGABIgCAAQgDAAgBgDg");
	this.shape_510.setTransform(13.2,90.3,0.58,0.58,180);

	this.shape_511 = new cjs.Shape();
	this.shape_511.graphics.f("rgba(210,145,14,0.439)").s().p("AgYBsQgfgKgVgaQglgugBhaIAAgFIAAgCIABgBIAAAAIABAAIAHgEIACgBIADgCIACgBQBZgwA+AZQAfAMARAbQARAbgDAgQgEAigUAfQgWAggfAOQgRAGgRAAQgNAAgPgEgAhABHQAXAYAeAEQAfAFAbgRQAJgGAJgJQgQAFgPgFQgQgEgKgNQgLgQABgTQgJAIgKAEQgaAMgagRQgKgFgIgKIgHgKQAJArAZAagAAlgRQgRAFgIANQgJAPAFASQAEARAPAJQAQAJARgFQASgFAJgQQAIgPgFgSQgGgQgQgIQgJgFgKAAQgGAAgGACgAhphCQACA7AdAWIAFAEQASAKAUgHQASgHAMgSQALgUgCgUQgDgWgSgMIgCgBQgNgHgQAAQgbAAgiATgABCgZQANAEAJALQAJAKADAMQAEgQgBgTQgCgfgZgXQgYgUgggEQgdgDghAKQANAAAMAFIAJAFQAPAIAIAPQAJAPgCARIgCAKQAOgKAPAAQAIAAAJAEg");
	this.shape_511.setTransform(17.4,70.9,0.58,0.58,180);

	this.shape_512 = new cjs.Shape();
	this.shape_512.graphics.f("rgba(210,145,14,0.439)").s().p("AALBzQgegCgYgYQgWgWgHggQgNg5AqhOIAAAAIACgEIABgBIAEgHIAAAAIAAgBIABAAIACgBIAGAAQA/ABApASIAAAKQgTgJgTgEQAIAFAJAKIAHAKQAJAOAAARQAAARgJAOQgDAEgFAFQALAAALACIAAAKQgWgJgSALQgPAIgEARQgFASAIAPQAJAOAQAFQAQAGAPgHIAAAOQgdANgbAAIgKAAgAg9AnQAJAeAYASQAZATAfgDQAOgCAGgCQgQgDgLgMQgLgMgCgPQgDgUALgRQgOADgHgBQgcgDgQgbQgGgKgCgKQgBgHAAgHQgOAsALAlgAgqgPIABACQAMASAWADQATACAUgLQATgMAHgSQAHgTgKgSIgEgGQgXgdg6gCQgdA2ARAkg");
	this.shape_512.setTransform(4.5,85.9,0.58,0.58,180);

	this.shape_513 = new cjs.Shape();
	this.shape_513.graphics.f("rgba(210,145,14,0.439)").s().p("AgRBpQghgMgbgeQgWgXgWgmIAAgBIAAgBIAAgBQA3hfBCgOQAigHAeAOQAeAOAPAeQAPAhgCAkQgDAngVAcQgTAagiAIQgLACgLAAQgVAAgTgIgAgFBjQAeAIAcgLQAdgLAPgcQAEgHAEgPIAAAAQgUAUgbgEQgagFgMgXQgDAKgHALQgPAWgggBQgKAAgNgEIgLgFQAeAhAkAKgAgog3QgnADggA0QAgA1AnADIAEAAQAVAAANgRQALgQAAgXQAAgVgLgQQgNgSgWAAgABCgqQgRAAgNANQgMANAAAQQAAASANAMQANANARAAQASAAAMgNQANgMAAgSQgBgRgMgMQgNgNgRAAIgBAAgAgGg5QAOAIAIAPIAEAMQAMgXAZgFQANgCANAEQANAEAJAKQgFgUgLgOQgSgZgggHQgfgFgcANQgbALgYAZQAKgFAMgCIAMgBIACAAQAQAAANAHg");
	this.shape_513.setTransform(23.4,51.2,0.58,0.58,180);

	this.shape_514 = new cjs.Shape();
	this.shape_514.graphics.f("rgba(210,145,14,0.439)").s().p("AhkBQIAAAAIgEgCIgBgBIgHgEIgBAAIAAAAIAAgBIgBgCIAAgGQABhmAxguQAZgWAggEQAggEAbARQAeAUASAhQASAjgDAiQgCAfgYAYQgWAWggAHQgOADgPAAQgvAAg7gggAAWAqQgDAcgbAQQgKAGgKACQgHABgHAAQAsAOAlgLQAegJASgYQATgZgDggQgCgNgCgGQgDAPgMALQgMALgPACQgUADgRgLQADAOgBAIgAhEgSIgGAEQgdAWgCA7QA2AdAkgRIACgBQASgMADgWQACgUgLgUQgMgSgSgHQgIgDgIAAQgLAAgKAGgAAag6QgPAJgEARQgEARAJAPQAIAOARAEQASAFAPgIQAPgJAFgQQAFgRgIgPQgJgQgSgFIgMgCQgMAAgKAHgAgahfQgeALgTAcQgQAXgHAfQAFgIAKgJIAKgHQAOgJARAAQARAAAOAJQAEADAFAFQgBgaASgRQAKgJANgCQAOgDANAEQgPgOgPgIQgQgHgRAAQgNAAgPAFg");
	this.shape_514.setTransform(17.4,31.4,0.58,0.58,180);

	this.shape_515 = new cjs.Shape();
	this.shape_515.graphics.f("rgba(210,145,14,0.439)").s().p("AghBzIgCAAIgBgBIAAAAIAAgBIgEgHIgBgCIgDgFQgwhZAZg+QAMgfAbgRQAbgRAfADQAZADAWAKIAAAOQgQgHgQAGQgQAGgIAPQgIAPAFAQQAFARAOAIQATALAVgJIAAAJQgLAEgKgBQAIAJAEAKQAMAagRAaQgFAKgKAIIgKAHQAWgFARgIIAAAKQgnAShCABgAgHgHQgWADgMASIgBACQgRAkAdA2QA6gCAXgdIAEgFQAKgSgHgUQgHgSgTgMQgRgJgQAAIgGAAgAAMhmQgeACgXAZQgUAYgEAgQgDAdAKAhQAAgNAFgMIAFgJQAIgPAPgIQAPgJAQACIAKACQgPgWAJgYQAEgNALgJQAKgJANgDQgOgDgPAAIgHAAg");
	this.shape_515.setTransform(4.5,16.4,0.58,0.58,180);

	this.shape_516 = new cjs.Shape();
	this.shape_516.graphics.f("rgba(210,145,14,0.439)").s().p("AgRKgQgogLgOglQgNggAOgjQANgfAcgZIANgKIACgCIABAAQANgJASgJIAigOIACgBQAhgQASgVQAagdgFguQgFgogZgjQgVgeghgQQgfgPghAKQgUAGgSAMIgjAZQgVAPgPAIIgEACIgSAHQg/ATgpgbQgZgQgKgcQgJgbAOggQAMgYAZgQQAXgPAdgGIASgCIACgBIACAAQAOgBAtAFQAuAFAdgIQAfgJASgeQAVghABgqQACglgQgiQgRgmghgMQgTgGgTgBQgQgBgjAEQghAEgTgBIgCAAIgCgBIgMgBQgdgFgXgOQgbgPgNgXQgOgYAFggQADgcAcgVQAUgPAdgDQAagCAbAHQAOAEALAFIAEACQAQAIApAfQAZARASAHQAkAOAfgNQAfgOAXgbQAfglAFgvQADgqgRgYQgLgQgVgPQgOgIghgPQgfgNgOgKIgLgIQgdgYgPgdQgRgjAKggQALgnAngOQAfgMAiAPQAfAMAZAeIAIALIABACIABABIAAAAQAJANAJATIAOAiIACAFQARAgAVASQAZAVApgCQAbgCAYgKIAAAQQgSAGgRADQgcADgZgGQgWgGgWgXQgQgRgLgXIgCgCIgBgCIgVgwIgBgDIgBgBQgQgggbgXQgkgbghACQgXABgRAOQgSAPgGAVQgKAgATAiQAQAdAeAVIAAABIAVAMIAgAOQAUAIAMAHIABABQAYAOARATQAhAlgOBAQgHAegRAaQgTAagaARQgSANgVAFQgXAGgdgKQgKgDgNgHQgIgDgJgGQgKgGgRgNIgbgUIgTgLIAAAAIgBAAQgZgMgagCQgdgDgXAKQgcAMgNAfQgGAPADASQACARAJANQAOAWAcAOQAZAMAdADIACAAQANABANgBIAhgEQAUgDANAAIABAAQAfAAAZAJQAqAPAUA3QALAdgCAfQgBAhgNAcQgLAYgPAPQgQAQgdAHQgUAFgUAAIgEAAQgMAAgUgDIgggDQgMgCgPABIgBAAQg8AGgfAlQgUAYADAhQABAQAKAOQAKAOAOAIQAWAOAfgBQAbgBAbgLIAFgDIABAAIAAAAIARgKIAtggQAHgGAJgFQAYgOAYgFQA1gMAvAtQAqApAIA4QADAcgGAZQgGAWgXAWQgNAMgSALQgUALgrASIgJAEQggATgUAcQgYAhAEAhQACAWAPARQAPASAWAFQAhAJAlgYQAZgQASgZIAAgBQAHgJAGgNQARgrAMgUQAPgaASgQQAlghBAAOQAMACAJAEIAAAQQgWgJgUgCQgrgFgYARQgSAMgOAUQgJAPgNAeQgOAfgJAOIgEAHIgBABIgEAFQgXAegeAPQgXAMgVAAQgKAAgLgDg");
	this.shape_516.setTransform(17.2,51.2,0.58,0.58,180);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_516},{t:this.shape_515},{t:this.shape_514},{t:this.shape_513},{t:this.shape_512},{t:this.shape_511},{t:this.shape_510},{t:this.shape_509},{t:this.shape_508},{t:this.shape_507},{t:this.shape_506},{t:this.shape_505},{t:this.shape_504},{t:this.shape_503},{t:this.shape_502},{t:this.shape_501},{t:this.shape_500},{t:this.shape_499},{t:this.shape_498},{t:this.shape_497},{t:this.shape_496},{t:this.shape_495},{t:this.shape_494},{t:this.shape_493},{t:this.shape_492},{t:this.shape_491},{t:this.shape_490},{t:this.shape_489},{t:this.shape_488},{t:this.shape_487},{t:this.shape_486},{t:this.shape_485},{t:this.shape_484},{t:this.shape_483},{t:this.shape_482},{t:this.shape_481},{t:this.shape_480},{t:this.shape_479},{t:this.shape_478},{t:this.shape_477},{t:this.shape_476},{t:this.shape_475},{t:this.shape_474},{t:this.shape_473},{t:this.shape_472},{t:this.shape_471},{t:this.shape_470},{t:this.shape_469},{t:this.shape_468},{t:this.shape_467},{t:this.shape_466},{t:this.shape_465},{t:this.shape_464},{t:this.shape_463},{t:this.shape_462},{t:this.shape_461},{t:this.shape_460},{t:this.shape_459},{t:this.shape_458},{t:this.shape_457},{t:this.shape_456},{t:this.shape_455},{t:this.shape_454},{t:this.shape_453},{t:this.shape_452},{t:this.shape_451},{t:this.shape_450},{t:this.shape_449},{t:this.shape_448},{t:this.shape_447},{t:this.shape_446},{t:this.shape_445},{t:this.shape_444},{t:this.shape_443},{t:this.shape_442},{t:this.shape_441},{t:this.shape_440},{t:this.shape_439},{t:this.shape_438},{t:this.shape_437},{t:this.shape_436},{t:this.shape_435},{t:this.shape_434},{t:this.shape_433},{t:this.shape_432},{t:this.shape_431},{t:this.shape_430},{t:this.shape_429},{t:this.shape_428},{t:this.shape_427},{t:this.shape_426},{t:this.shape_425},{t:this.shape_424},{t:this.shape_423},{t:this.shape_422},{t:this.shape_421},{t:this.shape_420},{t:this.shape_419},{t:this.shape_418},{t:this.shape_417},{t:this.shape_416},{t:this.shape_415},{t:this.shape_414},{t:this.shape_413},{t:this.shape_412},{t:this.shape_411},{t:this.shape_410},{t:this.shape_409},{t:this.shape_408},{t:this.shape_407},{t:this.shape_406},{t:this.shape_405},{t:this.shape_404},{t:this.shape_403},{t:this.shape_402},{t:this.shape_401},{t:this.shape_400},{t:this.shape_399},{t:this.shape_398},{t:this.shape_397},{t:this.shape_396},{t:this.shape_395},{t:this.shape_394},{t:this.shape_393},{t:this.shape_392},{t:this.shape_391},{t:this.shape_390},{t:this.shape_389},{t:this.shape_388},{t:this.shape_387},{t:this.shape_386},{t:this.shape_385},{t:this.shape_384},{t:this.shape_383},{t:this.shape_382},{t:this.shape_381},{t:this.shape_380},{t:this.shape_379},{t:this.shape_378},{t:this.shape_377},{t:this.shape_376},{t:this.shape_375},{t:this.shape_374},{t:this.shape_373},{t:this.shape_372},{t:this.shape_371},{t:this.shape_370},{t:this.shape_369},{t:this.shape_368},{t:this.shape_367},{t:this.shape_366},{t:this.shape_365},{t:this.shape_364},{t:this.shape_363},{t:this.shape_362},{t:this.shape_361},{t:this.shape_360},{t:this.shape_359},{t:this.shape_358},{t:this.shape_357},{t:this.shape_356},{t:this.shape_355},{t:this.shape_354},{t:this.shape_353},{t:this.shape_352},{t:this.shape_351},{t:this.shape_350},{t:this.shape_349},{t:this.shape_348},{t:this.shape_347},{t:this.shape_346},{t:this.shape_345},{t:this.shape_344},{t:this.shape_343},{t:this.shape_342},{t:this.shape_341},{t:this.shape_340},{t:this.shape_339},{t:this.shape_338},{t:this.shape_337},{t:this.shape_336},{t:this.shape_335},{t:this.shape_334},{t:this.shape_333},{t:this.shape_332},{t:this.shape_331},{t:this.shape_330},{t:this.shape_329},{t:this.shape_328},{t:this.shape_327},{t:this.shape_326},{t:this.shape_325},{t:this.shape_324},{t:this.shape_323},{t:this.shape_322},{t:this.shape_321},{t:this.shape_320},{t:this.shape_319},{t:this.shape_318},{t:this.shape_317},{t:this.shape_316},{t:this.shape_315},{t:this.shape_314},{t:this.shape_313},{t:this.shape_312},{t:this.shape_311},{t:this.shape_310},{t:this.shape_309},{t:this.shape_308},{t:this.shape_307},{t:this.shape_306},{t:this.shape_305},{t:this.shape_304},{t:this.shape_303},{t:this.shape_302},{t:this.shape_301},{t:this.shape_300},{t:this.shape_299},{t:this.shape_298},{t:this.shape_297},{t:this.shape_296},{t:this.shape_295},{t:this.shape_294},{t:this.shape_293},{t:this.shape_292},{t:this.shape_291},{t:this.shape_290},{t:this.shape_289},{t:this.shape_288},{t:this.shape_287},{t:this.shape_286},{t:this.shape_285},{t:this.shape_284},{t:this.shape_283},{t:this.shape_282},{t:this.shape_281},{t:this.shape_280},{t:this.shape_279},{t:this.shape_278},{t:this.shape_277},{t:this.shape_276},{t:this.shape_275},{t:this.shape_274},{t:this.shape_273},{t:this.shape_272},{t:this.shape_271},{t:this.shape_270},{t:this.shape_269},{t:this.shape_268},{t:this.shape_267},{t:this.shape_266},{t:this.shape_265},{t:this.shape_264},{t:this.shape_263},{t:this.shape_262},{t:this.shape_261},{t:this.shape_260},{t:this.shape_259},{t:this.shape_258},{t:this.shape_257},{t:this.shape_256},{t:this.shape_255},{t:this.shape_254},{t:this.shape_253},{t:this.shape_252},{t:this.shape_251},{t:this.shape_250},{t:this.shape_249},{t:this.shape_248},{t:this.shape_247},{t:this.shape_246},{t:this.shape_245},{t:this.shape_244},{t:this.shape_243},{t:this.shape_242},{t:this.shape_241},{t:this.shape_240},{t:this.shape_239},{t:this.shape_238},{t:this.shape_237},{t:this.shape_236},{t:this.shape_235},{t:this.shape_234},{t:this.shape_233},{t:this.shape_232},{t:this.shape_231},{t:this.shape_230},{t:this.shape_229},{t:this.shape_228},{t:this.shape_227},{t:this.shape_226},{t:this.shape_225},{t:this.shape_224},{t:this.shape_223},{t:this.shape_222},{t:this.shape_221},{t:this.shape_220},{t:this.shape_219},{t:this.shape_218},{t:this.shape_217},{t:this.shape_216},{t:this.shape_215},{t:this.shape_214},{t:this.shape_213},{t:this.shape_212},{t:this.shape_211},{t:this.shape_210},{t:this.shape_209},{t:this.shape_208},{t:this.shape_207},{t:this.shape_206},{t:this.shape_205},{t:this.shape_204},{t:this.shape_203},{t:this.shape_202},{t:this.shape_201},{t:this.shape_200},{t:this.shape_199},{t:this.shape_198},{t:this.shape_197},{t:this.shape_196},{t:this.shape_195},{t:this.shape_194},{t:this.shape_193},{t:this.shape_192},{t:this.shape_191},{t:this.shape_190},{t:this.shape_189},{t:this.shape_188},{t:this.shape_187},{t:this.shape_186},{t:this.shape_185},{t:this.shape_184},{t:this.shape_183},{t:this.shape_182},{t:this.shape_181},{t:this.shape_180},{t:this.shape_179},{t:this.shape_178},{t:this.shape_177},{t:this.shape_176},{t:this.shape_175},{t:this.shape_174},{t:this.shape_173},{t:this.shape_172},{t:this.shape_171},{t:this.shape_170},{t:this.shape_169},{t:this.shape_168},{t:this.shape_167},{t:this.shape_166},{t:this.shape_165},{t:this.shape_164},{t:this.shape_163},{t:this.shape_162},{t:this.shape_161},{t:this.shape_160},{t:this.shape_159},{t:this.shape_158},{t:this.shape_157},{t:this.shape_156},{t:this.shape_155},{t:this.shape_154},{t:this.shape_153},{t:this.shape_152},{t:this.shape_151},{t:this.shape_150},{t:this.shape_149},{t:this.shape_148},{t:this.shape_147},{t:this.shape_146},{t:this.shape_145},{t:this.shape_144},{t:this.shape_143},{t:this.shape_142},{t:this.shape_141},{t:this.shape_140},{t:this.shape_139},{t:this.shape_138},{t:this.shape_137},{t:this.shape_136},{t:this.shape_135},{t:this.shape_134},{t:this.shape_133},{t:this.shape_132},{t:this.shape_131},{t:this.shape_130},{t:this.shape_129},{t:this.shape_128},{t:this.shape_127},{t:this.shape_126},{t:this.shape_125},{t:this.shape_124},{t:this.shape_123},{t:this.shape_122},{t:this.shape_121},{t:this.shape_120},{t:this.shape_119},{t:this.shape_118},{t:this.shape_117},{t:this.shape_116},{t:this.shape_115},{t:this.shape_114},{t:this.shape_113},{t:this.shape_112},{t:this.shape_111},{t:this.shape_110},{t:this.shape_109},{t:this.shape_108},{t:this.shape_107},{t:this.shape_106},{t:this.shape_105},{t:this.shape_104},{t:this.shape_103},{t:this.shape_102},{t:this.shape_101},{t:this.shape_100},{t:this.shape_99},{t:this.shape_98},{t:this.shape_97},{t:this.shape_96},{t:this.shape_95},{t:this.shape_94},{t:this.shape_93},{t:this.shape_92},{t:this.shape_91},{t:this.shape_90},{t:this.shape_89},{t:this.shape_88},{t:this.shape_87},{t:this.shape_86},{t:this.shape_85},{t:this.shape_84},{t:this.shape_83},{t:this.shape_82},{t:this.shape_81},{t:this.shape_80},{t:this.shape_79},{t:this.shape_78},{t:this.shape_77},{t:this.shape_76},{t:this.shape_75},{t:this.shape_74},{t:this.shape_73},{t:this.shape_72},{t:this.shape_71},{t:this.shape_70},{t:this.shape_69},{t:this.shape_68},{t:this.shape_67},{t:this.shape_66},{t:this.shape_65},{t:this.shape_64},{t:this.shape_63},{t:this.shape_62},{t:this.shape_61},{t:this.shape_60},{t:this.shape_59},{t:this.shape_58},{t:this.shape_57},{t:this.shape_56},{t:this.shape_55},{t:this.shape_54},{t:this.shape_53},{t:this.shape_52},{t:this.shape_51},{t:this.shape_50},{t:this.shape_49},{t:this.shape_48},{t:this.shape_47},{t:this.shape_46},{t:this.shape_45},{t:this.shape_44},{t:this.shape_43},{t:this.shape_42},{t:this.shape_41},{t:this.shape_40},{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol2, new cjs.Rectangle(0,-77.6,620,188.6), null);


(lib.Symbol1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2
	this.instance = new lib.Tween16("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(318,51.8,0.7,0.7);
	this.instance.alpha = 0;
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(109).to({_off:false},0).to({scaleX:1,scaleY:1,x:321.2,alpha:1},24).wait(92).to({startPosition:0},0).to({scaleX:1.21,scaleY:1.21,x:322.8,alpha:0},20).wait(1083));

	// Layer 8
	this.instance_1 = new lib.Tween2("synched",0);
	this.instance_1.parent = this;
	this.instance_1.setTransform(310,205.5);
	this.instance_1.alpha = 0;
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(956).to({_off:false},0).to({y:160.6,alpha:1},23).to({y:-62},109).to({y:-104.9,alpha:0},21).wait(219));

	// Layer 7
	this.instance_2 = new lib.Tween4("synched",0);
	this.instance_2.parent = this;
	this.instance_2.setTransform(310,120.5);
	this.instance_2.alpha = 0;
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(812).to({_off:false},0).to({y:102.7,alpha:1},20).to({y:-3.3},119).to({y:-20.3,alpha:0},19).wait(358));

	// Layer 6
	this.instance_3 = new lib.Tween6("synched",0);
	this.instance_3.parent = this;
	this.instance_3.setTransform(310,5.7);
	this.instance_3.alpha = 0;
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(666).to({_off:false},0).to({y:17.5,alpha:1},21).to({y:84.4},119).to({y:94.5,alpha:0},18).wait(504));

	// Layer 5
	this.instance_4 = new lib.Tween8("synched",0);
	this.instance_4.parent = this;
	this.instance_4.setTransform(480,-86.3);
	this.instance_4.alpha = 0;
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(524).to({_off:false},0).to({y:-66.7,alpha:1},22).to({y:33.1},112).to({y:54.5,alpha:0},24).wait(646));

	// Layer 4
	this.instance_5 = new lib.Tween10("synched",0);
	this.instance_5.parent = this;
	this.instance_5.setTransform(310,188);
	this.instance_5.alpha = 0;
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(383).to({_off:false},0).to({y:153.2,alpha:1},20).to({y:-48.9},116).to({y:-87.2,alpha:0},22).wait(787));

	// Layer 3
	this.instance_6 = new lib.Tween12("synched",0);
	this.instance_6.parent = this;
	this.instance_6.setTransform(310,172);
	this.instance_6.alpha = 0;
	this.instance_6._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(231).to({_off:false},0).to({y:136.6,alpha:1},24).to({y:-40.2},120).to({y:-71.2,alpha:0},21).wait(932));

	// Layer 12
	this.instance_7 = new lib.Tween20("synched",0);
	this.instance_7.parent = this;
	this.instance_7.setTransform(310.9,50.3,0.504,0.504,0,0,0,0,0.1);
	this.instance_7.alpha = 0;
	this.instance_7._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(1128).to({_off:false},0).to({regY:0,scaleX:1,scaleY:1,alpha:1},17).wait(134).to({startPosition:0},0).to({scaleX:1.23,scaleY:1.23,alpha:0},24).wait(25));

	// Layer 10
	this.instance_8 = new lib.Symbol2();
	this.instance_8.parent = this;
	this.instance_8.setTransform(55.4,48.8,1,1,0,0,0,55.4,51.2);

	this.timeline.addTween(cjs.Tween.get(this.instance_8).wait(88).to({alpha:0.328},25).wait(992).to({alpha:0.441},21).wait(174).to({alpha:1},22).wait(6));

	// Layer 9
	this.instance_9 = new lib.Tween18("synched",0);
	this.instance_9.parent = this;
	this.instance_9.setTransform(310,51.5);
	this.instance_9.alpha = 0;
	this.instance_9._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_9).wait(4).to({_off:false},0).to({alpha:1},29).wait(49).to({startPosition:0},0).to({alpha:0},25).wait(1221));

	// Layer 1
	this.instance_10 = new lib.Tween14("synched",0);
	this.instance_10.parent = this;
	this.instance_10.setTransform(310,50);

	this.timeline.addTween(cjs.Tween.get(this.instance_10).wait(1328));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,-80,620,188.6);


// stage content:
(lib.xezine620x100 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#D0BA00").ss(1,1,1).p("Aw3qYMAhvAAAIAAUxMghvAAAg");
	this.shape.setTransform(310,50,2.865,0.741);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	// Layer 1
	this.instance = new lib.Symbol1();
	this.instance.parent = this;
	this.instance.setTransform(310,50,1,1,0,0,0,310,50);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(309.7,-30,620.6,188.6);
// library properties:
lib.properties = {
	width: 620,
	height: 100,
	fps: 26,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [
		{src:"images/xezine_620x100_atlas_.png?1497859394820", id:"xezine_620x100_atlas_"}
	],
	preloads: []
};




})(lib = lib||{}, images = images||{}, createjs = createjs||{}, ss = ss||{}, AdobeAn = AdobeAn||{});
var lib, images, createjs, ss, AdobeAn;