(function (lib, img, cjs, ss, an) {

var p; // shortcut to reference prototypes
lib.ssMetadata = [
		{name:"mangalustu_150x372_atlas_", frames: [[0,0,300,372],[0,748,224,230],[0,374,300,372]]}
];


// symbols:



(lib._1150 = function() {
	this.spriteSheet = ss["mangalustu_150x372_atlas_"];
	this.gotoAndStop(0);
}).prototype = p = new cjs.Sprite();



(lib._2150 = function() {
	this.spriteSheet = ss["mangalustu_150x372_atlas_"];
	this.gotoAndStop(1);
}).prototype = p = new cjs.Sprite();



(lib._3150 = function() {
	this.spriteSheet = ss["mangalustu_150x372_atlas_"];
	this.gotoAndStop(2);
}).prototype = p = new cjs.Sprite();



(lib.Tween31 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgGAwQgEgDAAgFQAAgEAEgDQADgDADAAQAEAAADADQADADABAEQgBAFgDADQgDADgEAAQgDAAgDgDgAgGAVIAAhHIANAAIAABHg");
	this.shape.setTransform(43.1,24.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AAOAjIAAgqQgBgHgDgDQgEgDgFAAIgGACQgDACgCADQgCAEAAAEIAAAoIgQAAIAAhEIANAAIACAIQAEgFAFgCQAEgCAFAAQAGAAAFACQAGADADAGQAEAFAAAIIAAAtg");
	this.shape_1.setTransform(38.2,25.7);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgHA0IAAhEIAPAAIAABEgAgHghQgCgDAAgFQAAgEACgDQADgDAEAAQAEAAAEADQACADABAEQgBAFgCADQgEACgEABQgEgBgDgCg");
	this.shape_2.setTransform(33.2,24);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgPAzQgFgCgEgGQgDgGgBgIIAAgWQABgHADgFQAEgFAFgDQAFgDAHAAIAJABQAEACAEAFIAAgtIAOAAIAABqIgNAAIgBgHQgFAEgEACQgEACgFAAQgGAAgFgDgAgGgBQgDABgBADQgCACAAAFIAAARQgBAFACACQACADACABIAGACQADAAAEgCQADgCACgDQACgEAAgEIAAgNQAAgEgCgEQgCgDgDgBIgHgCIgFABg");
	this.shape_3.setTransform(28.2,23.9);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AgMAhQgHgDgEgGQgEgFAAgJIAAgRQAAgJAEgGQADgHAGgDQAHgDAHAAQAHAAAGADQAHADAEAGQAEAGAAAJQAAAFgCADQgCADgDACQgEACgDAAIgaAAIAAACQAAAFACACQACADADABIAGABQAFAAAEgCIAIgGIAIAMQgFAFgGADQgHADgIAAQgGAAgGgDgAANgGQAAgHgEgDQgEgEgFAAQgGAAgDAEQgDADAAAHIAZAAIAAAAg");
	this.shape_4.setTransform(21.4,25.7);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AgPAxQgGgCgFgGIAFgNQAEADAFADQAEADAHAAQAFAAAEgFQAFgDAAgKIAAgIQgEAFgDACQgFACgEAAQgGAAgGgDQgGgDgDgGQgDgFgBgIIAAgUQABgIADgGQADgFAHgEQAFgDAGAAQAEABAFABQAFACADAEIABgHIAOAAIAABGQgBAKgEAHQgFAHgGAEQgIAEgHAAQgHAAgGgDgAgGgjQgDABgCADQgBADAAAEIAAASQgBAEACACQACADADABIAFABQADAAAEgCQADgBACgCQACgEAAgEIAAgNQAAgEgCgEQgCgDgDgCIgHgCIgFABg");
	this.shape_5.setTransform(10.3,27.3);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AANAjIAAgqQAAgHgDgDQgDgDgGAAIgGACQgDACgCADQgCAEgBAEIAAAoIgPAAIAAhEIANAAIACAIQAEgFAFgCQAEgCAEAAQAHAAAFACQAGADADAGQADAFABAIIAAAtg");
	this.shape_6.setTransform(3.3,25.7);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AgMAhQgHgDgEgGQgEgGAAgJQAAgGACgDQACgDADgBQADgCAFAAIAYAAIAAgDQAAgEgBgDQgCgCgDgBIgGgBQgFAAgEACQgFACgEADIgIgLQAFgFAHgDQAHgDAHAAQAHAAAGADQAGADAFAFQAEAGAAAJIAAARQAAAJgDAGQgEAGgGAEQgHADgIAAQgGAAgGgDgAgIARQADAEAFAAQAGAAADgEQADgEAAgGIgYAAQAAAGAEAEg");
	this.shape_7.setTransform(-3.5,25.7);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AgPAjQgDAAgDgCQgCgCgBgDQgCgCAAgEIABgEIACgEIAighIgiAAIAAgPIAlAAQAHAAACADQADAEAAAEIgBAGIgDAEIghAhIAmAAIAAAPg");
	this.shape_8.setTransform(-10.1,25.7);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AANAjIAAgqQAAgHgDgDQgDgDgGAAIgGACQgDACgCADQgCAEgBAEIAAAoIgOAAIAAhEIAMAAIACAIQAEgFAEgCQAFgCAEAAQAHAAAGACQAFADADAGQADAFAAAIIAAAtg");
	this.shape_9.setTransform(-20.8,25.7);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AgQAyQgFgDgDgGQgEgFABgIIAAgtIAOAAIAAAqQABAFACADQABACADACQADABADAAQADAAADgCQADgBACgEQACgDAAgFIAAgoIAPAAIAABFIgMAAIgCgKQgEAGgEACQgGADgEAAQgGAAgGgDgAAIgiQgDgDgBgEQABgFADgDQADgDAEAAQAEAAADADQAEADAAAFQAAAEgEADQgDADgEAAQgEAAgDgDgAgWgiQgCgDgBgEQABgFACgDQAEgDADAAQAGAAACADQADADAAAFQAAAEgDADQgCADgGAAQgDAAgEgDg");
	this.shape_10.setTransform(-27.9,24);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AgFAzIgGgCIABgLIAGACIAFAAIAEAAQAAgBAAAAQABAAAAgBQAAAAAAAAQAAgBAAAAQAAAAAAgBQAAAAAAAAQAAAAgBgBQAAAAAAAAIgEgBIgFAAIAAgPQgFAAgFgEQgFgDgDgGQgDgFAAgGIAAgSQAAgKAEgFQAEgGAGgDQAHgDAGAAQAGAAAGACQAFADAFAFIgHALIgIgFIgIgCQgEABgDACQgEADAAAHIAAASQAAAEACABQACAEADABIAFABQAFAAAEgCQAEgCADgDIAIAKIgIAHQgFADgFABIAAAEQAGACACAEQADADAAAEQAAAGgFAFQgFADgIAAIgGAAg");
	this.shape_11.setTransform(-34.3,27.3);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AgQAyQgFgDgDgGQgEgFABgIIAAgtIAOAAIAAAqQABAFACADQABACADACQADABADAAQADAAADgCQADgBACgEQACgDAAgFIAAgoIAPAAIAABFIgMAAIgCgKQgEAGgEACQgGADgEAAQgGAAgGgDgAAIgiQgDgDgBgEQABgFADgDQADgDAEAAQAEAAADADQAEADAAAFQAAAEgEADQgDADgEAAQgEAAgDgDgAgWgiQgCgDgBgEQABgFACgDQAEgDADAAQAGAAACADQADADAAAFQAAAEgDADQgCADgGAAQgDAAgEgDg");
	this.shape_12.setTransform(-41,24);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("AgOAhQgGgDgEgGQgEgGAAgIIAAgUQAAgHAEgGQAEgGAGgDQAFgDAGAAQAEAAAFACQAFACADAEIABgHIAOAAIAABFIgOAAIgCgIQgDAFgFACQgEACgEAAQgGAAgFgDgAgGgTQgDABgCADQgCADAAAEIAAARQABAEABADQACADADABIAFABQADAAADgCQAEgBACgEQACgDAAgFIAAgLQAAgFgCgDQgCgEgEgCIgGgBIgFABg");
	this.shape_13.setTransform(57.7,9.7);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FFFFFF").s().p("AgQAwQgGgCgFgFIAFgOQAEAEAFADQAGACAGAAQAFAAAFgEQAEgFABgJIAAgIQgFAFgEACQgFADgEgBQgGAAgGgCQgFgDgDgGQgDgFgBgHIAAguIAQAAIAAArQAAAFACACQABACADABQACACAEAAQADAAADgCQADgBACgDQACgEABgEIAAgpIAOAAIAABFQABALgFAGQgEAIgHADQgHAEgIAAQgHAAgHgDg");
	this.shape_14.setTransform(50.7,11.3);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FFFFFF").s().p("AgGA0IAAhEIAOAAIAABEgAgGghQgDgDAAgFQAAgEADgDQADgDADAAQAFAAADADQADADAAAEQAAAFgDADQgDACgFABQgDgBgDgCg");
	this.shape_15.setTransform(45.7,7.9);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FFFFFF").s().p("AgMAjIgKgCIABgPIAFABIAHACIAIAAQAEAAAEgBQADgCABgEQgBgDgEgCIgHgDIgLgDQgFgDgEgDQgDgEAAgHQAAgIADgEQAEgFAFgCQAFgCAHAAIALABIAMADIgCAOIgJgDIgMAAIgGABQgDABAAAEQAAADAEABIAIADIAKAEQAGACADADQAEAFgBAHQABAIgEAFQgEAFgGACQgGACgHAAIgLgBg");
	this.shape_16.setTransform(41.3,9.7);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#FFFFFF").s().p("AgPAhQgFgDgEgGQgEgGABgIIAAgUQgBgHAEgGQAEgGAFgDQAGgDAGAAQAEAAAFACQAFACADAEIABgHIANAAIAABFIgNAAIgCgIQgDAFgFACQgEACgEAAQgGAAgGgDgAgGgTQgDABgBADQgDADAAAEIAAARQAAAEACADQACADADABIAGABQACAAADgCQAEgBACgEQACgDAAgFIAAgLQAAgFgCgDQgCgEgEgCIgFgBIgGABg");
	this.shape_17.setTransform(35,9.7);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#FFFFFF").s().p("AgFAiQgCgCgCgEIgZg+IAQAAIASA1IATg1IAQAAIgZA+QgBAEgDACQgDABgDAAIgFgBg");
	this.shape_18.setTransform(27.8,9.7);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#FFFFFF").s().p("AgVAjIAAhEIAMAAIADAIQADgFAEgCQAFgCAFAAIAFAAIAGADIAAAQIgHgEIgHgBIgHACQgCACgDADQgCADAAAFIAAAog");
	this.shape_19.setTransform(21.6,9.6);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#FFFFFF").s().p("AgMAhQgHgDgEgGQgEgFAAgJIAAgRQAAgJADgGQAEgHAGgDQAHgDAHAAQAHAAAGADQAGADAFAGQAEAGAAAJQAAAFgCADQgCADgEACQgDACgDAAIgaAAIAAACQAAAFACACQACADADABIAGABQAFAAAEgCIAIgGIAIAMQgFAFgGADQgHADgIAAQgGAAgGgDgAANgGQAAgHgEgDQgEgEgFAAQgFAAgEAEQgDADAAAHIAZAAIAAAAg");
	this.shape_20.setTransform(15.5,9.7);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#FFFFFF").s().p("AgPAjQgDAAgDgCQgCgCgCgDQgBgCAAgEIABgEIACgEIAhghIghAAIAAgPIAlAAQAHAAACADQADAEAAAEIgBAGIgDAEIggAhIAlAAIAAAPg");
	this.shape_21.setTransform(8.9,9.7);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#FFFFFF").s().p("AgMAhQgHgDgEgGQgEgFAAgJIAAgRQAAgJAEgGQADgHAGgDQAHgDAHAAQAHAAAGADQAHADADAGQAFAGAAAJQAAAFgCADQgCADgDACQgEACgDAAIgaAAIAAACQAAAFACACQACADADABIAGABQAFAAAFgCIAHgGIAIAMQgFAFgHADQgGADgIAAQgGAAgGgDgAANgGQgBgHgDgDQgEgEgFAAQgFAAgEAEQgDADAAAHIAZAAIAAAAg");
	this.shape_22.setTransform(2.6,9.7);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#FFFFFF").s().p("AgVAjIAAhEIANAAIACAIQADgFAEgCQAEgCAGAAIAGAAIAFADIgBAQIgGgEIgHgBIgHACQgDACgCADQgCADAAAFIAAAog");
	this.shape_23.setTransform(-3.4,9.6);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#FFFFFF").s().p("AgKAMQAEgDABgFQACgDAAgGIAAgHIAOgCIAAAJQAAAHgBAFQgCAFgDADg");
	this.shape_24.setTransform(-12,12.9);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#FFFFFF").s().p("AAOAjIAAgqQgBgHgDgDQgDgDgGAAIgGACQgDACgCADQgCAEAAAEIAAAoIgQAAIAAhEIANAAIACAIQADgFAGgCQAEgCAFAAQAGAAAFACQAGADADAGQAEAFgBAIIAAAtg");
	this.shape_25.setTransform(-16.9,9.6);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#FFFFFF").s().p("AgQAgQgFgDgDgFQgEgGABgHIAAgtIAOAAIAAAqQAAAEADADQABADADABQADACADAAQACAAADgCQAEgCACgDQACgEAAgFIAAgnIAPAAIAABEIgMAAIgCgJQgEAFgFADQgFACgEAAQgGAAgGgDg");
	this.shape_26.setTransform(-24,9.7);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#FFFFFF").s().p("AgVAjIAAhEIAMAAIACAIQAEgFADgCQAFgCAGAAIAGAAIAFADIgBAQIgGgEIgHgBIgHACQgDACgBADQgDADAAAFIAAAog");
	this.shape_27.setTransform(-30.1,9.6);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#FFFFFF").s().p("AgPAgQgGgDgDgFQgDgGgBgHIAAgtIAQAAIAAAqQAAAEABADQACADADABQADACADAAQADAAACgCQAEgCACgDQACgEABgFIAAgnIAPAAIAABEIgNAAIgCgJQgEAFgFADQgEACgFAAQgGAAgFgDg");
	this.shape_28.setTransform(-36.5,9.7);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#FFFFFF").s().p("AgQAwQgGgCgFgFIAFgOQAEAEAFADQAFACAHAAQAFAAAFgEQAEgFABgJIAAgIQgFAFgEACQgFADgEgBQgGAAgGgCQgFgDgDgGQgDgFAAgHIAAguIAPAAIAAArQAAAFACACQABACADABQACACAEAAQACAAAEgCQADgBACgDQACgEABgEIAAgpIAOAAIAABFQABALgFAGQgEAIgHADQgHAEgHAAQgIAAgHgDg");
	this.shape_29.setTransform(-43.6,11.3);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#FFFFFF").s().p("AgQAgQgFgDgDgFQgDgGgBgHIAAgtIAQAAIAAAqQAAAEACADQABADADABQACACAEAAQADAAADgCQADgCACgDQACgEABgFIAAgnIAOAAIAABEIgMAAIgCgJQgEAFgEADQgFACgFAAQgGAAgGgDg");
	this.shape_30.setTransform(-50.7,9.7);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#FFFFFF").s().p("AgEA0QgFgCgEgEIgCAHIgMAAIAAhqIAOAAIAAAtQAEgFAEgCQAFgBAEAAQAGAAAGADQAGADADAFQADAFAAAHIAAAWQAAAIgDAGQgDAFgFADQgFADgGAAQgFAAgFgCgAgFAAQgDABgCADQgCAEgBAEIAAANQABAEACAEQACADADACQADACADAAQADAAADgCQADgBABgDQACgCAAgFIAAgRQAAgFgCgCIgEgEIgGgBIgGACg");
	this.shape_31.setTransform(-57.7,7.8);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#FFFFFF").s().p("AgKANQAEgEABgFQACgEAAgEIAAgIIAOgCIAAAKQAAAFgBAFQgCAFgDAEg");
	this.shape_32.setTransform(62.1,-3.1);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#FFFFFF").s().p("AgVAjIAAhEIAMAAIACAIQAEgFADgCQAFgCAGAAIAGAAIAFADIgBAQIgGgEIgHgBIgHACQgDACgBADQgDADAAAFIAAAog");
	this.shape_33.setTransform(59,-6.4);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#FFFFFF").s().p("AgMAhQgHgDgEgGQgEgGAAgJQAAgGACgDQACgDADgBQADgCAFAAIAZAAIAAgDQAAgEgCgDQgCgCgDgBIgGgBQgFAAgFACQgEACgEADIgIgLQAGgFAGgDQAHgDAHAAQAHAAAGADQAHADAEAFQAEAGAAAJIAAARQAAAJgEAGQgDAGgGAEQgHADgIAAQgGAAgGgDgAgIARQADAEAFAAQAGAAADgEQAEgEAAgGIgZAAQAAAGAEAEg");
	this.shape_34.setTransform(52.9,-6.4);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#FFFFFF").s().p("AgHA2IAAhrIAOAAIAABrg");
	this.shape_35.setTransform(47.9,-8.3);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#FFFFFF").s().p("AANAjIAAgqQAAgHgDgDQgDgDgGAAIgGACQgDACgCADQgCAEgBAEIAAAoIgPAAIAAhEIANAAIACAIQAEgFAFgCQAEgCAFAAQAGAAAFACQAGADADAGQAEAFAAAIIAAAtg");
	this.shape_36.setTransform(42.8,-6.4);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("#FFFFFF").s().p("AgNAhQgFgDgFgGQgEgGAAgJQAAgGACgDQACgDADgBQAEgCADAAIAaAAIAAgDQgBgEgBgDQgCgCgEgBIgFgBQgFAAgFACQgEACgEADIgHgLQAFgFAGgDQAHgDAHAAQAHAAAGADQAGADAFAFQAEAGAAAJIAAARQAAAJgEAGQgDAGgHAEQgFADgJAAQgGAAgHgDgAgIARQAEAEAEAAQAGAAAEgEQADgEAAgGIgZAAQAAAGAEAEg");
	this.shape_37.setTransform(36,-6.4);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("#FFFFFF").s().p("AgPAwQgHgCgFgFIAFgOQAEAEAFACQAGAEAGAAQAFAAAFgGQAEgEAAgJIAAgIQgEAFgEACQgFACgEABQgGAAgFgDQgGgDgDgGQgDgGAAgGIAAguIAOAAIAAArQAAAEACADQACACADABQADACADAAQACAAADgCQAEgCACgCQACgDAAgFIAAgpIAQAAIAABGQgBAJgEAIQgEAGgHAFQgHADgIAAQgHAAgGgDg");
	this.shape_38.setTransform(29,-4.7);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("#FFFFFF").s().p("AgMAhQgHgDgEgGQgEgGAAgJQAAgGACgDQACgDADgBQAEgCAEAAIAZAAIAAgDQAAgEgCgDQgCgCgDgBIgGgBQgFAAgFACQgEACgEADIgIgLQAGgFAGgDQAHgDAHAAQAHAAAGADQAHADAEAFQAEAGAAAJIAAARQAAAJgDAGQgEAGgGAEQgHADgIAAQgGAAgGgDgAgIARQADAEAFAAQAGAAADgEQADgEABgGIgZAAQAAAGAEAEg");
	this.shape_39.setTransform(22.2,-6.4);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f("#FFFFFF").s().p("AANAuQgGAAgFgCQgFgCgCgFQgDgFAAgIIAAghIgNAAIAAgPIANAAIAAgRIANgEIAAAVIAQAAIAAAPIgQAAIAAAfQABAFADACQADACADAAIADAAIADgBIAAAPIgEABIgEAAg");
	this.shape_40.setTransform(16.4,-7.4);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f("#FFFFFF").s().p("AgMAjIgKgCIABgPIAFABIAHACIAIAAQAEAAAEgBQADgCABgEQgBgDgEgCIgHgDIgLgDQgFgDgDgDQgEgEAAgHQAAgIADgEQAEgFAFgCQAFgCAHAAIALABIAMADIgCAOIgJgDIgMAAIgGABQgDABAAAEQAAADAEABIAIADIAKAEQAGACADADQAEAFgBAHQABAIgEAFQgEAFgGACQgGACgHAAIgLgBg");
	this.shape_41.setTransform(11.2,-6.4);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f("#FFFFFF").s().p("AgGA0IAAhEIANAAIAABEgAgGghQgDgDgBgFQABgEADgDQADgDADAAQAEAAADADQAEADAAAEQAAAFgEADQgDACgEABQgDgBgDgCg");
	this.shape_42.setTransform(6.9,-8.1);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f("#FFFFFF").s().p("AANAzIAAgoQgEAGgFABQgEADgFAAQgFAAgGgDQgFgDgEgGQgCgGAAgGIAAgWQgBgHAEgGQADgFAGgDQAGgDAGgBIAJACQAEACAEAEIACgGIAMAAIAABjgAgGgiQgDABgBAEQgCACgBAEIAAASQAAAEACACQACADACABIAHACQACAAAEgCQADgCACgDQACgDAAgFIAAgMQAAgEgCgEQgCgEgDgCIgGgBIgGABg");
	this.shape_43.setTransform(-2.3,-4.9);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f("#FFFFFF").s().p("AgOAhQgGgDgEgGQgEgGAAgIIAAgUQAAgHAEgGQAEgGAGgDQAFgDAGAAQAEAAAFACQAFACADAEIABgHIAOAAIAABFIgOAAIgCgIQgDAFgFACQgEACgEAAQgGAAgFgDgAgGgTQgDABgCADQgCADAAAEIAAARQABAEABADQACADADABIAFABQADAAADgCQAEgBACgEQACgDAAgFIAAgLQAAgFgCgDQgCgEgEgCIgGgBIgFABg");
	this.shape_44.setTransform(-9.3,-6.4);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f("#FFFFFF").s().p("AAdAjIAAgrQAAgGgDgDQgDgDgEAAIgGABIgEAEIgCAHIAAArIgOAAIAAgrQAAgGgDgDQgDgDgEAAIgGABIgEAEQgCADAAAEIAAArIgPAAIAAhEIANAAIACAIQACgFAEgCQAEgCAFAAQAFAAAEACQAEACACAFQAEgFAFgCQAFgCAFAAQAGAAAGADQAFADAEAGQADAFABAIIAAAsg");
	this.shape_45.setTransform(-18,-6.4);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f("#FFFFFF").s().p("AgHA2IAAhrIAPAAIAABrg");
	this.shape_46.setTransform(-24.8,-8.3);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f("#FFFFFF").s().p("AgPAhQgFgDgEgGQgDgGAAgIIAAgUQAAgHADgGQAEgGAFgDQAGgDAGAAQAFAAAEACQAFACADAEIABgHIAOAAIAABFIgOAAIgCgIQgDAFgFACQgEACgEAAQgGAAgGgDgAgGgTQgDABgCADQgCADAAAEIAAARQABAEABADQACADADABIAGABQACAAADgCQAEgBACgEQACgDAAgFIAAgLQAAgFgCgDQgCgEgEgCIgFgBIgGABg");
	this.shape_47.setTransform(-29.9,-6.4);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f("#FFFFFF").s().p("AgPAjQgDAAgDgCQgCgCgBgDQgCgCAAgEIABgEIACgEIAighIgiAAIAAgPIAlAAQAHAAACADQADAEAAAEIgBAGIgDAEIghAhIAmAAIAAAPg");
	this.shape_48.setTransform(-40.7,-6.4);

	this.shape_49 = new cjs.Shape();
	this.shape_49.graphics.f("#FFFFFF").s().p("AgPAjQgDAAgDgCQgCgCgCgDQgBgCAAgEIABgEIACgEIAhghIghAAIAAgPIAlAAQAHAAACADQADAEAAAEIgBAGIgDAEIggAhIAlAAIAAAPg");
	this.shape_49.setTransform(-46.9,-6.4);

	this.shape_50 = new cjs.Shape();
	this.shape_50.graphics.f("#FFFFFF").s().p("AgNAhQgFgDgFgGQgEgGAAgJQAAgGACgDQACgDADgBQADgCAEAAIAZAAIAAgDQABgEgCgDQgCgCgEgBIgFgBQgFAAgEACQgFACgEADIgHgLQAEgFAHgDQAHgDAHAAQAHAAAGADQAGADAFAFQAEAGAAAJIAAARQAAAJgDAGQgEAGgHAEQgFADgJAAQgGAAgHgDgAgIARQAEAEAEAAQAGAAAEgEQACgEAAgGIgYAAQAAAGAEAEg");
	this.shape_50.setTransform(-53.2,-6.4);

	this.shape_51 = new cjs.Shape();
	this.shape_51.graphics.f("#FFFFFF").s().p("AANA2IAAgsQAAgGgDgDQgDgCgGAAIgGABQgDABgCAEQgCADgBAFIAAApIgPAAIAAhrIAPAAIAAAuQAEgEAEgDQAFgCAEAAQAHAAAFADQAGADADAFQADAFABAIIAAAug");
	this.shape_51.setTransform(-60.2,-8.3);

	this.shape_52 = new cjs.Shape();
	this.shape_52.graphics.f("#FFFFFF").s().p("AAMA2IAAgUQAAgDgCgDIgFgEIgHADIgJAAIAAAbIgPAAIAAhrIAPAAIAABDQAIAAAFgDQAFgDACgFQADgEAAgGIAAgDIgBgDIAPgCIABAEIAAAEQAAAHgCAFQgDAHgFAEQAFADACAEQADAEAAAGIAAAVg");
	this.shape_52.setTransform(63.7,-24.3);

	this.shape_53 = new cjs.Shape();
	this.shape_53.graphics.f("#FFFFFF").s().p("AgHA0IAAhEIAPAAIAABEgAgHghQgCgDAAgFQAAgEACgDQADgDAEAAQAEAAAEADQACADAAAEQAAAFgCADQgEACgEABQgEgBgDgCg");
	this.shape_53.setTransform(58.8,-24.2);

	this.shape_54 = new cjs.Shape();
	this.shape_54.graphics.f("#FFFFFF").s().p("AAdAjIAAgrQAAgGgDgDQgDgDgEAAIgGABIgEAEIgCAHIAAArIgOAAIAAgrQAAgGgDgDQgDgDgEAAIgGABIgEAEQgCADAAAEIAAArIgPAAIAAhEIANAAIACAIQACgFAEgCQAEgCAFAAQAFAAAEACQAEACACAFQAEgFAFgCQAFgCAFAAQAGAAAGADQAFADAEAGQADAFABAIIAAAsg");
	this.shape_54.setTransform(52.2,-22.5);

	this.shape_55 = new cjs.Shape();
	this.shape_55.graphics.f("#FFFFFF").s().p("AgNAhQgGgDgEgGQgFgGAAgJIAAgRQAAgJAFgGQAEgGAGgDQAHgDAGAAQAHAAAHADQAGADAEAGQAEAGABAJIAAARQgBAJgEAGQgEAGgGADQgHADgHAAQgGAAgHgDgAgGgTQgDABgCADQgBADAAAEIAAARQAAAEABADIAFAEQADABADAAQADAAADgBIAFgEQACgDAAgEIAAgRQAAgEgCgDQgCgDgDgBQgDgBgDAAQgDAAgDABg");
	this.shape_55.setTransform(43.6,-22.4);

	this.shape_56 = new cjs.Shape();
	this.shape_56.graphics.f("#FFFFFF").s().p("AANAjIAAgqQAAgHgDgDQgDgDgGAAIgGACQgDACgCADQgCAEgBAEIAAAoIgOAAIAAhEIAMAAIACAIQAEgFAEgCQAFgCAEAAQAHAAAGACQAFADADAGQADAFABAIIAAAtg");
	this.shape_56.setTransform(36.7,-22.5);

	this.shape_57 = new cjs.Shape();
	this.shape_57.graphics.f("#FFFFFF").s().p("AgNAhQgGgDgEgGQgFgGAAgJIAAgRQAAgJAFgGQAEgGAGgDQAHgDAGAAQAHAAAHADQAGADAEAGQAEAGABAJIAAARQgBAJgEAGQgEAGgGADQgHADgHAAQgGAAgHgDgAgGgTQgDABgCADQgBADAAAEIAAARQAAAEABADIAFAEQADABADAAQADAAADgBIAFgEQACgDAAgEIAAgRQAAgEgCgDQgCgDgDgBQgDgBgDAAQgDAAgDABg");
	this.shape_57.setTransform(29.7,-22.4);

	this.shape_58 = new cjs.Shape();
	this.shape_58.graphics.f("#FFFFFF").s().p("AgVAjIAAhEIAMAAIADAIQADgFADgCQAGgCAFAAIAFAAIAGADIAAAQIgHgEIgHgBIgHACQgCACgCADQgCADgBAFIAAAog");
	this.shape_58.setTransform(23.8,-22.5);

	this.shape_59 = new cjs.Shape();
	this.shape_59.graphics.f("#FFFFFF").s().p("AAOAuQgHAAgFgCQgEgCgEgFQgDgFAAgIIAAghIgLAAIAAgPIALAAIAAgRIAPgEIAAAVIAPAAIAAAPIgPAAIAAAfQAAAFADACQACACAEAAIADAAIADgBIAAAPIgEABIgDAAg");
	this.shape_59.setTransform(18.5,-23.4);

	this.shape_60 = new cjs.Shape();
	this.shape_60.graphics.f("#FFFFFF").s().p("AgMAjIgLgCIACgPIAFABIAIACIAIAAQAEAAADgBQADgCABgEQgBgDgEgCIgHgDIgLgDQgFgDgEgDQgDgEAAgHQAAgIADgEQADgFAGgCQAGgCAGAAIALABIAMADIgCAOIgKgDIgKAAIgHABQgDABAAAEQAAADADABIAJADIAKAEQAFACAEADQAEAFAAAHQAAAIgEAFQgEAFgGACQgGACgGAAIgMgBg");
	this.shape_60.setTransform(13.3,-22.4);

	this.shape_61 = new cjs.Shape();
	this.shape_61.graphics.f("#FFFFFF").s().p("AgOAhQgGgDgEgGQgEgGAAgIIAAgUQAAgHAEgGQAEgGAGgDQAFgDAGAAQAEAAAFACQAFACADAEIABgHIAOAAIAABFIgOAAIgCgIQgDAFgFACQgEACgEAAQgGAAgFgDgAgGgTQgDABgCADQgCADAAAEIAAARQABAEABADQACADADABIAFABQADAAADgCQAEgBACgEQACgDAAgFIAAgLQAAgFgCgDQgCgEgEgCIgGgBIgFABg");
	this.shape_61.setTransform(6.9,-22.4);

	this.shape_62 = new cjs.Shape();
	this.shape_62.graphics.f("#FFFFFF").s().p("AANAyIAAgnQgEAFgEADQgFACgEAAQgHAAgFgDQgFgDgEgGQgDgFAAgIIAAgVQABgIADgFQAEgGAFgDQAFgDAHAAIAJACQAEACAEAEIACgHIAMAAIAABjgAgGgiQgDABgCADQgBADAAAEIAAASQgBAEACACQACADACABIAGABQADAAAEgCQADgBACgDQACgDAAgEIAAgNQAAgEgCgEQgCgDgDgCIgHgCIgFABg");
	this.shape_62.setTransform(-0.2,-20.9);

	this.shape_63 = new cjs.Shape();
	this.shape_63.graphics.f("#FFFFFF").s().p("AAOAjIAAgqQgBgHgDgDQgEgDgFAAIgGACQgDACgCADQgCAEAAAEIAAAoIgQAAIAAhEIANAAIACAIQAEgFAFgCQAEgCAFAAQAGAAAFACQAGADADAGQAEAFAAAIIAAAtg");
	this.shape_63.setTransform(-11.3,-22.5);

	this.shape_64 = new cjs.Shape();
	this.shape_64.graphics.f("#FFFFFF").s().p("AgNAhQgFgDgFgGQgEgGAAgJQAAgGACgDQACgDADgBQADgCAEAAIAZAAIAAgDQABgEgCgDQgCgCgEgBIgFgBQgFAAgEACQgFACgEADIgHgLQAEgFAHgDQAHgDAHAAQAHAAAGADQAGADAFAFQAEAGAAAJIAAARQAAAJgDAGQgEAGgHAEQgFADgJAAQgGAAgHgDgAgIARQAEAEAEAAQAGAAAEgEQACgEAAgGIgYAAQAAAGAEAEg");
	this.shape_64.setTransform(-18.1,-22.4);

	this.shape_65 = new cjs.Shape();
	this.shape_65.graphics.f("#FFFFFF").s().p("AgPAzQgGgCgDgGQgDgGAAgIIAAgWQAAgHADgFQADgFAGgDQAGgDAGAAIAJABQAEACAEAFIAAgtIAOAAIAABqIgNAAIgBgHQgFAEgEACQgFACgEAAQgGAAgFgDgAgGgBQgDABgBADQgCACAAAFIAAARQAAAFABACQACADACABIAHACQACAAAEgCQADgCACgDQACgEAAgEIAAgNQAAgEgCgEQgCgDgDgBIgGgCIgGABg");
	this.shape_65.setTransform(-25.1,-24.3);

	this.shape_66 = new cjs.Shape();
	this.shape_66.graphics.f("#FFFFFF").s().p("AAMA2IAAgUQAAgDgCgDIgFgEIgHADIgJAAIAAAbIgPAAIAAhrIAPAAIAABDQAIAAAFgDQAFgDACgFQADgEAAgGIAAgDIgBgDIAPgCIABAEIAAAEQAAAHgCAFQgDAHgFAEQAFADACAEQADAEAAAGIAAAVg");
	this.shape_66.setTransform(-31.8,-24.3);

	this.shape_67 = new cjs.Shape();
	this.shape_67.graphics.f("#FFFFFF").s().p("AgMAhQgHgDgEgGQgEgGAAgJQAAgGACgDQACgDADgBQADgCAFAAIAZAAIAAgDQAAgEgCgDQgCgCgDgBIgGgBQgFAAgFACQgEACgEADIgIgLQAGgFAGgDQAHgDAHAAQAHAAAGADQAHADAEAFQAEAGAAAJIAAARQAAAJgEAGQgDAGgGAEQgHADgIAAQgGAAgGgDgAgIARQADAEAFAAQAGAAADgEQAEgEAAgGIgZAAQAAAGAEAEg");
	this.shape_67.setTransform(-38.4,-22.4);

	this.shape_68 = new cjs.Shape();
	this.shape_68.graphics.f("#FFFFFF").s().p("AAdAjIAAgrQAAgGgDgDQgDgDgEAAIgGABIgEAEIgCAHIAAArIgOAAIAAgrQAAgGgDgDQgDgDgEAAIgGABIgEAEQgCADAAAEIAAArIgPAAIAAhEIANAAIACAIQACgFAEgCQAEgCAFAAQAFAAAEACQAEACACAFQAEgFAFgCQAFgCAFAAQAGAAAGADQAFADAEAGQADAFABAIIAAAsg");
	this.shape_68.setTransform(-47.1,-22.5);

	this.shape_69 = new cjs.Shape();
	this.shape_69.graphics.f("#FFFFFF").s().p("AgMAhQgHgDgEgGQgEgFAAgJIAAgRQAAgJAEgGQADgHAGgDQAHgDAHAAQAHAAAGADQAHADADAGQAFAGAAAJQAAAFgCADQgCADgDACQgEACgDAAIgaAAIAAACQAAAFACACQACADADABIAGABQAFAAAFgCIAHgGIAIAMQgFAFgHADQgGADgIAAQgGAAgGgDgAANgGQgBgHgDgDQgEgEgFAAQgFAAgEAEQgDADAAAHIAZAAIAAAAg");
	this.shape_69.setTransform(-55.4,-22.4);

	this.shape_70 = new cjs.Shape();
	this.shape_70.graphics.f("#FFFFFF").s().p("AgHAyIAAgpQgJgCgHgFQgHgEgEgHQgEgHAAgJIAAgYIAPAAIAAAVQAAALAGAGQAHAHAKAAQALAAAHgHQAFgGABgLIAAgVIAPAAIAAAYQAAAJgEAHQgEAHgHAEQgHAFgJACIAAApg");
	this.shape_70.setTransform(-62.6,-24);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_70},{t:this.shape_69},{t:this.shape_68},{t:this.shape_67},{t:this.shape_66},{t:this.shape_65},{t:this.shape_64},{t:this.shape_63},{t:this.shape_62},{t:this.shape_61},{t:this.shape_60},{t:this.shape_59},{t:this.shape_58},{t:this.shape_57},{t:this.shape_56},{t:this.shape_55},{t:this.shape_54},{t:this.shape_53},{t:this.shape_52},{t:this.shape_51},{t:this.shape_50},{t:this.shape_49},{t:this.shape_48},{t:this.shape_47},{t:this.shape_46},{t:this.shape_45},{t:this.shape_44},{t:this.shape_43},{t:this.shape_42},{t:this.shape_41},{t:this.shape_40},{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-68.3,-33.9,136.7,68);


(lib.Tween29 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib._3150();
	this.instance.parent = this;
	this.instance.setTransform(-150,-186);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-150,-186,300,372);


(lib.Tween28 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib._3150();
	this.instance.parent = this;
	this.instance.setTransform(-150,-186);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-150,-186,300,372);


(lib.Tween27 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#050601").s().p("Ar1GkIAAtHIXrAAIAANHg");
	this.shape.setTransform(0,-1.4);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-75.7,-43.4,151.6,84.1);


(lib.Tween26 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#050601").s().p("Ar1GWIAAsrIXrAAIAAMrg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-75.7,-40.5,151.6,81.2);


(lib.Tween25 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib._1150();
	this.instance.parent = this;
	this.instance.setTransform(-150,-186);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-150,-186,300,372);


(lib.Tween24 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib._1150();
	this.instance.parent = this;
	this.instance.setTransform(-150,-186);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-150,-186,300,372);


(lib.Tween23 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgRAzQgIgEgGgIQgFgIAAgMIAAgmQAAgLAFgIQAGgIAIgEQAJgEAIAAQAJAAAJAEQAIAEAGAIQAFAIAAALIAAAmQAAAMgFAIQgGAIgIAEQgJAEgJAAQgIAAgJgEgAgIgjQgEACgDAEQgDAFAAAFIAAAmQAAAGADAFQADAEAEACQAEACAEAAQAFAAAEgCQAEgCADgEQADgFAAgGIAAgmQAAgFgDgFQgDgEgEgCQgEgCgFAAQgEAAgEACg");
	this.shape.setTransform(42.1,112.3);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgRAzQgIgEgGgIQgFgIAAgMIAAgmQAAgLAFgIQAGgIAIgEQAJgEAIAAQAJAAAJAEQAIAEAGAIQAFAIAAALIAAAmQAAAMgFAIQgGAIgIAEQgJAEgJAAQgIAAgJgEgAgIgjQgEACgDAEQgDAFAAAFIAAAmQAAAGADAFQADAEAEACQAEACAEAAQAFAAAEgCQAEgCADgEQADgFAAgGIAAgmQAAgFgDgFQgDgEgEgCQgEgCgFAAQgEAAgEACg");
	this.shape_1.setTransform(33.4,112.3);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgHAkQgEgEAAgFQAAgEAEgEQADgEAEABQAFgBADAEQAEAEAAAEQAAAFgEAEQgDADgFAAQgEAAgDgDgAgHgSQgEgEAAgEQAAgFAEgEQADgDAEAAQAFAAADADQAEAEAAAFQAAAEgEAEQgDADgFAAQgEAAgDgDg");
	this.shape_2.setTransform(27.3,113.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AAFA3IAAgXIgdAAQgGAAgEgDQgEgDAAgGIAAgFIACgEIAdg3QACgGAEgCQAEgCAEABQAEAAAEABQAEACACADQABADAAAFIAAA2IARAAIAAARIgRAAIAAAXgAgTAPIAYAAIAAgxg");
	this.shape_3.setTransform(21.2,112.2);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AgYA3QgGgBgBgEQgDgDAAgGIABgFIAFgGIAWgUQAKgKAFgHQAHgIAAgHQAAgHgFgDQgDgEgHAAQgEAAgFADQgFACgEAHIgNgMQAHgJAIgFQAJgDAHAAQAKAAAHADQAIAEADAHQAEAHABAJQgBAMgGAJQgGAKgKAKIgUATIArAAIAAASg");
	this.shape_4.setTransform(12.3,112.3);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AgbAJIAAgRIA3AAIAAARg");
	this.shape_5.setTransform(-0.3,113.1);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AgRAzQgIgEgGgIQgFgIAAgMIAAgmQAAgLAFgIQAGgIAIgEQAJgEAIAAQAJAAAJAEQAIAEAGAIQAFAIAAALIAAAmQAAAMgFAIQgGAIgIAEQgJAEgJAAQgIAAgJgEgAgIgjQgEACgDAEQgDAFAAAFIAAAmQAAAGADAFQADAEAEACQAEACAEAAQAFAAAEgCQAEgCADgEQADgFAAgGIAAgmQAAgFgDgFQgDgEgEgCQgEgCgFAAQgEAAgEACg");
	this.shape_6.setTransform(-13,112.3);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AgRAzQgIgEgGgIQgFgIAAgMIAAgmQAAgLAFgIQAGgIAIgEQAJgEAIAAQAJAAAJAEQAIAEAGAIQAFAIAAALIAAAmQAAAMgFAIQgGAIgIAEQgJAEgJAAQgIAAgJgEgAgIgjQgEACgDAEQgDAFAAAFIAAAmQAAAGADAFQADAEAEACQAEACAEAAQAFAAAEgCQAEgCADgEQADgFAAgGIAAgmQAAgFgDgFQgDgEgEgCQgEgCgFAAQgEAAgEACg");
	this.shape_7.setTransform(-21.7,112.3);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AgHAkQgEgEAAgFQAAgEAEgEQADgEAEABQAFgBADAEQAEAEAAAEQAAAFgEAEQgDADgFAAQgEAAgDgDgAgHgSQgEgEAAgEQAAgFAEgEQADgDAEAAQAFAAADADQAEAEAAAFQAAAEgEAEQgDADgFAAQgEAAgDgDg");
	this.shape_8.setTransform(-27.8,113.8);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AgRAzQgIgEgGgIQgFgIAAgMIAAgmQAAgLAFgIQAGgIAIgEQAJgEAIAAQAJAAAJAEQAIAEAGAIQAFAIAAALIAAAmQAAAMgFAIQgGAIgIAEQgJAEgJAAQgIAAgJgEgAgIgjQgEACgDAEQgDAFAAAFIAAAmQAAAGADAFQADAEAEACQAEACAEAAQAFAAAEgCQAEgCADgEQADgFAAgGIAAgmQAAgFgDgFQgDgEgEgCQgEgCgFAAQgEAAgEACg");
	this.shape_9.setTransform(-34,112.3);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AAGA3IAAhXIgPASIgPgMIAYgXQABgDADAAIAHgBQAFAAAEADQAFAEgBAIIAABdg");
	this.shape_10.setTransform(-43.4,112.3);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AgGAjQgDgCAAgEQAAgFADgCQADgDADgBQAEABADADQADACAAAFQAAAEgDACQgDADgEAAQgDAAgDgDgAgGgVQgDgCAAgEQAAgFADgCQADgCADgBQAEABADACQADACAAAFQAAAEgDACQgDADgEAAQgDAAgDgDg");
	this.shape_11.setTransform(26.8,96.2);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AATAqIAAg2QAAgKgEgEQgEgFgHAAQgFAAgFAEQgFADgEAFQgDAGgBAHIAAAwIgLAAIAAhSIAKAAIABAMQAFgHAHgDQAGgDAGAAQAHAAAGADQAGADADAHQADAGABAKIAAA2g");
	this.shape_12.setTransform(21,95.8);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("AgRA5QgGgDgEgHQgDgGAAgKIAAg2IALAAIAAA2QAAAHADAEQACAEADACQAFACADAAQAFAAAFgDQAGgDACgFQAEgFAAgHIAAgyIAMAAIAABSIgKAAIgCgKQgFAGgGACQgHADgFAAQgHAAgGgDgAAIgrQgCgDgBgEQABgEACgDQADgCAEAAQAEAAADACQADADgBAEQABAEgDADQgDACgEABQgEgBgDgCgAgWgrQgCgDAAgEQAAgEACgDQADgCAFAAQAEAAACACQACADABAEQgBAEgCADQgCACgEABQgFgBgDgCg");
	this.shape_13.setTransform(12.6,94.1);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FFFFFF").s().p("AgOA7QgGgDgGgFIAFgJQAFAEAFACQAGADAFAAQAEAAAFgCQAFgCACgFQADgEAAgHIAAgVQgEAHgGAEQgGADgFAAQgIAAgGgDQgGgDgEgHQgEgGAAgHIAAgeQAAgIAEgHQAEgGAGgEQAGgDAIAAQAFAAAGADQAFACAFAGIABgKIALAAIAABaQgBAIgEAHQgDAHgHAEQgIAEgIAAQgHAAgHgCgAgKgwQgEACgCAEQgCAEgBAGIAAAdQAAAJAGAEQAEAEAHAAQAEAAAFgDQAFgDAEgFQADgEAAgHIAAgVQAAgGgDgFQgEgEgFgDQgEgDgFAAQgEAAgEACg");
	this.shape_14.setTransform(4.2,97.7);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FFFFFF").s().p("AgWAqIAAhSIALAAIABAKQAEgFAGgDQAGgDAGAAIAGAAIAEACIAAALIgFgCIgFgBQgGAAgFADQgFADgDAFQgDAFAAAIIAAAxg");
	this.shape_15.setTransform(-7.5,95.8);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FFFFFF").s().p("AgOAnQgHgEgFgGQgEgIgBgKQABgHADgEQAEgEAHAAIAkAAIAAgHQAAgHgDgEQgCgFgFgCQgFgDgFABQgGAAgFADQgGADgEAHIgIgHQAFgHAHgFQAIgFAJABQAJAAAHADQAHAFAEAGQAFAHAAAIIAAAYQAAAKgFAHQgEAHgHAEQgIACgIAAQgHABgHgEgAgSAHIgBAFQAAAGADAFQADAEAEADQAFACAEAAQAFAAAFgCQAEgDADgEQADgFAAgGIAAgHIgjAAQgBAAAAABQAAAAgBAAQAAAAAAABQgBAAAAAAg");
	this.shape_16.setTransform(-14.6,95.9);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#FFFFFF").s().p("AAeA8IAAg5Ig6AAIAAA5IgMAAIAAh3IAMAAIAAA0IA6AAIAAg0IAMAAIAAB3g");
	this.shape_17.setTransform(-23.7,94);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer 2
	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#990000").s().p("AnIDYQhRAAAAhRIAAkMQAAhRBRAAIORAAQBQAAAABRIAAEMQAABRhQAAg");
	this.shape_18.setTransform(-0.2,103.2);

	this.timeline.addTween(cjs.Tween.get(this.shape_18).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-54,81.6,107.5,43.1);


(lib.Tween20 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AAEAzIAAgVIgaAAQgGAAgEgDQgDgDgBgGIABgEIACgEIAag0QADgFAEgCQACgCAFABIAHABQADABACADQACADAAAFIAAAzIAQAAIAAAQIgQAAIAAAVgAgSAOIAWAAIAAgug");
	this.shape.setTransform(58.6,56.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgQAwQgIgEgFgHQgFgIAAgLIAAgjQAAgLAFgIQAFgHAIgEQAIgEAIAAQAJAAAIAEQAIAEAFAHQAFAIAAALIAAAjQAAALgFAIQgFAHgIAEQgIAEgJAAQgIAAgIgEgAgIghQgEACgCAEQgDAEAAAGIAAAjQAAAGADAEQACAEAEACQAEACAEAAQAEAAAEgCQAEgCADgEQADgEAAgGIAAgjQAAgGgDgEQgDgEgEgCQgEgCgEAAQgEAAgEACg");
	this.shape_1.setTransform(50.4,56.2);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AAEAzIAAgVIgaAAQgGAAgEgDQgDgDgBgGIABgEIACgEIAag0QADgFAEgCQACgCAFABIAHABQADABACADQACADAAAFIAAAzIAQAAIAAAQIgQAAIAAAVgAgSAOIAWAAIAAgug");
	this.shape_2.setTransform(37.4,56.2);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgQAwQgIgEgFgHQgFgIAAgLIAAgjQAAgLAFgIQAFgHAIgEQAIgEAIAAQAJAAAIAEQAIAEAFAHQAFAIAAALIAAAjQAAALgFAIQgFAHgIAEQgIAEgJAAQgIAAgIgEgAgIghQgEACgCAEQgDAEAAAGIAAAjQAAAGADAEQACAEAEACQAEACAEAAQAEAAAEgCQAEgCADgEQADgEAAgGIAAgjQAAgGgDgEQgDgEgEgCQgEgCgEAAQgEAAgEACg");
	this.shape_3.setTransform(29.2,56.2);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AAGAzIAAhQIgPAQIgNgMIAWgVIAEgDIAGgBQAFAAAEADQAEAEAAAHIAABXg");
	this.shape_4.setTransform(15.6,56.2);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AgaAzIAohVIgtAAIAAgQIAxAAQAHAAADAEQAEAEAAAFIgBAFIgCAFIgjBOg");
	this.shape_5.setTransform(8.4,56.2);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AgTAwQgIgFgFgIIAMgLQADAFAFADQAFADAHABQAEAAADgCQAEgCACgDQADgEAAgFQAAgDgCgDQgCgDgEgCQgDgCgFAAIgIAAIAAgPIAIAAQAEAAADgDQADgCABgDQACgDAAgDIgBgGIgEgFQgDgCgEAAQgFAAgGADQgFAEgEAFIgJgOQAGgHAIgDQAHgEAHAAQAIAAAHADQAGAEAFAGQAEAGAAAJQAAAGgDAGQgDAGgHAEQAJAEADAHQAEAGAAAHQAAAJgFAHQgFAGgHADQgIAEgIAAQgLAAgIgEg");
	this.shape_6.setTransform(-0.2,56.2);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AgWAzIAshbIg0AAIAAgKIAyAAQAGAAADADQACADAAAEIgBAFIgBAFIgmBRg");
	this.shape_7.setTransform(-13,56.2);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AgWAzIAshbIg0AAIAAgKIAyAAQAGAAADADQACADAAAEIgBAFIgBAFIgmBRg");
	this.shape_8.setTransform(-21.3,56.2);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AAJAzIAAgXIgiAAQgEAAgDgEQgDgDAAgEIABgDIAAgCIAhg5IADgEIAHgBQAEAAADACQADADAAAFIAAA6IARAAIAAAKIgRAAIAAAXgAgZASIAiAAIAAg7g");
	this.shape_9.setTransform(-34.3,56.1);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AgTAxQAKgHAHgHIAMgOIAJgPQgFAEgFACQgGADgEAAQgKgBgHgEQgHgFgEgHQgDgHAAgJQAAgIAEgIQADgHAIgFQAHgFAKAAQAKAAAIAFQAHAEAEAIQAEAHAAAJQAAAMgDAKQgEAKgGAKQgHAJgHAHQgGAHgIAFgAgKgmQgFACgDAFQgDAGAAAHQAAAIADAFQADAFAFACQAFADAFAAQAGAAAFgDQAFgCADgFQADgFABgIQgBgHgDgGQgDgFgFgCQgFgDgGAAQgEAAgGADg");
	this.shape_10.setTransform(-42.7,56.2);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AgTAxQAKgHAHgHIAMgOIAJgPQgFAEgFACQgGADgEAAQgKgBgHgEQgHgFgEgHQgDgHAAgJQAAgIAEgIQADgHAIgFQAHgFAKAAQAKAAAIAFQAHAEAEAIQAEAHAAAJQAAAMgDAKQgEAKgGAKQgHAJgHAHQgGAHgIAFgAgKgmQgFACgDAFQgDAGAAAHQAAAIADAFQADAFAFACQAFADAFAAQAGAAAFgDQAFgCADgFQADgFABgIQgBgHgDgGQgDgFgFgCQgFgDgGAAQgEAAgGADg");
	this.shape_11.setTransform(-51,56.2);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AgEAfIAAgaIgaAAIAAgJIAaAAIAAgaIAKAAIAAAaIAZAAIAAAJIgZAAIAAAag");
	this.shape_12.setTransform(-58.9,56);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("AAEAzIAAgVIgaAAQgGAAgEgDQgDgDgBgGIABgEIACgEIAag0QADgFAEgCQACgCAFABIAHABQADABACADQACADAAAFIAAAzIAQAAIAAAQIgQAAIAAAVgAgSAOIAWAAIAAgug");
	this.shape_13.setTransform(58.6,39.2);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FFFFFF").s().p("AgQAwQgIgEgFgHQgFgIAAgLIAAgjQAAgLAFgIQAFgHAIgEQAIgEAIAAQAJAAAIAEQAIAEAFAHQAFAIAAALIAAAjQAAALgFAIQgFAHgIAEQgIAEgJAAQgIAAgIgEgAgIghQgEACgCAEQgDAEAAAGIAAAjQAAAGADAEQACAEAEACQAEACAEAAQAEAAAEgCQAEgCADgEQADgEAAgGIAAgjQAAgGgDgEQgDgEgEgCQgEgCgEAAQgEAAgEACg");
	this.shape_14.setTransform(50.3,39.3);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FFFFFF").s().p("AAEAzIAAgVIgaAAQgGAAgEgDQgDgDgBgGIABgEIACgEIAag0QADgFAEgCQACgCAFABIAHABQADABACADQACADAAAFIAAAzIAQAAIAAAQIgQAAIAAAVgAgSAOIAWAAIAAgug");
	this.shape_15.setTransform(37.4,39.2);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FFFFFF").s().p("AgQAwQgIgEgFgHQgFgIAAgLIAAgjQAAgLAFgIQAFgHAIgEQAIgEAIAAQAJAAAIAEQAIAEAFAHQAFAIAAALIAAAjQAAALgFAIQgFAHgIAEQgIAEgJAAQgIAAgIgEgAgIghQgEACgCAEQgDAEAAAGIAAAjQAAAGADAEQACAEAEACQAEACAEAAQAEAAAEgCQAEgCADgEQADgEAAgGIAAgjQAAgGgDgEQgDgEgEgCQgEgCgEAAQgEAAgEACg");
	this.shape_16.setTransform(29.2,39.3);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#FFFFFF").s().p("AAEAzIAAgVIgaAAQgGAAgEgDQgDgDgBgGIABgEIACgEIAag0QADgFAEgCQACgCAFABIAHABQADABACADQACADAAAFIAAAzIAQAAIAAAQIgQAAIAAAVgAgSAOIAWAAIAAgug");
	this.shape_17.setTransform(16.3,39.2);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#FFFFFF").s().p("AAEAzIAAgVIgaAAQgGAAgEgDQgDgDgBgGIABgEIACgEIAag0QADgFAEgCQACgCAFABIAHABQADABACADQACADAAAFIAAAzIAQAAIAAAQIgQAAIAAAVgAgSAOIAWAAIAAgug");
	this.shape_18.setTransform(8.1,39.2);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#FFFFFF").s().p("AgTAwQgIgFgFgIIAMgLQADAFAFADQAFADAHABQAEAAADgCQAEgCACgDQADgEAAgFQAAgDgCgDQgCgDgEgCQgDgCgFAAIgIAAIAAgPIAIAAQAEAAADgDQADgCABgDQACgDAAgDIgBgGIgEgFQgDgCgEAAQgFAAgGADQgFAEgEAFIgJgOQAGgHAIgDQAHgEAHAAQAIAAAHADQAGAEAFAGQAEAGAAAJQAAAGgDAGQgDAGgHAEQAJAEADAHQAEAGAAAHQAAAJgFAHQgFAGgHADQgIAEgIAAQgLAAgIgEg");
	this.shape_19.setTransform(-0.3,39.3);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#FFFFFF").s().p("AAJAzIAAhbIgUAVIgJgHIAVgVQACgCADAAIAEgBQAEAAADADQADADABAGIAABZg");
	this.shape_20.setTransform(-13.7,39.2);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#FFFFFF").s().p("AgSAyIgMgFIACgKIABAAQAFADAHACIANABQAGABAEgDQAGgCADgFQAEgFgBgHQAAgGgCgEQgDgEgEgDQgGgCgGAAIgPAAQgEgBgEgCQgCgDAAgFIAAgCIAAgCIADgSQABgFACgEQACgEAEgDQAFgCAFAAIAfAAIAAAKIgdAAQgFAAgCABQgCACgBAEIgCAGIgDATIARAAQAKAAAHAEQAIADADAGQAEAHAAAIQgBALgFAHQgEAHgIADQgJADgHAAIgQgBg");
	this.shape_21.setTransform(-21.5,39.3);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#FFFFFF").s().p("AAIAzIAAgXIghAAQgFAAgCgEQgDgDAAgEIAAgDIACgCIAgg5IAEgEIAFgBQAEAAAEACQAEADAAAFIAAA6IAQAAIAAAKIgQAAIAAAXgAgYASIAgAAIAAg7g");
	this.shape_22.setTransform(-34.3,39.2);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#FFFFFF").s().p("AgTAxQAKgHAHgHIAMgOIAJgPQgFAEgFACQgGADgEAAQgKgBgHgEQgHgFgEgHQgDgHAAgJQAAgIAEgIQADgHAIgFQAHgFAKAAQAKAAAIAFQAHAEAEAIQAEAHAAAJQAAAMgDAKQgEAKgGAKQgHAJgHAHQgGAHgIAFgAgKgmQgFACgDAFQgDAGAAAHQAAAIADAFQADAFAFACQAFADAFAAQAGAAAFgDQAFgCADgFQADgFABgIQgBgHgDgGQgDgFgFgCQgFgDgGAAQgEAAgGADg");
	this.shape_23.setTransform(-42.7,39.2);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#FFFFFF").s().p("AgTAxQAKgHAHgHIAMgOIAJgPQgFAEgFACQgGADgEAAQgKgBgHgEQgHgFgEgHQgDgHAAgJQAAgIAEgIQADgHAIgFQAHgFAKAAQAKAAAIAFQAHAEAEAIQAEAHAAAJQAAAMgDAKQgEAKgGAKQgHAJgHAHQgGAHgIAFgAgKgmQgFACgDAFQgDAGAAAHQAAAIADAFQADAFAFACQAFADAFAAQAGAAAFgDQAFgCADgFQADgFABgIQgBgHgDgGQgDgFgFgCQgFgDgGAAQgEAAgGADg");
	this.shape_24.setTransform(-51,39.2);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#FFFFFF").s().p("AgEAfIAAgaIgaAAIAAgJIAaAAIAAgaIAKAAIAAAaIAZAAIAAAJIgZAAIAAAag");
	this.shape_25.setTransform(-58.9,39.1);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#FFFFFF").s().p("AAEAzIAAgVIgaAAQgGAAgEgDQgDgDgBgGIABgEIACgEIAag0QADgFAEgCQACgCAFABIAHABQADABACADQACADAAAFIAAAzIAQAAIAAAQIgQAAIAAAVgAgSAOIAWAAIAAgug");
	this.shape_26.setTransform(58.6,22.3);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#FFFFFF").s().p("AgQAwQgIgEgFgHQgFgIAAgLIAAgjQAAgLAFgIQAFgHAIgEQAIgEAIAAQAJAAAIAEQAIAEAFAHQAFAIAAALIAAAjQAAALgFAIQgFAHgIAEQgIAEgJAAQgIAAgIgEgAgIghQgEACgCAEQgDAEAAAGIAAAjQAAAGADAEQACAEAEACQAEACAEAAQAEAAAEgCQAEgCADgEQADgEAAgGIAAgjQAAgGgDgEQgDgEgEgCQgEgCgEAAQgEAAgEACg");
	this.shape_27.setTransform(50.3,22.3);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#FFFFFF").s().p("AAEAzIAAgVIgaAAQgGAAgEgDQgDgDgBgGIABgEIACgEIAag0QADgFAEgCQACgCAFABIAHABQADABACADQACADAAAFIAAAzIAQAAIAAAQIgQAAIAAAVgAgSAOIAWAAIAAgug");
	this.shape_28.setTransform(37.4,22.3);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#FFFFFF").s().p("AgQAwQgIgEgFgHQgFgIAAgLIAAgjQAAgLAFgIQAFgHAIgEQAIgEAIAAQAJAAAIAEQAIAEAFAHQAFAIAAALIAAAjQAAALgFAIQgFAHgIAEQgIAEgJAAQgIAAgIgEgAgIghQgEACgCAEQgDAEAAAGIAAAjQAAAGADAEQACAEAEACQAEACAEAAQAEAAAEgCQAEgCADgEQADgEAAgGIAAgjQAAgGgDgEQgDgEgEgCQgEgCgEAAQgEAAgEACg");
	this.shape_29.setTransform(29.2,22.3);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#FFFFFF").s().p("AgQAyQgHgBgHgEIAEgQIALAEQAGACAGAAQAHAAAFgEQAFgEAAgJQAAgFgEgEQgFgEgIAAIgNAAQgFAAgDgCQgDgCgBgCIgBgHIAAgCIAAgCIACgTIAEgJQADgEAEgDQAFgDAGAAIAgAAIAAAQIgeAAQgEAAgCACIgCAHIgBAPIALAAQAKAAAIAEQAHAEAEAGQADAGAAAIQAAALgEAHQgFAHgIAEQgIAEgJAAIgNgBg");
	this.shape_30.setTransform(16.3,22.4);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#FFFFFF").s().p("AgaAzIAohVIgtAAIAAgQIAxAAQAHAAADAEQAEAEAAAFIgBAFIgCAFIgjBOg");
	this.shape_31.setTransform(8.4,22.3);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#FFFFFF").s().p("AgTAwQgIgFgFgIIAMgLQADAFAFADQAFADAHABQAEAAADgCQAEgCACgDQADgEAAgFQAAgDgCgDQgCgDgEgCQgDgCgFAAIgIAAIAAgPIAIAAQAEAAADgDQADgCABgDQACgDAAgDIgBgGIgEgFQgDgCgEAAQgFAAgGADQgFAEgEAFIgJgOQAGgHAIgDQAHgEAHAAQAIAAAHADQAGAEAFAGQAEAGAAAJQAAAGgDAGQgDAGgHAEQAJAEADAHQAEAGAAAHQAAAJgFAHQgFAGgHADQgIAEgIAAQgLAAgIgEg");
	this.shape_32.setTransform(-0.3,22.3);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#FFFFFF").s().p("AgXAzQgEAAgDgDQgBgDAAgEIABgEIACgEIAZgVQAJgKAGgIQAGgIAAgHQAAgHgDgEQgCgEgEgCQgEgDgFAAQgFAAgGADQgFADgFAGIgHgIQAGgHAHgDQAIgDAHAAQAHAAAHADQAGADAEAHQAEAGABAKQAAAHgFAIQgEAHgHAIIgQAQIgSAQIA1AAIAAAKg");
	this.shape_33.setTransform(-13.1,22.2);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#FFFFFF").s().p("AAJAzIAAhbIgUAVIgIgHIAUgVQACgCADAAIAEgBQAFAAACADQADADAAAGIAABZg");
	this.shape_34.setTransform(-22,22.2);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#FFFFFF").s().p("AAIAzIAAgXIghAAQgFAAgCgEQgDgDAAgEIAAgDIACgCIAgg5IAEgEIAFgBQAEAAAEACQAEADAAAFIAAA6IAQAAIAAAKIgQAAIAAAXgAgYASIAgAAIAAg7g");
	this.shape_35.setTransform(-34.3,22.2);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#FFFFFF").s().p("AgTAxQAKgHAHgHIAMgOIAJgPQgFAEgFACQgGADgEAAQgKgBgHgEQgHgFgEgHQgDgHAAgJQAAgIAEgIQADgHAIgFQAHgFAKAAQAKAAAIAFQAHAEAEAIQAEAHAAAJQAAAMgDAKQgEAKgGAKQgHAJgHAHQgGAHgIAFgAgKgmQgFACgDAFQgDAGAAAHQAAAIADAFQADAFAFACQAFADAFAAQAGAAAFgDQAFgCADgFQADgFABgIQgBgHgDgGQgDgFgFgCQgFgDgGAAQgEAAgGADg");
	this.shape_36.setTransform(-42.7,22.3);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("#FFFFFF").s().p("AgTAxQAKgHAHgHIAMgOIAJgPQgFAEgFACQgGADgEAAQgKgBgHgEQgHgFgEgHQgDgHAAgJQAAgIAEgIQADgHAIgFQAHgFAKAAQAKAAAIAFQAHAEAEAIQAEAHAAAJQAAAMgDAKQgEAKgGAKQgHAJgHAHQgGAHgIAFgAgKgmQgFACgDAFQgDAGAAAHQAAAIADAFQADAFAFACQAFADAFAAQAGAAAFgDQAFgCADgFQADgFABgIQgBgHgDgGQgDgFgFgCQgFgDgGAAQgEAAgGADg");
	this.shape_37.setTransform(-51,22.3);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("#FFFFFF").s().p("AgEAfIAAgaIgaAAIAAgJIAaAAIAAgaIAKAAIAAAaIAZAAIAAAJIgZAAIAAAag");
	this.shape_38.setTransform(-58.9,22.1);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("#FFFFFF").s().p("AgPArQgHgEgEgHQgDgGAAgJQAAgLADgIQAEgKAFgIQAGgJAHgGIAMgLIAKACQgJAHgHAGQgFAGgFAHIgIAOQAFgEAEgCQAFgCAEAAQAKAAAFAEQAHAEADAGQADAIAAAHQAAAHgEAIQgCAGgIAFQgGAEgKAAQgJAAgGgEgAgJgBQgEACgDAEQgDAFAAAHQAAAHACAEQADAGAFACQAFACAEAAQAFAAAEgCQAFgCADgGQADgEAAgHQAAgHgDgFQgDgEgEgCQgFgCgFAAQgEAAgFACg");
	this.shape_39.setTransform(63.2,1);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f("#FFFFFF").s().p("AAJAuIAAhSIgUATIgHgGIATgTIAEgDIAEAAQAEgBADADQACADABAFIAABRg");
	this.shape_40.setTransform(55.1,0.9);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f("#FFFFFF").s().p("AgUAvQgFgBgBgDQgCgCAAgDIABgFIACgDIAWgTQAIgJAGgHQAFgIABgGQgBgGgCgEQgCgDgEgCQgDgDgFAAQgEAAgFADQgFADgEAFIgHgHQAFgGAHgDQAGgEAHAAQAGAAAGAEQAHACADAHQAEAFAAAJQAAAHgEAHQgEAGgHAHIgNAPIgQAPIAvAAIAAAJg");
	this.shape_41.setTransform(48.1,0.9);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f("#FFFFFF").s().p("AgHAOIAFgKQACgEAAgGIAAgHIAJgBIAAAJQgBAHgBAFQgBAFgEADg");
	this.shape_42.setTransform(38.6,5.4);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f("#FFFFFF").s().p("AgFAGQgCgCAAgEQAAgCACgDQADgCACAAQADAAADACQACADAAACQAAAEgCACQgDACgDAAQgCAAgDgCg");
	this.shape_43.setTransform(35.9,4.7);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f("#FFFFFF").s().p("AgGAzIgGgBIABgJIAGABIAFABIAFgBQABgBABAAQAAAAABgBQAAAAAAgBQAAAAABgBQgBgCgEgBIgIgBIAAgOQgGAAgFgEQgFgDgDgFQgDgFAAgHIAAgUQAAgJAEgGQAEgGAGgDQAGgDAGAAQAHAAAHAEQAHADAFAHIgIAGQgDgGgFgCQgEgDgGAAQgDAAgEACQgEACgDAEQgCAEAAAFIAAAVQAAAFACADQADAEAEACQAEACADAAQAGAAAFgDQAFgEAEgFIAHAEQgEAIgFAEQgHAEgHAAIAAAGQAIABADADQADAEAAAEQAAAGgFAEQgFAEgIAAIgGgBg");
	this.shape_44.setTransform(31.7,3.6);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f("#FFFFFF").s().p("AgOAwQgFgCgDgFQgDgGgBgJIAAgtIAKAAIAAAtQAAAGACADQACAEAEACQACABAFABQADgBAFgCQAEgCACgEQAEgFAAgFIAAgrIAKAAIAABGIgJAAIgBgJQgFAFgFACQgGACgEABQgGAAgFgEgAAIgkQgDgDAAgEQAAgDADgCQACgCADgBQADABADACQACACAAADQAAAEgCADQgDACgDAAQgDAAgCgCgAgSgkQgCgDAAgEQAAgDACgCQADgCADgBQADABACACQADACAAADQAAAEgDADQgCACgDAAQgDAAgDgCg");
	this.shape_45.setTransform(24.7,0.5);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f("#FFFFFF").s().p("AAPA3IAAgVQAAgFgDgEQgDgDgFgBIgIADIgKABIAAAeIgKAAIAAhsIAKAAIAABFQAJAAAFgDQAHgEAEgFQAEgFAAgHIAAgEIgBgCIAKgBIABADIAAADQAAAJgEAFQgDAHgGAEIAGADIAFAFQACAEAAAEIAAAXg");
	this.shape_46.setTransform(18,0.1);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f("#FFFFFF").s().p("AgMAhQgGgDgEgGQgEgGAAgJQABgGADgDQADgDAGAAIAeAAIAAgGQAAgGgCgEQgDgEgEgCQgEgCgEAAQgEAAgGADQgEADgEAGIgGgGQAEgHAGgEQAHgDAHAAQAIAAAGADQAGADADAGQAEAGAAAHIAAAUQAAAJgEAGQgEAFgFADQgHADgHAAQgGAAgGgDgAgPAGIgBAEQAAAGADAEQACADAEACQAEACADAAQAEAAAEgCQAEgCACgEQADgDAAgGIAAgFIgeAAQAAAAAAAAQgBAAAAAAQAAAAAAABQgBAAAAAAg");
	this.shape_47.setTransform(7.2,2);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f("#FFFFFF").s().p("AgOA0QgFgDgDgFQgDgGgBgHIAAgYQABgHADgGQADgEAFgDQAGgEAGAAQAFAAAFACQAFADAEAFIAAgvIAKAAIAABsIgJAAIgBgJQgFAFgFADQgFACgEAAQgHAAgFgDgAgIgGQgEABgCAEQgCACAAAGIAAAYQAAAFACAEQACADAEABQADACAEAAQAEAAAEgCQAEgCACgEQAEgEAAgGIAAgTQAAgGgEgDQgCgDgEgDQgFgCgDAAQgEAAgDACg");
	this.shape_48.setTransform(0.4,0.2);

	this.shape_49 = new cjs.Shape();
	this.shape_49.graphics.f("#FFFFFF").s().p("AgNAhQgGgDgDgFQgEgFAAgHIAAgZQAAgHAEgFQADgFAGgDQAGgDAFAAQAFAAAEACQAFACAEAFIABgIIAJAAIAABFIgJAAIgBgKQgEAGgFADQgEACgFAAQgGAAgFgDgAgJgZQgCACgCADQgCADgBAFIAAAZQABAFACADQACADACACQADABAEAAQAEAAAEgCQAFgCADgEQACgEAAgGIAAgSQAAgFgCgEQgDgEgFgCQgEgDgEAAQgDAAgEACg");
	this.shape_49.setTransform(-6.5,2);

	this.shape_50 = new cjs.Shape();
	this.shape_50.graphics.f("#FFFFFF").s().p("AgRAjQgEAAgDgDQgCgDAAgEIAAgDIACgEIApgsIgpAAIAAgIIAoAAQAFAAACACQACADABADIgBAFIgEAEIgnArIAtAAIAAAJg");
	this.shape_50.setTransform(-13,2);

	this.shape_51 = new cjs.Shape();
	this.shape_51.graphics.f("#FFFFFF").s().p("AAQAkIAAguQAAgIgEgEQgDgEgFAAQgEAAgFADQgEACgEAFQgCAFAAAGIAAApIgKAAIAAhGIAJAAIABAKQAEgFAGgDQAFgDAEAAQAHAAAEADQAGADACAFQADAGABAIIAAAug");
	this.shape_51.setTransform(-19.7,2);

	this.shape_52 = new cjs.Shape();
	this.shape_52.graphics.f("#FFFFFF").s().p("AgNAhQgGgDgDgFQgDgFgBgHIAAgZQABgHADgFQADgFAGgDQAGgDAGAAQAEAAAEACQAFACAEAFIABgIIAJAAIAABFIgJAAIgBgKQgEAGgFADQgEACgFAAQgGAAgFgDgAgJgZQgCACgCADQgDADAAAFIAAAZQAAAFADADQACADACACQAEABAEAAQADAAAFgCQADgCAEgEQACgEAAgGIAAgSQAAgFgCgEQgDgEgEgCQgFgDgDAAQgEAAgEACg");
	this.shape_52.setTransform(-26.8,2);

	this.shape_53 = new cjs.Shape();
	this.shape_53.graphics.f("#FFFFFF").s().p("AgGA1QgFgDgEgGIgCAKIgIAAIAAhsIAKAAIAAAvQADgFAGgDQAEgCAFAAQAGAAAFAEQAGADADAEQAEAGAAAHIAAAYQAAAHgEAGQgDAFgGADQgFADgGAAQgFAAgEgCgAgGgGQgEADgDADQgCADAAAGIAAATQAAAFACAEQADAEAEADQAFACADAAQAFAAADgCQACgBACgDQACgEABgFIAAgYQgBgGgCgCQgCgEgCgBQgEgCgEAAQgDAAgFACg");
	this.shape_53.setTransform(-33.5,0.2);

	this.shape_54 = new cjs.Shape();
	this.shape_54.graphics.f("#FFFFFF").s().p("AgNAhQgGgDgEgGQgDgGAAgHIAAgVQAAgIADgFQAEgGAGgDQAHgDAGAAQAIAAAFADQAGADAEAGQAEAFAAAIIAAAVQAAAHgEAGQgEAGgGADQgFADgIAAQgGAAgHgDgAgIgYQgDACgDADQgCAEAAAFIAAAVQAAAFACADQADAEADACQAEACAEAAQAEAAAFgCQADgCACgEQADgDAAgFIAAgVQAAgFgDgEQgCgDgDgCQgFgDgEAAQgEAAgEADg");
	this.shape_54.setTransform(-40.4,2);

	this.shape_55 = new cjs.Shape();
	this.shape_55.graphics.f("#FFFFFF").s().p("AgFBDIgFgBIABgJIAFABIAFAAIAGgBQAAAAABAAQAAAAABgBQAAAAAAgBQABAAAAgBQgBgDgDgBIgJAAIAAgOQgIgBgGgFQgHgDgEgIQgEgHAAgKIAAgiQAAgLAFgIQAFgIAIgDQAIgFAIAAQAGAAAGACQAGADAFADQAFAEADAGIgHAGQgFgIgGgDQgGgDgHgBQgGAAgFAEQgGACgEAHQgDAFAAAIIAAAiQAAAIADAGQAEAFAGADQAFAEAGAAQAHAAAGgEQAGgCAFgJIAHAGQgFAIgHAEQgHAFgIAAIAAAGQAHABADADQADAEAAAEQAAAGgEAEQgFAEgJgBIgGAAg");
	this.shape_55.setTransform(-47.4,2);

	this.shape_56 = new cjs.Shape();
	this.shape_56.graphics.f("#FFFFFF").s().p("AgFAGQgCgCAAgEQAAgCACgDQADgCACAAQADAAADACQACADAAACQAAAEgCACQgDACgDAAQgCAAgDgCg");
	this.shape_56.setTransform(-57.3,4.7);

	this.shape_57 = new cjs.Shape();
	this.shape_57.graphics.f("#FFFFFF").s().p("AgXAzQgFAAgDgDQgCgDAAgEIAAhRQAAgEACgDQADgDAFAAIAdAAQAHABAHADQAFAEAEAFQACAGAAAHIgBAIQgBAEgDADQgCAEgFACQAIADAEAHQADAGAAAIQAAAIgEAGQgDAHgGAFQgIAEgJAAgAgXAqIAbAAQAIAAADgDQAFgDACgFQACgEAAgGQAAgFgCgFQgCgEgEgDQgEgDgIAAIgbAAgAgXgHIAdAAQAGAAADgDQADgDACgDQACgFAAgDQAAgEgCgEQgCgEgEgDQgDgCgFAAIgdAAg");
	this.shape_57.setTransform(-62.4,0.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_57},{t:this.shape_56},{t:this.shape_55},{t:this.shape_54},{t:this.shape_53},{t:this.shape_52},{t:this.shape_51},{t:this.shape_50},{t:this.shape_49},{t:this.shape_48},{t:this.shape_47},{t:this.shape_46},{t:this.shape_45},{t:this.shape_44},{t:this.shape_43},{t:this.shape_42},{t:this.shape_41},{t:this.shape_40},{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-67.7,-8.9,135.4,74.6);


(lib.Tween18 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#BE1B09").s().p("ArtdEMAAAg6HIXbAAMAAAA6Hg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-75,-186,150,372);


(lib.Tween14 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib._2150();
	this.instance.parent = this;
	this.instance.setTransform(-84,-58,0.732,0.732);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-84,-58,164,168.4);


(lib.Symbol1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 11
	this.instance = new lib.Tween23("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(74.9,242.6,0.289,0.289);
	this.instance.alpha = 0;
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(228).to({_off:false},0).to({scaleX:1.07,scaleY:1.07,y:157.6,alpha:1},8).to({scaleX:1,scaleY:1,y:165.6},2).wait(23).to({startPosition:0},0).to({scaleX:1.05,scaleY:1.05,y:162.6},3).to({scaleX:1,scaleY:1,y:165.6},3).wait(25).to({startPosition:0},0).to({scaleX:1.05,scaleY:1.05,y:162.6},3).to({scaleX:1,scaleY:1,y:165.6},3).wait(29).to({startPosition:0},0).to({scaleX:1.05,scaleY:1.05,y:162.6},3).to({scaleX:1,scaleY:1,y:165.6},3).wait(173).to({startPosition:0},0).to({alpha:0},18).wait(5));

	// Layer 15
	this.instance_1 = new lib.Tween31("synched",0);
	this.instance_1.parent = this;
	this.instance_1.setTransform(75,197.3,0.372,0.372);
	this.instance_1.alpha = 0;
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(372).to({_off:false},0).to({scaleX:1.07,scaleY:1.07,alpha:1},9).to({scaleX:1,scaleY:1},3).wait(122).to({startPosition:0},0).to({alpha:0},18).wait(5));

	// Layer 2
	this.instance_2 = new lib.Tween14("synched",0);
	this.instance_2.parent = this;
	this.instance_2.setTransform(76.5,48.5,1.287,1.287);
	this.instance_2.alpha = 0;
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(347).to({_off:false},0).to({scaleX:0.94,scaleY:0.94,alpha:1},9).to({scaleX:1,scaleY:1},3).wait(147).to({startPosition:0},0).to({alpha:0},18).wait(5));

	// Layer 10
	this.instance_3 = new lib.Tween20("synched",0);
	this.instance_3.parent = this;
	this.instance_3.setTransform(75,303.9);
	this.instance_3.alpha = 0;
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(58).to({_off:false},0).to({alpha:1},11).wait(437).to({startPosition:0},0).to({alpha:0},18).wait(5));

	// Layer 13
	this.instance_4 = new lib.Tween26("synched",0);
	this.instance_4.parent = this;
	this.instance_4.setTransform(75.8,331.4);
	this.instance_4.alpha = 0;
	this.instance_4._off = true;

	this.instance_5 = new lib.Tween27("synched",0);
	this.instance_5.parent = this;
	this.instance_5.setTransform(75.8,331.4);
	this.instance_5.alpha = 0.898;
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(53).to({_off:false},0).to({_off:true,alpha:0.898},12).wait(464));
	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(53).to({_off:false},12).wait(441).to({startPosition:0},0).to({alpha:0},18).wait(5));

	// Layer 14
	this.instance_6 = new lib.Tween28("synched",0);
	this.instance_6.parent = this;
	this.instance_6.setTransform(150,186);
	this.instance_6.alpha = 0;
	this.instance_6._off = true;

	this.instance_7 = new lib.Tween29("synched",0);
	this.instance_7.parent = this;
	this.instance_7.setTransform(0,186);
	this.instance_7.alpha = 0;

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_6}]},166).to({state:[{t:this.instance_6}]},20).to({state:[{t:this.instance_6}]},141).to({state:[{t:this.instance_7}]},19).wait(183));
	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(166).to({_off:false},0).to({x:133.4,alpha:1},20).to({x:15.9},141).to({_off:true,x:0,alpha:0},19).wait(183));

	// Layer 12
	this.instance_8 = new lib.Tween24("synched",0);
	this.instance_8.parent = this;
	this.instance_8.setTransform(150,186);
	this.instance_8.alpha = 0;

	this.instance_9 = new lib.Tween25("synched",0);
	this.instance_9.parent = this;
	this.instance_9.setTransform(0,186);
	this.instance_9.alpha = 0;

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_8}]}).to({state:[{t:this.instance_8}]},19).to({state:[{t:this.instance_8}]},147).to({state:[{t:this.instance_9}]},20).wait(343));
	this.timeline.addTween(cjs.Tween.get(this.instance_8).to({x:134.7,alpha:1},19).to({x:16.2},147).to({_off:true,x:0,alpha:0},20).wait(343));

	// Layer 1
	this.instance_10 = new lib.Tween18("synched",0);
	this.instance_10.parent = this;
	this.instance_10.setTransform(75,186);

	this.timeline.addTween(cjs.Tween.get(this.instance_10).wait(529));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,300,372);


// stage content:
(lib.mangalustu150x372 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("rgba(253,192,1,0.698)").ss(0.8,1,1).p("Arm88IXNAAMAAAA55I3NAAg");
	this.shape.setTransform(75,186);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	// Layer 1
	this.instance = new lib.Symbol1();
	this.instance.parent = this;
	this.instance.setTransform(75,186,1,1,0,0,0,75,186);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(74.7,185.7,300.3,372.6);
// library properties:
lib.properties = {
	width: 150,
	height: 372,
	fps: 26,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [
		{src:"images/mangalustu_150x372_atlas_.png?1497380441106", id:"mangalustu_150x372_atlas_"}
	],
	preloads: []
};




})(lib = lib||{}, images = images||{}, createjs = createjs||{}, ss = ss||{}, AdobeAn = AdobeAn||{});
var lib, images, createjs, ss, AdobeAn;