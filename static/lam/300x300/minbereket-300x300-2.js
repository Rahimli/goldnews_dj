(function (lib, img, cjs, ss, an) {

var p; // shortcut to reference prototypes
lib.ssMetadata = [];


// symbols:



(lib.Image = function() {
	this.initialize(img.Image);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,277,425);


(lib.Image_0 = function() {
	this.initialize(img.Image_0);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,312,377);


(lib.all300 = function() {
	this.initialize(img.all300);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,300,526);


(lib.all2300 = function() {
	this.initialize(img.all2300);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,1207,300);


(lib.sosis300 = function() {
	this.initialize(img.sosis300);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,519,300);// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.Tween14 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.all2300();
	this.instance.parent = this;
	this.instance.setTransform(-603.5,-150);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-603.5,-150,1207,300);


(lib.Tween13 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.all2300();
	this.instance.parent = this;
	this.instance.setTransform(-603.5,-150);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-603.5,-150,1207,300);


(lib.Tween12 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.sosis300();
	this.instance.parent = this;
	this.instance.setTransform(-259.5,-150);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-259.5,-150,519,300);


(lib.Tween10 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.all300();
	this.instance.parent = this;
	this.instance.setTransform(-150,-263);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-150,-263,300,526);


(lib.Tween9 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.all300();
	this.instance.parent = this;
	this.instance.setTransform(-150,-263);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-150,-263,300,526);


(lib.Tween6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(0,120,68,0.31)").s().p("EgAuAgJQgogxAAglQAAggAWgvQAbg8AEgQQAOg1AEgZIAEgHQAFgHAEAAQANAAACAUQAIAuAMAaQAKAZAYAyQATAuAAAiQAAAlgoAxQguA2AAAGQgCgGgsg2gAHnfPQgzglgKgjQgJgfAKgzQAKhCABgQQgBg1gCgbIACgIQACgIADgBQAPgEAHATQASAnATAaIA1BAQAeAmAJAiQAJAjgaA5QgdBAAAAGQgEgFg4gogApBe2Qgag5AKgjQAIggAhgoQArgzAHgNQAagrALgcIAGgGQAHgFADABQAPADgFAUQgCApADAiQADAbALA2QAGAygIAgQgKAkgzAlQg5AogDAFQAAgFgehBgACiePQgsgKgmhPQgjhJgBg+IAuBCQA/BDBWADIgQAyQgTAogeAAQgGAAgGgCgAjfdlIgPgxQBVgDA/hCIAuhDQgBA9gjBKQgnBPgrALIgMABQgdAAgUgpgAlYd4QgngVgQhYQgOhQAOg6IAbBLQArBRBTAZIgcAsQgYAcgYAAQgLAAgLgGgAESdgIgbgsQBRgaArhRIAchMQAOA7gOBRQgQBWgoAWQgLAGgLAAQgXAAgYgbgAPccNQg7gWgTghQgRgbgDg0QgHhCgEgQQgNgugLgdIAAgJQAAgIAEgCQAIgFAJAGQAFAEACAEQAdAjAYARQAWASAtAeQAnAdASAfQATAfgLA/QgLBEACAIQgFgFhCgYgAwsbeQgKg/ASgfQARgcAqgeQA1gnALgLQAlglARgWIAIgEQAIgDADACQAMAHgJARQgNAmgFAiQgEAcgFA3QgFAxgRAdQgTAhg8AWQhBAZgFAFQACgIgLhEgAIsbjQg2g/gRg7IA+A0QBPAwBTgTIgCA0QgLA0gnACIgBAAQgsAAg4hBgAqQchQgngCgKgzIgDg0QBUATBOgwIA+g0QgRA6g1A+Qg5BCgsAAIgBAAgAs7bdQghgeAIhYQAFhSAeg1IAGBRQAXBaBIAtIgnAjQgWAPgTAAQgRAAgOgNgALybXIgngiQBIguAWhZIAHhQQAeA1AFBRQAIBZghAeQgOAMgQAAQgTAAgXgQgAPgYXQhDgwgggzIBKAiQBYAbBLgpIAMAzQADA0gmAMQgJADgKAAQgmAAg6gngAxSY5QglgMADg0IAMgzQBLApBYgbQAtgOAcgUQgfAzhEAuQg6AognAAQgJAAgJgCgAzmXLQgWgmAchTQAchNAqgsIgOBQQgCBcA5A+QgVAPgZAJQgRAGgOAAQgaAAgOgWgASdXZIgugYQA6g/gChcIgOhOQApAqAdBOQAbBTgWAlQgOAXgbAAQgNAAgRgGgAWNXPQg/gGgagaQgWgWgSgxQgYg/gIgNQgYgqgSgaQgGgPAEgEQALgKAPANQAhAXAgAOQAaALA0ASQAtATAZAYQAZAaAHA/QAGBFAEAGQgGgDhGgHgA3OWOQAGhAAZgZQAXgXAwgSQA/gYAOgIQApgWAagUIAIgCQAJgCACACQALAMgOAOQgVAfgQAiIgdBOQgSAsgZAaQgaAag+AFQhFAHgGADQACgEAIhGgAVRThQhMgcgsgqIBQAOQBcADA+g7IAZAuQAQAzghAUQgRAKgaAAQggAAgvgPgA3ITkQgigVASgyIAXguQA/A7BcgDQAtgBAigNQgrAqhNAbQgvAQggAAQgaAAgQgKgA47RUQgNgqAyhJQAuhEA1ggIgjBKQgZBXAoBMQgZAKgbADIgHAAQguAAgLgjgAX4R1IgzgMQAphMgbhYIgihJQA0AfAuBEQAzBJgOArQgKAigtAAIgJAAgAaAQlQgdgRgdgpQgng3gMgLQgbgdgegZIgFgHQgDgHABgEQAFgIAKABQAGABAEACQAoANAhAFQAbAEA4AEQAwAGAeASQAgASAXA8QAXBCAFAEQgGgChGAMQgVAEgSAAQghAAgVgMgA7eQsQhFgLgGABQAFgEAZhCQAVg7AggTQAcgQAzgFQBDgGAPgEQAmgKAmgNIAJAAQAIAAACADQAIANgSALQgfAcgVAZQgQAWggAtQgeAogeARQgUAMgiAAQgSAAgWgEgAZnNVQhRgHg3gcIBRgHQBagWAthJIAjAnQAeAsgcAdQgYAag+AAIgfgBgA7bM6QgagdAegsIAjgmQAtBJBZAVQAtALAjgFQg0AehSAGIgdACQhAAAgagbgA7uLDQg0gKgBgoQgBgtBDg5QA9g0A7gSIg0A+QgwBOATBUIgTABQgRAAgQgDgAaaIjIg0g+QA6AQA/A1QBDA6gBAsQgCAogzAKIg0ACQAThUgxhNgAdZJSQgfgJgoghQgzgpgOgIQgqgZgcgNIgGgGQgGgHABgDQADgJALgBQAEgBAGABQAuADAdgEQAagCA3gMQAxgGAhAJQAkAJAkA0QApA5AGAEQgHAAg/AdQgqASgeAAQgMAAgKgCgA+2JBQg/gdgHAAQAFgEAog4QAlg0AkgKQAfgIAzAIQBCALAQAAQAmACApgFIAIACQAIAEABADQAFAOgUAGQgjARgeAWIhAA0QgmAegiAKQgJACgKAAQgeAAgrgTgAaAGQIBMgbQBRgrAZhTIAsAcQAoAjgSAiQgWAnhWAQQgqAIgkAAQghAAgdgHgA8JGPQhWgRgVgnQgTgiAogjIAsgbQAZBRBRAsQApAWAjAFQgdAIgiAAQgkAAgpgIgAbuBaIhCguQA+ABBJAkQBPAmAKArQAKAmgwAXIgyAPQgDhVhDg/gA9pDfQgvgXAJgmQAKgrBPgmQBKgkA9gBIhCAuQhDA/gDBVQgagEgYgLgAdkBAQg9gbgPgEQg0gOgagDQgOgIAAgFQAAgJAKgDQAFgDAFAAQAogHAggNQAZgKAygZQAugSAigBQAlABAxAoQA2AtAGABQgHABg1AsQgxAoglAAQggABgvgXgEggIAAuQg2gtgGgBQAHgBA1gsQAxgpAkgBQAgAAAwAXQA7AbAQAEQAmALAoAHIAIAEQAHAFAAADQAAAOgVABQgnAIghAMIhLAiQgtAUgjgBQgkAAgxgngAbyhZQBDg/AChXIAyAQQAvAYgJAmQgKArhPAnQhKAig9ACgA8yhPQhQgngLgrQgIgmAwgYIAxgQQADBXBDA/QAhAgAhAOQg9gChJgigAbJl0IhLgbQA7gOBPAPQBYAQAVAnQASAjgoAiIgsAcQgZhRhRgtgA9hkTQgogiATgiQAUgoBXgQQBQgPA7AOIhLAcQhRAsgaBRQgXgKgUgSgAc0mqQhDgLgOgBQgrABglADQgPgCgDgGQgBgJAJgHQAEgDAEgCQAmgTAagTQAWgQAqglQAngfAhgHQAkgKA6AaQA/AdAHAAQgGADgpA6QgkAzgkAJQgPAFgVAAQgUAAgagFgA+GmrQgjgJglgzQgpg5gFgDQAHgBA/gdQA5gaAjAKQAgAIAoAgQA0AsAMAHQAqAZAdANIAGAGQAGAGgBADQgEAPgTgEQgngFgkAFQg2AKgcADQgWAEgTAAQgXAAgSgGgA7eorQhDg5AAgsQACgoA0gKQAagEAZADQgSBTAwBOQAYAnAbAXQg6gSg9g1gAadojQAwhOgShTIAzACQA0AKACAoQAAAshCA5Qg+A1g6ARgAYusoIhRgHQA0gdBTgHQBYgHAeAhQAcAcgeAsIgjAmQgthIhagVgA7aryQgegsAbgcQAfghBXAHQBTAGA0AdQgjgEgtALQhaAVgtBIQgTgPgQgWgAXWtbQgFgHAHgJIAHgHQAggbAVgaQARgVAfgvQAegnAegRQAggTA/ALQBEAMAHgCQgEAEgZBCQgXA7ggASQgbARg0AEQhDAGgPAEQgvANgdALIgGAAQgKAAgDgEgA3stcQgkgNgkgGQg3gFgcgDQgxgHgdgQQgggSgWg8QgahCgEgEQAGABBFgLQA/gKAfATQAcAPAfAqQAlA2AMAMQAlAlAWARIAEAHQAEAIgDADQgEAIgIAAQgFAAgHgEgA4TveQgyhKAMgpQANgmA0AEQAbABAYAKQgoBLAZBZQAOArAVAdQg0gfguhDgAXWvGQAahYgohMIAzgLQA1gEAKAmQANArgxBIQguBEg0AeQAVgcANgsgAUmymIhPAOQAsgqBNgbQBTgeAmAYQAiAUgSAyQgJAZgPAVQg/g5hcACgA3byfQgSgxAigWQAmgXBTAdQBNAcAsAqQghgNgugCQhcgCg/A6QgOgUgKgagATFzAQgHgGAFgKIAEgJQAZgkAMgdIAdhOQATguAZgYQAagaA+gGQBHgHAFgDQgEAGgGBGQgGA+gaAaQgXAXgwASQg+AYgPAIQgjATggAWQgJAEgFAAQgDAAgCgCgAzazHQgkgZgcgMIhPgeQgsgSgagYQgZgagGg/QgHhGgDgGQAGAEBFAHQA/AFAaAaQAXAXASAxQAXA+AIAOQAdAxANASQAGAPgEAEQgFAFgFAAQgHAAgJgHgAzd1PQgehTAYgmQAVgiAyARQAYAKAVAOQg6A/ADBcQABAuAMAhQgqgsgahMgASp0oQAChdg6g+IAugYQAygRAVAhQAXAmgdBTQgbBNgrAtQAOghABgvgAPe4TQBJgxArAMQAlAMgDA0QgDAbgIAYQhLgohZAaIhJAjQAgg1BCgugAvG3VQhZgbhMAoQgIgYgCgbQgFg1AmgKQArgNBKAyQBCAtAgA1QgegVgrgNgANg3TQgIgFACgKIADgKQAPgtAEgcIAIhSQAGgxAQgeQATgeA7gXQBBgZAGgFQgCAHAMBEQAKBAgSAfQgRAbgpAfQg3AmgMAMQglAlgRAUQgJAIgFAAIgEgBgAtr3YIgIgHQgcgigZgTQgtgfgWgSQgngdgSgdQgRggAJg/QAMhEgBgIQAFAFBAAZQA8AWASAgQAQAcAFAzQAGBDAEAPIAXBNQACAQgFACQgDACgEAAQgEAAgFgEgAtS5kQgIhYAggeQAdgbAsAdQAWAQAQATQhIAugWBaQgKAtADAjQgdg2gFhRgAMq4vQgVhbhIgtIAmgjQArgdAdAbQAhAfgHBXQgHBSgdA1QAEgkgLgsgAIr7eQA5hDAsABQAnABALA0QAFAagDAaQhTgThPAwIg9A0QARg7A1g9gAoj6dQhOgwhUATQgCgaAFgaQAKg0AngBQAsgBA6BCQA1A/ARA6QgWgbgogZgAmO8IQAQhXAngUQAigTAkAoQASAUAKAXQhSAagtBRIgbBLQgOg7APhQgAHB5/QgJgDgBgLIAAgKQADgpgDgiQgLg2gDgbQgIgxAKgiQAJgjAzglQA6goADgGQAAAHAdA/QAaA6gJAjQgIAgghAnQgrA0gIANQgYAqgNAdQgJALgFAAIgCAAgAF17MQgshRhSgZIAcgrQAjgoAiASQAoAVAQBXQAOBQgOA8QgFgkgWgpgAnK6HIgFgKQgTgngUgZIgzhAQgegngKghQgKgkAbg6QAdg/AAgGQADAFA5ApQA0AlAJAjQAJAggJAyQgLBCAAAQQAAA0ADAbQgDAQgGACIgEAAQgGAAgFgGgABQ8zQAmhPAsgKQAngJAWAvQALAZAFAZQhWADg/BDIguBCQABg9AjhKgAha7xQg/hDhVgCIAPgyQAXgwAmAJQArAKAnBQQAjBKABA9QgOghgggigAgJ7FIgCgKQgJgpgMgfIgihMQgTgtAAgiQAAglAogxQAsg2ABgGQACAGAsA2QAoAxABAlQgBAggVAvQgbA8gFAQQgKAhgIAsQgGAPgGAAQgIAAgEgKg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-211.7,-211.7,423.4,423.5);


(lib.Tween4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("A3bXcMAAAgu3MAu3AAAMAAAAu3g");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-150,-150,300,300);


(lib.Tween2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#007844").s().p("AgjByQgFgCgBgGIAAgGQACgZgCgUIgIgxQgFgcAGgUQAFgVAfgWQAhgYACgEQAAAEASAmQAPAjgFAVQgFASgUAXQgaAfgDAIQgPAZgIARQgFAHgDAAIgBAAg");
	this.shape.setTransform(30.5,-111);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#007844").s().p("AAbAeQgagvgwgPIARgaQAVgYATALQAYAMAKA0QAIAvgIAkQgDgVgOgZg");
	this.shape_1.setTransform(19.6,-107.2);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#007844").s().p("AgYgPQAhgoAbAAQAXABAGAfQADAQgCAOQgxgKguAcIglAfQAKgjAggkg");
	this.shape_2.setTransform(35.7,-103.6);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#007844").s().p("Ag7BmQgFgDABgGIACgGQAJgbACgRIAFgwQADgdAKgSQALgTAjgNQAngPADgCQgBADAHAqQAGAlgLATQgKAQgZASQgfAXgHAGQgWAXgLAMQgFAEgDAAIgCAAg");
	this.shape_3.setTransform(57.8,-99.4);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#007844").s().p("AAPAgQgNg1gqgbIAXgVQAZgSARAQQATATgEA0QgEAwgRAgQACgWgGgag");
	this.shape_4.setTransform(47,-98);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#007844").s().p("AgOgRQAqgdAaAHQAWAHgCAgQgBAOgGAPQgtgYgzAQIgsAUQATgfAogbg");
	this.shape_5.setTransform(60.8,-91.3);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#007844").s().p("AhQBTQgEgEACgGIADgFQAPgVAHgSIASgtQALgcAPgOQAOgQAmgDQAqgFADgBQgCADgEAqQgEAlgPAPQgOAOgdAKQgkAOgJAFQgVAMgTANIgIACIgDgBg");
	this.shape_6.setTransform(81.2,-81.1);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#007844").s().p("AgEAgQACg3gjglIAbgOQAdgLANAUQAOAXgSAxQgQAugYAaQAIgUAAgbg");
	this.shape_7.setTransform(71.9,-82.2);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#007844").s().p("AgfAFIgvAIQAagYAugQQAxgSAXAOQAUAMgLAdQgFAPgJANQgmgjg2ACg");
	this.shape_8.setTransform(82.1,-71.7);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#007844").s().p("AhkA/QgDgFAEgFIAFgEQASgQANgQQAKgNATgbQASgXARgLQATgLAlAHQApAHAEgBQgCACgPAoQgOAigTALQgQAKgfACQgnAEgJADQgcAHgSAHIgEAAQgFAAgCgCg");
	this.shape_9.setTransform(99.5,-57.7);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#007844").s().p("AgUAfQAQg0gYgtIAdgHQAggCAGAWQAIAageAqQgaApgfASQAMgRAIgag");
	this.shape_10.setTransform(91.5,-60.9);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#007844").s().p("AgfgOIgwgEQAggRAwgEQA1gEASATQAQARgSAZIgVAXQgbgqg1gNg");
	this.shape_11.setTransform(97.8,-46.9);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#007844").s().p("AgFAxQgogHgJAAQgaAAgWACQgJgBgBgEQgBgFAFgEIAFgDQAXgLAPgMQAOgIAZgWQAXgTATgFQAVgFAjAPQAmASAEAAQgEACgYAhQgWAfgVAFQgKADgMAAQgMAAgOgDg");
	this.shape_12.setTransform(111,-30.4);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#007844").s().p("AgYAeQAcgugKgyIAeACQAfAGABAXQAAAbgoAhQgkAggjAKg");
	this.shape_13.setTransform(103.8,-35.7);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#007844").s().p("AgdgaIgugRQAkgIAuAJQA1AJAMAYQALATgYAVIgaARQgPgwgvgag");
	this.shape_14.setTransform(107,-19.5);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#007844").s().p("AgQAnQgkgRgKgCQgfgIgPgDQgIgEAAgDQgBgFAHgCQACgCADAAQAYgEATgIQAQgGAdgOQAbgMAUAAQAXAAAcAYQAhAbAEAAQgFABggAbQgdAYgWAAQgTAAgbgNg");
	this.shape_15.setTransform(115,0);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#007844").s().p("AgbAfQAnglACgzIAdAJQAdAOgFAXQgGAZgwAWQgrAWglABg");
	this.shape_16.setTransform(109.2,-8.4);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#007844").s().p("AgbgeIgngbQAkABArAVQAwAXAGAYQAFAXgcAOIgeAJQgCgzgnglg");
	this.shape_17.setTransform(109,8.5);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#007844").s().p("AAQAzQgRgFgYgUQgfgZgIgEQgZgOgRgIIgEgEQgDgEAAgCQACgFAGgBIAGAAQAcACARgCQAQgCAhgGQAcgEAUAFQAVAGAWAfQAZAhADACQgEAAgmARQgZALgSAAQgHAAgGgBg");
	this.shape_18.setTransform(111,30.5);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#007844").s().p("AhLAsIAugRQAvgaAPgwIAbARQAYAUgMAUQgMAXg0AKQgYAFgVAAQgVAAgRgEg");
	this.shape_19.setTransform(107.2,19.6);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#007844").s().p("AgXgdIggglQAjAKAlAgQAoAhgBAbQgBAYgfAGIgeABQALgygcgug");
	this.shape_20.setTransform(103.6,35.7);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#007844").s().p("AABA5QgQgJgSgZQgXgggHgHQgQgRgSgPIgDgEQgCgFABgCQADgFAGABQADAAADACQAXAIAUADQAQACAhADQAdADASALQATALANAiQAPAoACACQgDgBgqAHQgNACgKAAQgUAAgNgHg");
	this.shape_21.setTransform(99.5,57.8);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#007844").s().p("AABAoQgvgDghgSIAwgEQA1gNAbgqIAVAXQASAZgQARQgPAQglAAIgTgBg");
	this.shape_22.setTransform(98,47);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#007844").s().p("AACBKIgegHQAYgtgQg0IgUgsQAfATAbApQAeAqgIAaQgGAVgbAAIgFgBg");
	this.shape_23.setTransform(91.3,60.9);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#007844").s().p("AAnBOQglgEgPgPQgNgOgLgdQgOgkgFgIQgOgZgLgQQgEgJADgCQAGgGAJAIQAUANATAJQAPAGAfALQAbALAOAPQAQAOAEAmQAEAqACADQgEgCgqgEg");
	this.shape_24.setTransform(81.1,81.2);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#007844").s().p("AgGAdQgugRgagYIAwAJQA2ABAlgjIAPAcQAKAdgUAMQgKAGgQAAQgTAAgbgJg");
	this.shape_25.setTransform(82.2,71.9);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#007844").s().p("AgJBMIgcgPQAjglgBg2IgJgwQAYAaARAuQARAxgOAXQgIANgQAAQgIAAgJgDg");
	this.shape_26.setTransform(71.7,82.1);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#007844").s().p("AAPBVQgjgOgLgTQgKgQgCgfQgEgngDgJQgHgcgHgSIAAgFQAAgFACgBQAFgDAGAEIAEAFQARAUAPALQANAKAaATQAXARALARQALATgHAmQgGApABAEQgDgDgngOg");
	this.shape_27.setTransform(57.7,99.6);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#007844").s().p("AgOASQgpgbgSgfIAsAUQAzAQAtgYIAHAdQACAggWAHQgGABgGAAQgXAAghgXg");
	this.shape_28.setTransform(60.9,91.5);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#007844").s().p("AgRBGIgXgVQAqgbANg1IAEgwQASAgADAwQAFA1gUASQgIAIgKAAQgKAAgOgKg");
	this.shape_29.setTransform(46.9,97.9);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#007844").s().p("AgMBWQgfgWgGgVQgFgTAGgdQAGgoAAgKQAAgfgBgQIABgFQABgFACAAQAJgDAEAMQALAXALAPIAfAnQASAXAFATQAGAVgQAiQgRAmAAAEQgCgDghgYg");
	this.shape_30.setTransform(30.5,111.1);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#007844").s().p("AgYARQgfgkgLgkIAlAfQAuAcAygKIgBAeQgGAfgYABIgBAAQgaAAghgng");
	this.shape_31.setTransform(35.7,103.8);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#007844").s().p("AgeA8IgRgbQAwgPAagvIARguQAIAkgIAvQgKA0gYAMQgHAEgFAAQgOAAgOgQg");
	this.shape_32.setTransform(19.5,107);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#007844").s().p("AgbBSQgYgeAAgWQAAgTANgbQARgkACgKQAJggACgOIACgEQADgFACAAQAIAAABAMQAFAcAHAPQAGAQAOAeQAMAaAAAUQAAAWgYAeQgbAggBAEQAAgEgbggg");
	this.shape_33.setTransform(0,115);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#007844").s().p("AAMBDQgZgGgXgwQgVgrAAgkIAbAnQAlAnAzACIgJAeQgMAXgSAAIgHAAg");
	this.shape_34.setTransform(8.5,109.2);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#007844").s().p("AgwArIgJgdQAzgCAlgnIAbgoQgBAlgVArQgXAvgYAHIgHABQgSAAgMgZg");
	this.shape_35.setTransform(-8.5,109);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#007844").s().p("AgoBIQgQgjAGgVQAFgSAUgXQAZgfAEgIQAPgaAHgQIAEgEQAEgDACAAQAIACgCAMQgCAZACAUIAIAxQAEAcgFAUQgGAVgeAWQghAYgCAEQAAgEgSgmg");
	this.shape_36.setTransform(-30.5,111);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("#007844").s().p("AgKBIQgXgMgKg1QgIgvAIgjIARAuQAaAvAwAPIgRAaQgOARgOAAQgGAAgHgEg");
	this.shape_37.setTransform(-19.6,107.2);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("#007844").s().p("AgjA4QgYgBgFgfIgCgeQAyAKAugcIAlgfQgKAjggAkQghAogaAAIgBAAg");
	this.shape_38.setTransform(-35.7,103.7);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("#007844").s().p("Ag9A5QgGglALgTQAKgQAZgSQAfgXAHgHQAWgWAKgNIAEgCQAFgCACABQAIAEgGALQgIAWgDAVQgCAQgDAgQgDAdgLASQgLATgiAOQgoAPgCACQABgEgHgpg");
	this.shape_39.setTransform(-57.8,99.5);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f("#007844").s().p("AgYBIQgUgSAFg1QADgwASggIAEAwQANA1AqAbIgXAVQgOAKgKAAQgKAAgIgIg");
	this.shape_40.setTransform(-47,98);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f("#007844").s().p("Ag1AoQgXgHADggIAGgdQAuAYAzgQQAbgIARgMQgTAfgoAaQgiAYgXAAQgGAAgFgBg");
	this.shape_41.setTransform(-60.8,91.4);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f("#007844").s().p("AhNAnQAEglAPgPQAOgOAdgKQAkgOAIgFQAZgOAQgLIAFgCQAFgBABACQAGAGgIAJQgMASgKAVIgRAtQgLAbgPAPQgOAQgmADQgpAEgEACQACgDAEgqg");
	this.shape_42.setTransform(-81.2,81.2);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f("#007844").s().p("AgfBCQgOgXASgxQAQguAYgaIgIAvQgCA3AjAlQgNAJgPAFQgKAEgHAAQgQAAgIgNg");
	this.shape_43.setTransform(-71.9,82.2);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f("#007844").s().p("AhBAgQgUgNALgdIAOgbQAlAjA2gCQAcAAAUgIQgaAYguAQQgbAKgTAAQgQAAgKgGg");
	this.shape_44.setTransform(-82.1,71.8);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f("#007844").s().p("Ag4A+QgqgHgDABQADgCAOgoQANgiATgLQARgKAfgDQAngDAJgDQAWgGAXgIIAFAAQAGAAABACQAEAIgKAGQgTARgMAPIgdAoQgSAXgRAKQgMAIgUAAQgLAAgNgDg");
	this.shape_45.setTransform(-99.5,57.7);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f("#007844").s().p("AgnA2QgHgZAegrQAagpAfgTIgUAsQgQA0AYAtQgOAGgPABIgFABQgbAAgHgVg");
	this.shape_46.setTransform(-91.5,61);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f("#007844").s().p("AhHAZQgQgRASgaIAVgWQAbAqA1ANQAaAGAWgCQggARgwAEIgRABQgnAAgPgQg");
	this.shape_47.setTransform(-97.9,47);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f("#007844").s().p("AhGApQgmgSgEAAQADgCAYghQAWgfAWgFQASgFAeAFQAnAGAKAAQAWABAZgCIAFABQAFACAAACQADAIgMAEQgVAKgRANIgnAeQgXASgTAGQgFABgHAAQgSAAgZgLg");
	this.shape_48.setTransform(-111.1,30.5);

	this.shape_49 = new cjs.Shape();
	this.shape_49.graphics.f("#007844").s().p("AgXBCQgfgGgBgYQAAgbAoghQAkggAjgKIgfAlQgcAuAKAyIgLAAQgJAAgKgBg");
	this.shape_49.setTransform(-103.8,35.8);

	this.shape_50 = new cjs.Shape();
	this.shape_50.graphics.f("#007844").s().p("AgHArQg0gKgMgXQgLgTAYgVIAagRQAPAwAvAaQAZANAVAEQgSAEgUAAQgVAAgYgFg");
	this.shape_50.setTransform(-107,19.6);

	this.shape_51 = new cjs.Shape();
	this.shape_51.graphics.f("#007844").s().p("AhRAcQgggbgEgBQAFAAAfgaQAegZAVAAQAUAAAbANQAkARAJACQAXAHAYAEIAEACQAEADABACQAAAIgNABQgYAFgTAHIgtAUQgaAMgVAAQgVAAgegYg");
	this.shape_51.setTransform(-114.9,0);

	this.shape_52 = new cjs.Shape();
	this.shape_52.graphics.f("#007844").s().p("AgrAxQgcgOAFgXQAGgYAwgXQArgVAkgBIgnAbQgnAlgCAzQgPgCgPgHg");
	this.shape_52.setTransform(-109.2,8.5);

	this.shape_53 = new cjs.Shape();
	this.shape_53.graphics.f("#007844").s().p("AgMAkQgvgWgHgZQgFgXAdgOIAdgJQACAzAnAlQAUATAUAJQglgBgrgWg");
	this.shape_53.setTransform(-109,-8.4);

	this.shape_54 = new cjs.Shape();
	this.shape_54.graphics.f("#007844").s().p("AgqAxQgVgFgWgfQgYghgEgCQAEAAAmgSQAjgPAVAFQASAFAXAUQAfAZAIAEQAZAPARAIIAEADQADAEAAACQgCAJgMgDQgXgCgWACIgxAIQgMACgLAAQgOAAgLgDg");
	this.shape_54.setTransform(-111,-30.5);

	this.shape_55 = new cjs.Shape();
	this.shape_55.graphics.f("#007844").s().p("Ag6AfQgYgVALgTQAMgYA0gJQAvgJAjAIIgtARQgvAagPAwQgOgGgMgLg");
	this.shape_55.setTransform(-107.2,-19.6);

	this.shape_56 = new cjs.Shape();
	this.shape_56.graphics.f("#007844").s().p("AgPAZQgoghAAgbQABgXAfgGQAQgDAOACQgKAxAcAuQAOAYARANQgjgKgkggg");
	this.shape_56.setTransform(-103.7,-35.7);

	this.shape_57 = new cjs.Shape();
	this.shape_57.graphics.f("#007844").s().p("ABXA+QgWgHgVgEIgxgFQgdgEgSgKQgTgLgNgiQgPgogDgCQAEABApgHQAmgGATALQAPAJASAZQAXAgAHAHQAWAWANAKIADAEQACAFgBACQgDAEgEAAQgEAAgEgCg");
	this.shape_57.setTransform(-99.5,-57.7);

	this.shape_58 = new cjs.Shape();
	this.shape_58.graphics.f("#007844").s().p("AhFATQgSgaAQgRQATgTA0AEQAwAEAgARQgWgCgaAGQg1ANgbAqQgMgJgJgNg");
	this.shape_58.setTransform(-98,-47);

	this.shape_59 = new cjs.Shape();
	this.shape_59.graphics.f("#007844").s().p("AgQAPQgegrAHgZQAHgWAgACQAPABAOAGQgYAtAQA0QAIAaAMARQgfgTgagog");
	this.shape_59.setTransform(-91.4,-60.8);

	this.shape_60 = new cjs.Shape();
	this.shape_60.graphics.f("#007844").s().p("ABEBQQgVgPgSgHIgtgSQgbgLgPgOQgQgPgDgmQgEgpgCgEQADACAqAEQAmAEAOAPQAOAOALAdQANAkAFAJQARAdAIALQAEAJgDACQgCADgEAAQgEAAgFgEg");
	this.shape_60.setTransform(-81.2,-81.2);

	this.shape_61 = new cjs.Shape();
	this.shape_61.graphics.f("#007844").s().p("AhKALQgLgdAUgNQAXgOAxASQAuAQAaAYQgUgHgbgBQg2gCgmAjQgJgMgFgPg");
	this.shape_61.setTransform(-82.2,-71.9);

	this.shape_62 = new cjs.Shape();
	this.shape_62.graphics.f("#007844").s().p("AgbAHQgSgxAOgWQANgVAdALQAPAFAMAJQgjAmACA2QABAbAHAUQgYgagQgug");
	this.shape_62.setTransform(-71.8,-82.1);

	this.shape_63 = new cjs.Shape();
	this.shape_63.graphics.f("#007844").s().p("AA0BkIgEgFQgRgUgPgLIgngdQgYgSgKgRQgLgTAGglQAHgpgBgEQADADAnAOQAjAOALATQAJARADAeQAEAnACAJIAOAuQABAJgDACIgEABQgCAAgEgCg");
	this.shape_63.setTransform(-57.7,-99.5);

	this.shape_64 = new cjs.Shape();
	this.shape_64.graphics.f("#007844").s().p("AAeAVQgzgQguAYQgFgPgBgPQgDgfAXgHQAZgHArAdQAoAbATAfQgRgMgbgIg");
	this.shape_64.setTransform(-60.9,-91.5);

	this.shape_65 = new cjs.Shape();
	this.shape_65.graphics.f("#007844").s().p("AgnAAQgFg1AUgSQARgQAZASQANAJAKAMQgqAbgNA1QgGAaACAWQgSgggDgwg");
	this.shape_65.setTransform(-46.9,-97.8);

	this.shape_66 = new cjs.Shape();
	this.shape_66.graphics.f("#007844").s().p("AAeBuIgDgGQgLgXgMgPIgegnQgSgXgFgTQgGgVAQgjQARgmAAgEQACAEAhAYQAfAWAFAVQAFATgFAdQgGAoAAAJQAAAgABAQQgBAJgEABIgCABQgEAAgDgEg");
	this.shape_66.setTransform(-30.5,-111);

	this.shape_67 = new cjs.Shape();
	this.shape_67.graphics.f("#007844").s().p("AAeAZQgugcgyAKQgBgOADgQQAGgfAXgBQAbAAAhAoQAgAkAKAjQgNgRgYgOg");
	this.shape_67.setTransform(-35.8,-103.8);

	this.shape_68 = new cjs.Shape();
	this.shape_68.graphics.f("#007844").s().p("AgqgHQAKg0AXgMQAUgLAVAYQAKAMAGAOQgwAPgaAvIgRAtQgIgjAJgvg");
	this.shape_68.setTransform(-19.6,-107);

	this.shape_69 = new cjs.Shape();
	this.shape_69.graphics.f("#007844").s().p("AgFBwIgCgGQgFgZgHgSIgUguQgMgaAAgUQAAgWAYgdQAbghAAgEQABAEAbAhQAYAdAAAWQAAATgNAbQgRAkgCAKQgGAUgFAaQgEAJgDAAQgFAAgCgGg");
	this.shape_69.setTransform(0,-114.9);

	this.shape_70 = new cjs.Shape();
	this.shape_70.graphics.f("#007844").s().p("AAfAcQglgngzgCIAJgdQAOgdAXAFQAYAHAXAvQAVArABAlQgIgUgTgUg");
	this.shape_70.setTransform(-8.5,-109.2);

	this.shape_71 = new cjs.Shape();
	this.shape_71.graphics.f("#007844").s().p("AgkgMQAXgwAZgGQAXgFAOAcQAHAPACAPQgzACglAnIgbAnQAAgkAVgrg");
	this.shape_71.setTransform(8.5,-109);

	this.shape_72 = new cjs.Shape();
	this.shape_72.graphics.f("#007844").s().p("AKLFDIgFgHQgEgKALgKQAMgLAZgKIA7gWIgMgiIAnAQIADAIIARgFIArAeIgsAOIAPAnIACACIgsgTIgEgLIgfALQgdAMgIAHQgKAHACAIIACADIAEADIgGAHgAqyE+QgTgKgdgNIgOAfIgFgCIgKgqQgdgOgKgBQgKgCgEAHIgBAEIAAAGIgGABIgKg0IgBgKIABgGQAEgJArATIAmARIAphWIgJgEQgpgSgDAKIgBADIAAADIgHAAIgHgsIgBgKQAAgDABgDQACgEAEAAQADgBAGACICnBNIALA0IgDgBIhcgtIgoBWIBDAgIAKA1gAKDDWQgVgFgQgKQgPgKgFgKQgIgNACgOQADgPAKgFQAIgFAOAEQAOAFAZAOIAUALIgEAHQgMgHgKgCQgKgDgFAEQgGADgDAKQgBAJADAHQAGAJAJABQAJABALgGQAUgLAIgZQAIgZgKgTQgEgGgEgEIgLgJIADgHIAbAKQAPAFAJAHQAIAGAEAHQAFAKAAAMQAAAMgEAOQgFAOgHAJQgHAJgLAHQgOAIgPAAIgHAAQgMAAgOgEgApFCNQgNgNACgcQACgeAUgeQgPgEgOABQgMACgGAGIgCAFIgCAGIgHgBIAFgzIAEgFQAIgKAnAFQAkAEBKASIgEAtQgngOgYADQgZAEgRATQgGAIgCAGQgBAHAFAEQAHAGANgEQANgEAKgNIAHgJIAEgKIAGAAQACAXgDASQgEARgHAJQgLAMgQADIgIABQgLAAgIgGgAIOBkQAjgxAJgVQAJgVgIgGQgKgHghAGQggAHgqAUIgGgFQAhgtAHgQQAGgPgJgHQgLgIgkAIQgiAIgrAWIgJgHIAXgbQAcgjAIgSQAKgRgHgGIgFgCIgIgCIACgHIAiACQAZABAGAEQAIAGgGAMQgEAMgeApQA5gLAVgCQAZgCAHAGQAJAGgGAPQgFAQgZAlQAsgLAfgDQAdgCAIAGQAPAMg9BdIgKAPgAnigwQgwg/gOgOQgNgNgHAFIgEAEIgEAGIgHgDIARgvQAEgPAEgCQAFgEAFACQAEACALANIB6CaIgTAzgAlugcQgQgDgHgMQgIgRAMgZQANgZAdgYQgPgKgKgCQgNgDgHAEIgEADIgEAFIgGgDIARgiQADgIADgEQACgDADgBQALgGAjASQAgARBAAsIgWAoQgegagZgGQgWgGgZANQgIAEgDAGQgFAGAEAFQAEAJAOAAQANABAPgIIAJgGIAHgIIAGACQgGAXgKAPQgJAPgLAFQgKAFgLAAIgJAAgAEChMQgKgEgIgKQgIgIgFgOQgGgOAAgLQAAgMAEgMQAGgPAKgKQAKgKATgJQAUgJARgCQATgDAKAFQAOAGAHAMQAHAMgDALQgEAIgNAGQgPAGgbAFIgWAFIgBgIQANgCAJgFQAJgGACgFQACgHgEgIQgFgJgHgDQgJgDgIAFQgIAFgFAMQgIAVAKAYQAKAZAUAIQAGACAGAAQAHABAIgBIACAHIgbAKQgRAGgJAAIgCAAQgIAAgIgCgAjji3QgWhOgIgQQgIgRgJACIgEACIgGAFIgGgGIAggmQAKgMAEgBQAHgCAEAEQADADAEAQIA8C8IgkApgABrivQgZgjgxgzIgDBsIgxAcQAEgUAEgnIAMhsQgKgMgKgHQgKgHgHAAIgFAAIgGACIgDgIIAfgSIADgBQALgHAFABQALAAArArQArAtAwA+IACgbQADgmgBgUQgBgSgDgLIAygdQgFAfgFAvQgGA+gFA/IgnAYQgEgagYgig");
	this.shape_72.setTransform(-0.5,-55.7);

	this.shape_73 = new cjs.Shape();
	this.shape_73.graphics.f("#FFFFFF").s().p("AKIFUQgGgEgGgGQgGgFgCgFQgHgRARgQQANgMAcgLIAwgSIgLgkIgNAJQgQAJgRABQgRACgUgGQgVgFgTgMQgRgKgHgOQgJgRADgSQADgTAPgJQALgHATAGQAOAEAcAPIATALQAFgSgHgNIgGgHIgJgHIgJgFIALgbIAmAOQARAHAKAGQAJAHAGAKQAGANABAPQAAAOgGAPQgEAPgJAMIgCACIA3AXIABADIAMgFIBDAwIg3ARIAWA3IhJggIgCgFIgTAHQgcAMgHAFIgDADIABABIAKAHIgVAYgAJ4CqQgDABAAAEQgBAFACADQABAEAEAAQAGABAHgEIAJgHQgKgGgIgBIgFgBIgCABgArkE2IgNAfIgUgJIgLgrQgWgKgJgBIgBAAIAAAOIgcACIgMg+IgBgLQABgHACgEQAGgNAYAFQAJACAWAKIAdANIAfhCQgRgHgKgCIABAJIgeAAIgJhCIACgKQAEgJAJgCQAGgCAJAEQAOAEAWALICKBAIAQBRIhpgzIgeBBIA+AeIAQBRgApTCVQgTgRADgiQADgZAMgZIgIAAQgIABgDADIgBACIgBADIgBALIgegDIAHhBQADgGADgDQAMgPAtAGQArAFBGARIACABIhKhhIgWgZIgHgGIgHAPIgagMIATg5QAFgSAHgFQALgIALAFQAHADAMAQIB/CeIgeBOIgagmIgIA+IgOgFQgWgHgKgCIAAABQABAZgDATQgDAUgKALQgOAPgUAEIgLABQgPAAgKgJgAo5BQQgFAGAAADIAAAAQACACAGgDQAKgDAJgKIAEgHIADgGQgQAEgNAOgAHzBtIALgQQAfgsAMgYQAEgLgBgDQgGgEgaAGQgjAHglATIgGADIgTgQIAFgJIAaglQAJgPADgHQADgHgCgBQgGgFgfAHQgiAHgpAWIgGADIgZgTIAfgkQAdglAGgOIAEgJIgUgDIALgdIAJABIAhACQAdACAHAGQAPALgIAUQgEALgOAUQAggGASgBQAdgDALAJQAPAMgIAXQgFANgLASQAcgGAUgCQAigCALAJQAQAMgQAhQgNAdgeAuIgNAVgAl3gSQgWgDgJgRQgMgVAQggQALgXAVgSIgIgDQgIgCgDACIgCABIgCADIgGAJIgagNIAWgtIAIgNQAFgGAEgCQARgJAoAWQAiARA+AsIAEADIgih2IgLgfQgBgEgEgEIgEADIgIAIIgVgUIAmguQALgPAJgDQANgDAJAIQAFAGAGATIAbBYIAiBpIg4A/IgLgwIgeA5IgLgKQgQgNgMgGIAAAAQgHAZgKAPQgLASgNAHQgNAGgOAAIgLgBgAllhdQgGADgCAEQACACAHAAQAJABAMgGIAHgFIAEgFIgGgBQgOAAgNAHgAD3hCQgNgEgKgNQgJgKgGgQQgGgNAAgQQgBgOAFgOQAHgRANgMQALgLAUgKQAWgKATgCQATgDAPAGQASAHAJAPQAKASgGAQQgEALgSAJQgMAFghAHIgVAEQAHARAOAFIAJACIAMAAIAKgBIAIAaIglAOQgQAHgNAAIgEAAQgKAAgJgDgAEYi5QgFADgDAIIgDALQAOgDAFgDQAGgDAAgCIgBgFQgCgFgFgBIgDgBIgDABgAB0hyQgCgWgYghQgTgdgkglIgDA+IACAaIhMAsIAFgYIAIg7QAGgxAGg1QgIgIgHgGQgIgFgEgBIgFACIgLAEIgLgbIAogYIABAAIABgBQAPgIAIABQAQABAsAuQAlAlAmAvQACgkAAgSQgBgSgEgIIgDgJIBQguIgEAYQgEAZgFA0IgNCDIg6Akg");
	this.shape_73.setTransform(0.2,-55.7);

	this.shape_74 = new cjs.Shape();
	this.shape_74.graphics.f("#007844").s().p("AgYDWIgGhzQgNgLgMgGQgLgGgIABIgFACIgGACIgEgHIAggaQAKgJAGAAQANgBAyAmQAyAkBAA9IgDgeQgEgqgDgTQgEgTgGgKIAxgnQAAAZACA6IAJCFIglAgQgIgagfggQgfghg9gtIACAbIAMBXIgtAmIgBhAgAjFCRQAThQACgXQABgUgJgDIgGAAIgIABIgCgHIAxgSQAPgHAGACQAHABABAFQABAFgDARIgWBgIgUBtIg3ATgAD9DoQgMgCgOgIQgNgIgIgKQgJgKgEgNQgEgQACgPQACgSAJgQQALgVANgNQANgNANgEQAOgFAPAGQAOAFAEAMIABAEIgKATIADACQgHAMgTAWIgQASIgGgFQAJgKAFgLQAEgKgCgGQgCgGgKgFQgIgEgJACQgKADgDAJQgDALAEAMQAHAXAXAOQAZAOAVgHQAHgBAGgEQAFgEAGgGIAHAEIgQAaQgKAPgHAHQgJAIgJACQgFACgGAAIgMgBgAkkCiQgNgXgagJQgKgDgHABQgHACgCAFQgEAKAKALQAJAMAQAFQAHACAFAAQAHABAFgBIACAHQgXALgRAEQgUADgLgEQgPgFgLgQQgJgOAFgNQAGgUAdgIQAdgJAnAGQgCgQgHgNQgGgLgIgDIgGgBIgHABIgCgHIA1gQIAHABQAMAFALAoQAMAqAKBMIguANQgDgrgMgXgAFtCoQgZgqgNgPQgOgQgIAFIgDADIgGAIIgDgDIABgCQAHgLgBgIIAGgOQALgZAFgDQAJgFAKAKQALAKAcAsQAKg4AIgaQAIgYAJgFQAIgFAOALQAMAJAcApQAIg0AIgaQAJgdAJgGQATgKBEBhIAKAPIgWAuQghgzgSgTQgRgRgKAFQgLAGgHAkQgGAlADAuIgGAEQgig0gMgLQgNgMgKAFQgMAHgHAnQgGAnAFAyIgKAGgAmrA3QAohJAHgUQAHgTgJgFIgFgBIgIgBIAAgIIA0gGQARgCAEADQAGADAAAFQABAFgJAQIgtBWIgwBkIg6AFgAoPByQAKgrgGgYQgEgagYgQQgHgFgIgBQgHgBgEAEQgGAIAGAOQAGAOANAJIALAGIAMADIAAAHQgYAEgUgBQgSgDgKgHQgOgKgEgRQgGgRAIgLQANgQAeABQAeAAAjAQQADgQgDgNQgCgMgIgGIgEgDIgHgBIAAgHIAoAAQALAAADABQAEAAAEACQAKAIgBAqQgBApgNBNgAElBsgAJsAMQgHgIABgNQAAgRAIgdIAGgXIAIACQgEAPABAKQAAAKAFAFQAFAFAKAAQALgBAGgGQAHgGgBgKQgBgKgKgKQgQgRgcgCQgcgBgQAPQgGAGgCAFQgEAFgDAJIgIgBIADgfQABgTAEgIQAEgJAHgHQAIgIANgEQANgEAPACQAOAAANAGQALAFAKAJQAKAMAHAPQAEAPAAAVQgBAZgEAPQgGASgKAJQgLAKgPACIgEAAQgNAAgHgHgAqygOIACgCIBThGIhChMIg8AzIg4gNIBVhHIgYgbIAEgEIAuAIQAZgWAHgJQAFgKgEgFIgEgDIgGgDIACgHIBAAQIAGAEQAHAIglAhIghAeIBCBNIAIgHQAigfgHgIIgCgCIgEgCIADgHIA4AQIAFAEQADADgBAEQgBAEgEAEIgdAbIh2BngAL3hxQgQgEgZgRIg0goIgYAfIgIgsIAGgIIgPgMIACg4IAmAgIAZgkIACgDIAIAxIgIALIAcAVQAaASAMAEQAOAFAEgHIACgDIAAgFIAJABIgBAjQgBAIgBAGIgDAJQgFAGgIAAIgJgBg");
	this.shape_74.setTransform(-0.2,66.5);

	this.shape_75 = new cjs.Shape();
	this.shape_75.graphics.f("#FFFFFF").s().p("AgcEUIgBg+IgFhuQgHgHgMgGQgIgEgFABIgCAAIgOAIIgPgaIAmgeIAAgBIABgBQAOgLAJAAQASgCA1AoQAtAhAwAtQgDglgFgWQgDgRgFgKIgEgIIBKg7IAAAXQAAAcADA3IADAqQACgNAJgPQAKgVAPgQQAPgQAQgEQATgGARAHQAIADAFAEQALgWAIgFQAQgJAQAQQAKAJAPAWQAHgjAGgTQAKgdAMgHQAQgKAUASQALAKAOASQAFgbAHgYQAKgiAOgIQAPgJAXAVIABgZQACgTAFgLQAEgMAJgJQALgKAOgEQAQgEARABIAFABIgGgkIACgDIgLgJIAEhVIAuAnIAEgGIANgUIASgZIAOBRIgEAGIAEADIAOALQAZAQALAEIAFACIAAgDIABgLIAfADIgCAuQAAAKgCAGQgBAHgEAGQgKAQgYgGQgPgEgdgTIgsghIgKANQAMAGAIAIQANAOAGARQAGARAAAXQgBAagFASQgHATgLAMQgPAOgTABQgUADgNgNQgLgLABgTQABgNAHgjIAHgYQgVAAgMALQgDAEgCAFQgEAEgCAHIgCAKIgJgBQAQASAdAoIAPAVIgjBGIgLgRQgegwgTgUQgKgKgDAAQgHAEgFAeQgGAhADAvIAAAIIgXAMIgggyIgRgUQgIgFgBAAQgIAFgFAhQgGAkAEAyIABAHIgbAQIgWgsQgagsgLgMIgIgHIgLASIgEgDQgHAKgNAOIgQATQATAJAPgFIAJgEIARgPIAYAQIgXAkQgKAQgJAIQgLAJgKADQgNAEgRgDQgPgEgOgIQgOgIgKgMQgJgLgDgKIADAtIg3AvIgFgRQgHgWgdgfQgcgcgqghIAAABIAHBCIAGAaIhHA6gADyBzQgEACgBADQgCAGACAJIAGAMIAAAAQAKgMACgGQACgHAAgCQAAgBgFgDIgGgBIgEAAgAKOhKQgDANAAAIQAAAGACACQACACAFAAQAFAAAEgEQADgCAAgFQgBgHgGgGQgEgEgHgEgAjeDhIg7AQIgBgOQgBgWgDgPIgCABQgYAMgTAEQgVAEgPgFQgUgHgMgSQgLgRADgPIhSAGIARgdIg/gCIAEgOQAFgTABgSIgCABQgYAEgWgCQgVgCgNgJQgRgNgFgVQgFgMADgLIhNgRIBehPIgzg7Ig3AvIhVgTIASgPIAqgjIAigcIgYgbIARgPIAuAIQASgQAHgKIABgBIgEgCIgLgEIAKgcIBBAQIALAEQAGACAEAFQAKALgQAUQgHAKgTAQIgYAWIAzA8QASgSAEgGIgJgDIALgcIA4AQIAMAEQAFACADAEQAHAIgCAJQgBAHgHAHIgdAcIgfAbIAsAAIAQABQAIACAEADQAQAMgCAvQgBApgKBBIA7hxQAMgXADgJIACgKIgRgBIgBgdIA+gHQAVgDAHAEQANAHAAAMQAAAKgKARIhTCkQAMgJAPgEQAYgIAiADIgFgKQgEgIgFgBIgCgBIgOADIgJgcIAygPQAMgEAEAAQAIgBAEACQASAGAOAvQAKAoAKBBIAch7QAFgdAAgGQABgEgBgHIgBAAIgQAEIgJgcIA7gWQASgIAJACQANAEAEALQACAJgEASIgVBgQgLA0gKA5IgBAGIhSAdgAlTCKIAAABQgBADAEAGQAIAIANAFIAJACIAIAAQgLgRgTgGQgEgCgEAAIgDAAgAowASQAFAKALAJIAIAFIAIACQgFgTgRgMQgFgEgFgBIgBAAQgBAEACAGgApGgbIAJAAQAaAAAdAMIgBgKQgBgJgFgDIgCgBIgDgBIgLgBIAAgWg");
	this.shape_75.setTransform(-0.9,66.6);

	this.shape_76 = new cjs.Shape();
	this.shape_76.graphics.f("#FFFFFF").s().p("AFOD4QgEgKAAgWIACgvIgcgCIAWgXIAHABIABgOIAigWIgCAkIAhgBIgaAZIgKAAIAAAZQAAAYADAIQADAJAFAAIADAAQAAAAAAAAQABAAAAgBQABAAAAAAQABAAAAgBIADAGIgWAOIgJAEQgDACgDAAQgIAAgEgLgADdD8QgJgGAAgJQAAgHAIgIQAHgHATgMIAPgJIADAFQgHAEgGAHQgFAGAAAFQAAAFAFAFQAFAFAHAAQAIAAADgGQAFgFAAgKQAAgSgOgPQgOgOgQAAIgKABIgJAEIgEgEIAQgPQALgJAGgCQAHgDAHAAQAIAAAIAEQAHAEAJAJQAHAHADAJQAFAKAAAIQAAALgGAMQgEALgKAJQgNAMgLAHQgLAGgKAAQgLAAgJgHgABRC7QAAg7gCgQQgDgPgFAAIgFABIgFACIgDgFIAagSIAEgCQAKgIAEAAQAFAAACAEQACADAAANIAECWIgjAWgAgRD8QgIgHAAgIQAAgHAHgIQAKgJAQgKIAOgJIAEAFQgJAFgFAGQgFAFAAAGQAAAGAFAEQAGAFAGAAQAIAAAEgGQAEgGAAgJQAAgSgNgPQgPgOgOAAQgGAAgEABQgGACgEACIgEgEIARgPQAJgJAGgCQAHgDAHAAQAHAAAJAEQAJAFAHAIQAHAHAEAJQADAKAAAIQAAANgEAKQgFALgLAJQgKALgMAIQgMAGgLAAQgJAAgJgHgAh1D6QgBgygCgQQgCgPgHAAIgEAAIgGADIgCgGIAlgZQAEgCACAAQAFAAADAFQACAGAAAMIAAAHQAOgPAIgHQALgIADAAQAFAAAFAHQAFAHADANIgcAXQgCgJgEgFQgDgEgEAAQgGAAgEAHQgCAIAAAbIAAAbIgfATgAjjD8QgJgGAAgJQAAgHAIgIQAHgHATgMIAPgJIADAFQgJAFgEAGQgFAGgBAFQAAAFAGAFQAFAFAHAAQAHAAAFgGQAEgFAAgKQAAgSgOgPQgOgOgQAAIgKABIgKAEIgDgEIAQgPQALgJAGgCQAHgDAHAAQAIAAAIAEQAHAEAJAJQAHAHAEAJQADAJAAAJQAAAMgFALQgDAJgLALQgMAMgMAHQgLAGgKAAQgLAAgJgHgACbD1QgPgRgTgcQAPgLAGgGQAGgFgBgGQAAgFgDgCQgDgCgLgDIAcgVQAMABAHAEQAIAFgBAGQAAAFgFAHQgIAHgTAOQAWAbAIAIQALALADAAIAEgBIABAAIAEAEIgaAVIgGABQgGAAgMgOgAlREBIghgIQADgWAAgRIABg1IAAgkIggADIAfgcIArgEQAgAAASALQATAKAAATQAAAIgEAHQgGAHgGAEQANAHAHAJQAGAKABAKQAAALgHAJQgIAKgRAMQgTAOgJAEQgKAFgJAAQgGAAgIgCgAlNDgIASAGIAOACQAOAAAFgFQAGgFAAgLQAAgOgLgKQgMgKgRgCIAAgEQANgDAIgHQAHgHAAgJQAAgMgLgHQgLgGgUAAIgEAAgAmKAnQAAAAgBAAQAAAAAAAAQgBgBAAAAQgBAAAAgBQgCgCAAgDIAAgMIAEgiIADgdIABh2QAAgJgEgOQgEgKgNAAIgIABIgOAGIgGABQgDAAgEgCQgDgCAAgEIAAgDIABgGQAAAAAAgBQABAAAAAAQAAgBABAAQAAAAABAAIBZgtIACABIAFgBIAEAAQAFAAABAFQACAGAAAIIgEAzIAACEIAAAHIACAIIADAGIALAPQADAEAAADIgBAGQgBACgEACIg1AhIgFABgAD8gMQgggngBg0IABgVQAIgtAegoQAfgoAjgIIAOgBQAxABAgAoQAgAnAAA0IAAAGQgCAogYAkQgXAkgbATQgMAIgLAEQgLAFgJAAQgyAAgegogAERjJQgNATgCAXIgBAHIAAAGIACAUQAHAjAYAfQAaAfAdAKIAJACIAKABQAUAAANgRQAMgQAFgXIAAgNQAAgJgCgLIgFgTQgLghgagdQgZgdgcgEIgEgBIgFAAIAAgBQgWABgNATgAgGgMQgfgoAAgzIABgVQAHguAdgnQAfgoAjgIIAOgBQAyABAgAoQAfAoAAAzIAAAGQgCAogXAkQgYAkgbATQgLAIgMAEQgLAFgJAAQgxAAgfgogAAPjJQgMASgDAYIAAAHIAAAQIABAKQAHAjAZAfQAYAfAfAKIAJACIAJABQAUAAANgRQANgSADgVIABgNQAAgLgCgJIgFgTQgMgigZgcQgYgdgdgEIgEgBIgEAAIAAgBQgXABgNATgAkHgMQgggnAAg0IABgVQAHgtAegoQAggoAjgIIANgBQAyABAfAoQAhAnAAA0IAAAGQgCAogYAkQgXAkgcATQgLAIgLAEQgLAFgKAAQgxAAgfgogAjyjJQgMATgDAXIgBAHIAAAGIACAUQAHAjAZAfQAZAfAdAKIAKACIAJABQAVAAANgRQAMgSAEgVIABgNQAAgJgDgLIgEgTQgMghgZgdQgagdgbgEIgFgBIgEAAIAAgBQgXABgNATg");
	this.shape_76.setTransform(-0.2,1.7);

	this.shape_77 = new cjs.Shape();
	this.shape_77.graphics.f("#007844").s().p("AgdJSIgmgFIgkgMIgbgLIgfgTIgsgmIgUAAIgpgFIgTgEIg3gWIghgUIgPgLIgbgaIgXgdIgTggIAAgBIgjgSIghgYIgigiIgbgoIgQglIgEgOIgcgEIgbgHIgqgVIgigdIgQgVIgRgdIgHgJIgGgFIgCgCIgCgCIgCgCIgCAAIgCgCIgDgBIgCgCIgDgBIgCgBIgDgBIgDgBIgDgBIgOgHIgcgQIgYgTIAggYIAEgCIAhgQIADgBIAFgCIADgBIACgBIADgBIAKgIIAEgEIAJgLIANgYIAUgZIATgSIAVgPIAYgMIAMgGIAxgKIACAAIAGgZIAJgZIAUgmIAUgbIASgTIAigcIAPgJIAfgQIAEgIIAUgfIAYgdIAkgfIApgXIASgIIAmgMIApgFIAegBIAAgBIAagYIAegVIAagOIAigOIAlgJIAngEIAnACIA0ALIAjAOIAxAeIAgAdIAAABIAUAAIAzAGIAvAPIAiASIAfAWIAiAhIALAPIAUAfIAEAIIAfAQIAWAPIAbAWIAXAaIAYAkIASAnIAHAZIABAHIABAAIAjAFIAUAGIAfAPIAWAOIAcAaIAWAgIADAFIAIAOIAIAJIADABIACACIAGAFIADABIAFADIADABIACABIADACIAHADIADABIAgAPIAhAVIghAXIgmASIgEABIgFACIgDABIgDACIgCABIgFADIgJAHIgEAEIgMASIgDAGIgWAeIgSATIgVAQIgRAKIgZALIgbAHIghAFIgKAcIgSAkIgTAaIgiAiIghAYIgjASIAAABIgJAQIgaAmIgiAhIgXARIgzAbIgwANIgpAFIgUAAIgVAUIgdAWIgmAVIhBAUIgfADgAhvImQAyAYA4AAQA3AAAygYQAxgXAhgpIAWABQBKAAA9goQA7gnAchAQA5gWAogtQAogvALg7IALAAQAzAAApgZQAogYAVgqQAPgeAfgPIA2gaIg2gaQgfgOgPgeQgVgpgogZQgpgZgzAAIgIAAQgHhBgqgzQgogzg+gXQgchBg7gmQg9gohKgBIgWABQghgogxgYQgygYg3AAQg4AAgyAYQgxAYghAoIgWgBQhKABg9AoQg7AmgcBBQg+AXgoAzQgqA0gHBAIgCAAQgzAAgpAZQgoAZgVApQgPAegeAOQgsATgJAHQAJAHAsAUQAeAOAPAeQAVAqAoAYQApAZAzAAIAFAAQALA7AoAvQAoAtA5AWQAcBAA7AnQA9AoBKAAIAWgBQAhApAxAXgAhfHfQgqgUgegjIgSABQhBAAg1gjQg0gjgYg3QgygTgjgoQgjgogJgzIgFAAQgsAAgkgXQgjgVgSgkQgOgagagMQgmgSgIgGQAIgHAmgQQAagMAOgaQASgkAjgVQAkgXAsAAIACAAQAHg4AkgtQAjgsA2gUQAYg5A0ghQA1gkBBAAIASABQAegjAqgUQArgVAxAAQAxAAArAVQAqAUAeAjIASgBQBBAAA1AkQA0AhAYA5QA2AUAjAsQAkAtAHA5IAHgBQAsAAAkAXQAjAVASAkQAOAaAaAMIAwAXIgwAYQgaAMgOAaQgSAkgjAVQgkAXgsAAIgKgBQgJAzgjApQgjAogyATQgYA3g0AjQg1AjhBAAIgSgBQgeAjgqAUQgrAWgxAAQgxAAgrgWg");
	this.shape_77.setTransform(-0.3,0);

	this.shape_78 = new cjs.Shape();
	this.shape_78.graphics.f("#FFFFFF").s().p("Ah4JZQg2gYgngsIgKAAQhRAAhEgpQhFgqgihHQg5gZgpgsQgpgvgQg5Qg2gEgsgdQgsgdgZgwQgGgMgJgIQgHgHgNgGIgegNQgQgJgNgJIgbgXIAbgUQANgLAQgHIAegOQANgGAHgHQAJgHAGgNQAYgvAsgdQAqgcA0gFQAOhAAqgzQAqgzA+gbQAihGBFgqQBEgqBRAAIAKAAQAngrA2gYQA2gYA8ABQA7gBA2AYQA2AYAnArIAKAAQBRAABEAqQBEAqAjBGQA9AbArAzQAqAyAOBAQA2AEAtAdQAsAdAZAwQALAWAWALIAgAOQASAHAMAJIAiAWIgiAYQgMAIgSAJIggANQgWALgLAWQgZAxguAeQguAcg4ADQgRA5gpAvQgpAsg4AZQgjBHhEAqQhEAphRAAIgKAAQgnAsg2AYQg2AXg7gBQg8ABg2gXg");

	this.shape_79 = new cjs.Shape();
	this.shape_79.graphics.f("#FFDE23").s().p("AmCOVQi0hLiJiKQiKiKhLizQhPi5AAjKQAAjKBPi5QBLiyCKiKQCJiKC0hMQC4hODKAAQDKAAC6BOQCzBMCJCKQCKCKBMCyQBOC5AADKQAADKhOC5QhMCziKCKQiJCKizBLQi6BPjKAAQjKAAi4hPg");

	this.shape_80 = new cjs.Shape();
	this.shape_80.graphics.f("#007844").s().p("AmRO2Qi5hPiOiOQiPiPhOi4QhRjAAAjSQAAjRBRi/QBOi5CPiPQCOiPC5hOQDBhRDQAAQDSAAC/BRQC5BOCPCPQCPCPBOC5QBRC/AADRQAADShRDAQhOC4iPCPQiPCOi5BPQi/BRjSAAQjRAAjAhRg");

	this.shape_81 = new cjs.Shape();
	this.shape_81.graphics.f("#FFFFFF").s().p("AmhPcQjAhRiViVQiUiUhRjAQhUjHAAjbQAAjZBUjHQBRjBCUiUQCViVDAhRQDIhUDZgBQDaABDHBUQDBBRCUCVQCVCUBRDBQBUDHABDZQgBDbhUDHQhRDAiVCUQiUCVjBBRQjHBVjagBQjZABjIhVg");

	this.shape_82 = new cjs.Shape();
	this.shape_82.graphics.f("#FFFFFF").s().p("AnsSQQjjhhiwivQivivhgjkQhkjrAAkCQAAkBBkjrQBgjkCvivQCwivDjhhQDrhjEBAAQECAADrBjQDkBhCvCvQCvCvBgDkQBkDrAAEBQAAEChkDrQhgDkivCvQivCvjkBhQjrBjkCAAQkBAAjrhjg");

	this.shape_83 = new cjs.Shape();
	this.shape_83.graphics.f("#007844").s().p("An4SsQjphji0izQizi0hjjpQhmjxAAkIQAAkHBmjxQBjjpCzi0QC0izDphjQDxhmEHAAQEIAADxBmQDpBjC0CzQC0C0BiDpQBmDxAAEHQAAEIhmDxQhiDpi0C0Qi0CzjpBjQjxBmkIAAQkHAAjxhmg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_83},{t:this.shape_82},{t:this.shape_81},{t:this.shape_80},{t:this.shape_79},{t:this.shape_78},{t:this.shape_77},{t:this.shape_76},{t:this.shape_75},{t:this.shape_74},{t:this.shape_73},{t:this.shape_72},{t:this.shape_71},{t:this.shape_70},{t:this.shape_69},{t:this.shape_68},{t:this.shape_67},{t:this.shape_66},{t:this.shape_65},{t:this.shape_64},{t:this.shape_63},{t:this.shape_62},{t:this.shape_61},{t:this.shape_60},{t:this.shape_59},{t:this.shape_58},{t:this.shape_57},{t:this.shape_56},{t:this.shape_55},{t:this.shape_54},{t:this.shape_53},{t:this.shape_52},{t:this.shape_51},{t:this.shape_50},{t:this.shape_49},{t:this.shape_48},{t:this.shape_47},{t:this.shape_46},{t:this.shape_45},{t:this.shape_44},{t:this.shape_43},{t:this.shape_42},{t:this.shape_41},{t:this.shape_40},{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-129.8,-129.7,259.6,259.6);


(lib.ClipGroup_0 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("AlvGsQAaggAagmQCIAPCsgcQCrgbB8g3QgkBjjOAwQh6AciCAAQhPAAhSgKgAkuFTQBJhwAoh+QApiBAFiJQBfACCKghQCRgiB2g5QgNAugBBXIACCdQADDBgsAcQiCBPikAbQhVAOhjAAQg7AAhBgFgAiPjIQAAgqgDglQCCAFCOgrQCYgsBahMIgLBBQgFAlgFAIQgGAKgfASQhaA1iBAiQh9AhhtACg");
	mask.setTransform(36.8,43.8);

	// Layer 3
	this.instance = new lib.Image_0();
	this.instance.parent = this;
	this.instance.setTransform(0,0,0.239,0.239);

	var maskedShapeInstanceList = [this.instance];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.ClipGroup_0, new cjs.Rectangle(0,0,73.6,87.7), null);


(lib.ClipGroup = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("AjLG7QgEglADgKQAEgLAZgZQBLhIB0hBQByg+BpgcIAFAWQAJAhANArQh/AbiBBMQiHBPhFBggAjuDiIgniYQgyi6AkgmQBshsCYhBQB/g2CpgdQgrB+gJCEQgJCIAdCGQheAUh/BCQiCBEhmBTQACgwgUhVgAhZmHQCzhcDZgZQgRAjgRAtQiGATihBEQifBEhsBTQAMhpC8hgg");
	mask.setTransform(35.6,50.9);

	// Layer 3
	this.instance = new lib.Image();
	this.instance.parent = this;
	this.instance.setTransform(0,0,0.239,0.239);

	var maskedShapeInstanceList = [this.instance];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.ClipGroup, new cjs.Rectangle(4.9,0,61.4,101.7), null);


(lib.Tween8 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AAGHGQiwgMiig0Qixg4hlhWQh0hhAEh3QAnA2BHA3QBEA1BbAxIADAAIAXgGIApgRIAbgRIAIgHIAdgdIAuhNIAPgkIB5l1ICjhJIhlFkIgSAqIgcA1IgIAMIgkAsIgpAmIgkAYIg1AaIg9AUIhTASIC/AQIBlgCIA2gIIAMgDIAsgPIAcgQIAWgRIAWgVIAcgmIAYgrIAKgdIAIgrIgBgnIgEgOIgDgHIgHgMIgNgNIgTgNIgFgCIg4gSIgDgCIgCgBIgDgDIAAgBIABgBIAAAAIBqARIAVAGIBIAbIAUANIAVARQAVAYABAZIgCAvIgEAVIgIAVIgKATIgWAbIgSARIgyAhQhcAmhYANIhKAFIktgGIgDgBQCvBaC4AqQDGAtCMgeQAYgFANgFQASgHAOgLIARgPIAkgtIAUgmIAMgqIAEgrIgBgeIgIgrIgVgxIhCh8IgthiIgRhNIAAgXIACgXIAJgfQAQgeAkgnIAQgOIACgBIABgBIACgBIACgCIAWgPIAvgZIAlgNIBEgLIAmgBIArAEIAqAMIAYAMIATAPIAHAHIAGAJIAIATIACAKIAAALIgDAfIgJAcIgHANIgTAXIgTAQIgqAbIggANIgeAIIAgg1IAZg6IAJgpIAAgkIgDgOIgFgNIgHgKIgKgIIgMgGIgNgDIgXgBIgOACIgHACIgOAGIgSAMIgFAFIgLALIgPAUIgIARIgHAZIgEAbIABAfIAFAeIAJAfIBrDNIADAGIADAGIADAFIAbBDIAMA6IgDA5IgEAZIgJAfIgKAWIgQAbIgUAYIgMAMIgaAWIg9AkIhDAbIgjAKIgfAIIgfAJQhUAWhzAAQgsAAgwgDgAoHAjIgSgkIgKgfIgFghIgCgvIAFgmIALgnIARgnIAWgkIASgWIAegeIAjgcIA3ghIAzgUIBigYIBagKIChACIBnATIAwASIAmAWIAPAMIAMAOIAJAOIAGAPIAEAQIACARIAAAFIgBADIAAAFIAAADIAAADIgDAQQgQArgjAaIgjAVIgoAQIicAiIAEgDIAXgJIAfgUIAWgVIAaghIAPgdIAMgfIAEgUIADgfIgDgcIgIgYIgOgVIgTgQIgYgNIgdgJIghgGIhTAAIhWAPIhSAcIhQArIg7ArIg2AwIg0A/IgaAuIgLAeIgFAeIAAAdIAGAbIANAaIASAaIAIAJIgLAOg");
	this.shape.setTransform(-4.5,-1.4,0.884,0.884);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#373535").s().p("Ah5HEIisgmIigg5Ihpg4IhWhDIgrg0Igeg4IgQg+IgChGIA5BGIA6A1IBaBBIBQAwIAfgJIAlgSIAYgRIAHgGIAOgOIAXghIAgg8IAFgMIB/mGICshMIg8gFIhAAEIg/ALIg8AUIhPAmIg5AmIhJA/IgoAuIgLAPIgZAsIgFAPIgIAcIgCAbIAAANIAGAYIAMAYIAIAMIAYAeIgdAhIgegrIgSgoIgKgsIgEgwIADgoIAKgoIAKgcIATgnIAZgkIAdghIAjgdIA2gkIAygWIBMgXIBWgOICNgHIBWAGIAvAIIBEATIAtAVIAiAaIAOAOIAKAQIAGAOIARgYIAegjIATgQIACgBIABgBIACgBIALgJIA9ghIA1gRIA4gIIBUADIAYAFIAcALIAeASIAKAIIAPATIAFALIAGAXIAAAeIgCAQIgLAfIgRAbIgLANIgbAWIgeATIgpARIg/APIArhCIAXguIAIgVIAHgeIADgbIgBgPIgDgMIgEgKIgGgIIgHgGIgJgFIgLgCIgUgBIgSAEIgLAFIgRALIgFAEIgOAPIgNAVIgDAHIgFAPIgFAhIAAATIAFAnIAGATIAhBIIBcCtIAZBEIAIAwIgBApIgIA1IgLAgIgPAeIgTAbIgkAkIgeAWIhAAjIhGAZIiQAjIiVAJgAjFFTICfAsIBZAOIB/AHIA6gFIA3gKIAYgIIAOgIIAHgFIAmgoIASgaIAUgvIAIgoIAAgnIgIgyIgVgwIhGiFIgphaIgPg9IgSAWIgSAPIgcASIghAQIgJADIhnAYIAeAGIBJAWIAmARIAoAeIAKANIAIANIADAHIAEAQIAAAtIgDAYIgHAXIgWApIgaAcIgUARIgqAbIhcAgIhdAUIhKAGIjwgDgAgxhXIgWBGIghBFIgIANIgkAwIgJAKIgsAnIgyAgIgbAOIhNAbIgIACIACABICnAGIBggKIAsgNIATgJIAcgSIAUgSIAKgKIAcgkIAWgqIAKgbIAGgbIACgbIgBgPIgCgNIgCgGIgFgLIgHgJIgHgHIgMgJIgIgEIg1gQIgFgCIgHgFIgDgCgAgvhfIAbgUIACgEIACABIAJgGIAbgLIAOgJIAVgSIAageIAPgbIAJgTIAJgcIADgUIABggIgFgXIgEgKIgGgJIgGgJIgIgHIgJgHIgKgGIgZgLIgGgBg");
	this.shape_1.setTransform(-4.6,-1.4,0.884,0.884);

	this.instance = new lib.ClipGroup_0();
	this.instance.parent = this;
	this.instance.setTransform(100.6,14,0.884,0.884,0,0,0,37.4,45.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#D3AE6D").s().p("AlvGsQAaggAagmQCIAPCsgcQCrgbB8g3QgkBjjOAwQh6AciCAAQhPAAhSgKgAkuFTQBJhwAoh+QApiBAFiJQBfACCKghQCRgiB2g5QgNAugBBXIACCdQADDBgsAcQiCBPikAbQhVAOhjAAQg7AAhBgFgAiPjIQAAgqgDglQCCAFCOgrQCYgsBahMIgLBBQgFAlgFAIQgGAKgfASQhaA1iBAiQh9AhhtACg");
	this.shape_2.setTransform(100.1,12.9,0.884,0.884);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#D3AE6D").s().p("AgVB0QgCgDgJhBQgMhUgihSQBWANAwBEQA0BJhFA2QgmAdgOAAQgGAAgCgDg");
	this.shape_3.setTransform(124.3,-36,0.884,0.884);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#373535").s().p("AkCJFIgFgBIgKAAIgBAAIgJAAIgDgBIg1gDIgBAAIgkgFIgCAAIgBAAIgGgBIgDAAIgQgCIAHgZQBsiBA7idQA8iiAAiuQAAg0gFg2QC8AQC6hQIAFgRQgWihg9iZIAzACQCJAcA3BWQA9BfgfBoQg1B7AMDwQASD5hYBmQhCBLifAhIgHACIgBAAIg0AJIgBAAIg1AGIgBABIgIABIgBAAIgmADIgmABIgCABg");
	this.shape_4.setTransform(100.5,2.1,0.884,0.884);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#373535").s().p("AgTBwQgTgIgMgSQgOgTgFgYQgHgkAMghQALghAcgZQAbgYApgJIABAFQgcANgOAOQgOAPgHASQgGASgBAZQAFgFAFgDQAKgGAJgCQAXgEAUAOQATAOAGAdQAEAUgFAUQgGAUgNAMQgPANgRADIgNACQgMAAgNgGgAgLgEQgFAAgKAHIAFAgQAEAVAIAUQAIATAIAHQAHAFAGgCQAHgBAFgKQADgJgEgaQgIgrgOgOQgHgGgJAAIgEAAg");
	this.shape_5.setTransform(24.1,104.3,0.884,0.884);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#373535").s().p("Ag8BuQAZgHASgNQARgOAJgSQAKgSACgWIgQAIIgNADQgZABgRgQQgSgQgCgeQgBgWAHgSQAHgRAQgMQAQgLASgBQASgCARAKQASAKALAUQALATADAbQACAhgOAgQgPAggeAVQgeAVgsAFgAgWheQgGAJACAbQAEAtALAOQAHAKALgBQAGAAAKgGIgBgeQgCgdgIgZQgFgNgHgGQgEgDgGAAQgJABgDAHg");
	this.shape_6.setTransform(6.7,106,0.884,0.884);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#373535").s().p("AhIBrQAbgGASgLQATgKAKgSQALgQAGgXQgKAFgHABQgHACgHAAQgYgBgQgSQgQgSABgeQABgVAJgSQAKgRARgKQARgJAQABQATAAAQAMQASANAIATQAJAWAAAZQgCAigRAeQgTAggfAQQggASgsAAgAgNhdQgHAKAAAaQgCArAKARQAGAKALABQAGAAALgEIACgfQABgegGgYQgDgOgHgHQgEgDgFAAIgBAAQgHAAgFAGg");
	this.shape_7.setTransform(-13,105.6,0.884,0.884);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#373535").s().p("Ag5BeIABgGIAGABQAOACAGgCQAGgCADgGQADgFADgUIARhuQACgOgBgEQAAgEgDgDQgDgDgFgBQgJgCgMAEIgCgHIBUgXIAFABIgcCtQgDAVABAFQABAFAFAFQAFAEAMACIAFABIgBAGg");
	this.shape_8.setTransform(-29.7,104.3,0.884,0.884);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#D3AE6D").s().p("Ah+C1IikgVIg0gLIiEgkIgPgFIgDgBIBNkZICTAdIBeAMID4AEICYgNICxgjIAOgEIBMEXIgXAIIiuAwIjRAcg");
	this.shape_9.setTransform(-0.6,103.6,0.884,0.884);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#373535").s().p("AlHCqIi3g0IBVk1IAKADIBYATIDaAaIDHAAIC2gUICbgjIBUEyIgkAOIjCA0IjBAYIjFABg");
	this.shape_10.setTransform(-0.6,103.5,0.884,0.884);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#D3AE6D").s().p("AhvEEIh0gLIhwgTIidgqIhRgeIBwmcIAIACIBwAcIDQAdIA+AEIDCgEIDOgeICNglIBwGZIhmAnIi4AxIjJAZIh7ADg");
	this.shape_11.setTransform(-0.6,102.9,0.884,0.884);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#373535").s().p("AlFEAQiTgdiBg3IB7nAQDdBFD1AAQEFAADnhOIB6G9QiFA9iaAgQifAhioAAQihAAiYgeg");
	this.shape_12.setTransform(-0.6,102.7,0.884,0.884);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#373535").s().p("AMlEXIAEgDIAEAFQAFAGAEADQADABAFgBQADgBALgHIBfhFQALgJABgCQADgDgBgFQgBgGgEgFIgEgFIAEgDIA3BLIgEACIgEgEQgFgGgEgDQgEgCgEACQgFABgJAIIhfBEQgLAIgBADQgDADABAFQABAGAEAFIAEAFIgEADgAueE2IACgFQAbAMAVgEQAWgEALgQQAIgLgBgNQgBgKgJgIQgFgDgHgBQgHgBgIADQgIACgRAJQgYANgNADQgMAEgMgCQgMgBgKgHQgRgMgDgWQgDgVAOgUQAGgHAGgEIANgJQAIgFADgDQACgCgBgEIgGgIIADgEIAwAiIgDAEQgUgKgUAFQgTAEgLAOQgIAKACALQABAKAHAGQAFACAGABQAGABAKgDIAdgOQAfgQARgBQASAAAOAJQASAOADAXQACAYgPAVQgGAIgFAEQgGAFgKAGQgGAEgCADQgCACAAAFQgBAEADAGIgCADgAJ4BeIADgEIAEAEQAHAGAEABQAEABAEgDQADAAAKgLIBPhUQAKgJAAgEQACgDgCgFQgBgEgGgGIgEgEIADgEIBFBAIgDADIgGgFQgFgFgGgBQgEgBgEACQgDABgJAJIhNBTQgKALAAADQgCAEACAEQACADAJAJIALAKQAKAJAKADQAKADANgEQAKgCAVgLIAEADIgvApgArgCNQgigGgbgfIgEgHIBkhTQgUgVgUgIQgagLgVARQgQAOgDASQgCASAKATIgEADIgkgrIADgEQAKAHAGgGQAEgCAIgPQAJgOAMgJQAdgZAkAEQAlAFAYAeQAbAegDAkQgDAlgeAaQgbAWgdAAIgKgBgAr2BNQAWAaAOAKQAZAQAUgRQAigdgzg8gAHoACIACgDIADABQAJAFAFgBQADAAADgEIABgDIAEgjIg2geIgPALQgIAHgCAEQgEAGAEAHQABADAMAIIgCAEIg0gdIADgDQAKADAHgDQALgEAPgMIB8hdIACACIgOCdQgCAUACAJQADAGAGAFIgCADgAHYhKIAvAaIAGhCgAoSABIAGgCQAHgDADgEQACgDAAgFQAAgDgFgMIgvhrQgHgNgCgCQgDgDgEAAQgFAAgHADIgGACIgCgFIBVglIACAEIgGADQgHADgDAEQgCAEAAAEQAAAEAFAMIAVAtIA5gZIgVguQgEgKgEgEQgDgDgFgBQgGABgFACIgGADIgCgFIBVgmIACAFIgGACQgHAEgDAEQgCADAAAFQAAACAFANIAwBqQAFANADADQADACAFAAQAEABAHgDIAGgCIACAEIhVAlIgCgEIAGgCQAGgCAEgFQACgEAAgEQAAgEgFgMIgXgyIg5AZIAXAyQAFAMADADQADADAFAAQAEAAAIgCIAFgDIACAFIhVAkgADOhmIABgFIAJADQAIABAEgCQAFgBACgEQACgCADgOIAHgjIgZhSQgHgYgEgGQgEgEgJgDIABgEIBTASIgBAEIgEgBQgHgBgEABQgDACAAADQgCAEAHAUIATBAIAqgtQAQgQABgHQABgDgDgEQgEgDgLgEIABgEIA1ALIgBAFQgIgBgHADQgGADgUAWIgzA3IgJApIgCARQABAEAEADQADAEAHABIAKACIgBAEgAkKhoIAGAAQAIgCADgEQADgCACgFQABgDgDgNIgShyQgCgNgCgEQgCgDgFgCQgDgCgJABIgFABIgBgEIBdgQIABAGIgIABQgHABgFADQgDACgBAFQgBADACANIASBwQACAOACACQACAEAFACIAQgCIAOgDQAPgCAIgGQAIgGAFgMQAFgKADgXIAFgBIAEA/IibAYgAgLh9IAAgEIAGAAQAGgBAGgCQAEgDABgDIACgRIACh0IgBgRQgCgDgEgDQgDgCgIgBIgGAAIAAgEIBcABIAAAFIgGgBQgIAAgFADQgDACgCAEQgCAEAAAMIgCB1QAAAOABADQACAEAEACQAEACAIABIAGAAIAAAFgAAYlAQgFgGAAgIQAAgIAGgFQAFgGAIAAQAJAAAFAGQAFAFAAAIQAAAJgGAFQgFAGgIgBQgIAAgGgFg");
	this.shape_13.setTransform(-0.6,-83.6,0.884,0.884);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#373535").ss(2.8,0,0,22.9).p("AAANFQCqAACbhCQCWg/B0h0QB0h0BAiWQBCicAAiqQAAiphCicQhAiWh0h0Qh0h0iWg/QibhCiqAAQipAAicBCQiWA/h0B0Qh0B0g/CWQhCCcAACpQAACqBCCcQA/CWB0B0QB0B0CWA/QCcBCCpAAg");
	this.shape_14.setTransform(-0.6,-5,0.884,0.884);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.rf(["#D53F52","#CC3E4D","#B93C45","#A43137","#972D30","#5E403E"],[0,0.169,0.298,0.439,0.631,1],0,0,0,0,0,118.4).s().p("AlFMDQiVhAh1h0QhzhzhAiWQhCicAAiqQAAipBCibQBAiXBzh0QB1h0CVg/QCchCCpAAQCqAACbBCQCWA/B1B0QBzB0BACXQBCCbAACpQAACqhCCcQhACWhzBzQh1B0iWBAQibBCiqAAQipAAichCg");
	this.shape_15.setTransform(-0.6,-5,0.884,0.884);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#373535").ss(2.7,0,0,22.9).p("AAAvKQDGAAC2BNQCvBJCHCHQCHCGBKCuQBMC0AADFQAADFhMC1QhKCuiHCGQiHCHivBJQi2BNjGAAQjFAAi1hNQivhJiHiHQiHiGhLiuQhMi1AAjFQAAjFBMi0QBLiuCHiGQCHiHCvhJQC1hNDFAAg");
	this.shape_16.setTransform(-0.6,-4.9,0.884,0.884);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#D3AE6D").s().p("Al9OLQixhLiIiIQiJiJhLixQhOi2ABjIQgBjHBOi3QBLiwCJiJQCIiICxhLQC2hNDHgBQDHABC4BNQCwBLCICIQCJCJBLCwQBOC3AADHQAADIhOC2QhLCxiJCJQiICIiwBLQi4BOjHgBQjHABi2hOg");
	this.shape_17.setTransform(-0.6,-5,0.884,0.884);

	this.instance_1 = new lib.ClipGroup();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-109,2.5,0.884,0.884,0,0,0,33.1,51);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#D3AE6D").s().p("AjLG7QgEglADgKQAEgLAZgZQBLhIB0hBQByg+BpgcIAFAWQAJAhANArQh/AbiBBMQiHBPhFBggAjuDiIgniYQgyi6AkgmQBshsCYhBQB/g2CpgdQgrB+gJCEQgJCIAdCGQheAUh/BCQiCBEhmBTQACgwgUhVgAhZmHQCzhcDZgZQgRAjgRAtQiGATihBEQifBEhsBTQAMhpC8hgg");
	this.shape_18.setTransform(-106.9,2.4,0.884,0.884);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#D3AE6D").s().p("Ag3A9QhEg7A2hEQAogyANAKQADACAXA9QAhBQAzBHIgWABQhIAAg3gwg");
	this.shape_19.setTransform(-112.9,54.2,0.884,0.884);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#373535").s().p("AiiIxQhShMAEhtQAWiEhGjnQhOjtA+h4QArhWCVhJIAMgGIABgBIAygVIABgBIArgQIAAAAIAHgDIABAAIAMgEIACgBIAOgFIABAAIAHgCIAAgBIAwgOIABAAIALgDIABgBIAzgNIABAAIAbgGIACAAIAXgFIABAAIAKgCIABgBIAKgBIADgBIAmgGIACAAIAkgFIABAAIABgBIAPgBIACAAIAIgBIAAAZQhKCXgTCnQgUCsArCpQAMAzATAyQi9AfieB7QAAADgIAHQgHAHAAADQBIC6BTBhQgYAIgMABIgVAAQh7AAhEhBg");
	this.shape_20.setTransform(-106.9,10.5,0.884,0.884);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#373535").ss(2.7,0,0,22.9).p("AAAzyQEDAADsBjQDkBhCwCvQCxCvBgDkQBkDrAAEBQAAEChkDrQhgDkixCvQiwCvjkBhQjsBjkDAAQkCAAjthjQjkhhiwivQiwivhgjkQhkjrAAkCQAAkBBkjrQBgjkCwivQCwivDkhhQDthjECAAg");
	this.shape_21.setTransform(-0.6,-4.9,0.884,0.884);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#FFFFFF").s().p("AnvSQQjkhhiwivQiwivhgjkQhkjrAAkCQAAkBBkjrQBgjkCwivQCwivDkhhQDthjECAAQEDAADsBjQDkBhCwCvQCxCvBgDkQBkDrAAEBQAAEChkDrQhgDkixCvQiwCvjkBhQjsBjkDAAQkCAAjthjg");
	this.shape_22.setTransform(-0.6,-4.9,0.884,0.884);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#D3AE6D").s().p("AoSToQj0hoi9i8Qi8i9hoj0Qhrj+AAkVQAAkUBrj9QBoj1C8i8QC9i+D0hnQD+hrEUAAQEVAAD9BrQD1BnC8C+QC9C8BoD1QBrD9AAEUQAAEVhrD+QhoD0i9C9Qi8C8j1BoQj9BrkVAAQkUAAj+hrgAlIsFQiXBAh1B0Qh0B1hACWQhDCcAACqQAACrBDCcQBACXB0B0QB1B1CXA/QCdBCCrAAQCrAACdhCQCXg/B1h1QB1h0BAiXQBCicAAirQAAiqhCicQhAiWh1h1Qh1h0iXhAQidhCirAAQirAAidBCg");
	this.shape_23.setTransform(-0.6,-5,0.884,0.884);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#373535").s().p("AodUDQj6hqjBjBQjAjAhqj6QhtkDAAkbQAAkaBtkDQBqj5DAjCQDBjAD6hqQEDhtEagBQEbABEDBtQD6BpDADBQDBDCBqD5QBtEDAAEaQAAEbhtEDQhqD6jBDAQjADBj6BqQkDBtkbAAQkbAAkChtg");
	this.shape_24.setTransform(-0.6,-5,0.884,0.884);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.instance_1},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.instance},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-138.3,-128,274.6,256);


(lib.Symbol2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Tween6("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(211.7,211.8);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({rotation:90},80).to({rotation:180},81).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,423.4,423.5);


(lib.Symbol1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 11
	this.instance = new lib.Tween2("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(150.6,153.3,0.151,0.151);
	this.instance.alpha = 0;
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(118).to({_off:false},0).to({scaleX:0.82,scaleY:0.82,y:153.2,alpha:1},27).wait(51).to({startPosition:0},0).to({regX:0.1,regY:0.1,scaleX:1.16,scaleY:1.16,x:150.7,alpha:0},27).wait(687));

	// Layer 12
	this.instance_1 = new lib.Tween8("synched",0);
	this.instance_1.parent = this;
	this.instance_1.setTransform(150.1,153.9,0.293,0.293);
	this.instance_1.alpha = 0;
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(22).to({_off:false},0).to({scaleX:0.82,scaleY:0.82,alpha:1},27).wait(44).to({startPosition:0},0).to({scaleX:0.92,scaleY:0.92,alpha:0},28).wait(789));

	// Layer 2
	this.instance_2 = new lib.Symbol2();
	this.instance_2.parent = this;
	this.instance_2.setTransform(151.4,151.6,0.702,0.702,0,0,0,211.8,211.8);
	this.instance_2.alpha = 0;
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(4).to({_off:false},0).to({alpha:1},14).wait(199).to({alpha:0},8).wait(685));

	// Layer 14
	this.instance_3 = new lib.Tween13("synched",0);
	this.instance_3.parent = this;
	this.instance_3.setTransform(603.5,150);
	this.instance_3.alpha = 0;
	this.instance_3._off = true;

	this.instance_4 = new lib.Tween14("synched",0);
	this.instance_4.parent = this;
	this.instance_4.setTransform(-302.6,150);
	this.instance_4.alpha = 0;

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_3}]},537).to({state:[{t:this.instance_3}]},22).to({state:[{t:this.instance_3}]},324).to({state:[{t:this.instance_4}]},23).wait(4));
	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(537).to({_off:false},0).to({x:551.8,alpha:1},22).to({x:-246},324).to({_off:true,x:-302.6,alpha:0},23).wait(4));

	// Layer 13
	this.instance_5 = new lib.Tween12("synched",0);
	this.instance_5.parent = this;
	this.instance_5.setTransform(40.5,150);
	this.instance_5.alpha = 0;
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(412).to({_off:false},0).to({x:79.3,alpha:1},26).to({x:226.7},99).to({x:259.5,alpha:0},22).wait(351));

	// Layer 3
	this.instance_6 = new lib.Tween9("synched",0);
	this.instance_6.parent = this;
	this.instance_6.setTransform(150,263);
	this.instance_6.alpha = 0;
	this.instance_6._off = true;

	this.instance_7 = new lib.Tween10("synched",0);
	this.instance_7.parent = this;
	this.instance_7.setTransform(150,37);
	this.instance_7.alpha = 0;

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_6}]},227).to({state:[{t:this.instance_6}]},26).to({state:[{t:this.instance_6}]},159).to({state:[{t:this.instance_7}]},26).wait(472));
	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(227).to({_off:false},0).to({y:235.2,alpha:1},26).to({y:64.9},159).to({_off:true,y:37,alpha:0},26).wait(472));

	// Layer 1
	this.instance_8 = new lib.Tween4("synched",0);
	this.instance_8.parent = this;
	this.instance_8.setTransform(150,150);

	this.timeline.addTween(cjs.Tween.get(this.instance_8).wait(910));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,300,300);


// stage content:
(lib.minbereket300x3002 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("rgba(69,69,69,0.4)").ss(1,1,1).p("A3U3UMAupAAAMAAAAupMgupAAAg");
	this.shape.setTransform(150,150);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	// Layer 1
	this.instance = new lib.Symbol1();
	this.instance.parent = this;
	this.instance.setTransform(150,150,1,1,0,0,0,150,150);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(149.7,149.7,300.6,300.6);
// library properties:
lib.properties = {
	width: 300,
	height: 300,
	fps: 26,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [
		{src:"/static/lam/300x300/images/Image.png?1498230826691", id:"Image"},
		{src:"/static/lam/300x300/images/Image_0.png?1498230826691", id:"Image_0"},
		{src:"/static/lam/300x300/images/all300.png?1498230826691", id:"all300"},
		{src:"/static/lam/300x300/images/all2300.png?1498230826691", id:"all2300"},
		{src:"/static/lam/300x300/images/sosis300.png?1498230826691", id:"sosis300"}
	],
	preloads: []
};




})(lib = lib||{}, images = images||{}, createjs = createjs||{}, ss = ss||{}, AdobeAn = AdobeAn||{});
var lib, images, createjs, ss, AdobeAn;