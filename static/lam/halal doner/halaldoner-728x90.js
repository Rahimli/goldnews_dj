(function (lib, img, cjs, ss, an) {

var p; // shortcut to reference prototypes
lib.ssMetadata = [
		{name:"halaldoner_728x90_atlas_", frames: [[0,234,195,358],[0,594,338,180],[0,0,395,232]]}
];


// symbols:



(lib.donerr = function() {
	this.spriteSheet = ss["halaldoner_728x90_atlas_"];
	this.gotoAndStop(0);
}).prototype = p = new cjs.Sprite();



(lib.logox2 = function() {
	this.spriteSheet = ss["halaldoner_728x90_atlas_"];
	this.gotoAndStop(1);
}).prototype = p = new cjs.Sprite();



(lib.qabda = function() {
	this.spriteSheet = ss["halaldoner_728x90_atlas_"];
	this.gotoAndStop(2);
}).prototype = p = new cjs.Sprite();



(lib.Tween12 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#007844").s().p("AgjByQgFgCgBgGIAAgGQACgZgCgUIgIgxQgFgcAGgUQAFgVAfgWQAhgYACgEQAAAEASAmQAPAjgFAVQgFASgUAXQgaAfgDAIQgPAZgIARQgFAHgDAAIgBAAg");
	this.shape.setTransform(7,-25.7,0.231,0.231);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#007844").s().p("AAbAeQgagvgwgPIARgaQAVgYATALQAYAMAKA0QAIAvgIAkQgDgVgOgZg");
	this.shape_1.setTransform(4.5,-24.8,0.231,0.231);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#007844").s().p("AgYgPQAhgoAbAAQAXABAGAfQADAQgCAOQgxgKguAcIglAfQAKgjAggkg");
	this.shape_2.setTransform(8.3,-24,0.231,0.231);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#007844").s().p("Ag7BmQgFgDABgGIACgGQAJgbACgRIAFgwQADgdAKgSQALgTAjgNQAngPADgCQgBADAHAqQAGAlgLATQgKAQgZASQgfAXgHAGQgWAXgLAMQgFAEgDAAIgCAAg");
	this.shape_3.setTransform(13.4,-23,0.231,0.231);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#007844").s().p("AAPAgQgNg1gqgbIAXgVQAZgSARAQQATATgEA0QgEAwgRAgQACgWgGgag");
	this.shape_4.setTransform(10.9,-22.7,0.231,0.231);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#007844").s().p("AgOgRQAqgdAaAHQAWAHgCAgQgBAOgGAPQgtgYgzAQIgsAUQATgfAogbg");
	this.shape_5.setTransform(14.1,-21.1,0.231,0.231);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#007844").s().p("AhQBTQgEgEACgGIADgFQAPgVAHgSIASgtQALgcAPgOQAOgQAmgDQAqgFADgBQgCADgEAqQgEAlgPAPQgOAOgdAKQgkAOgJAFQgVAMgTANIgIACIgDgBg");
	this.shape_6.setTransform(18.8,-18.8,0.231,0.231);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#007844").s().p("AgEAgQACg3gjglIAbgOQAdgLANAUQAOAXgSAxQgQAugYAaQAIgUAAgbg");
	this.shape_7.setTransform(16.6,-19,0.231,0.231);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#007844").s().p("AgfAFIgvAIQAagYAugQQAxgSAXAOQAUAMgLAdQgFAPgJANQgmgjg2ACg");
	this.shape_8.setTransform(19,-16.6,0.231,0.231);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#007844").s().p("AhkA/QgDgFAEgFIAFgEQASgQANgQQAKgNATgbQASgXARgLQATgLAlAHQApAHAEgBQgCACgPAoQgOAigTALQgQAKgfACQgnAEgJADQgcAHgSAHIgEAAQgFAAgCgCg");
	this.shape_9.setTransform(23,-13.3,0.231,0.231);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#007844").s().p("AgUAfQAQg0gYgtIAdgHQAggCAGAWQAIAageAqQgaApgfASQAMgRAIgag");
	this.shape_10.setTransform(21.1,-14.1,0.231,0.231);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#007844").s().p("AgfgOIgwgEQAggRAwgEQA1gEASATQAQARgSAZIgVAXQgbgqg1gNg");
	this.shape_11.setTransform(22.6,-10.9,0.231,0.231);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#007844").s().p("AgFAxQgogHgJAAQgaAAgWACQgJgBgBgEQgBgFAFgEIAFgDQAXgLAPgMQAOgIAZgWQAXgTATgFQAVgFAjAPQAmASAEAAQgEACgYAhQgWAfgVAFQgKADgMAAQgMAAgOgDg");
	this.shape_12.setTransform(25.7,-7,0.231,0.231);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#007844").s().p("AgYAeQAcgugKgyIAeACQAfAGABAXQAAAbgoAhQgkAggjAKg");
	this.shape_13.setTransform(24,-8.3,0.231,0.231);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#007844").s().p("AgdgaIgugRQAkgIAuAJQA1AJAMAYQALATgYAVIgaARQgPgwgvgag");
	this.shape_14.setTransform(24.7,-4.5,0.231,0.231);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#007844").s().p("AgQAnQgkgRgKgCQgfgIgPgDQgIgEAAgDQgBgFAHgCQACgCADAAQAYgEATgIQAQgGAdgOQAbgMAUAAQAXAAAcAYQAhAbAEAAQgFABggAbQgdAYgWAAQgTAAgbgNg");
	this.shape_15.setTransform(26.6,0,0.231,0.231);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#007844").s().p("AgbAfQAnglACgzIAdAJQAdAOgFAXQgGAZgwAWQgrAWglABg");
	this.shape_16.setTransform(25.2,-2,0.231,0.231);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#007844").s().p("AgbgeIgngbQAkABArAVQAwAXAGAYQAFAXgcAOIgeAJQgCgzgnglg");
	this.shape_17.setTransform(25.2,1.9,0.231,0.231);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#007844").s().p("AAQAzQgRgFgYgUQgfgZgIgEQgZgOgRgIIgEgEQgDgEAAgCQACgFAGgBIAGAAQAcACARgCQAQgCAhgGQAcgEAUAFQAVAGAWAfQAZAhADACQgEAAgmARQgZALgSAAQgHAAgGgBg");
	this.shape_18.setTransform(25.7,7,0.231,0.231);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#007844").s().p("AhLAsIAugRQAvgaAPgwIAbARQAYAUgMAUQgMAXg0AKQgYAFgVAAQgVAAgRgEg");
	this.shape_19.setTransform(24.8,4.5,0.231,0.231);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#007844").s().p("AgXgdIggglQAjAKAlAgQAoAhgBAbQgBAYgfAGIgeABQALgygcgug");
	this.shape_20.setTransform(24,8.2,0.231,0.231);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#007844").s().p("AABA5QgQgJgSgZQgXgggHgHQgQgRgSgPIgDgEQgCgFABgCQADgFAGABQADAAADACQAXAIAUADQAQACAhADQAdADASALQATALANAiQAPAoACACQgDgBgqAHQgNACgKAAQgUAAgNgHg");
	this.shape_21.setTransform(23,13.3,0.231,0.231);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#007844").s().p("AABAoQgvgDghgSIAwgEQA1gNAbgqIAVAXQASAZgQARQgPAQglAAIgTgBg");
	this.shape_22.setTransform(22.6,10.9,0.231,0.231);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#007844").s().p("AACBKIgegHQAYgtgQg0IgUgsQAfATAbApQAeAqgIAaQgGAVgbAAIgFgBg");
	this.shape_23.setTransform(21.1,14.1,0.231,0.231);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#007844").s().p("AAnBOQglgEgPgPQgNgOgLgdQgOgkgFgIQgOgZgLgQQgEgJADgCQAGgGAJAIQAUANATAJQAPAGAfALQAbALAOAPQAQAOAEAmQAEAqACADQgEgCgqgEg");
	this.shape_24.setTransform(18.8,18.8,0.231,0.231);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#007844").s().p("AgGAdQgugRgagYIAwAJQA2ABAlgjIAPAcQAKAdgUAMQgKAGgQAAQgTAAgbgJg");
	this.shape_25.setTransform(19,16.6,0.231,0.231);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#007844").s().p("AgJBMIgcgPQAjglgBg2IgJgwQAYAaARAuQARAxgOAXQgIANgQAAQgIAAgJgDg");
	this.shape_26.setTransform(16.6,19,0.231,0.231);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#007844").s().p("AAPBVQgjgOgLgTQgKgQgCgfQgEgngDgJQgHgcgHgSIAAgFQAAgFACgBQAFgDAGAEIAEAFQARAUAPALQANAKAaATQAXARALARQALATgHAmQgGApABAEQgDgDgngOg");
	this.shape_27.setTransform(13.3,23,0.231,0.231);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#007844").s().p("AgOASQgpgbgSgfIAsAUQAzAQAtgYIAHAdQACAggWAHQgGABgGAAQgXAAghgXg");
	this.shape_28.setTransform(14.1,21.1,0.231,0.231);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#007844").s().p("AgRBGIgXgVQAqgbANg1IAEgwQASAgADAwQAFA1gUASQgIAIgKAAQgKAAgOgKg");
	this.shape_29.setTransform(10.8,22.6,0.231,0.231);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#007844").s().p("AgMBWQgfgWgGgVQgFgTAGgdQAGgoAAgKQAAgfgBgQIABgFQABgFACAAQAJgDAEAMQALAXALAPIAfAnQASAXAFATQAGAVgQAiQgRAmAAAEQgCgDghgYg");
	this.shape_30.setTransform(7,25.7,0.231,0.231);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#007844").s().p("AgYARQgfgkgLgkIAlAfQAuAcAygKIgBAeQgGAfgYABIgBAAQgaAAghgng");
	this.shape_31.setTransform(8.3,24,0.231,0.231);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#007844").s().p("AgeA8IgRgbQAwgPAagvIARguQAIAkgIAvQgKA0gYAMQgHAEgFAAQgOAAgOgQg");
	this.shape_32.setTransform(4.5,24.7,0.231,0.231);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#007844").s().p("AgbBSQgYgeAAgWQAAgTANgbQARgkACgKQAJggACgOIACgEQADgFACAAQAIAAABAMQAFAcAHAPQAGAQAOAeQAMAaAAAUQAAAWgYAeQgbAggBAEQAAgEgbggg");
	this.shape_33.setTransform(0,26.6,0.231,0.231);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#007844").s().p("AAMBDQgZgGgXgwQgVgrAAgkIAbAnQAlAnAzACIgJAeQgMAXgSAAIgHAAg");
	this.shape_34.setTransform(2,25.2,0.231,0.231);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#007844").s().p("AgwArIgJgdQAzgCAlgnIAbgoQgBAlgVArQgXAvgYAHIgHABQgSAAgMgZg");
	this.shape_35.setTransform(-2,25.2,0.231,0.231);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#007844").s().p("AgoBIQgQgjAGgVQAFgSAUgXQAZgfAEgIQAPgaAHgQIAEgEQAEgDACAAQAIACgCAMQgCAZACAUIAIAxQAEAcgFAUQgGAVgeAWQghAYgCAEQAAgEgSgmg");
	this.shape_36.setTransform(-7,25.6,0.231,0.231);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("#007844").s().p("AgKBIQgXgMgKg1QgIgvAIgjIARAuQAaAvAwAPIgRAaQgOARgOAAQgGAAgHgEg");
	this.shape_37.setTransform(-4.5,24.8,0.231,0.231);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("#007844").s().p("AgjA4QgYgBgFgfIgCgeQAyAKAugcIAlgfQgKAjggAkQghAogaAAIgBAAg");
	this.shape_38.setTransform(-8.3,24,0.231,0.231);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("#007844").s().p("Ag9A5QgGglALgTQAKgQAZgSQAfgXAHgHQAWgWAKgNIAEgCQAFgCACABQAIAEgGALQgIAWgDAVQgCAQgDAgQgDAdgLASQgLATgiAOQgoAPgCACQABgEgHgpg");
	this.shape_39.setTransform(-13.3,23,0.231,0.231);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f("#007844").s().p("AgYBIQgUgSAFg1QADgwASggIAEAwQANA1AqAbIgXAVQgOAKgKAAQgKAAgIgIg");
	this.shape_40.setTransform(-10.9,22.6,0.231,0.231);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f("#007844").s().p("Ag1AoQgXgHADggIAGgdQAuAYAzgQQAbgIARgMQgTAfgoAaQgiAYgXAAQgGAAgFgBg");
	this.shape_41.setTransform(-14.1,21.1,0.231,0.231);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f("#007844").s().p("AhNAnQAEglAPgPQAOgOAdgKQAkgOAIgFQAZgOAQgLIAFgCQAFgBABACQAGAGgIAJQgMASgKAVIgRAtQgLAbgPAPQgOAQgmADQgpAEgEACQACgDAEgqg");
	this.shape_42.setTransform(-18.8,18.8,0.231,0.231);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f("#007844").s().p("AgfBCQgOgXASgxQAQguAYgaIgIAvQgCA3AjAlQgNAJgPAFQgKAEgHAAQgQAAgIgNg");
	this.shape_43.setTransform(-16.6,19,0.231,0.231);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f("#007844").s().p("AhBAgQgUgNALgdIAOgbQAlAjA2gCQAcAAAUgIQgaAYguAQQgbAKgTAAQgQAAgKgGg");
	this.shape_44.setTransform(-19,16.6,0.231,0.231);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f("#007844").s().p("Ag4A+QgqgHgDABQADgCAOgoQANgiATgLQARgKAfgDQAngDAJgDQAWgGAXgIIAFAAQAGAAABACQAEAIgKAGQgTARgMAPIgdAoQgSAXgRAKQgMAIgUAAQgLAAgNgDg");
	this.shape_45.setTransform(-23,13.3,0.231,0.231);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f("#007844").s().p("AgnA2QgHgZAegrQAagpAfgTIgUAsQgQA0AYAtQgOAGgPABIgFABQgbAAgHgVg");
	this.shape_46.setTransform(-21.2,14.1,0.231,0.231);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f("#007844").s().p("AhHAZQgQgRASgaIAVgWQAbAqA1ANQAaAGAWgCQggARgwAEIgRABQgnAAgPgQg");
	this.shape_47.setTransform(-22.6,10.8,0.231,0.231);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f("#007844").s().p("AhGApQgmgSgEAAQADgCAYghQAWgfAWgFQASgFAeAFQAnAGAKAAQAWABAZgCIAFABQAFACAAACQADAIgMAEQgVAKgRANIgnAeQgXASgTAGQgFABgHAAQgSAAgZgLg");
	this.shape_48.setTransform(-25.7,7,0.231,0.231);

	this.shape_49 = new cjs.Shape();
	this.shape_49.graphics.f("#007844").s().p("AgXBCQgfgGgBgYQAAgbAoghQAkggAjgKIgfAlQgcAuAKAyIgLAAQgJAAgKgBg");
	this.shape_49.setTransform(-24,8.3,0.231,0.231);

	this.shape_50 = new cjs.Shape();
	this.shape_50.graphics.f("#007844").s().p("AgHArQg0gKgMgXQgLgTAYgVIAagRQAPAwAvAaQAZANAVAEQgSAEgUAAQgVAAgYgFg");
	this.shape_50.setTransform(-24.7,4.5,0.231,0.231);

	this.shape_51 = new cjs.Shape();
	this.shape_51.graphics.f("#007844").s().p("AhRAcQgggbgEgBQAFAAAfgaQAegZAVAAQAUAAAbANQAkARAJACQAXAHAYAEIAEACQAEADABACQAAAIgNABQgYAFgTAHIgtAUQgaAMgVAAQgVAAgegYg");
	this.shape_51.setTransform(-26.6,0,0.231,0.231);

	this.shape_52 = new cjs.Shape();
	this.shape_52.graphics.f("#007844").s().p("AgrAxQgcgOAFgXQAGgYAwgXQArgVAkgBIgnAbQgnAlgCAzQgPgCgPgHg");
	this.shape_52.setTransform(-25.2,1.9,0.231,0.231);

	this.shape_53 = new cjs.Shape();
	this.shape_53.graphics.f("#007844").s().p("AgMAkQgvgWgHgZQgFgXAdgOIAdgJQACAzAnAlQAUATAUAJQglgBgrgWg");
	this.shape_53.setTransform(-25.2,-2,0.231,0.231);

	this.shape_54 = new cjs.Shape();
	this.shape_54.graphics.f("#007844").s().p("AgqAxQgVgFgWgfQgYghgEgCQAEAAAmgSQAjgPAVAFQASAFAXAUQAfAZAIAEQAZAPARAIIAEADQADAEAAACQgCAJgMgDQgXgCgWACIgxAIQgMACgLAAQgOAAgLgDg");
	this.shape_54.setTransform(-25.7,-7.1,0.231,0.231);

	this.shape_55 = new cjs.Shape();
	this.shape_55.graphics.f("#007844").s().p("Ag6AfQgYgVALgTQAMgYA0gJQAvgJAjAIIgtARQgvAagPAwQgOgGgMgLg");
	this.shape_55.setTransform(-24.8,-4.5,0.231,0.231);

	this.shape_56 = new cjs.Shape();
	this.shape_56.graphics.f("#007844").s().p("AgPAZQgoghAAgbQABgXAfgGQAQgDAOACQgKAxAcAuQAOAYARANQgjgKgkggg");
	this.shape_56.setTransform(-24,-8.3,0.231,0.231);

	this.shape_57 = new cjs.Shape();
	this.shape_57.graphics.f("#007844").s().p("ABXA+QgWgHgVgEIgxgFQgdgEgSgKQgTgLgNgiQgPgogDgCQAEABApgHQAmgGATALQAPAJASAZQAXAgAHAHQAWAWANAKIADAEQACAFgBACQgDAEgEAAQgEAAgEgCg");
	this.shape_57.setTransform(-23,-13.4,0.231,0.231);

	this.shape_58 = new cjs.Shape();
	this.shape_58.graphics.f("#007844").s().p("AhFATQgSgaAQgRQATgTA0AEQAwAEAgARQgWgCgaAGQg1ANgbAqQgMgJgJgNg");
	this.shape_58.setTransform(-22.7,-10.9,0.231,0.231);

	this.shape_59 = new cjs.Shape();
	this.shape_59.graphics.f("#007844").s().p("AgQAPQgegrAHgZQAHgWAgACQAPABAOAGQgYAtAQA0QAIAaAMARQgfgTgagog");
	this.shape_59.setTransform(-21.1,-14.1,0.231,0.231);

	this.shape_60 = new cjs.Shape();
	this.shape_60.graphics.f("#007844").s().p("ABEBQQgVgPgSgHIgtgSQgbgLgPgOQgQgPgDgmQgEgpgCgEQADACAqAEQAmAEAOAPQAOAOALAdQANAkAFAJQARAdAIALQAEAJgDACQgCADgEAAQgEAAgFgEg");
	this.shape_60.setTransform(-18.8,-18.8,0.231,0.231);

	this.shape_61 = new cjs.Shape();
	this.shape_61.graphics.f("#007844").s().p("AhKALQgLgdAUgNQAXgOAxASQAuAQAaAYQgUgHgbgBQg2gCgmAjQgJgMgFgPg");
	this.shape_61.setTransform(-19,-16.6,0.231,0.231);

	this.shape_62 = new cjs.Shape();
	this.shape_62.graphics.f("#007844").s().p("AgbAHQgSgxAOgWQANgVAdALQAPAFAMAJQgjAmACA2QABAbAHAUQgYgagQgug");
	this.shape_62.setTransform(-16.6,-19,0.231,0.231);

	this.shape_63 = new cjs.Shape();
	this.shape_63.graphics.f("#007844").s().p("AA0BkIgEgFQgRgUgPgLIgngdQgYgSgKgRQgLgTAGglQAHgpgBgEQADADAnAOQAjAOALATQAJARADAeQAEAnACAJIAOAuQABAJgDACIgEABQgCAAgEgCg");
	this.shape_63.setTransform(-13.3,-23,0.231,0.231);

	this.shape_64 = new cjs.Shape();
	this.shape_64.graphics.f("#007844").s().p("AAeAVQgzgQguAYQgFgPgBgPQgDgfAXgHQAZgHArAdQAoAbATAfQgRgMgbgIg");
	this.shape_64.setTransform(-14.1,-21.2,0.231,0.231);

	this.shape_65 = new cjs.Shape();
	this.shape_65.graphics.f("#007844").s().p("AgnAAQgFg1AUgSQARgQAZASQANAJAKAMQgqAbgNA1QgGAaACAWQgSgggDgwg");
	this.shape_65.setTransform(-10.8,-22.6,0.231,0.231);

	this.shape_66 = new cjs.Shape();
	this.shape_66.graphics.f("#007844").s().p("AAeBuIgDgGQgLgXgMgPIgegnQgSgXgFgTQgGgVAQgjQARgmAAgEQACAEAhAYQAfAWAFAVQAFATgFAdQgGAoAAAJQAAAgABAQQgBAJgEABIgCABQgEAAgDgEg");
	this.shape_66.setTransform(-7,-25.7,0.231,0.231);

	this.shape_67 = new cjs.Shape();
	this.shape_67.graphics.f("#007844").s().p("AAeAZQgugcgyAKQgBgOADgQQAGgfAXgBQAbAAAhAoQAgAkAKAjQgNgRgYgOg");
	this.shape_67.setTransform(-8.3,-24,0.231,0.231);

	this.shape_68 = new cjs.Shape();
	this.shape_68.graphics.f("#007844").s().p("AgqgHQAKg0AXgMQAUgLAVAYQAKAMAGAOQgwAPgaAvIgRAtQgIgjAJgvg");
	this.shape_68.setTransform(-4.5,-24.7,0.231,0.231);

	this.shape_69 = new cjs.Shape();
	this.shape_69.graphics.f("#007844").s().p("AgFBwIgCgGQgFgZgHgSIgUguQgMgaAAgUQAAgWAYgdQAbghAAgEQABAEAbAhQAYAdAAAWQAAATgNAbQgRAkgCAKQgGAUgFAaQgEAJgDAAQgFAAgCgGg");
	this.shape_69.setTransform(0,-26.6,0.231,0.231);

	this.shape_70 = new cjs.Shape();
	this.shape_70.graphics.f("#007844").s().p("AAfAcQglgngzgCIAJgdQAOgdAXAFQAYAHAXAvQAVArABAlQgIgUgTgUg");
	this.shape_70.setTransform(-2,-25.2,0.231,0.231);

	this.shape_71 = new cjs.Shape();
	this.shape_71.graphics.f("#007844").s().p("AgkgMQAXgwAZgGQAXgFAOAcQAHAPACAPQgzACglAnIgbAnQAAgkAVgrg");
	this.shape_71.setTransform(2,-25.2,0.231,0.231);

	this.shape_72 = new cjs.Shape();
	this.shape_72.graphics.f("#007844").s().p("AKLFDIgFgHQgEgKALgKQAMgLAZgKIA7gWIgMgiIAnAQIADAIIARgFIArAeIgsAOIAPAnIACACIgsgTIgEgLIgfALQgdAMgIAHQgKAHACAIIACADIAEADIgGAHgAqyE+QgTgKgdgNIgOAfIgFgCIgKgqQgdgOgKgBQgKgCgEAHIgBAEIAAAGIgGABIgKg0IgBgKIABgGQAEgJArATIAmARIAphWIgJgEQgpgSgDAKIgBADIAAADIgHAAIgHgsIgBgKQAAgDABgDQACgEAEAAQADgBAGACICnBNIALA0IgDgBIhcgtIgoBWIBDAgIAKA1gAKDDWQgVgFgQgKQgPgKgFgKQgIgNACgOQADgPAKgFQAIgFAOAEQAOAFAZAOIAUALIgEAHQgMgHgKgCQgKgDgFAEQgGADgDAKQgBAJADAHQAGAJAJABQAJABALgGQAUgLAIgZQAIgZgKgTQgEgGgEgEIgLgJIADgHIAbAKQAPAFAJAHQAIAGAEAHQAFAKAAAMQAAAMgEAOQgFAOgHAJQgHAJgLAHQgOAIgPAAIgHAAQgMAAgOgEgApFCNQgNgNACgcQACgeAUgeQgPgEgOABQgMACgGAGIgCAFIgCAGIgHgBIAFgzIAEgFQAIgKAnAFQAkAEBKASIgEAtQgngOgYADQgZAEgRATQgGAIgCAGQgBAHAFAEQAHAGANgEQANgEAKgNIAHgJIAEgKIAGAAQACAXgDASQgEARgHAJQgLAMgQADIgIABQgLAAgIgGgAIOBkQAjgxAJgVQAJgVgIgGQgKgHghAGQggAHgqAUIgGgFQAhgtAHgQQAGgPgJgHQgLgIgkAIQgiAIgrAWIgJgHIAXgbQAcgjAIgSQAKgRgHgGIgFgCIgIgCIACgHIAiACQAZABAGAEQAIAGgGAMQgEAMgeApQA5gLAVgCQAZgCAHAGQAJAGgGAPQgFAQgZAlQAsgLAfgDQAdgCAIAGQAPAMg9BdIgKAPgAnigwQgwg/gOgOQgNgNgHAFIgEAEIgEAGIgHgDIARgvQAEgPAEgCQAFgEAFACQAEACALANIB6CaIgTAzgAlugcQgQgDgHgMQgIgRAMgZQANgZAdgYQgPgKgKgCQgNgDgHAEIgEADIgEAFIgGgDIARgiQADgIADgEQACgDADgBQALgGAjASQAgARBAAsIgWAoQgegagZgGQgWgGgZANQgIAEgDAGQgFAGAEAFQAEAJAOAAQANABAPgIIAJgGIAHgIIAGACQgGAXgKAPQgJAPgLAFQgKAFgLAAIgJAAgAEChMQgKgEgIgKQgIgIgFgOQgGgOAAgLQAAgMAEgMQAGgPAKgKQAKgKATgJQAUgJARgCQATgDAKAFQAOAGAHAMQAHAMgDALQgEAIgNAGQgPAGgbAFIgWAFIgBgIQANgCAJgFQAJgGACgFQACgHgEgIQgFgJgHgDQgJgDgIAFQgIAFgFAMQgIAVAKAYQAKAZAUAIQAGACAGAAQAHABAIgBIACAHIgbAKQgRAGgJAAIgCAAQgIAAgIgCgAjji3QgWhOgIgQQgIgRgJACIgEACIgGAFIgGgGIAggmQAKgMAEgBQAHgCAEAEQADADAEAQIA8C8IgkApgABrivQgZgjgxgzIgDBsIgxAcQAEgUAEgnIAMhsQgKgMgKgHQgKgHgHAAIgFAAIgGACIgDgIIAfgSIADgBQALgHAFABQALAAArArQArAtAwA+IACgbQADgmgBgUQgBgSgDgLIAygdQgFAfgFAvQgGA+gFA/IgnAYQgEgagYgig");
	this.shape_72.setTransform(-0.1,-12.9,0.231,0.231);

	this.shape_73 = new cjs.Shape();
	this.shape_73.graphics.f("#FFFFFF").s().p("AKIFUQgGgEgGgGQgGgFgCgFQgHgRARgQQANgMAcgLIAwgSIgLgkIgNAJQgQAJgRABQgRACgUgGQgVgFgTgMQgRgKgHgOQgJgRADgSQADgTAPgJQALgHATAGQAOAEAcAPIATALQAFgSgHgNIgGgHIgJgHIgJgFIALgbIAmAOQARAHAKAGQAJAHAGAKQAGANABAPQAAAOgGAPQgEAPgJAMIgCACIA3AXIABADIAMgFIBDAwIg3ARIAWA3IhJggIgCgFIgTAHQgcAMgHAFIgDADIABABIAKAHIgVAYgAJ4CqQgDABAAAEQgBAFACADQABAEAEAAQAGABAHgEIAJgHQgKgGgIgBIgFgBIgCABgArkE2IgNAfIgUgJIgLgrQgWgKgJgBIgBAAIAAAOIgcACIgMg+IgBgLQABgHACgEQAGgNAYAFQAJACAWAKIAdANIAfhCQgRgHgKgCIABAJIgeAAIgJhCIACgKQAEgJAJgCQAGgCAJAEQAOAEAWALICKBAIAQBRIhpgzIgeBBIA+AeIAQBRgApTCVQgTgRADgiQADgZAMgZIgIAAQgIABgDADIgBACIgBADIgBALIgegDIAHhBQADgGADgDQAMgPAtAGQArAFBGARIACABIhKhhIgWgZIgHgGIgHAPIgagMIATg5QAFgSAHgFQALgIALAFQAHADAMAQIB/CeIgeBOIgagmIgIA+IgOgFQgWgHgKgCIAAABQABAZgDATQgDAUgKALQgOAPgUAEIgLABQgPAAgKgJgAo5BQQgFAGAAADIAAAAQACACAGgDQAKgDAJgKIAEgHIADgGQgQAEgNAOgAHzBtIALgQQAfgsAMgYQAEgLgBgDQgGgEgaAGQgjAHglATIgGADIgTgQIAFgJIAaglQAJgPADgHQADgHgCgBQgGgFgfAHQgiAHgpAWIgGADIgZgTIAfgkQAdglAGgOIAEgJIgUgDIALgdIAJABIAhACQAdACAHAGQAPALgIAUQgEALgOAUQAggGASgBQAdgDALAJQAPAMgIAXQgFANgLASQAcgGAUgCQAigCALAJQAQAMgQAhQgNAdgeAuIgNAVgAl3gSQgWgDgJgRQgMgVAQggQALgXAVgSIgIgDQgIgCgDACIgCABIgCADIgGAJIgagNIAWgtIAIgNQAFgGAEgCQARgJAoAWQAiARA+AsIAEADIgih2IgLgfQgBgEgEgEIgEADIgIAIIgVgUIAmguQALgPAJgDQANgDAJAIQAFAGAGATIAbBYIAiBpIg4A/IgLgwIgeA5IgLgKQgQgNgMgGIAAAAQgHAZgKAPQgLASgNAHQgNAGgOAAIgLgBgAllhdQgGADgCAEQACACAHAAQAJABAMgGIAHgFIAEgFIgGgBQgOAAgNAHgAD3hCQgNgEgKgNQgJgKgGgQQgGgNAAgQQgBgOAFgOQAHgRANgMQALgLAUgKQAWgKATgCQATgDAPAGQASAHAJAPQAKASgGAQQgEALgSAJQgMAFghAHIgVAEQAHARAOAFIAJACIAMAAIAKgBIAIAaIglAOQgQAHgNAAIgEAAQgKAAgJgDgAEYi5QgFADgDAIIgDALQAOgDAFgDQAGgDAAgCIgBgFQgCgFgFgBIgDgBIgDABgAB0hyQgCgWgYghQgTgdgkglIgDA+IACAaIhMAsIAFgYIAIg7QAGgxAGg1QgIgIgHgGQgIgFgEgBIgFACIgLAEIgLgbIAogYIABAAIABgBQAPgIAIABQAQABAsAuQAlAlAmAvQACgkAAgSQgBgSgEgIIgDgJIBQguIgEAYQgEAZgFA0IgNCDIg6Akg");
	this.shape_73.setTransform(0,-12.9,0.231,0.231);

	this.shape_74 = new cjs.Shape();
	this.shape_74.graphics.f("#007844").s().p("AgYDWIgGhzQgNgLgMgGQgLgGgIABIgFACIgGACIgEgHIAggaQAKgJAGAAQANgBAyAmQAyAkBAA9IgDgeQgEgqgDgTQgEgTgGgKIAxgnQAAAZACA6IAJCFIglAgQgIgagfggQgfghg9gtIACAbIAMBXIgtAmIgBhAgAjFCRQAThQACgXQABgUgJgDIgGAAIgIABIgCgHIAxgSQAPgHAGACQAHABABAFQABAFgDARIgWBgIgUBtIg3ATgAD9DoQgMgCgOgIQgNgIgIgKQgJgKgEgNQgEgQACgPQACgSAJgQQALgVANgNQANgNANgEQAOgFAPAGQAOAFAEAMIABAEIgKATIADACQgHAMgTAWIgQASIgGgFQAJgKAFgLQAEgKgCgGQgCgGgKgFQgIgEgJACQgKADgDAJQgDALAEAMQAHAXAXAOQAZAOAVgHQAHgBAGgEQAFgEAGgGIAHAEIgQAaQgKAPgHAHQgJAIgJACQgFACgGAAIgMgBgAkkCiQgNgXgagJQgKgDgHABQgHACgCAFQgEAKAKALQAJAMAQAFQAHACAFAAQAHABAFgBIACAHQgXALgRAEQgUADgLgEQgPgFgLgQQgJgOAFgNQAGgUAdgIQAdgJAnAGQgCgQgHgNQgGgLgIgDIgGgBIgHABIgCgHIA1gQIAHABQAMAFALAoQAMAqAKBMIguANQgDgrgMgXgAFtCoQgZgqgNgPQgOgQgIAFIgDADIgGAIIgDgDIABgCQAHgLgBgIIAGgOQALgZAFgDQAJgFAKAKQALAKAcAsQAKg4AIgaQAIgYAJgFQAIgFAOALQAMAJAcApQAIg0AIgaQAJgdAJgGQATgKBEBhIAKAPIgWAuQghgzgSgTQgRgRgKAFQgLAGgHAkQgGAlADAuIgGAEQgig0gMgLQgNgMgKAFQgMAHgHAnQgGAnAFAyIgKAGgAmrA3QAohJAHgUQAHgTgJgFIgFgBIgIgBIAAgIIA0gGQARgCAEADQAGADAAAFQABAFgJAQIgtBWIgwBkIg6AFgAoPByQAKgrgGgYQgEgagYgQQgHgFgIgBQgHgBgEAEQgGAIAGAOQAGAOANAJIALAGIAMADIAAAHQgYAEgUgBQgSgDgKgHQgOgKgEgRQgGgRAIgLQANgQAeABQAeAAAjAQQADgQgDgNQgCgMgIgGIgEgDIgHgBIAAgHIAoAAQALAAADABQAEAAAEACQAKAIgBAqQgBApgNBNgAEsBXIAAAAgAJsAMQgHgIABgNQAAgRAIgdIAGgXIAIACQgEAPABAKQAAAKAFAFQAFAFAKAAQALgBAGgGQAHgGgBgKQgBgKgKgKQgQgRgcgCQgcgBgQAPQgGAGgCAFQgEAFgDAJIgIgBIADgfQABgTAEgIQAEgJAHgHQAIgIANgEQANgEAPACQAOAAANAGQALAFAKAJQAKAMAHAPQAEAPAAAVQgBAZgEAPQgGASgKAJQgLAKgPACIgEAAQgNAAgHgHgAqygOIACgCIBThGIhChMIg8AzIg4gNIBVhHIgYgbIAEgEIAuAIQAZgWAHgJQAFgKgEgFIgEgDIgGgDIACgHIBAAQIAGAEQAHAIglAhIghAeIBCBNIAIgHQAigfgHgIIgCgCIgEgCIADgHIA4AQIAFAEQADADgBAEQgBAEgEAEIgdAbIh2BngAL3hxQgQgEgZgRIg0goIgYAfIgIgsIAGgIIgPgMIACg4IAmAgIAZgkIACgDIAIAxIgIALIAcAVQAaASAMAEQAOAFAEgHIACgDIAAgFIAJABIgBAjQgBAIgBAGIgDAJQgFAGgIAAIgJgBg");
	this.shape_74.setTransform(0,15.4,0.231,0.231);

	this.shape_75 = new cjs.Shape();
	this.shape_75.graphics.f("#FFFFFF").s().p("AgcEUIgBg+IgFhuQgHgHgMgGQgIgEgFABIgCAAIgOAIIgPgaIAmgeIAAgBIABgBQAOgLAJAAQASgCA1AoQAtAhAwAtQgDglgFgWQgDgRgFgKIgEgIIBKg7IAAAXQAAAcADA3IADAqQACgNAJgPQAKgVAPgQQAPgQAQgEQATgGARAHQAIADAFAEQALgWAIgFQAQgJAQAQQAKAJAPAWQAHgjAGgTQAKgdAMgHQAQgKAUASQALAKAOASQAFgbAHgYQAKgiAOgIQAPgJAXAVIABgZQACgTAFgLQAEgMAJgJQALgKAOgEQAQgEARABIAFABIgGgkIACgDIgLgJIAEhVIAuAnIAEgGIANgUIASgZIAOBRIgEAGIAEADIAOALQAZAQALAEIAFACIAAgDIABgLIAfADIgCAuQAAAKgCAGQgBAHgEAGQgKAQgYgGQgPgEgdgTIgsghIgKANQAMAGAIAIQANAOAGARQAGARAAAXQgBAagFASQgHATgLAMQgPAOgTABQgUADgNgNQgLgLABgTQABgNAHgjIAHgYQgVAAgMALQgDAEgCAFQgEAEgCAHIgCAKIgJgBQAQASAdAoIAPAVIgjBGIgLgRQgegwgTgUQgKgKgDAAQgHAEgFAeQgGAhADAvIAAAIIgXAMIgggyIgRgUQgIgFgBAAQgIAFgFAhQgGAkAEAyIABAHIgbAQIgWgsQgagsgLgMIgIgHIgLASIgEgDQgHAKgNAOIgQATQATAJAPgFIAJgEIARgPIAYAQIgXAkQgKAQgJAIQgLAJgKADQgNAEgRgDQgPgEgOgIQgOgIgKgMQgJgLgDgKIADAtIg3AvIgFgRQgHgWgdgfQgcgcgqghIAAABIAHBCIAGAaIhHA6gADyBzQgEACgBADQgCAGACAJIAGAMIAAAAQAKgMACgGQACgHAAgCQAAgBgFgDIgGgBIgEAAgAKOhKQgDANAAAIQAAAGACACQACACAFAAQAFAAAEgEQADgCAAgFQgBgHgGgGQgEgEgHgEgAjeDhIg7AQIgBgOQgBgWgDgPIgCABQgYAMgTAEQgVAEgPgFQgUgHgMgSQgLgRADgPIhSAGIARgdIg/gCIAEgOQAFgTABgSIgCABQgYAEgWgCQgVgCgNgJQgRgNgFgVQgFgMADgLIhNgRIBehPIgzg7Ig3AvIhVgTIASgPIAqgjIAigcIgYgbIARgPIAuAIQASgQAHgKIABgBIgEgCIgLgEIAKgcIBBAQIALAEQAGACAEAFQAKALgQAUQgHAKgTAQIgYAWIAzA8QASgSAEgGIgJgDIALgcIA4AQIAMAEQAFACADAEQAHAIgCAJQgBAHgHAHIgdAcIgfAbIAsAAIAQABQAIACAEADQAQAMgCAvQgBApgKBBIA7hxQAMgXADgJIACgKIgRgBIgBgdIA+gHQAVgDAHAEQANAHAAAMQAAAKgKARIhTCkQAMgJAPgEQAYgIAiADIgFgKQgEgIgFgBIgCgBIgOADIgJgcIAygPQAMgEAEAAQAIgBAEACQASAGAOAvQAKAoAKBBIAch7QAFgdAAgGQABgEgBgHIgBAAIgQAEIgJgcIA7gWQASgIAJACQANAEAEALQACAJgEASIgVBgQgLA0gKA5IgBAGIhSAdgAlTCKIAAABQgBADAEAGQAIAIANAFIAJACIAIAAQgLgRgTgGQgEgCgEAAIgDAAgAowASQAFAKALAJIAIAFIAIACQgFgTgRgMQgFgEgFgBIgBAAQgBAEACAGgApGgbIAJAAQAaAAAdAMIgBgKQgBgJgFgDIgCgBIgDgBIgLgBIAAgWg");
	this.shape_75.setTransform(-0.2,15.4,0.231,0.231);

	this.shape_76 = new cjs.Shape();
	this.shape_76.graphics.f("#FFFFFF").s().p("AFOD4QgEgKAAgWIACgvIgcgCIAWgXIAHABIABgOIAigWIgCAkIAhgBIgaAZIgKAAIAAAZQAAAYADAIQADAJAFAAIADAAQAAAAAAAAQABAAAAgBQABAAAAAAQABAAAAgBIADAGIgWAOIgJAEQgDACgDAAQgIAAgEgLgADdD8QgJgGAAgJQAAgHAIgIQAHgHATgMIAPgJIADAFQgHAEgGAHQgFAGAAAFQAAAFAFAFQAFAFAHAAQAIAAADgGQAFgFAAgKQAAgSgOgPQgOgOgQAAIgKABIgJAEIgEgEIAQgPQALgJAGgCQAHgDAHAAQAIAAAIAEQAHAEAJAJQAHAHADAJQAFAKAAAIQAAALgGAMQgEALgKAJQgNAMgLAHQgLAGgKAAQgLAAgJgHgABRC7QAAg7gCgQQgDgPgFAAIgFABIgFACIgDgFIAagSIAEgCQAKgIAEAAQAFAAACAEQACADAAANIAECWIgjAWgAgRD8QgIgHAAgIQAAgHAHgIQAKgJAQgKIAOgJIAEAFQgJAFgFAGQgFAFAAAGQAAAGAFAEQAGAFAGAAQAIAAAEgGQAEgGAAgJQAAgSgNgPQgPgOgOAAQgGAAgEABQgGACgEACIgEgEIARgPQAJgJAGgCQAHgDAHAAQAHAAAJAEQAJAFAHAIQAHAHAEAJQADAKAAAIQAAANgEAKQgFALgLAJQgKALgMAIQgMAGgLAAQgJAAgJgHgAh1D6QgBgygCgQQgCgPgHAAIgEAAIgGADIgCgGIAlgZQAEgCACAAQAFAAADAFQACAGAAAMIAAAHQAOgPAIgHQALgIADAAQAFAAAFAHQAFAHADANIgcAXQgCgJgEgFQgDgEgEAAQgGAAgEAHQgCAIAAAbIAAAbIgfATgAjjD8QgJgGAAgJQAAgHAIgIQAHgHATgMIAPgJIADAFQgJAFgEAGQgFAGgBAFQAAAFAGAFQAFAFAHAAQAHAAAFgGQAEgFAAgKQAAgSgOgPQgOgOgQAAIgKABIgKAEIgDgEIAQgPQALgJAGgCQAHgDAHAAQAIAAAIAEQAHAEAJAJQAHAHAEAJQADAJAAAJQAAAMgFALQgDAJgLALQgMAMgMAHQgLAGgKAAQgLAAgJgHgACbD1QgPgRgTgcQAPgLAGgGQAGgFgBgGQAAgFgDgCQgDgCgLgDIAcgVQAMABAHAEQAIAFgBAGQAAAFgFAHQgIAHgTAOQAWAbAIAIQALALADAAIAEgBIABAAIAEAEIgaAVIgGABQgGAAgMgOgAlREBIghgIQADgWAAgRIABg1IAAgkIggADIAfgcIArgEQAgAAASALQATAKAAATQAAAIgEAHQgGAHgGAEQANAHAHAJQAGAKABAKQAAALgHAJQgIAKgRAMQgTAOgJAEQgKAFgJAAQgGAAgIgCgAlNDgIASAGIAOACQAOAAAFgFQAGgFAAgLQAAgOgLgKQgMgKgRgCIAAgEQANgDAIgHQAHgHAAgJQAAgMgLgHQgLgGgUAAIgEAAgAmKAnQAAAAgBAAQAAAAAAAAQgBgBAAAAQgBAAAAgBQgCgCAAgDIAAgMIAEgiIADgdIABh2QAAgJgEgOQgEgKgNAAIgIABIgOAGIgGABQgDAAgEgCQgDgCAAgEIAAgDIABgGQAAAAAAgBQABAAAAAAQAAgBABAAQAAAAABAAIBZgtIACABIAFgBIAEAAQAFAAABAFQACAGAAAIIgEAzIAACEIAAAHIACAIIADAGIALAPQADAEAAADIgBAGQgBACgEACIg1AhIgFABgAD8gMQgggngBg0IABgVQAIgtAegoQAfgoAjgIIAOgBQAxABAgAoQAgAnAAA0IAAAGQgCAogYAkQgXAkgbATQgMAIgLAEQgLAFgJAAQgyAAgegogAERjJQgNATgCAXIgBAHIAAAGIACAUQAHAjAYAfQAaAfAdAKIAJACIAKABQAUAAANgRQAMgQAFgXIAAgNQAAgJgCgLIgFgTQgLghgagdQgZgdgcgEIgEgBIgFAAIAAgBQgWABgNATgAgGgMQgfgoAAgzIABgVQAHguAdgnQAfgoAjgIIAOgBQAyABAgAoQAfAoAAAzIAAAGQgCAogXAkQgYAkgbATQgLAIgMAEQgLAFgJAAQgxAAgfgogAAPjJQgMASgDAYIAAAHIAAAQIABAKQAHAjAZAfQAYAfAfAKIAJACIAJABQAUAAANgRQANgSADgVIABgNQAAgLgCgJIgFgTQgMgigZgcQgYgdgdgEIgEgBIgEAAIAAgBQgXABgNATgAkHgMQgggnAAg0IABgVQAHgtAegoQAggoAjgIIANgBQAyABAfAoQAhAnAAA0IAAAGQgCAogYAkQgXAkgcATQgLAIgLAEQgLAFgKAAQgxAAgfgogAjyjJQgMATgDAXIgBAHIAAAGIACAUQAHAjAZAfQAZAfAdAKIAKACIAJABQAVAAANgRQAMgSAEgVIABgNQAAgJgDgLIgEgTQgMghgZgdQgagdgbgEIgFgBIgEAAIAAgBQgXABgNATg");
	this.shape_76.setTransform(-0.1,0.4,0.231,0.231);

	this.shape_77 = new cjs.Shape();
	this.shape_77.graphics.f("#007844").s().p("AgdJSIgmgFIgkgMIgbgLIgfgTIgsgmIgUAAIgpgFIgTgEIg3gWIghgUIgPgLIgbgaIgXgdIgTggIAAgBIgjgSIghgYIgigiIgbgoIgQglIgEgOIgcgEIgbgHIgqgVIgigdIgQgVIgRgdIgHgJIgGgFIgCgCIgCgCIgCgCIgCAAIgCgCIgDgBIgCgCIgDgBIgCgBIgDgBIgDgBIgDgBIgOgHIgcgQIgYgTIAggYIAEgCIAhgQIADgBIAFgCIADgBIACgBIADgBIAKgIIAEgEIAJgLIANgYIAUgZIATgSIAVgPIAYgMIAMgGIAxgKIACAAIAGgZIAJgZIAUgmIAUgbIASgTIAigcIAPgJIAfgQIAEgIIAUgfIAYgdIAkgfIApgXIASgIIAmgMIApgFIAegBIAAgBIAagYIAegVIAagOIAigOIAlgJIAngEIAnACIA0ALIAjAOIAxAeIAgAdIAAABIAUAAIAzAGIAvAPIAiASIAfAWIAiAhIALAPIAUAfIAEAIIAfAQIAWAPIAbAWIAXAaIAYAkIASAnIAHAZIABAHIABAAIAjAFIAUAGIAfAPIAWAOIAcAaIAWAgIADAFIAIAOIAIAJIADABIACACIAGAFIADABIAFADIADABIACABIADACIAHADIADABIAgAPIAhAVIghAXIgmASIgEABIgFACIgDABIgDACIgCABIgFADIgJAHIgEAEIgMASIgDAGIgWAeIgSATIgVAQIgRAKIgZALIgbAHIghAFIgKAcIgSAkIgTAaIgiAiIghAYIgjASIAAABIgJAQIgaAmIgiAhIgXARIgzAbIgwANIgpAFIgUAAIgVAUIgdAWIgmAVIhBAUIgfADgAhvImQAyAYA4AAQA3AAAygYQAxgXAhgpIAWABQBKAAA9goQA7gnAchAQA5gWAogtQAogvALg7IALAAQAzAAApgZQAogYAVgqQAPgeAfgPIA2gaIg2gaQgfgOgPgeQgVgpgogZQgpgZgzAAIgIAAQgHhBgqgzQgogzg+gXQgchBg7gmQg9gohKgBIgWABQghgogxgYQgygYg3AAQg4AAgyAYQgxAYghAoIgWgBQhKABg9AoQg7AmgcBBQg+AXgoAzQgqA0gHBAIgCAAQgzAAgpAZQgoAZgVApQgPAegeAOQgsATgJAHQAJAHAsAUQAeAOAPAeQAVAqAoAYQApAZAzAAIAFAAQALA7AoAvQAoAtA5AWQAcBAA7AnQA9AoBKAAIAWgBQAhApAxAXgAhfHfQgqgUgegjIgSABQhBAAg1gjQg0gjgYg3QgygTgjgoQgjgogJgzIgFAAQgsAAgkgXQgjgVgSgkQgOgagagMQgmgSgIgGQAIgHAmgQQAagMAOgaQASgkAjgVQAkgXAsAAIACAAQAHg4AkgtQAjgsA2gUQAYg5A0ghQA1gkBBAAIASABQAegjAqgUQArgVAxAAQAxAAArAVQAqAUAeAjIASgBQBBAAA1AkQA0AhAYA5QA2AUAjAsQAkAtAHA5IAHgBQAsAAAkAXQAjAVASAkQAOAaAaAMIAwAXIgwAYQgaAMgOAaQgSAkgjAVQgkAXgsAAIgKgBQgJAzgjApQgjAogyATQgYA3g0AjQg1AjhBAAIgSgBQgeAjgqAUQgrAWgxAAQgxAAgrgWg");
	this.shape_77.setTransform(-0.1,0,0.231,0.231);

	this.shape_78 = new cjs.Shape();
	this.shape_78.graphics.f("#FFFFFF").s().p("Ah4JZQg2gYgngsIgKAAQhRAAhEgpQhFgqgihHQg5gZgpgsQgpgvgQg5Qg2gEgsgdQgsgdgZgwQgGgMgJgIQgHgHgNgGIgegNQgQgJgNgJIgbgXIAbgUQANgLAQgHIAegOQANgGAHgHQAJgHAGgNQAYgvAsgdQAqgcA0gFQAOhAAqgzQAqgzA+gbQAihGBFgqQBEgqBRAAIAKAAQAngrA2gYQA2gYA8ABQA7gBA2AYQA2AYAnArIAKAAQBRAABEAqQBEAqAjBGQA9AbArAzQAqAyAOBAQA2AEAtAdQAsAdAZAwQALAWAWALIAgAOQASAHAMAJIAiAWIgiAYQgMAIgSAJIggANQgWALgLAWQgZAxguAeQguAcg4ADQgRA5gpAvQgpAsg4AZQgjBHhEAqQhEAphRAAIgKAAQgnAsg2AYQg2AXg7gBQg8ABg2gXg");
	this.shape_78.setTransform(0,0,0.231,0.231);

	this.shape_79 = new cjs.Shape();
	this.shape_79.graphics.f("#FFDE23").s().p("AmCOVQi0hLiJiKQiKiKhLizQhPi5AAjKQAAjKBPi5QBLiyCKiKQCJiKC0hMQC4hODKAAQDKAAC6BOQCzBMCJCKQCKCKBMCyQBOC5AADKQAADKhOC5QhMCziKCKQiJCKizBLQi6BPjKAAQjKAAi4hPg");
	this.shape_79.setTransform(0,0,0.231,0.231);

	this.shape_80 = new cjs.Shape();
	this.shape_80.graphics.f("#007844").s().p("AmRO2Qi5hPiOiOQiPiPhOi4QhRjAAAjSQAAjRBRi/QBOi5CPiPQCOiPC5hOQDBhRDQAAQDSAAC/BRQC5BOCPCPQCPCPBOC5QBRC/AADRQAADShRDAQhOC4iPCPQiPCOi5BPQi/BRjSAAQjRAAjAhRg");
	this.shape_80.setTransform(0,0,0.231,0.231);

	this.shape_81 = new cjs.Shape();
	this.shape_81.graphics.f("#FFFFFF").s().p("AmhPcQjAhRiViVQiUiUhRjAQhUjHAAjbQAAjZBUjHQBRjBCUiUQCViVDAhRQDIhUDZgBQDaABDHBUQDBBRCUCVQCVCUBRDBQBUDHABDZQgBDbhUDHQhRDAiVCUQiUCVjBBRQjHBVjagBQjZABjIhVg");
	this.shape_81.setTransform(0,0,0.231,0.231);

	this.shape_82 = new cjs.Shape();
	this.shape_82.graphics.f("#FFFFFF").s().p("AnsSQQjjhhiwivQivivhgjkQhkjrAAkCQAAkBBkjrQBgjkCvivQCwivDjhhQDrhjEBAAQECAADrBjQDkBhCvCvQCvCvBgDkQBkDrAAEBQAAEChkDrQhgDkivCvQivCvjkBhQjrBjkCAAQkBAAjrhjg");
	this.shape_82.setTransform(0,0,0.231,0.231);

	this.shape_83 = new cjs.Shape();
	this.shape_83.graphics.f("#007844").s().p("An4SsQjphji0izQizi0hjjpQhmjxAAkIQAAkHBmjxQBjjpCzi0QC0izDphjQDxhmEHAAQEIAADxBmQDpBjC0CzQC0C0BiDpQBmDxAAEHQAAEIhmDxQhiDpi0C0Qi0CzjpBjQjxBmkIAAQkHAAjxhmg");
	this.shape_83.setTransform(0,0,0.231,0.231);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_83},{t:this.shape_82},{t:this.shape_81},{t:this.shape_80},{t:this.shape_79},{t:this.shape_78},{t:this.shape_77},{t:this.shape_76},{t:this.shape_75},{t:this.shape_74},{t:this.shape_73},{t:this.shape_72},{t:this.shape_71},{t:this.shape_70},{t:this.shape_69},{t:this.shape_68},{t:this.shape_67},{t:this.shape_66},{t:this.shape_65},{t:this.shape_64},{t:this.shape_63},{t:this.shape_62},{t:this.shape_61},{t:this.shape_60},{t:this.shape_59},{t:this.shape_58},{t:this.shape_57},{t:this.shape_56},{t:this.shape_55},{t:this.shape_54},{t:this.shape_53},{t:this.shape_52},{t:this.shape_51},{t:this.shape_50},{t:this.shape_49},{t:this.shape_48},{t:this.shape_47},{t:this.shape_46},{t:this.shape_45},{t:this.shape_44},{t:this.shape_43},{t:this.shape_42},{t:this.shape_41},{t:this.shape_40},{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-30,-30,60,60);


(lib.Tween10 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgVAtQgGAAgDgEQgDgEgBgFIABgEIADgFIAzg4IgzAAIAAgLIAyAAQAHAAACADQAEADAAAFQAAAEgCACIgDAFIgzA3IA6AAIAAAMg");
	this.shape.setTransform(56.3,33);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgDAuQgIAAgGgDQgHgEgEgHQgFgGAAgKIAAgfQAAgJAFgHQAEgHAHgDQAHgEAIAAQAGAAAGADQAGADAFAGIABgLIAMAAIAABZIgMAAIgBgMQgFAHgGADQgGADgFAAIgCAAgAgLggQgEACgDAFQgCAEAAAGIAAAfQAAAHACAEQADAEAEACQAEACAFAAQAEAAAGgDQAGgDADgFQAEgFAAgHIAAgXQAAgHgEgFQgDgFgGgDQgFgDgFAAQgFAAgEACg");
	this.shape_1.setTransform(47.8,32.9);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgGAIQgDgDAAgFQAAgEADgDQADgCADAAQAEAAAEACQADADgBAEQABAFgDADQgEACgEAAQgDAAgDgCg");
	this.shape_2.setTransform(41.7,36.4);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgGBBIAAhZIAMAAIAABZgAgGgvQgDgCAAgFQAAgEADgDQADgCADgBQAEABADACQADADAAAEQAAAFgDACQgDADgEAAQgDAAgDgDg");
	this.shape_3.setTransform(38,31);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AgFBFIAAiJIALAAIAACJg");
	this.shape_4.setTransform(34.2,30.5);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AgDAuQgIAAgGgDQgHgEgEgHQgFgGAAgKIAAgfQAAgJAFgHQAEgHAHgDQAHgEAIAAQAGAAAGADQAGADAFAGIABgLIAMAAIAABZIgMAAIgBgMQgFAHgGADQgGADgFAAIgCAAgAgLggQgEACgDAFQgCAEAAAGIAAAfQAAAHACAEQADAEAEACQAEACAFAAQAEAAAGgDQAGgDADgFQAEgFAAgHIAAgXQAAgHgEgFQgDgFgGgDQgFgDgFAAQgFAAgEACg");
	this.shape_5.setTransform(27.7,32.9);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AgPA/QgHgDgGgFIAFgLQAEAFAHACQAGADAGAAQAFAAAFgCQAFgCADgFQADgEAAgIIAAgWQgFAHgGAEQgHAEgGgBQgHABgHgEQgHgEgEgGQgFgHAAgIIAAg+IANAAIAAA+QAAAFACAEQADAFAEACQADACAFAAQAFgBAGgCQAFgEAEgFQAEgEAAgJIAAg3IANAAIAABiQgBAKgEAHQgFAIgIADQgIAFgIAAQgHAAgIgDg");
	this.shape_6.setTransform(18.9,35);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AgGBBIAAhZIAMAAIAABZgAgGgvQgDgCAAgFQAAgEADgDQADgCADgBQAEABADACQADADAAAEQAAAFgDACQgDADgEAAQgDAAgDgDg");
	this.shape_7.setTransform(12.7,31);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AgFBFIAAiJIALAAIAACJg");
	this.shape_8.setTransform(8.8,30.5);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AAVBFIAAg8QAAgJgFgFQgFgEgHgBQgFAAgFAEQgGADgEAEQgEAGAAAIIAAA2IgNAAIAAiJIANAAIAAA+QAGgIAHgEQAHgDAGAAQAHAAAHADQAGAEAEAHQAEAHAAAIIAAA9g");
	this.shape_9.setTransform(2.3,30.5);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AgDAuQgIAAgGgDQgHgEgEgHQgFgGAAgKIAAgfQAAgJAFgHQAEgHAHgDQAHgEAIAAQAGAAAGADQAGADAFAGIABgLIAMAAIAABZIgMAAIgBgMQgFAHgGADQgGADgFAAIgCAAgAgLggQgEACgDAFQgCAEAAAGIAAAfQAAAHACAEQADAEAEACQAEACAFAAQAEAAAGgDQAGgDADgFQAEgFAAgHIAAgXQAAgHgEgFQgDgFgGgDQgFgDgFAAQgFAAgEACg");
	this.shape_10.setTransform(-6.7,32.9);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AgOAuIgOgDIACgLIALACQAGABAHAAQAFAAAFgBQAEgCADgDQAEgDAAgGQAAgGgGgDQgEgDgIgCIgOgEQgHgCgFgFQgFgFgBgJQABgJAEgFQAEgGAHgDQAHgDAJAAIAMABIAPADIgCALIgMgCIgNgCQgIAAgFADQgGADAAAJQABAGAEADQAGADAHABIAOAFQAHABAGAFQAEAFAAAKQABANgKAHQgIAGgQAAIgMAAg");
	this.shape_11.setTransform(-14.6,32.9);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AgGAIQgDgDAAgFQAAgEADgDQACgCAEAAQAFAAADACQACADAAAEQAAAFgCADQgDACgFAAQgEAAgCgCg");
	this.shape_12.setTransform(-20.2,36.4);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("AAOAsQgFgCgEgDQgDgEgCgEQgBAEgDAEQgEADgFACIgJABQgJABgGgEQgHgEgEgFQgDgHAAgIIAAg/IAMAAIAAA/QABAHAEAEQAEAGAIAAQAHgBAEgFQAGgEAAgHIAAg/IALAAIAAA/QAAAHAGAEQAEAFAHABQAIAAAEgGQAEgEABgHIAAg/IAMAAIAAA/QAAAIgDAHQgEAFgHAEQgHAEgIgBIgJgBg");
	this.shape_13.setTransform(-28.3,33);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FFFFFF").s().p("AAOAsQgFgCgDgDQgEgEgCgEQgBAEgDAEQgEADgFACIgJABQgIABgHgEQgHgEgDgFQgEgHgBgIIAAg/IAOAAIAAA/QAAAHAEAEQAFAGAHAAQAHgBAEgFQAFgEABgHIAAg/IALAAIAAA/QAAAHAGAEQAEAFAHABQAHAAAFgGQAFgEAAgHIAAg/IAMAAIAAA/QAAAIgDAHQgFAFgGAEQgGAEgJgBIgJgBg");
	this.shape_14.setTransform(-41.1,33);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FFFFFF").s().p("AAOAsQgEgCgEgDQgEgEgCgEQgBAEgEAEQgDADgFACIgJABQgJABgGgEQgGgEgEgFQgEgHgBgIIAAg/IAOAAIAAA/QgBAHAFAEQAEAGAIAAQAHgBAFgFQAEgEAAgHIAAg/IANAAIAAA/QAAAHAEAEQAFAFAHABQAIAAAEgGQAFgEgBgHIAAg/IAOAAIAAA/QgBAIgEAHQgEAFgGAEQgHAEgIgBIgJgBg");
	this.shape_15.setTransform(-53.9,33);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FFFFFF").s().p("AAVAuIAAg7QAAgLgFgFQgEgEgHAAQgFAAgGADQgGAEgEAGQgEAGAAAIIAAA0IgNAAIAAhZIAMAAIABANQAFgIAHgDQAHgEAHAAQAHAAAHAEQAGADAEAHQAEAHAAALIAAA7g");
	this.shape_16.setTransform(78.3,13.6);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#FFFFFF").s().p("AgDAuQgIAAgGgDQgHgEgEgHQgFgGAAgKIAAgfQAAgJAFgHQAEgHAHgDQAHgEAIAAQAGAAAGADQAGADAFAGIABgLIAMAAIAABZIgMAAIgBgMQgFAHgGADQgGADgFAAIgCAAgAgLggQgEACgDAFQgCAEAAAGIAAAfQAAAHACAEQADAEAEACQAEACAFAAQAEAAAGgDQAGgDADgFQAEgFAAgHIAAgXQAAgHgEgFQgDgFgGgDQgFgDgFAAQgFAAgEACg");
	this.shape_17.setTransform(69.3,13.7);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#FFFFFF").s().p("AgOBVIgGgBIACgMIAFABIAFABQAHAAACgFQAEgGAAgOIAAheIANAAIAABfQAAASgHAJQgHAJgMAAIgGgBgAAEhDQgDgDAAgEQAAgFADgDQADgCAEAAQAEAAADACQADADAAAFQAAAEgDADQgDADgEAAQgEAAgDgDg");
	this.shape_18.setTransform(62.2,13.8);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#FFFFFF").s().p("AgGBBIAAhZIAMAAIAABZgAgGgvQgDgCAAgFQAAgEADgDQADgCADgBQAEABADACQADADAAAEQAAAFgDACQgDADgEABQgDgBgDgDg");
	this.shape_19.setTransform(59.2,11.7);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#FFFFFF").s().p("AgDAuQgIAAgGgDQgHgEgEgHQgFgGAAgKIAAgfQAAgJAFgHQAEgHAHgDQAHgEAIAAQAGAAAGADQAGADAFAGIABgLIAMAAIAABZIgMAAIgBgMQgFAHgGADQgGADgFAAIgCAAgAgLggQgEACgDAFQgCAEAAAGIAAAfQAAAHACAEQADAEAEACQAEACAFAAQAEAAAGgDQAGgDADgFQAEgFAAgHIAAgXQAAgHgEgFQgDgFgGgDQgFgDgFAAQgFAAgEACg");
	this.shape_20.setTransform(52.7,13.7);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#FFFFFF").s().p("AgJBDQgGgEgFgHIgCANIgLAAIAAiKIANAAIAAA8QAFgHAGgDQAHgDAGAAQAIAAAGAEQAHAEAEAHQAFAHAAAIIAAAfQAAAKgFAHQgEAGgHAEQgHAEgHAAIgCAAQgFAAgGgDgAgHgIQgGADgDAFQgEAEAAAHIAAAYQAAAIAEAFQADAFAGADQAFADAFAAQAFAAAEgCQAEgCADgFQACgEAAgHIAAgfQAAgHgDgDQgCgEgEgCQgEgCgFAAQgFAAgFACg");
	this.shape_21.setTransform(44.2,11.3);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#FFFFFF").s().p("AgYAuIAAhZIAMAAIABALQAFgHAGgDQAGgDAHAAIAGABIAGABIAAAMIgGgCIgGAAQgHAAgFADQgFADgDAFQgEAGAAAIIAAA2g");
	this.shape_22.setTransform(36.6,13.6);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#FFFFFF").s().p("AgQAqQgIgEgFgIQgEgHAAgJIAAgZQAAgMAEgHQAFgIAJgEQAHgDAIAAQAJAAAIAEQAIADAFAIQAEAIABALQAAAIgFAEQgEAEgHAAIgoAAIAAAIQAAAIAEAEQACAFAGADQAEACAGAAQAGAAAGgDQAHgEAEgHIAJAIQgFAIgIAFQgJAEgKAAQgJAAgIgEgAgJgfQgGACgCAFQgEAFAAAIIAAAGIAnAAQAAAAABAAQAAAAABAAQAAgBAAAAQABAAAAgBIABgFQAAgHgDgFQgDgFgFgCQgFgDgGAAQgEAAgFADg");
	this.shape_23.setTransform(29,13.7);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#FFFFFF").s().p("AgWAtQgFAAgDgEQgEgDABgGIABgEIACgFIAzg4IgzAAIAAgLIAzAAQAFAAAEADQACADAAAFQABAEgCACIgDAFIgzA3IA5AAIAAAMg");
	this.shape_24.setTransform(20.8,13.7);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#FFFFFF").s().p("AAgBBIAAgxIg/AAIAAAxIgNAAIAAhWQAAgMAGgLQAGgJAKgGQAKgGAMABQANgBAKAGQAKAGAGAJQAGALAAAMIAABWgAgPgxQgIAFgEAHQgEAHAAAJIAAAZIA/AAIAAgZQAAgJgEgHQgEgHgIgFQgHgEgJAAQgIAAgHAEg");
	this.shape_25.setTransform(11,11.7);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#FFFFFF").s().p("AgJBGIAAhOIgQAAIAAgLIAQAAIAAgTQAAgMADgGQADgHAFgDQAEgDAHAAIAGAAIAHACIgDAMIgFgCIgFgBQgFAAgDAFQgCAFAAALIAAASIAWAAIAAALIgWAAIAABOg");
	this.shape_26.setTransform(2.3,11.2);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#FFFFFF").s().p("AgQAqQgIgEgFgIQgEgHgBgJIAAgZQABgMAEgHQAFgIAIgEQAIgDAIAAQAJAAAIAEQAHADAGAIQAEAIAAALQAAAIgEAEQgEAEgHAAIgnAAIAAAIQAAAIACAEQADAFAFADQAFACAFAAQAHAAAHgDQAGgEAEgHIAJAIQgFAIgJAFQgIAEgKAAQgJAAgIgEgAgKgfQgFACgDAFQgCAFAAAIIAAAGIAlAAQABAAABAAQAAAAABAAQAAgBAAAAQABAAAAgBIABgFQAAgHgEgFQgCgFgGgCQgFgDgFAAQgFAAgFADg");
	this.shape_27.setTransform(-4.7,13.7);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#FFFFFF").s().p("AgQAqQgIgEgFgIQgEgHgBgJIAAgZQABgMAEgHQAFgIAIgEQAIgDAIAAQAJAAAIAEQAHADAGAIQAEAIAAALQAAAIgEAEQgEAEgHAAIgnAAIAAAIQAAAIACAEQADAFAFADQAFACAFAAQAHAAAHgDQAGgEAEgHIAJAIQgFAIgJAFQgIAEgKAAQgJAAgIgEgAgKgfQgFACgDAFQgCAFAAAIIAAAGIAlAAQABAAABAAQAAAAABAAQAAgBAAAAQABAAAAgBIABgFQAAgHgEgFQgCgFgGgCQgFgDgFAAQgFAAgFADg");
	this.shape_28.setTransform(-12.8,13.7);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#FFFFFF").s().p("AgeBBQgFAAgEgEQgDgDgBgGIAAhnQABgGADgDQAEgDAFgBIAmAAQAJABAIAEQAHAFAEAHQAFAHAAAJIgCAJQgCAGgEAEQgDAFgGADQALAEAFAJQAEAIAAAKQAAAKgEAJQgFAIgJAFQgIAGgMAAgAgeA1IAkAAQAIAAAHgEQAFgDACgGQAEgHAAgGQAAgHgEgGQgCgGgFgEQgGgDgJAAIgkAAgAgegKIAmAAQAHAAAEgDQAEgEADgFQACgEAAgFQAAgFgDgFQgCgFgEgDQgEgDgHAAIgmAAg");
	this.shape_29.setTransform(-21.8,11.7);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#FFFFFF").s().p("AgfBRIAzihIAMAAIgzChg");
	this.shape_30.setTransform(-31,13);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#FFFFFF").s().p("AArAuIAAg7QAAgHgDgFQgCgEgEgCQgDgCgEAAQgFAAgFACQgEADgDAFQgDAFgBAHIAAA5IgLAAIAAg8QAAgKgFgFQgEgEgHAAQgFAAgFADQgEACgDAFQgDAFAAAHIAAA5IgNAAIAAhZIALAAIACAMQADgHAHgDQAGgEAGAAQAGAAAGAEQAFAEADAHQAFgHAHgEQAHgEAIAAQAHAAAGAEQAGADAEAHQAEAHAAALIAAA7g");
	this.shape_31.setTransform(-41.3,13.6);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#FFFFFF").s().p("AgRAqQgHgEgEgHQgGgHAAgKIAAgbQAAgJAGgIQAEgHAHgEQAJgEAIAAQAJAAAJAEQAHAEAFAHQAEAIABAJIAAAbQgBAKgEAHQgFAHgHAEQgJAEgJAAQgIAAgJgEgAgKgfQgFACgDAFQgCAFAAAGIAAAbQAAAGACAFQADAFAFACQAFADAFAAQAGAAAFgDQAFgCADgFQADgFAAgGIAAgbQAAgGgDgFQgDgFgFgCQgFgDgGAAQgFAAgFADg");
	this.shape_32.setTransform(-52.2,13.7);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#FFFFFF").s().p("AAAAuQgIAAgIgDQgHgEgFgHQgFgHAAgLIAAgZQAAgMAFgHQAFgIAIgDQAHgEAIAAQAJAAAJAEQAJAFAGAJIgKAHQgEgHgGgDQgGgEgHAAQgFAAgEADQgFACgEAFQgDAFAAAHIAAAZQAAAIADAFQAEAFAFACQAEACAFAAQAIAAAHgEQAGgEAEgIIAJAHQgFAKgJAFQgJAFgJAAIgCAAg");
	this.shape_33.setTransform(-60.1,13.7);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#FFFFFF").s().p("AgGAIQgDgDAAgFQAAgEADgDQADgCADAAQAEAAAEACQACADAAAEQAAAFgCADQgEACgEAAQgDAAgDgCg");
	this.shape_34.setTransform(-66.3,17.2);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#FFFFFF").s().p("AgJBDQgGgEgFgHIgCANIgLAAIAAiKIANAAIAAA8QAFgHAGgDQAHgDAGAAQAIAAAGAEQAHAEAEAHQAFAHAAAIIAAAfQAAAKgFAHQgEAGgHAEQgHAEgHAAIgCAAQgFAAgGgDgAgHgIQgGADgDAFQgEAEAAAHIAAAYQAAAIAEAFQADAFAGADQAFADAFAAQAFAAAEgCQAEgCADgFQACgEAAgHIAAgfQAAgHgDgDQgCgEgEgCQgEgCgFAAQgFAAgFACg");
	this.shape_35.setTransform(-72.3,11.3);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#FFFFFF").s().p("AgJBGIAAhOIgQAAIAAgLIAQAAIAAgTQAAgMADgGQADgHAEgDQAFgDAHAAIAGAAIAHACIgCAMIgGgCIgEgBQgGAAgCAFQgDAFAAALIAAASIAWAAIAAALIgWAAIAABOg");
	this.shape_36.setTransform(-79.8,11.2);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("#FFFFFF").s().p("AgTA5QgIgBgIgEIAEgTQAHAEAHABQAGACAHAAQAIAAAGgFQAGgEAAgKQABgGgGgFQgFgEgKAAIgPAAQgGAAgDgDQgDgCgCgDIgBgIIAAgCIAAgCIADgWQABgGADgFQADgFAFgDQAGgDAHAAIAmAAIAAASIgkAAQgEAAgCADQgCADgBAFIgCARIANAAQAMAAAJAFQAJAFAEAGQAEAIAAAJQAAAMgFAIQgGAJgJAEQgJAFgLAAIgDAAQgGAAgGgCg");
	this.shape_37.setTransform(70.1,-11.4);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("#FFFFFF").s().p("AgfA6IAwhhIg0AAIAAgTIA4AAQAIABAEAEQADAEABAHIgBAFIgCAFIgpBag");
	this.shape_38.setTransform(61,-11.5);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("#FFFFFF").s().p("AghAKIAAgSIBDAAIAAASg");
	this.shape_39.setTransform(51.8,-10.6);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f("#FFFFFF").s().p("AgTA5QgIgBgIgEIAEgTQAHAEAHABQAGACAHAAQAIAAAGgFQAGgEAAgKQABgGgGgFQgFgEgKAAIgPAAQgGAAgDgDQgDgCgCgDIgBgIIAAgCIAAgCIADgWQABgGADgFQADgFAFgDQAGgDAHAAIAmAAIAAASIgkAAQgEAAgCADQgCADgBAFIgCARIANAAQAMAAAJAFQAJAFAEAGQAEAIAAAJQAAAMgFAIQgGAJgJAEQgJAFgLAAIgDAAQgGAAgGgCg");
	this.shape_40.setTransform(43.1,-11.4);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f("#FFFFFF").s().p("AAFA7IAAgYIgfAAQgHAAgEgEQgEgDAAgHIAAgEIACgFIAfg8QADgGAEgCQAEgCAFAAQAEAAAEABQAEACACAEQACADABAFIAAA7IARAAIAAATIgRAAIAAAYgAgVAQIAaAAIAAg1g");
	this.shape_41.setTransform(33.6,-11.6);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f("#FFFFFF").s().p("AghAKIAAgSIBDAAIAAASg");
	this.shape_42.setTransform(24.8,-10.6);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f("#FFFFFF").s().p("AAHA7IAAhdIgSATIgOgOIAZgYIAFgEQADgBAEAAQAFAAAFAEQAFAEgBAJIAABkg");
	this.shape_43.setTransform(15.3,-11.6);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f("#FFFFFF").s().p("AAHA7IAAhdIgRATIgQgOIAagYIAFgEQADgBADAAQAHAAAEAEQAEAEABAJIAABkg");
	this.shape_44.setTransform(5.8,-11.6);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f("#FFFFFF").s().p("AgaA7QgGgBgCgEQgCgEAAgFQAAgDABgDQABgEADgCIAZgWQAKgLAHgIQAGgIAAgHQAAgIgFgEQgEgFgGAAQgFAAgFAEQgGADgEAHIgPgNQAIgKAKgFQAJgEAIAAQAKAAAIAEQAHAEAFAIQAEAIABAKQgBALgGALQgIALgKAKIgVAVIAvAAIAAATg");
	this.shape_45.setTransform(-2.9,-11.6);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f("#FFFFFF").s().p("AgTA5QgIgBgIgEIAEgTQAHAEAHABQAGACAHAAQAIAAAGgFQAGgEAAgKQABgGgGgFQgFgEgKAAIgPAAQgGAAgDgDQgDgCgCgDIgBgIIAAgCIAAgCIADgWQABgGADgFQADgFAFgDQAGgDAHAAIAmAAIAAASIgkAAQgEAAgCADQgCADgBAFIgCARIANAAQAMAAAJAFQAJAFAEAGQAEAIAAAJQAAAMgFAIQgGAJgJAEQgJAFgLAAIgDAAQgGAAgGgCg");
	this.shape_46.setTransform(-17.7,-11.4);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f("#FFFFFF").s().p("AgTA5QgIgBgIgEIAEgTQAHAEAHABQAGACAHAAQAIAAAGgFQAGgEAAgKQABgGgGgFQgFgEgKAAIgPAAQgGAAgDgDQgDgCgCgDIgBgIIAAgCIAAgCIADgWQABgGADgFQADgFAFgDQAGgDAHAAIAmAAIAAASIgkAAQgEAAgCADQgCADgBAFIgCARIANAAQAMAAAJAFQAJAFAEAGQAEAIAAAJQAAAMgFAIQgGAJgJAEQgJAFgLAAIgDAAQgGAAgGgCg");
	this.shape_47.setTransform(-27.1,-11.4);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f("#FFFFFF").s().p("AAKA7IAAgbIgnAAQgFAAgEgEQgDgEAAgEIABgDIABgDIAmhCQABgDADgCQADgBAEAAQAEAAAFADQADADAAAGIAABDIATAAIAAALIgTAAIAAAbgAgdAVIAnAAIAAhFg");
	this.shape_48.setTransform(-42.1,-11.6);

	this.shape_49 = new cjs.Shape();
	this.shape_49.graphics.f("#FFFFFF").s().p("AgWA5QALgIAJgIQAHgJAGgIIAKgRQgFAFgGADQgGACgGAAQgMAAgHgGQgIgFgEgIQgEgJAAgJQAAgKAEgJQAEgJAJgFQAIgGAMAAQAMAAAIAGQAJAFAFAIQAEAJAAALQAAANgEALQgEANgIAKQgHALgIAIQgIAJgIAFgAgLgsQgGADgEAGQgEAGAAAJQAAAJAEAGQAEAFAFADQAGADAGAAQAGAAAGgDQAGgDAEgFQAEgGAAgJQAAgJgEgGQgEgGgGgDQgFgDgHAAQgFAAgGADg");
	this.shape_49.setTransform(-51.7,-11.5);

	this.shape_50 = new cjs.Shape();
	this.shape_50.graphics.f("#FFFFFF").s().p("AgWA5QALgIAJgIQAHgJAGgIIAKgRQgFAFgGADQgGACgGAAQgMAAgHgGQgIgFgEgIQgEgJAAgJQAAgKAEgJQAEgJAJgFQAIgGAMAAQAMAAAIAGQAJAFAFAIQAEAJAAALQAAANgEALQgEANgIAKQgHALgIAIQgIAJgIAFgAgLgsQgGADgEAGQgEAGAAAJQAAAJAEAGQAEAFAFADQAGADAGAAQAGAAAGgDQAGgDAEgFQAEgGAAgJQAAgJgEgGQgEgGgGgDQgFgDgHAAQgFAAgGADg");
	this.shape_50.setTransform(-61.3,-11.5);

	this.shape_51 = new cjs.Shape();
	this.shape_51.graphics.f("#FFFFFF").s().p("AgFAkIAAgfIgeAAIAAgJIAeAAIAAgfIAMAAIAAAfIAdAAIAAAJIgdAAIAAAfg");
	this.shape_51.setTransform(-70.5,-11.7);

	this.shape_52 = new cjs.Shape();
	this.shape_52.graphics.f("#FFFFFF").s().p("AgVA3QgIgGgEgIQgEgIAAgJQAAgHACgHQADgGAFgEQAFgFAIgCQgJgFgDgGQgDgGAAgHQAAgHADgGQAEgHAGgEQAHgEAJAAQAKAAAGAEQAHAEAEAHQADAGAAAHQAAAHgDAGQgEAGgIAFQAIACAFAFQAFAEADAGQACAHAAAHQAAAJgEAIQgEAIgJAGQgIAFgNAAQgMAAgJgFgAgMAJQgFAFAAAIQAAAFACAEQADAFAEACQAEADAEAAQAFAAAEgDQAEgCADgFQACgEAAgFQAAgIgFgFQgFgFgIAAQgHAAgFAFgAgHgnQgEADAAAGQAAAFADAEQAEAEAEAAQAFAAAEgEQADgEAAgFQAAgGgEgDQgDgEgFAAQgEAAgDAEg");
	this.shape_52.setTransform(70.1,-29.9);

	this.shape_53 = new cjs.Shape();
	this.shape_53.graphics.f("#FFFFFF").s().p("AgfA6IAwhhIg0AAIAAgTIA4AAQAIAAAEAFQADAEABAGIgBAGIgCAFIgpBag");
	this.shape_53.setTransform(61,-29.8);

	this.shape_54 = new cjs.Shape();
	this.shape_54.graphics.f("#FFFFFF").s().p("AghAKIAAgSIBDAAIAAASg");
	this.shape_54.setTransform(51.8,-28.9);

	this.shape_55 = new cjs.Shape();
	this.shape_55.graphics.f("#FFFFFF").s().p("AgVA3QgIgGgEgIQgEgIAAgJQAAgHACgHQADgGAFgEQAFgFAIgCQgJgFgDgGQgDgGAAgHQAAgHADgGQAEgHAGgEQAHgEAJAAQAKAAAGAEQAHAEAEAHQADAGAAAHQAAAHgDAGQgEAGgIAFQAIACAFAFQAFAEADAGQACAHAAAHQAAAJgEAIQgEAIgJAGQgIAFgNAAQgMAAgJgFgAgMAJQgFAFAAAIQAAAFACAEQADAFAEACQAEADAEAAQAFAAAEgDQAEgCADgFQACgEAAgFQAAgIgFgFQgFgFgIAAQgHAAgFAFgAgHgnQgEADAAAGQAAAFADAEQAEAEAEAAQAFAAAEgEQADgEAAgFQAAgGgEgDQgDgEgFAAQgEAAgDAEg");
	this.shape_55.setTransform(43.1,-29.9);

	this.shape_56 = new cjs.Shape();
	this.shape_56.graphics.f("#FFFFFF").s().p("AgfA6IAwhhIg0AAIAAgTIA4AAQAIAAAEAFQADAEABAGIgBAGIgCAFIgpBag");
	this.shape_56.setTransform(34,-29.8);

	this.shape_57 = new cjs.Shape();
	this.shape_57.graphics.f("#FFFFFF").s().p("AghAKIAAgSIBDAAIAAASg");
	this.shape_57.setTransform(24.8,-28.9);

	this.shape_58 = new cjs.Shape();
	this.shape_58.graphics.f("#FFFFFF").s().p("AgaA7QgGgBgCgEQgCgEgBgFQABgDABgDQACgEACgCIAZgWQAKgLAGgIQAHgIAAgHQAAgIgEgEQgFgFgHAAQgEAAgGAEQgFADgFAHIgNgNQAHgKAJgFQAKgEAHAAQALAAAIAEQAIAEAEAIQAEAIAAAKQAAALgGALQgIALgKAKIgVAVIAuAAIAAATg");
	this.shape_58.setTransform(16,-29.9);

	this.shape_59 = new cjs.Shape();
	this.shape_59.graphics.f("#FFFFFF").s().p("AgcA4QAPgLAKgLQAJgMAFgIIgIACIgGABQgJAAgIgFQgIgFgFgHQgFgJAAgLQAAgLAFgJQAGgJAJgFQAJgFAKAAQAMAAAIAFQAIAFAGAJQAEAJAAALQAAANgEALQgFAMgHALQgGALgIAIQgIAJgIAFgAgIgmQgFACgDAFQgCAEAAAHQAAAGACAFQADAEAFACQAEACAEAAQAFAAAFgCQADgCAEgEQACgFAAgGQAAgHgCgEQgEgFgDgCQgFgCgFAAQgEAAgEACg");
	this.shape_59.setTransform(6.6,-29.9);

	this.shape_60 = new cjs.Shape();
	this.shape_60.graphics.f("#FFFFFF").s().p("AgaA7QgGgBgCgEQgCgEAAgFQAAgDABgDQABgEADgCIAZgWQAKgLAHgIQAGgIAAgHQAAgIgFgEQgEgFgGAAQgFAAgFAEQgGADgEAHIgPgNQAIgKAKgFQAJgEAIAAQAKAAAIAEQAHAEAFAIQAEAIABAKQgBALgGALQgIALgKAKIgVAVIAvAAIAAATg");
	this.shape_60.setTransform(-2.9,-29.9);

	this.shape_61 = new cjs.Shape();
	this.shape_61.graphics.f("#FFFFFF").s().p("AgSA3QgJgEgGgJQgGgIAAgNIAAgpQAAgNAGgIQAGgJAJgEQAJgFAJAAQAKAAAKAFQAJAEAFAJQAGAIAAANIAAApQAAANgGAIQgFAJgJAEQgKAFgKAAQgJAAgJgFgAgJgmQgEACgDAFQgDAEAAAHIAAApQAAAHADAEQADAFAEACQAFACAEAAQAFAAAFgCQAFgCADgFQADgEAAgHIAAgpQAAgHgDgEQgDgFgFgCQgFgCgFAAQgEAAgFACg");
	this.shape_61.setTransform(-17.8,-29.9);

	this.shape_62 = new cjs.Shape();
	this.shape_62.graphics.f("#FFFFFF").s().p("AgTA5QgIgBgIgEIAEgTQAHAEAHABQAGACAHAAQAIAAAGgFQAGgEAAgKQABgGgGgFQgFgEgKAAIgPAAQgGAAgDgDQgDgCgCgDIgBgIIAAgCIAAgCIADgWQABgGADgFQADgFAFgDQAGgDAHAAIAmAAIAAASIgkAAQgEAAgCADQgCADgBAFIgCARIANAAQAMAAAJAFQAJAFAEAGQAEAIAAAJQAAAMgFAIQgGAJgJAEQgJAFgLAAIgDAAQgGAAgGgCg");
	this.shape_62.setTransform(-27.1,-29.8);

	this.shape_63 = new cjs.Shape();
	this.shape_63.graphics.f("#FFFFFF").s().p("AAKA7IAAgbIgnAAQgFAAgEgEQgDgEAAgEIABgDIABgDIAmhCQABgDADgCQADgBAEAAQAEAAAFADQADADAAAGIAABDIATAAIAAALIgTAAIAAAbgAgdAVIAnAAIAAhFg");
	this.shape_63.setTransform(-42.1,-29.9);

	this.shape_64 = new cjs.Shape();
	this.shape_64.graphics.f("#FFFFFF").s().p("AgWA5QALgIAJgIQAHgJAGgIIAKgRQgFAFgGADQgGACgGAAQgMAAgHgGQgIgFgEgIQgEgJAAgJQAAgKAEgJQAEgJAJgFQAIgGAMAAQAMAAAIAGQAJAFAFAIQAEAJAAALQAAANgEALQgEANgIAKQgHALgIAIQgIAJgIAFgAgLgsQgGADgEAGQgEAGAAAJQAAAJAEAGQAEAFAFADQAGADAGAAQAGAAAGgDQAGgDAEgFQAEgGAAgJQAAgJgEgGQgEgGgGgDQgFgDgHAAQgFAAgGADg");
	this.shape_64.setTransform(-51.7,-29.9);

	this.shape_65 = new cjs.Shape();
	this.shape_65.graphics.f("#FFFFFF").s().p("AgWA5QALgIAJgIQAHgJAGgIIAKgRQgFAFgGADQgGACgGAAQgMAAgHgGQgIgFgEgIQgEgJAAgJQAAgKAEgJQAEgJAJgFQAIgGAMAAQAMAAAIAGQAJAFAFAIQAEAJAAALQAAANgEALQgEANgIAKQgHALgIAIQgIAJgIAFgAgLgsQgGADgEAGQgEAGAAAJQAAAJAEAGQAEAFAFADQAGADAGAAQAGAAAGgDQAGgDAEgFQAEgGAAgJQAAgJgEgGQgEgGgGgDQgFgDgHAAQgFAAgGADg");
	this.shape_65.setTransform(-61.3,-29.9);

	this.shape_66 = new cjs.Shape();
	this.shape_66.graphics.f("#FFFFFF").s().p("AgFAkIAAgfIgeAAIAAgKIAeAAIAAgeIAMAAIAAAeIAdAAIAAAKIgdAAIAAAfg");
	this.shape_66.setTransform(-70.5,-30);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_66},{t:this.shape_65},{t:this.shape_64},{t:this.shape_63},{t:this.shape_62},{t:this.shape_61},{t:this.shape_60},{t:this.shape_59},{t:this.shape_58},{t:this.shape_57},{t:this.shape_56},{t:this.shape_55},{t:this.shape_54},{t:this.shape_53},{t:this.shape_52},{t:this.shape_51},{t:this.shape_50},{t:this.shape_49},{t:this.shape_48},{t:this.shape_47},{t:this.shape_46},{t:this.shape_45},{t:this.shape_44},{t:this.shape_43},{t:this.shape_42},{t:this.shape_41},{t:this.shape_40},{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-85.2,-43.7,170.4,87.6);


(lib.Tween8 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.qabda();
	this.instance.parent = this;
	this.instance.setTransform(-197.5,-116);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-197.5,-116,395,232);


(lib.Tween6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgJAKQgEgEAAgGQAAgFAEgFQADgDAGAAQAGAAAEADQAEAFABAFQgBAGgEAEQgEAEgGAAQgGAAgDgEg");
	this.shape.setTransform(131.1,23.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AghBAIAAh9IAQAAIABAQQAHgJAJgEQAIgFAKAAIAIABIAIACIAAARIgIgDIgIgBQgJAAgIAEQgHAEgFAIQgFAIAAALIAABMg");
	this.shape_1.setTransform(126.8,18.8);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgXA7QgKgFgHgLQgHgKAAgQQABgLAFgGQAGgGAKAAIA3AAIAAgLQAAgKgEgHQgEgGgHgEQgHgDgIgBQgJAAgJAGQgJAFgFAJIgMgLQAGgKAMgIQAMgGAOgBQANABALAGQALAGAGAKQAHAKAAANIAAAjQAAARgHAKQgHALgLAEQgLAFgMABQgLgBgMgFgAgcAMQgBACgBAEQABAKAEAHQAFAHAGAEQAIADAGAAQAIAAAGgDQAIgEAEgHQAEgHAAgLIAAgIIg1AAQgDAAgCADg");
	this.shape_2.setTransform(116.2,18.9);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgIBhIAAjBIARAAIAADBg");
	this.shape_3.setTransform(107.7,15.5);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AghBAIAAh9IAQAAIABAQQAHgJAJgEQAIgFAKAAIAIABIAIACIAAARIgIgDIgIgBQgJAAgIAEQgHAEgFAIQgFAIAAALIAABMg");
	this.shape_4.setTransform(100.7,18.8);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AgXA7QgKgFgHgLQgGgKgBgQQAAgLAGgGQAGgGAJAAIA4AAIAAgLQABgKgFgHQgEgGgHgEQgHgDgIgBQgJAAgJAGQgJAFgFAJIgMgLQAHgKALgIQAMgGAOgBQANABALAGQALAGAHAKQAGAKAAANIAAAjQAAARgHAKQgHALgKAEQgMAFgMABQgLgBgMgFgAgcAMQgBACAAAEQgBAKAFAHQAFAHAGAEQAIADAGAAQAHAAAIgDQAGgEAFgHQAFgHgBgLIAAgIIg2AAQgDAAgBADg");
	this.shape_5.setTransform(90.1,18.9);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AAdBAIAAhTQAAgOgHgHQgGgHgJAAQgIAAgIAFQgIAFgGAIQgFAJAAALIAABJIgSAAIAAh9IAQAAIACATQAHgLAKgFQAKgFAJAAQAKAAAJAFQAJAFAFAKQAGAKAAAOIAABTg");
	this.shape_6.setTransform(77.9,18.8);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AgXBWQgLgGgGgKQgHgKAAgNIAAgoQAAgMAHgKQAGgKALgGQALgGAMAAQANAAALAGQALAGAGAKQAHAKAAAMIAAAoQAAANgHAKQgGAKgLAGQgLAFgNAAQgMAAgLgFgAgOgRQgHAEgEAGQgEAHAAAHIAAAoQAAAIAEAHQAEAGAHAEQAHAEAHAAQAJAAAGgEQAHgEAEgGQAEgHAAgIIAAgoQAAgHgEgHQgEgGgHgEQgGgEgJAAQgHAAgHAEgAANhCQgEgEAAgGQAAgGAEgEQAEgEAGAAQAGAAAEAEQAFAEAAAGQAAAGgFAEQgEAEgGABQgGgBgEgEgAgghCQgFgEAAgGQAAgGAFgEQAEgEAGAAQAGAAAEAEQAEAEAAAGQAAAGgEAEQgEAEgGABQgGgBgEgEg");
	this.shape_7.setTransform(65.7,16.2);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AgFBhQgLAAgJgFQgJgFgGgKQgFgJgBgOIAAgqQABgNAFgJQAGgKAKgFQAJgGALAAQAIAAAKAFQAJAEAGAJIAAhUIASAAIAADBIgPAAIgDgQQgHAJgKAEQgIAFgHAAIgCgBgAgPgMQgGADgEAGQgDAFAAAKIAAAqQAAAKAEAGQADAGAGADQAGADAGAAQAHAAAIgEQAHgEAFgHQAFgHAAgKIAAgiQAAgKgFgGQgFgHgHgEQgIgEgHAAQgHAAgFADg");
	this.shape_8.setTransform(53.4,15.6);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AghBAIAAh9IAQAAIABAQQAHgJAJgEQAIgFAKAAIAIABIAIACIAAARIgIgDIgIgBQgJAAgIAEQgHAEgFAIQgFAIAAALIAABMg");
	this.shape_9.setTransform(35.7,18.8);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AgIA/IAAh9IARAAIAAB9g");
	this.shape_10.setTransform(28,18.9);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AgeA/QgIAAgEgGQgFgFAAgHIACgHIADgGIBIhOIhIAAIAAgQIBHAAQAIAAAEAFQAFAFAAAGQAAAEgCAEQgCAEgDAEIhHBNIBQAAIAAAQg");
	this.shape_11.setTransform(19.7,18.9);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AgEBAQgLAAgJgFQgKgFgGgJQgFgJgBgNIAAgsQABgNAGgKQAGgJAJgFQAKgGAKAAQAJAAAJAFQAJAEAGAJIACgQIAQAAIAAB9IgQAAIgCgSQgGAKgJAFQgIAFgIAAIgCgBgAgPgtQgGADgEAGQgDAGAAAJIAAAsQAAAJADAFQAEAGAFADQAGADAGAAQAIAAAHgEQAIgEAFgHQAFgIAAgKIAAggQAAgKgFgHQgFgHgHgEQgIgEgHAAQgHAAgFADg");
	this.shape_12.setTransform(7.8,18.9);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("AAdBhIAAhUQAAgOgHgGQgHgHgJAAQgIAAgIAFQgIAEgFAHQgFAHAAALIAABNIgSAAIAAjBIASAAIAABXQAHgLAKgFQAKgFAJAAQALAAAIAFQAJAFAFAJQAGAKAAANIAABVg");
	this.shape_13.setTransform(-4.5,15.5);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FFFFFF").s().p("AgRBbIgJgCIABgRIAJADIALABQAFAAAFgCQAEgBAAgFQAAgEgGgCQgFgBgLAAIAAgZIgNgBIgNgCIACgRIAPAEIASABQAIAAAHgCQAHgCAEgEQAEgFABgIQgBgHgHgFQgHgEgLgDIgTgGQgKgDgHgHQgIgHAAgNQABgMAFgIQAGgIAKgEQAKgEALAAQAJAAAJACIAUAEIgCAQQgHgDgJgBQgJgCgLAAQgKAAgHAEQgIAEAAAMQAAAIAHAEQAHAEALADIATAGQAKADAHAIQAIAHAAANQAAAQgLAJQgLAJgUACIAAAKQANABAGAGQAGAGAAAIQAAALgJAHQgIAGgQAAIgLgBg");
	this.shape_14.setTransform(-23.4,21.7);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FFFFFF").s().p("AgIA/IAAh9IARAAIAAB9g");
	this.shape_15.setTransform(-31.3,18.9);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FFFFFF").s().p("AA7BAIAAhSQAAgKgDgHQgDgGgFgDQgFgDgGAAQgHAAgGAEQgHAEgEAHQgEAGAAAKIAABQIgRAAIAAhTQAAgPgGgGQgHgHgIAAQgIAAgGAEQgHAEgEAHQgEAHAAAKIAABPIgSAAIAAh9IAPAAIADARQAFgJAIgFQAJgFAJAAQAJAAAHAFQAIAGAEAKQAHgKAKgGQAKgFALAAQAJAAAJAFQAJAFAFAKQAGAJAAAPIAABTg");
	this.shape_16.setTransform(-43.2,18.8);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#FFFFFF").s().p("AAdBAIAAhTQAAgOgHgHQgGgHgJAAQgIAAgIAFQgIAFgGAIQgFAJAAALIAABJIgSAAIAAh9IAQAAIACATQAHgLAKgFQAKgFAJAAQAKAAAJAFQAJAFAFAKQAGAKAAAOIAABTg");
	this.shape_17.setTransform(-58.8,18.8);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#FFFFFF").s().p("AgEBAQgLAAgJgFQgKgFgGgJQgFgJgBgNIAAgsQABgNAGgKQAGgJAJgFQAKgGAKAAQAJAAAJAFQAJAEAGAJIACgQIAQAAIAAB9IgQAAIgCgSQgGAKgJAFQgIAFgIAAIgCgBgAgPgtQgGADgEAGQgDAGAAAJIAAAsQAAAJADAFQAEAGAFADQAGADAGAAQAIAAAHgEQAIgEAFgHQAFgIAAgKIAAggQAAgKgFgHQgFgHgHgEQgIgEgHAAQgHAAgFADg");
	this.shape_18.setTransform(-71.5,18.9);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#FFFFFF").s().p("AgIBhIAAjBIARAAIAADBg");
	this.shape_19.setTransform(-80.2,15.5);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#FFFFFF").s().p("AghBAIAAh9IAQAAIABAQQAHgJAJgEQAIgFAKAAIAIABIAIACIAAARIgIgDIgIgBQgJAAgIAEQgHAEgFAIQgFAIAAALIAABMg");
	this.shape_20.setTransform(-87.2,18.8);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#FFFFFF").s().p("AgIA/IAAh9IARAAIAAB9g");
	this.shape_21.setTransform(-94.9,18.9);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#FFFFFF").s().p("AgeA/QgIAAgEgGQgFgFAAgHIACgHIADgGIBIhOIhIAAIAAgQIBHAAQAIAAAEAFQAFAFAAAGQAAAEgCAEQgCAEgDAEIhHBNIBQAAIAAAQg");
	this.shape_22.setTransform(-103.2,18.9);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#FFFFFF").s().p("AgEBAQgLAAgJgFQgKgFgGgJQgFgJgBgNIAAgsQABgNAGgKQAGgJAJgFQAKgGAKAAQAJAAAJAFQAJAEAGAJIACgQIAQAAIAAB9IgQAAIgCgSQgGAKgJAFQgIAFgIAAIgCgBgAgPgtQgGADgEAGQgDAGAAAJIAAAsQAAAJADAFQAEAGAFADQAGADAGAAQAIAAAHgEQAIgEAFgHQAFgIAAgKIAAggQAAgKgFgHQgFgHgHgEQgIgEgHAAQgHAAgFADg");
	this.shape_23.setTransform(-115.1,18.9);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#FFFFFF").s().p("AAdBhIAAhUQAAgOgHgGQgHgHgJAAQgIAAgIAFQgIAEgFAHQgFAHAAALIAABNIgSAAIAAjBIASAAIAABXQAHgLAKgFQAKgFAJAAQALAAAIAFQAJAFAFAJQAGAKAAANIAABVg");
	this.shape_24.setTransform(-127.3,15.5);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#FFFFFF").s().p("AAYBAIAAhNQAAgMgHgGQgFgGgKABQgFAAgHADQgFADgEAGQgEAGAAAJIAABJIgbAAIAAh9IAWAAIAEAPQAGgJAJgEQAIgEAJAAQAMAAAKAFQAJAGAGAKQAGAJAAAPIAABSg");
	this.shape_25.setTransform(195.7,-12.9);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#FFFFFF").s().p("AgXA7QgMgFgHgKQgIgLAAgQQABgKADgHQADgFAHgDQAGgDAGAAIAvAAIAAgFQAAgIgEgFQgDgEgFgCQgGgCgGAAQgJAAgIAEQgIAEgHAGIgNgVQAIgJAMgGQAMgEAOAAQAMAAAMAEQAMAGAHAKQAIAKgBAQIAAAgQABAQgHALQgGALgLAGQgMAFgOABQgMAAgMgGgAgPAfQAGAIAKgBQAKABAGgIQAGgGAAgMIguAAQABAMAHAGg");
	this.shape_26.setTransform(183.5,-12.8);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#FFFFFF").s().p("AgHBhQgLAAgKgFQgKgFgGgKQgGgKAAgPIAAgnQAAgNAGgKQAGgKAKgFQAKgGAMAAQAIAAAIAEQAIADAHAIIAAhRIAaAAIAADBIgXAAIgDgOQgIAIgIAEQgHAEgHAAIgCgBgAgMgCQgFACgDAFQgDAFAAAIIAAAfQAAAIADAFQADAFAFACQAFACAGAAQAFAAAGgDQAGgDAEgGQAEgGAAgJIAAgWQAAgJgEgGQgEgGgGgCQgGgDgFAAQgGAAgFACg");
	this.shape_27.setTransform(170.9,-16.1);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#FFFFFF").s().p("AAYBAIAAhNQgBgMgFgGQgHgGgJABQgGAAgFADQgHADgDAGQgEAGAAAJIAABJIgcAAIAAh9IAXAAIADAPQAIgJAIgEQAJgEAIAAQALAAAKAFQALAGAFAKQAGAJABAPIAABSg");
	this.shape_28.setTransform(158.4,-12.9);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#FFFFFF").s().p("AgMBeIAAh8IAaAAIAAB8gAgMg9QgFgFgBgIQABgJAFgFQAFgFAHAAQAJAAAFAFQAFAFAAAJQAAAIgFAFQgFAFgJABQgHgBgFgFg");
	this.shape_29.setTransform(149.3,-16);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#FFFFFF").s().p("AgnBAIAAh9IAXAAIADAPQAHgIAHgEQAJgFAJAAQAGAAAFACIAKAEIgCAeQgFgFgGgCQgGgDgGABQgHAAgGADQgFADgEAGQgEAGAAAJIAABJg");
	this.shape_30.setTransform(142.2,-12.9);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#FFFFFF").s().p("AgWA7QgMgFgIgKQgIgLABgQQgBgKAEgHQAEgFAFgDQAHgDAHAAIAuAAIAAgFQAAgIgEgFQgDgEgGgCQgGgCgFAAQgJAAgIAEQgIAEgHAGIgOgVQAJgJAMgGQANgEANAAQANAAALAEQALAGAIAKQAHAKABAQIAAAgQgBAQgGALQgHALgLAGQgLAFgPABQgLAAgLgGgAgPAfQAHAIAIgBQALABAGgIQAGgGAAgMIgtAAQAAAMAHAGg");
	this.shape_31.setTransform(131.2,-12.8);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#FFFFFF").s().p("AgNBhIAAjBIAbAAIAADBg");
	this.shape_32.setTransform(122.1,-16.2);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#FFFFFF").s().p("AADBPQgIgDgGgJQgFgJgBgPIAAg9IgUAAIAAgaIAUAAIAAgeIAbgHIAAAlIAdAAIAAAaIgdAAIAAA6QAAAJAFAEQAFADAHAAIAGAAIAEgCIAAAbIgGABIgHAAIgCAAQgLAAgIgDg");
	this.shape_33.setTransform(115,-14.6);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#FFFFFF").s().p("AgXA7QgLgFgIgKQgHgLAAgQQAAgKADgHQADgFAGgDQAGgDAIAAIAuAAIAAgFQAAgIgDgFQgEgEgFgCQgHgCgFAAQgJAAgIAEQgIAEgHAGIgOgVQAJgJAMgGQAMgEAOAAQAMAAAMAEQALAGAIAKQAHAKAAAQIAAAgQAAAQgGALQgHALgKAGQgLAFgPABQgMAAgMgGgAgPAfQAHAIAJgBQAKABAGgIQAGgGAAgMIguAAQABAMAHAGg");
	this.shape_34.setTransform(105,-12.8);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#FFFFFF").s().p("AAYBAIAAhNQAAgMgHgGQgFgGgKABQgFAAgHADQgFADgEAGQgEAGAAAJIAABJIgbAAIAAh9IAWAAIAEAPQAGgJAJgEQAIgEAJAAQAMAAAKAFQAJAGAGAKQAGAJAAAPIAABSg");
	this.shape_35.setTransform(85,-12.9);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#FFFFFF").s().p("AgdA6QgJgFgHgKQgFgKgBgOIAAhRIAcAAIAABMQAAAJADAFQAEAEAFADQAEACAGAAQAFAAAGgCQAGgEAEgGQAEgGAAgJIAAhIIAcAAIAAB8IgXAAIgEgRQgGAKgKAEQgIAEgKABQgKAAgKgGg");
	this.shape_36.setTransform(72.2,-12.7);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("#FFFFFF").s().p("AgdBXQgMgFgIgHIAIgaQAIAHAJAFQAJAFANAAQAKAAAIgIQAIgKAAgPIAAgPQgGAJgJAEQgIAFgJgBQgLAAgKgFQgKgFgFgLQgGgKgBgMIAAhTIAcAAIAABOQAAAIADAFQADAEAGACQAFADAFAAQAGAAAFgDQAGgEAEgFQAEgGAAgJIAAhJIAcAAIAAB+QgBASgHANQgJANgMAGQgMAHgPAAQgNAAgMgFg");
	this.shape_37.setTransform(59.4,-9.9);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("#FFFFFF").s().p("AgYA7QgLgFgIgLQgHgLAAgQIAAgfQAAgQAHgLQAIgLALgFQAMgGAMABQANgBALAGQAMAFAIALQAHALAAAQIAAAfQAAAQgHALQgIALgMAFQgLAGgNAAQgMAAgMgGgAgLgiQgFACgEAFQgDAFAAAHIAAAfQAAAIADAFQAEAEAFADQAGACAFAAQAGAAAGgCQAFgDAEgEQADgFAAgIIAAgfQAAgHgDgFQgEgFgFgCQgGgDgGAAQgFAAgGADg");
	this.shape_38.setTransform(46.8,-12.8);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("#FFFFFF").s().p("AAYBcIAAhIQgHAKgJADQgIAFgJAAQgLAAgJgGQgKgFgFgKQgGgLAAgNIAAgnQAAgOAGgJQAGgLAKgFQAKgGAMABQAIAAAIADQAIADAHAHIADgLIAXAAIAAC0gAgMg+QgFACgDAGQgDAFAAAIIAAAfQAAAIADAEQADAFAFADQAFABAGAAQAFAAAGgCQAGgEAEgFQAEgGAAgJIAAgVQAAgKgEgFQgEgGgGgEQgGgDgFAAQgGAAgFACg");
	this.shape_39.setTransform(34.3,-10.1);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f("#FFFFFF").s().p("AgXA7QgMgFgHgKQgIgLAAgQQABgKADgHQADgFAHgDQAGgDAGAAIAvAAIAAgFQAAgIgEgFQgDgEgFgCQgGgCgGAAQgJAAgIAEQgIAEgHAGIgNgVQAIgJAMgGQAMgEAOAAQAMAAAMAEQAMAGAHAKQAIAKgBAQIAAAgQABAQgHALQgGALgLAGQgMAFgOABQgMAAgMgGgAgPAfQAGAIAKgBQAKABAGgIQAGgGAAgMIguAAQABAMAHAGg");
	this.shape_40.setTransform(14.7,-12.8);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f("#FFFFFF").s().p("AAAA/QgFABgEgEQgFgDgDgGIgthyIAdAAIAhBhIAihhIAdAAIgtByQgDAGgEADQgFAEgEAAIgCgBg");
	this.shape_41.setTransform(1.9,-12.7);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f("#FFFFFF").s().p("AgNBhIAAjBIAbAAIAADBg");
	this.shape_42.setTransform(-15.1,-16.2);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f("#FFFFFF").s().p("AgFBAQgMAAgKgFQgKgFgHgLQgGgKAAgOIAAglQAAgOAGgKQAHgKAKgGQAKgGAMAAQAIAAAIAEQAJADAFAIIACgNIAYAAIAAB9IgXAAIgDgOQgHAIgIAEQgHAEgGAAIgCgBgAgMgjQgFACgDAGQgDAFAAAIIAAAeQAAAIADAFQADAFAFACQAFACAGAAQAFAAAGgDQAGgDAEgGQAEgGAAgJIAAgVQAAgJgEgGQgEgGgGgDQgGgDgFAAQgGAAgFACg");
	this.shape_43.setTransform(-24.3,-12.8);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f("#FFFFFF").s().p("AA1BAIAAhQQAAgKgGgFQgGgFgHAAQgFAAgFACQgEACgDAFQgDAEAAAHIAABQIgaAAIAAhQQgBgKgGgFQgFgGgHABQgGAAgFACQgEACgDAFQgDAFAAAIIAABOIgcAAIAAh9IAXAAIADAPQAFgIAHgEQAIgFAIAAQAIAAAIAFQAIAEAEAJQAHgJAJgEQAIgFAKAAQALAAAKAGQAKAGAHAKQAGAKAAAPIAABQg");
	this.shape_44.setTransform(-39.9,-12.9);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f("#FFFFFF").s().p("AgTAXQAHgIADgHQADgHAAgKIAAgNIAagEIAAAQQAAANgDAJQgCAIgHAHg");
	this.shape_45.setTransform(-59.5,-7);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f("#FFFFFF").s().p("AgPBbIgLgCIACgUIAKACIALABQADAAADgBQADgBAAgDQAAgBAAAAQAAgBgBAAQAAgBgBAAQAAgBgBAAIgGgCIgKAAIAAgZIgPgBIgOgDIADgbIAJADIANACIAPABQAIAAAGgDQAGgDAAgGQAAgGgGgDIgPgFIgTgHQgKgEgGgHQgHgIAAgMQAAgOAGgJQAGgIAKgEQAKgDANAAQAJAAALACIAVAFIgEAZIgRgEQgJgCgKAAQgIAAgFACQgFADgBAHQABAFAGACQAGADAJACIATAHQAKAEAGAIQAGAIABANQgBARgKAJQgKAJgQACIAAAIQALADAEAGQAFAGAAAIQgBAMgIAHQgJAGgOAAIgMgBg");
	this.shape_46.setTransform(-67.2,-10.1);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f("#FFFFFF").s().p("AgNBeIAAh8IAbAAIAAB8gAgMg9QgGgFAAgIQAAgJAGgFQAFgFAHAAQAJAAAEAFQAGAFAAAJQAAAIgGAFQgEAFgJABQgHgBgFgFg");
	this.shape_47.setTransform(-75,-16);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f("#FFFFFF").s().p("AA1BAIAAhQQAAgKgGgFQgGgFgHAAQgFAAgFACQgFACgCAFQgDAEAAAHIAABQIgaAAIAAhQQgBgKgGgFQgFgGgHABQgGAAgEACQgFACgDAFQgDAFAAAIIAABOIgcAAIAAh9IAXAAIADAPQAFgIAIgEQAGgFAJAAQAJAAAHAFQAIAEAFAJQAGgJAJgEQAJgFAJAAQALAAAKAGQALAGAGAKQAHAKgBAPIAABQg");
	this.shape_48.setTransform(-86.9,-12.9);

	this.shape_49 = new cjs.Shape();
	this.shape_49.graphics.f("#FFFFFF").s().p("AgNBhIAAjBIAbAAIAADBg");
	this.shape_49.setTransform(-99.1,-16.2);

	this.shape_50 = new cjs.Shape();
	this.shape_50.graphics.f("#FFFFFF").s().p("AgNBeIAAh8IAbAAIAAB8gAgMg9QgGgFAAgIQAAgJAGgFQAFgFAHAAQAJAAAEAFQAGAFAAAJQAAAIgGAFQgEAFgJABQgHgBgFgFg");
	this.shape_50.setTransform(-104.6,-16);

	this.shape_51 = new cjs.Shape();
	this.shape_51.graphics.f("#FFFFFF").s().p("AgDBAIgTgBIgUgEIADgaIAJACIANACIAPABQAIAAAGgDQAGgDAAgHQAAgFgGgDIgPgGIgTgGQgKgEgGgHQgHgHAAgNQAAgOAGgIQAGgIAKgEQAKgEANAAQAJAAALACIAVAFIgEAZIgRgEQgJgBgKAAQgIgBgFADQgFACgBAHQABAFAGADQAGADAJACIATAHQAKAEAGAHQAGAHABAOQgBAOgGAJQgHAIgLAEQgKAEgLAAIgDgBg");
	this.shape_51.setTransform(-112.4,-12.8);

	this.shape_52 = new cjs.Shape();
	this.shape_52.graphics.f("#FFFFFF").s().p("AgXA7QgMgFgHgKQgIgLAAgQQAAgKAEgHQADgFAHgDQAGgDAGAAIAvAAIAAgFQAAgIgEgFQgDgEgGgCQgFgCgGAAQgJAAgIAEQgIAEgHAGIgNgVQAIgJAMgGQAMgEAOAAQAMAAAMAEQAMAGAHAKQAIAKAAAQIAAAgQAAAQgHALQgGALgLAGQgMAFgPABQgLAAgMgGgAgPAfQAGAIAJgBQALABAGgIQAGgGAAgMIguAAQABAMAHAGg");
	this.shape_52.setTransform(-123.3,-12.8);

	this.shape_53 = new cjs.Shape();
	this.shape_53.graphics.f("#FFFFFF").s().p("AAVBhIAAgkQAAgGgDgEQgEgFgEgCIgPAEQgHABgJAAIAAAwIgbAAIAAjBIAbAAIAAB5QAPAAAJgGQAJgFAFgJQAEgHAAgLIAAgGIgBgFIAcgEIAAAHIABAHQAAAOgEAKQgGALgIAIQAIAFAFAIQAEAHAAAKIAAAmg");
	this.shape_53.setTransform(-135.2,-16.2);

	this.shape_54 = new cjs.Shape();
	this.shape_54.graphics.f("#FFFFFF").s().p("AgNBhIAAjBIAbAAIAADBg");
	this.shape_54.setTransform(-151.8,-16.2);

	this.shape_55 = new cjs.Shape();
	this.shape_55.graphics.f("#FFFFFF").s().p("AgFBAQgMAAgKgFQgKgFgHgLQgGgKAAgOIAAglQAAgOAGgKQAHgKAKgGQAKgGAMAAQAIAAAIAEQAJADAFAIIACgNIAYAAIAAB9IgXAAIgDgOQgHAIgIAEQgHAEgGAAIgCgBgAgMgjQgFACgDAGQgDAFAAAIIAAAeQAAAIADAFQADAFAFACQAFACAGAAQAFAAAGgDQAGgDAEgGQAEgGAAgJIAAgVQAAgJgEgGQgEgGgGgDQgGgDgFAAQgGAAgFACg");
	this.shape_55.setTransform(-161,-12.8);

	this.shape_56 = new cjs.Shape();
	this.shape_56.graphics.f("#FFFFFF").s().p("AgNBhIAAjBIAbAAIAADBg");
	this.shape_56.setTransform(-170.2,-16.2);

	this.shape_57 = new cjs.Shape();
	this.shape_57.graphics.f("#FFFFFF").s().p("AgFBAQgMAAgKgFQgKgFgHgLQgGgKAAgOIAAglQAAgOAGgKQAHgKAKgGQAKgGAMAAQAIAAAIAEQAJADAFAIIACgNIAYAAIAAB9IgXAAIgDgOQgHAIgIAEQgHAEgGAAIgCgBgAgMgjQgFACgDAGQgDAFAAAIIAAAeQAAAIADAFQADAFAFACQAFACAGAAQAFAAAGgDQAGgDAEgGQAEgGAAgJIAAgVQAAgJgEgGQgEgGgGgDQgGgDgFAAQgGAAgFACg");
	this.shape_57.setTransform(-179.4,-12.8);

	this.shape_58 = new cjs.Shape();
	this.shape_58.graphics.f("#FFFFFF").s().p("AAoBaIAAhRIhPAAIAABRIgbAAIAAi0IAbAAIAABKIBPAAIAAhKIAbAAIAAC0g");
	this.shape_58.setTransform(-193.8,-15.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_58},{t:this.shape_57},{t:this.shape_56},{t:this.shape_55},{t:this.shape_54},{t:this.shape_53},{t:this.shape_52},{t:this.shape_51},{t:this.shape_50},{t:this.shape_49},{t:this.shape_48},{t:this.shape_47},{t:this.shape_46},{t:this.shape_45},{t:this.shape_44},{t:this.shape_43},{t:this.shape_42},{t:this.shape_41},{t:this.shape_40},{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-203.6,-32.5,407.2,65.1);


(lib.Tween4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.logox2();
	this.instance.parent = this;
	this.instance.setTransform(-81,-44,0.481,0.481);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-81,-44,162.5,86.6);


(lib.Tween2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.donerr();
	this.instance.parent = this;
	this.instance.setTransform(-97.5,-179);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-97.5,-179,195,358);


(lib.Tween1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.donerr();
	this.instance.parent = this;
	this.instance.setTransform(-97.5,-179);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-97.5,-179,195,358);


(lib.Symbol1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 8
	this.instance = new lib.Tween12("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(28.7,29.7,2.884,2.884,0,0,0,0.1,0.1);
	this.instance.alpha = 0;
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(97).to({_off:false},0).to({regX:0,regY:0,scaleX:0.77,scaleY:0.77,x:28.6,y:29.4,alpha:1},9).to({scaleX:0.92,scaleY:0.92},3).wait(286).to({startPosition:0},0).to({alpha:0},20).wait(13));

	// Layer 7
	this.instance_1 = new lib.Tween10("synched",0);
	this.instance_1.parent = this;
	this.instance_1.setTransform(390.4,45.3);
	this.instance_1.alpha = 0;
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(236).to({_off:false},0).to({x:421.4,alpha:1},19).wait(140).to({startPosition:0},0).to({alpha:0},20).wait(13));

	// Layer 5
	this.instance_2 = new lib.Tween8("synched",0);
	this.instance_2.parent = this;
	this.instance_2.setTransform(505.7,44.1);
	this.instance_2.alpha = 0;
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(196).to({_off:false},0).to({scaleX:0.3,scaleY:0.3,y:45.6,alpha:1},15).to({scaleX:0.38,scaleY:0.38,y:46.1},3).wait(9).to({startPosition:0},0).to({x:497.7},2).to({x:648.1},8).wait(162).to({startPosition:0},0).to({alpha:0},20).wait(13));

	// Layer 6
	this.instance_3 = new lib.Tween6("synched",0);
	this.instance_3.parent = this;
	this.instance_3.setTransform(499.6,48.1,0.294,0.294);
	this.instance_3.alpha = 0;
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(53).to({_off:false},0).to({scaleX:1.06,scaleY:1.06,alpha:1},12).to({scaleX:1,scaleY:1},3).wait(115).to({startPosition:0},0).to({regY:0.1,scaleX:1.09,scaleY:1.09,y:48.2},3).to({regY:0,scaleX:0.22,scaleY:0.22,y:48.1,alpha:0},16).wait(226));

	// Layer 4
	this.instance_4 = new lib.Tween4("synched",0);
	this.instance_4.parent = this;
	this.instance_4.setTransform(187.3,45,0.488,0.488);
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(30).to({_off:false},0).to({scaleX:1.07,scaleY:1.07},12).to({scaleX:0.98,scaleY:0.98},3).wait(350).to({startPosition:0},0).to({alpha:0},20).wait(13));

	// Layer 3
	this.instance_5 = new lib.Tween1("synched",0);
	this.instance_5.parent = this;
	this.instance_5.setTransform(52,179);
	this.instance_5.alpha = 0;

	this.instance_6 = new lib.Tween2("synched",0);
	this.instance_6.parent = this;
	this.instance_6.setTransform(52,-87.5);
	this.instance_6.alpha = 0;

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_5}]}).to({state:[{t:this.instance_5}]},22).to({state:[{t:this.instance_5}]},373).to({state:[{t:this.instance_6}]},27).wait(6));
	this.timeline.addTween(cjs.Tween.get(this.instance_5).to({y:166.1,alpha:1},22).to({y:-71.6},373).to({_off:true,y:-87.5,alpha:0},27).wait(6));

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#050505").s().p("Eg43AHCIAAuDMBxvAAAIAAODg");
	this.shape.setTransform(364,45);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(428));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-45.5,0,773.5,358);


// stage content:
(lib.halaldoner728x90 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Symbol1();
	this.instance.parent = this;
	this.instance.setTransform(364,45,1,1,0,0,0,364,45);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(318.5,45,773.5,358);
// library properties:
lib.properties = {
	width: 728,
	height: 90,
	fps: 26,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [
		{src:"images/halaldoner_728x90_atlas_.png?1497876662151", id:"halaldoner_728x90_atlas_"}
	],
	preloads: []
};




})(lib = lib||{}, images = images||{}, createjs = createjs||{}, ss = ss||{}, AdobeAn = AdobeAn||{});
var lib, images, createjs, ss, AdobeAn;