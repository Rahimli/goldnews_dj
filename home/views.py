from xml.dom.minidom import parseString
import json
import requests
import time
import xmltodict as xmltodict
from django.contrib.auth import logout
from django.contrib.auth.decorators import login_required
from django.core.mail import send_mail
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.db.models import Prefetch, Max
from django.http import Http404, HttpResponse, JsonResponse
from django.shortcuts import render, render_to_response, get_object_or_404
# Create your views here.
from django.template import RequestContext
# from eventlet.green import urllib2
from django.template.loader import render_to_string

from home.models import *
from userprofile.models import *
from eblog import blog_settings
from xml.dom import minidom
import bs4 as bs
import urllib.request
import urllib.parse

def currency_header():
    now = timezone.now()
    yesterday = timezone.now() - timezone.timedelta(1)
    url_str = 'http://cbar.az/currencies/' + '{0:0=2d}'.format(now.day) + '.' + '{0:0=2d}'.format(
        now.month) + '.' + str(now.year) + '.xml'
    url_str_yesterday = 'http://cbar.az/currencies/' + '{0:0=2d}'.format(yesterday.day) + '.' + '{0:0=2d}'.format(
        yesterday.month) + '.' + str(yesterday.year) + '.xml'
    # return HttpResponse(url_str_yesterday)
    file = urllib.request.urlopen(url_str)
    data = file.read()
    file.close()
    data = xmltodict.parse(data)
    bank_metals = data['ValCurs']['ValType'][-1]['Valute']
    #******************************************************************
    file_yesterday = urllib.request.urlopen(url_str_yesterday)
    data_yesterday = file_yesterday.read()
    file_yesterday.close()
    data_yesterday = xmltodict.parse(data_yesterday)
    bank_metals_yesterday = data_yesterday['ValCurs']['ValType'][-1]['Valute']
    # ******************************************************************
    dic = {}
    i = 1
    j = 0
    for bank_metal in bank_metals:
        # return HttpResponse(bank_metal)
        increase = float(bank_metal['Value']) - float(bank_metals_yesterday[0]['Value'])
        if increase > 0:
            inc = 'inc'
        elif increase < 0:
            inc = 'dec'
        else:
            inc = 'noc'
        if bank_metal['@Code'] == 'USD' or bank_metal['@Code'] == 'EUR' or bank_metal['@Code'] == 'RUB':
            if bank_metal['@Code'] == 'USD':
                i = 0
            if bank_metal['@Code'] == 'EUR':
                i = 1
            if bank_metal['@Code'] == 'RUB':
                i = 1
            dic[i]= {
                'code':bank_metal['@Code'],
                'nominal':bank_metal['Nominal'],
                'name':bank_metal['Name'],
                'value':bank_metal['Value'],
                'increase':inc
            }
            i += 1
            j += 1
    return dic

def context_processor(request):
    now = timezone.now()

    last_48 = now - timezone.timedelta(days=2)
    # return HttpResponse(str(last_48))
    last_48_list = []
    newss_views = NewsView.objects.all()
    yesterday = timezone.now() - timezone.timedelta(days=1)
    context = {}
    # base_last_48_most_read_news_views = newss_views.filter(date__range=(last_48, now)).values('news__id').annotate(
    #     count=Count('news__id')).order_by('-count')[:10]
    # for base_last_48_most_read_news in base_last_48_most_read_news_views:
    #     last_48_list.append(base_last_48_most_read_news['news__id'])
    base_categorys = Category.objects.filter(parent=None).filter(publish_start_date__lte=now).order_by('order_index')
    socials = Social.objects.filter(is_active=True).order_by('order_index')
    base_tags = Tag.objects.all().order_by('date')[:20]
    #
    most_read_newss = News.objects.filter(is_active=True).filter(publish_start_date__gte=last_48).order_by('-read_count')[:10]
    site_info = SiteInformation.objects.filter(publish=True).order_by('-date').first()
    base_ads = Adss.objects.filter(active=True).order_by('-date').first()
    currencies_today = Currency.objects.filter(date=now.date())
    # # currencies_yesterday = Currency.objects.filter(date=yesterday)
    if not currencies_today.count() == 45:
        currencies = Currency.objects.all()
        currencies.delete()
        url_str = 'http://cbar.az/currencies/' + '{0:0=2d}'.format(now.day) + '.' + '{0:0=2d}'.format(
            now.month) + '.' + str(now.year) + '.xml'
        url_str_yesterday = 'http://cbar.az/currencies/' + '{0:0=2d}'.format(yesterday.day) + '.' + '{0:0=2d}'.format(
            yesterday.month) + '.' + str(yesterday.year) + '.xml'
        # # return HttpResponse(url_str_yesterday)
        file = urllib.request.urlopen(url_str)
        data = file.read()
        file.close()
        data = xmltodict.parse(data)
        bank_metals = data['ValCurs']['ValType'][-1]['Valute']
        #******************************************************************
        #******************************************************************
        file_yesterday = urllib.request.urlopen(url_str_yesterday)
        data_yesterday = file_yesterday.read()
        file_yesterday.close()
        data_yesterday = xmltodict.parse(data_yesterday)
        bank_metals_yesterday = data_yesterday['ValCurs']['ValType'][-1]['Valute']
        # ******************************************************************
        # # ******************************************************************

        dic = {}
        i = 4
        j = 0
        for bank_metal in bank_metals:
            # return HttpResponse(bank_metal)

            increase = 0
            for bank_metal_yesterday in bank_metals_yesterday:
                if bank_metal['@Code'] == bank_metal_yesterday['@Code']:
                    increase = bank_metal_yesterday['Value']
            i+=1
            # if increase > 0:
            #     inc = 'inc'
            # elif increase < 0:
            #     inc = 'dec'
            # else:
            #     inc = 'noc'
            # order_index = 0
            if bank_metal['@Code'] =='USD' :
                order_index = 1
            elif bank_metal['@Code'] == 'EUR':
                order_index = 2
            elif bank_metal['@Code'] == 'RUB':
                order_index = 3
            elif bank_metal['@Code'] == 'TRY':
                order_index = 4
            else:
                order_index = i
            # inc = float(bank_metal['Value']) - float(bank_metals_yesterday[0]['Value'])
            # if float(bank_metal['Value']) > float(bank_metals_yesterday[0]['Value']):
            #     inc = 'inc'
            # elif float(bank_metal['Value']) < float(bank_metals_yesterday[0]['Value']):
            #     inc = 'dec'
            # else:
            #     inc = 'noc'
            currency_today_save = Currency(currency=bank_metal['Name'],
                                     code=bank_metal['@Code'],
                                     rate=bank_metal['Value'],
                                     difference=increase,
                                     order_index=order_index,
                                     date=now)
            currency_today_save.save()
    #
    city = City.objects.filter(is_active=True,date=now.date()).order_by('order').first()
    if not city:
        city_o = City.objects.filter(is_active=True).order_by('order').first()
        try:
            # raise Http404
            url_str = 'http://api.wunderground.com/api/7044c78126e083fa/geolookup/conditions/forecast/lang:EN/q/Azerbaijan/' + city_o.code + '.xml'
            file = urllib.request.urlopen(url_str)
            weathers = file.read()
            file.close()
            # weathers = weathers[1][0]
            data = xmltodict.parse(weathers)
            # return HttpResponse('sasa')
            # return HttpResponse(data['response']['forecast']['simpleforecast']['forecastdays'])
            data = data['response']['forecast']['simpleforecast']['forecastdays']['forecastday'][0]
            dic = {}
            i = 0
            # for d in data:
            # return HttpResponse(d['icon_url'])
            city_o.high = data['high']['celsius']
            city_o.low = data['low']['celsius']
            city_o.icon_url = data['icon_url']
            city_o.conditions = data['conditions']
            city_o.tz_short = data['date']['tz_short']
            city_o.date = now.date()
            city_o.save()
        except:
            pass
        city = City.objects.filter(is_active=True, date=now.date()).order_by('order').first()
    # _____________________*************************************-------------------------------------
    currencies_today = Currency.objects.filter(date=now).order_by('order_index')
    base_weathers = Weather.objects.filter(date=now)
    if request.user.is_authenticated():
        try:
            user_profile_base = UserProfile.objects.get(user=request.user)
        except:
            # user_profile_base = None
            user_profile_base = UserProfile(user=request.user)
            user_profile_base.save()
            user_profile_base = UserProfile.objects.get(user=request.user)
        # finally:
        #     user_profile_base = UserProfile.objects.get(user=request.user)
        context.update({
            'user_profile_base':user_profile_base,
        })
    base_weathers = Weather.objects.filter(date=now).order_by('city_id')
    context.update({
                'now':now,
                'yesterday':yesterday,
                'base_categorys':base_categorys,
                'base_tags':base_tags,
                'most_read_newss':most_read_newss,
                'site_info' : site_info,
                'base_ads' : base_ads,
                'currencies_today' : currencies_today,
                'base_city' : city,
                # 'base_header_currencies':dic,

                'base_socials': socials,
               })

    return context

def index(request):
    now = timezone.now()
    # index_categorys = Category.objects.filter(parent=None).filter(publish_start_date__lte=now).order_by('order_index').prefetch_related(Prefetch('news', queryset=News.objects.select_related('categorys')))
    #
    # for index_category in index_categorys:
    #     for index_category_news in index_category.news:
    #         print(index_category_news.name)
    # bakeries = Bakery.objects.annotate(hottest_cake_baked_at=Max('cake__baked_at'))
    # hottest_cakes = Cake.objects.filter(baked_at__in=[b.hottest_cake_baked_at for b in bakeries])
    #
    # hottest_cake_ids = Bakery.objects.annotate(hottest_cake_id=Max('cake__id')).values_list('hottest_cak‌​e_id',flat=True)
    # hottest_cakes = Cake.objects.filter(id__in=hottest_cake_ids)
    # index_categorys = Category.objects.annotate(hottest_cake_id=Max('news__id')).values_list('hottest_news_id',flat=True)
    # last_48 = now - timezone.timedelta(days=1)
    # return HttpResponse(str(last_48.date()))
    # body_text = 'kp-home'
    # recent_newss = News.objects.filter(is_active=True).order_by('-publish_start_date').filter(publish_start_date__lte=now)
    # slider_newss = News.objects.filter(show_slider=True).filter(is_active=True).order_by('-publish_start_date').filter(publish_start_date__lte=now)

    base_slider_newss = News.objects.filter(show_slider=True).filter(is_active=True).order_by(
        '-publish_start_date').filter(publish_start_date__lte=now)[:17]

    home_multimedia = Multimedia.objects.filter(type='gallery_news').filter(is_active=True).order_by('-publish_start_date').filter(publish_start_date__lte=now).first()
    home_multimedia_video = Multimedia.objects.filter(type='video_news').filter(is_active=True).order_by('-publish_start_date').filter(publish_start_date__lte=now).first()

    # return HttpResponse(now.date())
    context = {
                'home_multimedia':home_multimedia,
                'home_multimedia_video' : home_multimedia_video,
                'base_slider_newss' : base_slider_newss,
    }
    return render(request, 'home/index.html', context=context)
def all_news(request):
    now = timezone.now()
    # body_text = 'kp-home'
    recent_list = News.objects.filter(is_active=True).order_by('-publish_start_date').filter(publish_start_date__lte=now)
    paginator = Paginator(recent_list, 66)  # Show 25 contacts per page

    page = request.GET.get('page')
    try:
        recent_newss = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        recent_newss = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        recent_newss = paginator.page(paginator.num_pages)
    # return HttpResponse(home_multimedia.first_name)
    context = {
                'datas':recent_newss
    }
    return render(request, 'home/all_news.html', context=context)

def create_subscriber(request):
    if request.method == 'POST' and request.is_ajax():
        email = request.POST.get('email')
        subscriber = Subscriber.objects.filter(email=email).first()
        if subscriber is None:
            s = Subscriber(email=email)
            s.save()
            message1 = "Abunəliyiniz Uğurla qeydə alındı"
            res = 1
            data = {"message1":message1,'res':res}
        else:
            message1 = "Siz Artiq Abunə olmusunuz"
            res = 0
            data = {"message1":message1,'res':res}
        return JsonResponse(data)
    return Http404


import xml.etree.ElementTree as ET
import xmltodict
import urllib
import urllib.request
import urllib.error
import sys
import xml.etree.ElementTree as etree
# from __future__ import with_statement # we'll use this later, has to be here
from argparse import ArgumentParser

import requests
# from BeautifulSoup import BeautifulStoneSoup as Soup
def pages(request,page_slug):
    now = timezone.now()
    yesterday = timezone.now() - timezone.timedelta(1)
    if page_slug == 'currency':


        # return HttpResponse(dic[1].values())
        return render(request, 'home/currency.html', context={})
    elif page_slug == 'weather':
        cities = City.objects.exclude(is_active=True, date=now.date()).order_by('order')
        # return HttpResponse(cities.count())
        if cities:
            # return HttpResponse('var')
            for city in cities:
                # if not city:
                    # return HttpResponse('sasas')
                try:
                    # raise Http404
                    url_str = 'http://api.wunderground.com/api/7044c78126e083fa/geolookup/conditions/forecast/lang:EN/q/Azerbaijan/' + city.code + '.xml'
                    file = urllib.request.urlopen(url_str)
                    weathers = file.read()
                    file.close()
                    # weathers = weathers[1][0]
                    data = xmltodict.parse(weathers)
                    # return HttpResponse('sasa')
                    # return HttpResponse(data['response']['forecast']['simpleforecast']['forecastdays'])
                    data = data['response']['forecast']['simpleforecast']['forecastdays']['forecastday'][0]
                    dic = {}
                    i = 0
                    # for d in data:
                    # return HttpResponse(d['icon_url'])

                    city.high = data['high']['celsius']
                    city.low = data['low']['celsius']
                    city.icon_url = data['icon_url']
                    city.conditions = data['conditions']
                    city.tz_short = data['date']['tz_short']
                    # city.date = now.date()
                    # city.save()
                except:
                    pass
                city.date = now.date()
                city.save()
                    # return HttpResponse('sasas')
        cities = City.objects.filter(is_active=True, date=now.date()).order_by('order')
        context = {

                    'cities' : cities,
                    # 'slider_newss' : slider_newss,
        }
        return render(request, 'home/weather.html', context=context)
    else:
        raise Http404


def _html_weather(city):
    _html_data = ''
    try:
        city_o = get_object_or_404(City,id=city,is_active=True)
        # city_o = City.objects.filter(id=city).filter(is_active=True).order_by('order').first()
        url_str = 'http://api.wunderground.com/api/7044c78126e083fa/geolookup/conditions/forecast/lang:EN/q/Azerbaijan/' + str(city_o.code) + '.xml'
        file = urllib.request.urlopen(url_str)
        weathers = file.read()
        file.close()
        # weathers = weathers[1][0]
        data = xmltodict.parse(weathers)
        # return HttpResponse('sasa')
        # return HttpResponse(data['response']['forecast']['simpleforecast']['forecastdays'])
        data = data['response']['forecast']['txt_forecast']['forecastdays']['forecastday']
        dic = {}
        i = 0
        for d in data:
            # return HttpResponse(d['icon_url'])
            dic[i] = {
                # 'date':d['@Code'],
                # 'day':bank_metal['Nominal'],
                'icon_url': d['icon_url'],
                'icon': d['icon'],
                'title': d['title'],
                'fcttext_metric': d['fcttext_metric'],
                # 'pretty_short':d['pretty_short'],
                # 'situation':bank_metal['Value'],
                # 'min':inc,
                # 'max':inc,
                # 'windy':inc,
                # 'humidity':inc,
            }
            i += 1
        weathers = dic
        _html_data = '{0}{1}'.format(_html_data,render_to_string('include/weather_part.html',{'weathers': weathers,}))
        # for weather in weathers:
        #     _html_data = '{0}{1}'.format(_html_data,
    except PageNotAnInteger:
        return ''
    except EmptyPage:
        return ''

    return _html_data
