from ckeditor_uploader.fields import RichTextUploadingField
from django.db import models

# Create your models here.
def ads_upload_location(instance,filename):
    return "ads/%s/%s" %(instance.title,filename)
class Adss(models.Model):
    active = models.BooleanField(default=False)
    title = models.CharField(max_length=255)
    header_horizontal_ads_1_image = models.ImageField(max_length=500,upload_to='ads/header_horizontal_ads_1_image',blank=True)
    header_horizontal_ads_1_script = models.TextField(blank=True)
    header_horizontal_ads_1_link = models.URLField(blank=True,null=True,)
    header_horizontal_ads_1_active = models.BooleanField(default=False)
    header_horizontal_ads_2_image = models.ImageField(max_length=500,upload_to='ads/header_horizontal_ads_2_image',blank=True)
    header_horizontal_ads_2_script = models.TextField(blank=True)
    header_horizontal_ads_2_link = models.URLField(blank=True,null=True,)
    header_horizontal_ads_2_active = models.BooleanField(default=False)
    headline_1_ads_image = models.ImageField(max_length=500,upload_to='ads/headline_1_ads_image',blank=True)
    headline_1_ads_script = models.TextField(blank=True)
    headline_1_ads_link = models.URLField(blank=True,null=True,)
    headline_1_ads_active = models.BooleanField(default=False)
    headline_2_ads_image = models.ImageField(max_length=500,upload_to='ads/headline_2_ads_image',blank=True)
    headline_2_ads_script = models.TextField(blank=True)
    headline_2_ads_link = models.URLField(blank=True,null=True,)
    headline_2_ads_active = models.BooleanField(default=False)
    home_category_vertical_ads_1_image = models.ImageField(blank=True,upload_to='ads/home_category_vertical_ads_1_image')
    home_category_vertical_ads_1_link = models.URLField(blank=True,null=True,)
    home_category_vertical_ads_1_script = models.TextField(blank=True)
    home_category_vertical_ads_1_active = models.BooleanField(default=False)
    home_category_vertical_ads_3_image = models.ImageField(max_length=500,blank=True,upload_to='ads/home_category_vertical_ads_3_image')
    home_category_vertical_ads_3_link = models.URLField(blank=True,null=True,)
    home_category_vertical_ads_3_script = models.TextField(blank=True)
    home_category_vertical_ads_3_active = models.BooleanField(default=False)
    home_category_vertical_ads_4_image = models.ImageField(max_length=500,blank=True,upload_to='ads/home_category_vertical_ads_4_image')
    home_category_vertical_ads_4_link = models.URLField(blank=True,null=True,)
    home_category_vertical_ads_4_script = models.TextField(blank=True)
    home_category_vertical_ads_4_active = models.BooleanField(default=False)
    home_category_vertical_ads_5_image = models.ImageField(max_length=500,blank=True,upload_to='ads/home_category_vertical_ads_5_image')
    home_category_vertical_ads_5_link = models.URLField(blank=True,null=True,)
    home_category_vertical_ads_5_script = models.TextField(blank=True)
    home_category_vertical_ads_5_active = models.BooleanField(default=False)
    home_category_vertical_ads_7_image = models.ImageField(max_length=500,blank=True,upload_to='ads/home_category_vertical_ads_7_image')
    home_category_vertical_ads_7_link = models.URLField(blank=True,null=True,)
    home_category_vertical_ads_7_script = models.TextField(blank=True)
    home_category_vertical_ads_7_active = models.BooleanField(default=False)
    home_category_vertical_ads_other_image = models.ImageField(max_length=500,blank=True,upload_to='ads/home_category_vertical_ads_other_image')
    home_category_vertical_ads_other_link = models.URLField(blank=True,null=True,)
    home_category_vertical_ads_other_script = models.TextField(blank=True)
    home_category_vertical_ads_other_active = models.BooleanField(default=False)
    category_vertical_ads_image = models.ImageField(max_length=500,blank=True,upload_to='ads/home_category_vertical_ads_image')
    category_vertical_ads_link = models.URLField(blank=True,null=True,)
    category_vertical_ads_script = models.TextField(blank=True)
    category_vertical_ads_active = models.BooleanField(default=False)
    video_vertical_ads_image = models.ImageField(max_length=500,blank=True,upload_to='ads/home_video_vertical_ads_image')
    video_vertical_ads_link = models.URLField(blank=True,null=True,)
    video_vertical_ads_script = models.TextField(blank=True)
    video_vertical_ads_active = models.BooleanField(default=False)
    gallery_vertical_ads_image = models.ImageField(max_length=500,blank=True,upload_to='ads/home_gallery_vertical_ads_image')
    gallery_vertical_ads_link = models.URLField(blank=True,null=True,)
    gallery_vertical_ads_script = models.TextField(blank=True)
    gallery_vertical_ads_active = models.BooleanField(default=False)
    news_vertical_ads_image = models.ImageField(max_length=500,upload_to='ads/news_vertical_ads_image',blank=True,null=True,)
    news_vertical_ads_script = models.TextField(blank=True,null=True)
    news_vertical_active = models.BooleanField(default=False)
    news_vertical_ads_link = models.URLField(blank=True,null=True,)
    news_horizental_top_ads_image = models.ImageField(max_length=500,upload_to='ads/news_horizental_top_ads_image',blank=True,null=True,)
    news_horizental_top_ads_link = models.URLField(blank=True,null=True,)
    news_horizental_top_ads_script = models.TextField(blank=True,null=True)
    news_horizental_top_active = models.BooleanField(default=False)
    news_horizental_bottom_ads_image = models.ImageField(max_length=500,upload_to='ads/news_horizental_bottom_ads_image',blank=True,null=True,)
    news_horizental_bottom_ads_script = models.TextField(blank=True,null=True,)
    news_horizental_bottom_active = models.BooleanField(default=False)
    news_horizental_bottom_ads_link = models.URLField(blank=True,null=True,)
    date = models.DateTimeField(auto_now_add=True)
    def __str__(self):
        return self.title
    class Meta:
        verbose_name_plural = "Reklamlar"
        verbose_name = "Reklam"


class Subscriber(models.Model):
    email = models.EmailField()
    date = models.DateTimeField(auto_now_add=True)
    def __str__(self):
        return self.email
    class Meta:
        verbose_name_plural = "Abunələr"
        verbose_name = "Abunə"



class SearchWord(models.Model):
    search_key = models.CharField(max_length=200)
    result_count = models.IntegerField()
    user_ip = models.CharField(max_length=255)
    date = models.DateTimeField(auto_now_add=True)
    def __str__(self):
        return self.search_key
    class Meta:
        verbose_name_plural = "Axtarilar Sözlər"
        verbose_name = "Axtarılan Söz"


def site_information_upload_location(instance,filename):
    return "site-nformation/%s/%s" %(instance.site_name,filename)
class SiteInformation(models.Model):
    site_name = models.CharField(max_length=255)
    image = models.ImageField(max_length=500,upload_to=site_information_upload_location,blank=True,null=True)
    meta_description = models.TextField(blank=True,null=True)
    meta_keywords = models.CharField(max_length=10240, null=False)
    date = models.DateTimeField(auto_now_add=True)
    publish = models.BooleanField()
    def __str__(self):
        return self.site_name
    class Meta:
        verbose_name_plural = "Saytın Informasiyaları"
        verbose_name = "Saytın Informasiyası"

class City(models.Model):
    name = models.CharField(max_length=255)
    code = models.CharField(max_length=255)
    order = models.IntegerField()
    is_active = models.BooleanField(default=True)
    high = models.CharField(max_length=255,blank=True,null=True)
    low = models.CharField(max_length=255,blank=True,null=True)
    icon_url = models.URLField(blank=True,null=True)
    conditions = models.CharField(max_length=255,blank=True,null=True)
    tz_short = models.CharField(max_length=255,blank=True,null=True)
    date = models.DateField()
    class Meta:
        verbose_name_plural = "Şəhərlər"
        verbose_name = "Şəhər"
    def __str__(self):
        return self.name
class Weather(models.Model):
    city = models.ForeignKey('City')
    high = models.CharField(max_length=255,blank=True,null=True)
    low = models.CharField(max_length=255,blank=True,null=True)
    icon_url = models.URLField(blank=True,null=True)
    conditions = models.CharField(max_length=255,blank=True,null=True)
    tz_short = models.CharField(max_length=255,blank=True,null=True)
    date = models.DateField(blank=True,null=True)

    # pretty = models.CharField(blank=True,null=True)
class Social(models.Model):
    title = models.CharField(max_length=255)
    order_index = models.IntegerField()
    fa_icon = models.CharField(max_length=30,help_text='example: fa fa-facebook-square - <a target="_blank" href="http://fontawesome.io/icons/">fontawesome.io<a>')
    url = models.URLField()
    is_active = models.BooleanField()
    date  = models.DateTimeField(auto_now_add=True)
    def __str__(self):
        return self.title

    class Meta:
        verbose_name_plural = "Sosial Medya"
        verbose_name = "Sosial Medya"

class Currency(models.Model):
    currency = models.CharField(max_length=255)
    code = models.CharField(max_length=255)
    rate = models.DecimalField(max_digits=19, decimal_places=5)
    difference = models.DecimalField(max_digits=19, decimal_places=5,blank=True,null=True)
    order_index = models.IntegerField(blank=True)
    # is_active = models.BooleanField()
    date  = models.DateField(auto_now_add=True)
    def __str__(self):
        return self.currency

    class Meta:
        verbose_name_plural = "Valyuta"
        verbose_name = "valyuta"

# class Bakery(models.Model):
#     town = models.CharField(max_length=255)
#
# class Cake(models.Model):
#     bakery = models.ForeignKey(Bakery)
#     baked_at = models.DateTimeField()