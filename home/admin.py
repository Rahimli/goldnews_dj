from django.contrib import admin
from .models import *
# Register your models here.
admin.site.register(Adss)


# class SubscriberAdmin(admin.ModelAdmin):
#     list_display = ("email", "date",)
#     list_filter = ("email", "date",)
#     search_fields = ("email", )
#     # prepopulated_fields = {'slug': ('title',)}
# admin.site.register(Subscriber,SubscriberAdmin)

admin.site.register(SiteInformation)


class CityAdmin(admin.ModelAdmin):
    list_display = ("name","code", "date",)
    list_filter = ("name","code",  "date",)
    search_fields = ("name","code", )
    # prepopulated_fields = {'slug': ('title',)}
admin.site.register(City,CityAdmin)

# admin.site.register(SiteInformation)



# class SearchWordAdmin(admin.ModelAdmin):
#     list_display = ("search_key", 'result_count','user_ip', "date",)
#     list_filter = ("search_key", 'result_count','user_ip', "date",)
#     search_fields = ("search_key",'user_ip', 'result_count',)
# admin.site.register(SearchWord,SearchWordAdmin)

admin.site.register(Social)

class CurrencyAdmin(admin.ModelAdmin):
    list_display = ("currency","code", "rate","difference","order_index","date",)
    list_filter = ("currency","code", "rate","difference","order_index","date",)
    search_fields = ("currency","code", "rate","difference","order_index",)
    # prepopulated_fields = {'slug': ('title',)}
admin.site.register(Currency,CurrencyAdmin)

class WeatherAdmin(admin.ModelAdmin):
    list_display = ("city","high", "low","icon_url","conditions","tz_short","date",)
    list_filter = ("city","high", "low","icon_url","conditions","tz_short","date",)
    search_fields = ("city","high", "low","icon_url","conditions","tz_short",)
    # prepopulated_fields = {'slug': ('title',)}
admin.site.register(Weather,WeatherAdmin)
# admin.site.register(Currency)