from django.conf.urls import url, patterns
from django.conf.urls.i18n import i18n_patterns

from .views import *


urlpatterns = patterns('',
	url(r'^$', index, name='index'),
	url(r'^all-news/$', all_news, name='all_news'),
	url(r'^pages/(?P<page_slug>[\w-]+)/$', pages, name='pages'),
	# url(r'^about/$', about, name='about'),
	# url(r'^contact/$', contact, name='contact'),
	# url(r'^contact/ajax/$', contact_ajax, name='contact_ajax'),
	# url(r'^services/$', services, name='services'),
    url(r'^user/subscriber/$',create_subscriber , name='create_subscriber'),

    # url(r'^contact/$', 'home.views.contact',name='contact'),
)