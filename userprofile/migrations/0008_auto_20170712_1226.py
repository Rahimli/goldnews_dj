# -*- coding: utf-8 -*-
# Generated by Django 1.9.7 on 2017-07-12 08:26
from __future__ import unicode_literals

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('userprofile', '0007_auto_20170425_0809'),
    ]

    operations = [
        migrations.AlterField(
            model_name='userprofile',
            name='key_expires',
            field=models.DateTimeField(blank=True, default=datetime.datetime(2017, 7, 12, 8, 26, 5, 748824, tzinfo=utc), null=True),
        ),
        migrations.AlterField(
            model_name='userprofile',
            name='reset_key_expires',
            field=models.DateTimeField(blank=True, default=datetime.datetime(2017, 7, 12, 8, 26, 5, 748824, tzinfo=utc), null=True),
        ),
    ]
