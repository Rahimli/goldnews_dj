from ckeditor_uploader.fields import RichTextUploadingField

# from .models import *
from content.models import *
from userprofile.models import *
from ckeditor_uploader.widgets import CKEditorUploadingWidget
from django import forms
def get_categorys_choices():
    categorys = Category.objects.all()
    return categorys

# TEST_CHOICES =

my_default_errors = {
    'required': 'This field is required',
    'invalid': 'Enter a valid value'
}
# NEWSTYPE_CHOICES = (
#       ('', "News Type"),
#       ('standart_news', "Standart News"),
#       ('video_news', "Video News"),
#       ('gallery_news', "Gellery News"),
#      )
NEWSTYPE_CHOICES = (
    ('nn', "Standart News"),
    ('vn', "Video News"),
    ('gn', "Gellery News"),
     )
class NewsForm(forms.Form):

    categorys = forms.MultipleChoiceField(choices=[], required=True)
    is_active = forms.BooleanField(required=False)
    image = forms.ImageField(required=True)
    first_name = forms.CharField(max_length=255,required=False,error_messages=my_default_errors)
    second_name = forms.CharField(max_length=255,required=False,error_messages=my_default_errors)
    # is_active = forms.BooleanField(label='is Publish')
    show_slider = forms.BooleanField(required=False)
    short_text = forms.CharField(max_length=255,required=False)
    full_text = forms.CharField(widget=CKEditorUploadingWidget(),required=False)
    meta_keywords = forms.CharField(max_length=100,required=False)
    meta_description = forms.CharField(widget=forms.Textarea,required=False)
    tags = forms.CharField(max_length=255, required=False)
    # identified_data = forms.CharField(max_length=255,widget=forms.HiddenInput())
    def __init__(self,*args, **kwargs):
        super(NewsForm, self).__init__(*args, **kwargs)
        # self.fields['categorys'].choices = [(x.pk, x.name) for x in Category.objects.all()]
        # userprofile = UserProfile.objects.filter(has_profile_page=True)
        categorys_obj = Category.objects.all()
        # categorys = self.categorys
        # print(self.fields['categorys'])
        # if categorys_list:
        #     categorys_obj.filter(id__in=categorys_list)
            # for category in categorys_list:
            #     categorys.filter(id__in=)
        # identified = self.fields['identified']
        # news_obj = News.objects.filter(unique_name=str(identified).strip())
        # self.fields['sponsors'].choices = [(x.pk, x.prof_sponsor_name) for x in userprofile]
        self.fields['categorys'].choices = [[x.id, x.name] for x in categorys_obj]
    # def clean(self):
    #     cleaned_data = self.cleaned_data
    #     unique_name = cleaned_data.get('unique_name')
    #     identified = cleaned_data.get('identified_data')
    #     # print(identified)
    #     if News.objects.filter(unique_name=unique_name).exists() and identified:
    #         self._errors['unique_name'] = unique_name +' '+ ' allready exists'
    #
    #     return cleaned_data

class NewsUpdateForm(forms.Form):

    categorys = forms.MultipleChoiceField(choices=[], required=True)
    is_active = forms.BooleanField(required=False)
    image = forms.ImageField(required=True)
    first_name = forms.CharField(max_length=255,required=False,error_messages=my_default_errors)
    second_name = forms.CharField(max_length=255,required=False,error_messages=my_default_errors)
    # is_active = forms.BooleanField(label='is Publish')
    show_slider = forms.BooleanField(required=False)
    short_text = forms.CharField(max_length=255,required=False)
    full_text = forms.CharField(widget=CKEditorUploadingWidget(),required=True)
    meta_keywords = forms.CharField(max_length=255,required=False)
    meta_description = forms.CharField(widget=forms.Textarea,required=False)
    tags = forms.CharField(max_length=255, required=False)
    # identified_data = forms.CharField(max_length=255,widget=forms.HiddenInput())
    def __init__(self,*args, **kwargs):
        super(NewsUpdateForm, self).__init__(*args, **kwargs)
        # self.fields['categorys'].choices = [(x.pk, x.name) for x in Category.objects.all()]
        categorys_obj = Category.objects.all()
        # categorys = self.categorys
        # print(self.fields['categorys'])
        # if categorys_list:
        #     categorys_obj.filter(id__in=categorys_list)
            # for category in categorys_list:
            #     categorys.filter(id__in=)
        # identified = self.fields['identified']
        # news_obj = News.objects.filter(unique_name=str(identified).strip())
        self.fields['categorys'].choices = [[x.id, x.name] for x in categorys_obj]
    def clean(self):
        cleaned_data = self.cleaned_data
        # unique_name = cleaned_data.get('unique_name')
        # identified = cleaned_data.get('identified_data')
        # print(identified)
        # if News.objects.filter(unique_name=unique_name).exists() and identified:
        #     self._errors['unique_name'] = unique_name +' '+ ' allready exists'

        return cleaned_data


Multimedia_CHOICES = (
    ('video_news', "Video News"),
    ('gallery_news', "Gellery News"),
)

class MultimediaForm(forms.Form):

    is_active = forms.BooleanField(required=False)
    image = forms.ImageField(required=True)
    first_name = forms.CharField(max_length=255,required=False,error_messages=my_default_errors)
    second_name = forms.CharField(max_length=255,required=False,error_messages=my_default_errors)
    type = forms.ChoiceField(choices=Multimedia_CHOICES, required=True)
    # show_slider = forms.BooleanField(label='Unique Name')
    short_text = forms.CharField(max_length=255,required=False)
    meta_keywords = forms.CharField(max_length=255,required=False)
    meta_description = forms.CharField(widget=forms.Textarea,required=False)
    # identified_data = forms.CharField(max_length=255,widget=forms.HiddenInput())
    # def __init__(self,*args, **kwargs):
    #     super(MultimediaForm, self).__init__(*args, **kwargs)
    #     # self.fields['categorys'].choices = [(x.pk, x.name) for x in Category.objects.all()]
    #     # userprofile = UserProfile.objects.filter(has_profile_page=True)
    #     categorys_obj = Category.objects.all()
    #     # categorys = self.categorys
    #     # print(self.fields['categorys'])
    #     # if categorys_list:
    #     #     categorys_obj.filter(id__in=categorys_list)
    #         # for category in categorys_list:
    #         #     categorys.filter(id__in=)
    #     # identified = self.fields['identified']
    #     # news_obj = News.objects.filter(unique_name=str(identified).strip())
    #     # self.fields['sponsors'].choices = [(x.pk, x.prof_sponsor_name) for x in userprofile]
    #     self.fields['categorys'].choices = [[x.id, x.name] for x in categorys_obj]
    # def clean(self):
    #     cleaned_data = self.cleaned_data
    #     unique_name = cleaned_data.get('unique_name')
    #     identified = cleaned_data.get('identified_data')
    #     # print(identified)
    #     if News.objects.filter(unique_name=unique_name).exists() and identified:
    #         self._errors['unique_name'] = unique_name +' '+ ' allready exists'
    #
    #     return cleaned_data