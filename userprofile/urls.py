from django.conf.urls import url, patterns

from .views import *


urlpatterns = patterns('',
    url(r'^logout/$', logout_page,name='logout'),
    url(r'^login/user/$', login_user,name='login_user'),
    url(r'^profile/$', myprofile, name='myprofile'),
    url(r'^profile/(?P<user_name>\w+)/$', profile, name='profile'),
    url(r'^profile/search/news/$', search, name='u_search'),
    url(r'^profile/multimedia/list/(?P<t_slug>\w+)/(?P<p_slug>\w+)/$', listmultimedia, name='listmultimedia'),
    url(r'^multimedia/(?P<v_id>[\w-]+)/(?P<v_slug>[\w-]+)/preview/$', preview_multimedia,name='preview_multimedia'),
    url(r'^profile/news/list/(?P<p_slug>\w+)/$', listnews, name='listnews'),
    url(r'^news/(?P<a_id>[\w-]+)/(?P<a_slug>[\w-]+)/preview/$', preview_news, name='preview_news'),
    url(r'^profile/news/add/$', createnews, name='createnews'),
    url(r'^profile/news/(?P<n_id>[\w-]+)/edit/$', editnews, name='editnews'),
    url(r'^profile/multimedia/(?P<m_id>[\w-]+)/edit/$', editmultimedia, name='editmultimedia'),
    url(r'^profile/news/(?P<n_id>[\w-]+)/delete/$', deletenews, name='deletenews'),
    # url(r'^profile/news//$', deletenews, name='deletenews'),
    # url(r'^catalogs/(?P<c_slug>[\w-]+)/$', catalog_category, name='catalog_category'),
    # url(r'^catalog/detail/(?P<b_id>[\w-]+)/$', book_detail, name='book_detail'),
    url(r'^settings/change-profiles-info/$', profiles_info_change, name='profiles_info'),
    url(r'^settings/change-profiles-ajax/$', change_profile_ajax, name='change_profile_ajax'),
    url(r'^settings/reset-password/$', change_password, name='change_password'),
    # reset Password
    url(r'^reset/password/$', reset_password, name='reset_password'),
    url(r'^reset/password/ajax1/$', forgotPassword_ajax1, name='forgotPassword_ajax1'),
    url(r'^change/password/ajax1/$', changePassword_ajax, name='changePassword_ajax'),
    url(r'^reset/password/change/(?P<user_name>\w+)/(?P<activation_key>[\w-]+)/$', forgotPassword_changepassword, name='forgotPassword_changepassword'),

                       # url(r'^reset$', reset, name='reset'),
    # # Map the 'app.hello.reset_confirm' view that wraps around built-in password
    # # reset confirmation view, to the password reset confirmation links.
    # url(r'^reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
    #     reset_confirm, name='password_reset_confirm'),
    #
    # # Map the 'app.hello.success' view to the success message page.
    # url(r'^success/$', success, name='success'),
)
