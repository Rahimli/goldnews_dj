import datetime
import uuid
from django.utils import timezone

# now = timezone.now()
from django.db import models
from ckeditor_uploader.fields import RichTextUploadingField
from content.models import *
# Create your models here.
from eblog import settings

def profile_image_upload_location(instance,filename):
    return "profile/photo/%s/%s" %(instance.user.username,filename)

def profile_cover_upload_location(instance,filename):
    return "profile/cover/%s/%s" %(instance.user.username,filename)


class UserProfile(models.Model):
    user = models.OneToOneField(settings.AUTH_USER_MODEL)
    has_profile_page = models.BooleanField(default=False)
    link = models.URLField(blank=True,null=True)
    prof_img = models.ImageField(max_length=500,upload_to=profile_image_upload_location,null=True,blank=True)
    prof_cover = models.ImageField(max_length=500,upload_to=profile_cover_upload_location,null=True,blank=True)
    prof_facebook = models.ImageField(max_length=500,upload_to=profile_cover_upload_location,null=True,blank=True)
    prof_sponsor_name = models.CharField(max_length=50,blank=True,null=True)
    facebook = models.CharField(max_length=250,blank=True,null=True)
    twitter = models.CharField(max_length=250,blank=True,null=True)
    activation_key = models.CharField(max_length=40, blank=True)
    reset_token_key = models.CharField(max_length=40, blank=True)
    key_expires = models.DateTimeField(default=timezone.now(),blank=True,null=True)
    reset_key_expires = models.DateTimeField(default=timezone.now(), blank=True, null=True)

    def __str__(self):
        return self.user.username
    class Meta:
        verbose_name_plural = "İstifaddəçi Profilləri"
        verbose_name = "İstifaddəçi Profili"