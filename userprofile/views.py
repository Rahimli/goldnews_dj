import re
from django.contrib.auth import logout, authenticate
import hashlib
import random

import datetime
from django.contrib.auth import logout
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.contrib.auth.hashers import make_password
from django.core.mail import send_mail
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.db.models import Q
from django.http import HttpResponseRedirect, JsonResponse, Http404, HttpResponse
from django.shortcuts import render, render_to_response, get_object_or_404
# Create your views here.
from django.template import RequestContext
# from example.app.pipeline import username
from django.utils.text import slugify
from django.utils.translation import ugettext as _

from content.models import *
from .forms import *
from .models import *
from django.contrib import auth

from eblog import settings
# Create your views here.
@login_required(login_url='index')
def logout_page(request):
    logout(request)
    return HttpResponseRedirect('/')

def login_user(request):
    logout(request)
    username = password = ''
    if request.method == 'POST':
        #POST goes here . is_ajax is must to capture ajax requests. Beginners pit.
        if request.is_ajax():
            username = request.POST['username']
            password = request.POST['password']
            if '@' in username:
                try:
                    user_email = settings.AUTH_USER_MODEL.objects.get(email=username)
                    user = auth.authenticate(username=user_email.username,
                                             password=password)
                except:
                    user = auth.authenticate(username=username,
                                             password=password)
            else:
                user = auth.authenticate(username=username,
                                         password=password)
            if user:
                if user.is_active:
                    auth.login(request, user)
                    message1 = ""
                    message_code = 0
                else:
                    message1 = "Xaiş edirik Hesabınızı aktivləşdirin !"
                    message_code = 1
            else:
                if '@' in username:
                    message1 = "Email  ve ya Parol sehvdir !"
                else:
                    message1 = "İstifadəçi Adı  ve ya Parol sehvdir !"

                message_code = 2
            data = {"message2":message1,"message_code":message_code}
            return JsonResponse(data)
        else:
            raise Http404
    else:
        raise Http404
@login_required(login_url='index')
def change_profile_ajax(request):
    user = request.user
    if request.method == 'POST' and request.is_ajax():
        form_data = request.POST
        first_name = form_data['first_name']
        last_name = form_data['last_name']
        password = form_data['password']
        core = authenticate(username=user.username,password=password)
        if core:
            user.first_name = first_name
            user.last_name = last_name
            user.save()
            message = 'success'
        else:
            message = 'invalid' + user.password
        data = {"message":message,}
        return JsonResponse(data)
    else:
        raise Http404

from django.contrib.auth.decorators import login_required


def profile(request,user_name):
    body_text = 'kp-category-1'
    user = get_object_or_404(User,username=user_name)
    user_profile = get_object_or_404(UserProfile,user=user)
    news_list = News.objects.filter(Q(author=user)|Q(sponsors=user_profile))
    # news_list = News.objects.filter(Q(author=user))
    paginator = Paginator(news_list, 32) # Show 25 contacts per page

    page = request.GET.get('page')
    try:
        newss = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        newss = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        newss = paginator.page(paginator.num_pages)
    # user = get_object_or_404(User,username=user.username,is_active=True)
    # profile = Profile.objects.get(user=user)
    # my_rate_films = Film_Rating.objects.filter(profile=profile)
    # my_news = Article.objects.filter(author=user).order_by('-create_date')

    context = {
        'user_profile':user_profile,
        'body_text':body_text,
        'newss':newss,
        # 'last_news':last_news
    }
    return render(request, 'userprofile/profileview.html', context=context)
    # return render_to_response('userprofile/profileview.html', context, context_instance=RequestContext(request))

@login_required(login_url='index')
def myprofile(request):
    user = request.user
    user_profile = UserProfile.objects.get(user=user)
    # user = get_object_or_404(User,username=user.username,is_active=True)
    # profile = Profile.objects.get(user=user)
    # my_rate_films = Film_Rating.objects.filter(profile=profile)
    # my_news = Article.objects.filter(author=user).order_by('-create_date')

    context = {
        'user_profile':user_profile,
        # 'last_news':last_news
    }

    return render(request, 'userprofile/index.html', context=context)

# ************************************************ News Manuplate ***********************************************************


@login_required(login_url='index')
def listnews(request,p_slug):
    user = request.user
    if user.is_superuser:
        create_messages = request.GET.get("create-messages")
        user_profile = UserProfile.objects.get(user=user)
        # news_list = News.objects.filter()
        if p_slug == 'all':
            news_list = News.objects.all().order_by('-date')
        elif p_slug == 'unpublished':
            news_list = News.objects.filter(is_active=False).order_by('-date')
        elif p_slug == 'published':
            news_list = News.objects.filter(is_active=True).order_by('-date')
        else:
            raise Http404

        paginator = Paginator(news_list, 25) # Show 25 contacts per page

        page = request.GET.get('page')
        try:
            newss = paginator.page(page)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            newss = paginator.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of results.
            newss = paginator.page(paginator.num_pages)
        context = {
            'newss':newss,
            'create_messages':create_messages
        }
        return render(request, 'userprofile/news_list.html', context=context)
    else:
        raise Http404
@login_required(login_url='index')
def createnews(request):
    user = request.user
    user_profile = get_object_or_404(UserProfile,user=user)
    next_url = request.GET.get('next_url')
    form = NewsForm(request.POST or None, request.FILES or None)
    context = {
        'form':form,
        'next_url':next_url,
    }
    if request.method == 'POST':
        if form.is_valid() and request.user.is_authenticated():
            clean_data = form.cleaned_data
            next_url = request.GET.get('next_url')
            # instance = form.save(commit=False)

            categorys = clean_data.get('categorys')
            o_categorys = Category.objects.filter(id__in=categorys)
            # return HttpResponse(o_categorys)
            # return HttpResponse(form.cleaned_data.get('categorys'))
            is_active = clean_data.get('is_active')
            show_slider = clean_data.get('show_slider')
            image = clean_data.get('image')
            first_name = clean_data.get('first_name')
            second_name = clean_data.get('second_name')
            short_text = clean_data.get('short_text')
            full_text = clean_data.get('full_text')
            meta_keywords = clean_data.get('meta_keywords')
            meta_description = clean_data.get('meta_description')
            tags_text = clean_data.get('tags')
            slug = slugify(first_name+'-'+second_name)
            tags_objs = Tag.objects.filter(id=0)
            if first_name != '' or second_name != '':
                pass
            else:
                form.add_error('first_name','ya prefix daxil edin')
                form.add_error('second_name','ya da suffix daxil edin')
                return render(request, 'userprofile/news_create_edit.html', context=context)
            if show_slider:
                show_slider = True
            else:
                show_slider = False
            if is_active:
                is_active = True
            else:
                is_active = False
            if str(tags_text).lower().strip():
                tags = str(tags_text).split(",")
                for tag in tags:
                    tag_o = Tag.objects.filter(slug=slugify(str(tag).lower().strip()))
                    if not tag_o and slugify(tag):
                        tag_c = Tag(title=tag,slug=slugify(str(tag).lower()))
                        tag_c.save()
                    else:
                        pass
                tags_objs = Tag.objects.filter(title__in=tags)
            news_c = News(
                        is_active=is_active,
                        author=user,
                        image=image,
                        # type='standart_news',
                        first_name=first_name,
                        second_name=second_name,
                        slug=slug[:50],
                        show_slider=show_slider,
                        short_text=short_text,
                        full_text=full_text,
                        meta_keywords=meta_keywords,
                        meta_description=meta_description,
                        publish_start_date=datetime.datetime.now(),
                        read_count=0,
                        # tags=tags,
                    )
            news_c.save()
            for o_category in o_categorys:
                news_c.categorys.add(o_category)
            # for o_sponsor in o_sponsors:
            #     news_c.sponsors.add(o_sponsor)
            if tags_objs:
                for tags_obj in tags_objs:
                    news_c.tags.add(tags_obj)
            news_c.save()
            next_url = request.GET.get('next_url')
            if not next_url:
                next_url = '/az/profile/news/list/all/'
            return HttpResponseRedirect(next_url+'?create-messages=' + 'Succesfuly Added')

    return render_to_response('userprofile/news_create_edit.html', context, context_instance=RequestContext(request))

@login_required(login_url='index')
def editnews(request,n_id):
    # return HttpResponse()
    user = request.user
    if not user.is_superuser:
        raise Http404
    # sponsors = UserProfile.objects.filter(has_profile_page=True)
    next_url = request.GET.get('next_url')
    news_c = get_object_or_404(News,id=n_id)
    tags_str = ''
    for news_c_tag in news_c.tags.all():
        tags_str += news_c_tag.slug + ','
    categorys_list = []
    for news_c_category in news_c.categorys.all():
        categorys_list.append(news_c_category.id)
    # return HttpResponse(categorys_list)
    # form = MyForm(request.POST or None, instance=instance)
    form = NewsUpdateForm(request.POST or None, request.FILES or None,
                    initial={
                             'categorys':categorys_list,
                             'is_active':news_c.is_active,
                             'show_slider':news_c.show_slider,
                             'image':news_c.image.url,
                             'first_name':news_c.first_name,
                             'second_name':news_c.second_name,
                             'short_text':news_c.short_text,
                             'full_text':news_c.full_text,
                             'meta_keywords':news_c.meta_keywords,
                             'meta_description':news_c.meta_description,
                             'tags':tags_str,
                             },
                    )
    context = {
        'form':form,
        'news_c':news_c,
        'next_url':next_url,
    }
    if request.method == 'POST':
        if form.is_valid() and request.user.is_authenticated():
            clean_data = form.cleaned_data
            # instance = form.save(commit=False)

            # identi = clean_data.get('identified')
            # return HttpResponse(identi)
            next_url = request.GET.get('next_url')
            categorys = clean_data.get('categorys')
            o_categorys = Category.objects.filter(id__in=categorys)
            # return HttpResponse(o_categorys)
            # return HttpResponse(form.cleaned_data.get('categorys'))
            is_active = clean_data.get('is_active')
            show_slider = clean_data.get('show_slider')
            image = clean_data.get('image')
            first_name = clean_data.get('first_name')
            second_name = clean_data.get('second_name')
            short_text = clean_data.get('short_text')
            full_text = clean_data.get('full_text')
            meta_keywords = clean_data.get('meta_keywords')
            meta_description = clean_data.get('meta_description')
            if first_name != '' or second_name != '':
                pass
            else:
                form.add_error('first_name','ya prefix daxil edin')
                form.add_error('second_name','ya da suffix daxil edin')

                return render(request, 'userprofile/news_create_edit.html', context=context)
            # newss = News.objects.filter(unique_name=unique_name).exclude(id=news_c.id)
            # if newss:
            #     form.add_error('unique_name',unique_name + ' allready exists')
            #     return render_to_response('userprofile/news_create_edit.html', context,
            #                               context_instance=RequestContext(request))

            tags_text = clean_data.get('tags')
            slug = slugify(first_name + '-' + second_name)
            tags_objs = Tag.objects.filter(id=0)
            if str(tags_text).lower().strip():
                tags = str(tags_text).split(",")
                for tag in tags:
                    tag_o = Tag.objects.filter(slug=slugify(str(tag).lower().strip()))
                    if not tag_o and slugify(tag):
                        tag_c = Tag(title=tag,slug=slugify(str(tag).lower()))
                        tag_c.save()
                    else:
                        pass
                tags_objs = Tag.objects.filter(title__in=tags)
            if show_slider:
                show_slider = True
            else:
                show_slider = False
            if is_active:
                is_active = True
            else:
                is_active = False
            # if not is_active:
            #     is_active = False
            # return HttpResponse(is_active)
            news_c.is_active=is_active
            news_c.show_slider=show_slider
            news_c.updated_by_user=user
            news_c.image = image
            news_c.first_name = first_name
            news_c.second_name = second_name
            news_c.short_text = short_text
            news_c.full_text = full_text
            news_c.meta_description = meta_description
            news_c.meta_keywords = meta_keywords
            news_c.slug = slug[:50]
            # news_c.slug = slug
            # news_c.categorys.all().delete()
            # news_c.sponsors.all().delete()
            # news_c.tags.remove(news_c.tags.all())
            for news_c_category in news_c.categorys.all():
                news_c.categorys.remove(news_c_category)
            for o_category in o_categorys:
                news_c.categorys.add(o_category)
            # for o_sponsor in o_sponsors:
            #     news_c.sponsors.add(o_sponsor)
            if tags_objs:
                for tags_obj in tags_objs:
                    news_c.tags.add(tags_obj)
            news_c.save()
            return HttpResponseRedirect(next_url)

            # else:
        #     raise Http404
    # context = {
    #     'form':form,
    #     'news_c':news_c,
    # }

    return render(request, 'userprofile/news_create_edit.html', context=context)


@login_required(login_url='index')
def deletenews(request,n_id):
    user = request.user
    if not user.is_superuser:
        raise Http404
    news_c = get_object_or_404(News,id=n_id)
    news_c.delete()
    next_url = request.GET.get('next_url')
    if not next_url:
        next_url = '/az/profile/news/list/all/'
    return  HttpResponseRedirect(next_url)



def preview_news(request, a_id=None, a_slug=None):
    # category = get_object_or_404(Category,slug=c_slug)
    user = request.user
    if not user.is_superuser:
        raise Http404
    now = timezone.now()
    # category = get_object_or_404(Category,slug=c_slug)
    news = get_object_or_404(News, id=str(a_id))
    related_topics = News.objects.filter(categorys=news.categorys.all()).exclude(id=a_id).filter(
        publish_start_date__lte=now)
    work_deatil_path = True
    # now = timezone.now()
    # return HttpResponse(now.day  - news.create_date.day)
    news_page = True

    context = {
        # 'category':category,
        'news': news,
        'work_deatil_path': work_deatil_path,
        'related_topics': related_topics,
        'news_page': news_page,
    }

    return render(request, 'content/news.html', context=context)


# ****************************************************************************************************************
def listmultimedia(request,t_slug,p_slug):
    user = request.user
    if user.is_superuser:
        create_messages = request.GET.get("create-messages")
        user_profile = UserProfile.objects.get(user=user)
        # news_list = News.objects.filter()
        if t_slug == 'video':
            type = 'video_news'
            title2 = _('video_news')
        elif t_slug == 'gallery':
            type = 'gallery_news'
            title2 = _('gallery_news')
        else:
            raise Http404
        if p_slug == 'all':
            multimedia_list = Multimedia.objects.filter(type=type)
            title1 = _('all')
        elif p_slug == 'unpublished':
            multimedia_list = Multimedia.objects.filter(is_active=False).filter(type=type)
            title1 = _('unpublished')
        elif p_slug == 'published':
            multimedia_list = Multimedia.objects.filter(is_active=True).filter(type=type)
            title1 = _('published')
        else:
            raise Http404

        paginator = Paginator(multimedia_list, 25) # Show 25 contacts per page

        page = request.GET.get('page')
        try:
            multimedias = paginator.page(page)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            multimedias = paginator.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of results.
            multimedias = paginator.page(paginator.num_pages)
        context = {
            'multimedias':multimedias,
            'create_messages':create_messages,
            'title1':title1,
            'title2':title2,
            'type':t_slug,
            'p_slug':p_slug,
        }

        return render(request, 'userprofile/multimedia_list.html', context=context)
    else:
        raise Http404



@login_required(login_url='index')
def editmultimedia(request,m_id):
    user = request.user
    if user.is_superuser:

        # sponsors = UserProfile.objects.filter(has_profile_page=True)
        next_url = request.GET.get('next_url')
        multimedia = get_object_or_404(Multimedia,id=m_id)
        form = MultimediaForm(request.POST or None, request.FILES or None,
                        initial={
                                 'is_active':multimedia.is_active,
                                 'image':multimedia.image.url,
                                 'first_name':multimedia.first_name,
                                 'second_name':multimedia.second_name,
                                 'type':multimedia.type,
                                 'short_text':multimedia.short_text,
                                 'meta_keywords':multimedia.meta_keywords,
                                 'meta_description':multimedia.meta_description,
                                 },
                        )
        context = {
            'form':form,
            'multimedia':multimedia,
            'next_url':next_url,
        }
        if request.method == 'POST':
            if form.is_valid() and request.user.is_authenticated():
                clean_data = form.cleaned_data
                next_url = request.GET.get('next_url')
                is_active = clean_data.get('is_active')
                image = clean_data.get('image')
                first_name = clean_data.get('first_name')
                second_name = clean_data.get('second_name')
                short_text = clean_data.get('short_text')
                meta_keywords = clean_data.get('meta_keywords')
                meta_description = clean_data.get('meta_description')
                if first_name !='' or second_name !='':
                    pass
                else:
                    form.add_error('first_name','ya prefix daxil edin')
                    form.add_error('second_name','ya da suffix daxil edin')
                    return render_to_response('userprofile/news_create_edit.html', context,
                                              context_instance=RequestContext(request))
                # newss = News.objects.filter(unique_name=unique_name).exclude(id=news_c.id)
                # if newss:
                #     form.add_error('unique_name',unique_name + ' allready exists')
                #     return render_to_response('userprofile/news_create_edit.html', context,
                #                               context_instance=RequestContext(request))

                slug = slugify(first_name + '-' + second_name)
                tags_objs = Tag.objects.filter(id=0)
                if not is_active:
                    is_active = False
                # return HttpResponse(is_active)
                multimedia.is_active=is_active
                multimedia.updated_by_user=user
                multimedia.image = image
                multimedia.first_name = first_name
                multimedia.second_name = second_name
                multimedia.short_text = short_text
                multimedia.meta_description = meta_description
                multimedia.meta_keywords = meta_keywords
                multimedia.slug = slug[:50]
                multimedia.save()
                return HttpResponseRedirect(next_url)
            # return HttpResponse()
                # else:
            #     raise Http404
        # context = {
        #     'form':form,
        #     'news_c':news_c,
        # }

        return render(request, 'userprofile/multimedia_edit.html', context=context)
    else:
        raise Http404



@login_required(login_url='index')
def deletemultimedia(request,m_id):
    user = request.user
    if not user.is_superuser:
        raise Http404
    next_url = request.GET.get('next_url')
    if not next_url:
        next_url = '/az/profile/multimedia/list/video/all/'
    # sponsors = UserProfile.objects.filter(has_profile_page=True)
    multimedia = get_object_or_404(Multimedia,id=m_id)
    multimedia.delete()

    return  HttpResponseRedirect(next_url)


def preview_multimedia(request, v_id=None, v_slug=None):
    # category = get_object_or_404(Category,slug=c_slug)
    user = request.user
    if not user.is_superuser:
        raise Http404
    now = timezone.now()
    multimedia = get_object_or_404(Multimedia, id=str(v_id), slug=v_slug)
    if multimedia.type == 'video_news':
        template = 'content/video.html'
        context = {
            'video': multimedia,
        }
    elif multimedia.type == 'gallery_news':
        context = {
            'gallery': multimedia,
        }
        template = 'content/gallery.html'
    else:
        raise Http404

    return render(request, template, context=context)


# @login_required(login_url='index')
# def editnews(request,n_id):
#     user = request.user
#     if user.is_superuser:
#
#         # sponsors = UserProfile.objects.filter(has_profile_page=True)
#         next_url = request.GET.get('next_url')
#         news_c = get_object_or_404(News, id=n_id)
#         tags_str = ''
#         for news_c_tag in news_c.tags.all():
#             tags_str += news_c_tag.slug + ','
#         categorys_list = []
#         for news_c_category in news_c.categorys.all():
#             categorys_list.append(news_c_category.id)
#         # form = MyForm(request.POST or None, instance=instance)
#         form = NewsUpdateForm(request.POST or None, request.FILES or None,
#                         initial={
#
#                                  'is_active':news_c.is_active,
#                                  'image':news_c.image.url,
#                                  'first_name':news_c.first_name,
#                                  'second_name':news_c.second_name,
#                                  'short_text':news_c.short_text,
#                                  'full_text':news_c.full_text,
#                                  'meta_keywords':news_c.meta_keywords,
#                                  'meta_description':news_c.meta_description,
#                                  'tags':tags_str,
#                                  },
#                         )
#         context = {
#             'form':form,
#             'news_c':news_c,
#             'next_url':next_url,
#         }
#         if request.method == 'POST':
#             if form.is_valid() and request.user.is_authenticated():
#                 clean_data = form.cleaned_data
#                 # instance = form.save(commit=False)
#
#                 # identi = clean_data.get('identified')
#                 # return HttpResponse(identi)
#                 next_url = request.GET.get('next_url')
#                 categorys = clean_data.get('categorys')
#                 o_categorys = Category.objects.filter(id__in=categorys)
#                 # return HttpResponse(o_categorys)
#                 # return HttpResponse(form.cleaned_data.get('categorys'))
#                 is_active = clean_data.get('is_active')
#                 image = clean_data.get('image')
#                 first_name = clean_data.get('first_name')
#                 second_name = clean_data.get('second_name')
#                 short_text = clean_data.get('short_text')
#                 full_text = clean_data.get('full_text')
#                 meta_keywords = clean_data.get('meta_keywords')
#                 meta_description = clean_data.get('meta_description')
#                 # newss = News.objects.filter(unique_name=unique_name).exclude(id=news_c.id)
#                 # if newss:
#                 #     form.add_error('unique_name',unique_name + ' allready exists')
#                 #     return render_to_response('userprofile/news_create_edit.html', context,
#                 #                               context_instance=RequestContext(request))
#
#                 tags_text = clean_data.get('tags')
#                 slug = slugify(first_name,second_name)
#                 tags_objs = Tag.objects.filter(id=0)
#                 if str(tags_text).lower().strip():
#                     tags = str(tags_text).split(",")
#                     for tag in tags:
#                         tag_o = Tag.objects.filter(slug=slugify(str(tag).lower().strip()))
#                         if not tag_o and slugify(tag):
#                             tag_c = Tag(title=tag,slug=slugify(str(tag).lower()))
#                             tag_c.save()
#                         else:
#                             pass
#                     tags_objs = Tag.objects.filter(title__in=tags)
#                 if not is_active:
#                     is_active = False
#                 # return HttpResponse(is_active)
#                 news_c.is_active=is_active
#                 news_c.updated_by_user=user
#                 news_c.image = image
#                 news_c.first_name = first_name
#                 news_c.second_name = second_name
#                 news_c.short_text = short_text
#                 news_c.full_text = full_text
#                 news_c.meta_description = meta_description
#                 news_c.meta_keywords = meta_keywords
#                 news_c.slug = slug
#                 # news_c.categorys.all().delete()
#                 # news_c.sponsors.all().delete()
#                 # news_c.tags.remove(news_c.tags.all())
#                 for news_c_category in news_c.categorys.all():
#                     news_c.categorys.remove(news_c_category)
#                 for o_category in o_categorys:
#                     news_c.categorys.add(o_category)
#                 # for o_sponsor in o_sponsors:
#                 #     news_c.sponsors.add(o_sponsor)
#                 if tags_objs:
#                     for tags_obj in tags_objs:
#                         news_c.tags.add(tags_obj)
#                 news_c.save()
#                 return HttpResponseRedirect(next_url)
#
#                 # else:
#             #     raise Http404
#         # context = {
#         #     'form':form,
#         #     'news_c':news_c,
#         # }
#         return render_to_response('userprofile/news_create_edit.html', context, context_instance=RequestContext(request))
#     else:
#         raise Http404



@login_required(login_url='index')
def change_password(request):
    # user = request.user
    # user = get_object_or_404(User,username=user.username,is_active=True)
    # profile = Profile.objects.get(user=user)
    # my_rate_films = Film_Rating.objects.filter(profile=profile)
    # my_news = Article.objects.filter(author=user).order_by('-create_date')

    context = {
        # 'my_newss':my_news,
        # 'new_films':new_films,
        # 'last_news':last_news
    }

    return render(request, 'userprofile/change_password.html', context=context)

@login_required(login_url='index')
def search(request):
    search = request.GET.get("search")
    if not search:

        context = {
            # 'search':search,
        }
        return render_to_response('userprofile/search.html',
                                  context,
                                  context_instance=RequestContext(request))
    # return HttpResponse('nese')
    user = request.user
    user_profile = UserProfile.objects.get(user=user)
    news_list = News.objects.filter(author=user).filter(Q(first_name__icontains=search) | Q(second_name__icontains=search) | Q(full_text__icontains=search))
    paginator = Paginator(news_list, 25) # Show 25 contacts per page

    page = request.GET.get('page')
    try:
        newss = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        newss = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        newss = paginator.page(paginator.num_pages)
    context = {
        'newss':newss,
        'search':search,
        # 'last_news':last_news
    }
    return render(request, 'userprofile/search.html', context=context)



@login_required(login_url='index')
def profiles_info_change(request):
    user = request.user
    user = get_object_or_404(User,username=user.username,is_active=True)
    # profile = Profile.objects.get(user=user)
    # my_rate_films = Film_Rating.objects.filter(profile=profile)
    # my_news = Article.objects.filter(author=user).order_by('-create_date')

    context = {
        # 'my_newss':my_news,
        # 'new_films':new_films,
        # 'last_news':last_news
    }

    return render(request, 'userprofile/profiles_info_change.html', context=context)




# def create_subscriber(request):
#     if request.method == 'POST' and request.is_ajax():
#         email = request.POST.get('email')
#         subscriber = Subscriber.objects.filter(email=email).first()
#         if subscriber is None:
#             s = Subscriber(email=email)
#             s.save()
#             message1 = "Abunəliyiniz Uğurla qeydə alındı"
#             res = 1
#             data = {"message1":message1,'res':res}
#         else:
#             message1 = "Siz Artiq Abunə olmusunuz"
#             res = 0
#             data = {"message1":message1,'res':res}
#         return JsonResponse(data)
#     return Http404



#**************************** RESET Password  &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
def reset_password(request):
    # categorys = Category.objects.filter(is_active=True).order_by('ord')
    # if request.user.is_authenticated():
    #         logout(request)
    context = {
    }
    return render_to_response('userprofile/forgot_password.html', context, context_instance=RequestContext(request))


@login_required(login_url='index')
def changePassword_ajax(request):
    if request.method == 'POST' and request.is_ajax():
        data_p = request.POST
        current_password = data_p.get('change-current-password')
        new_password = data_p.get('change-new-password')
        repeat_password = data_p.get('change-repeat-password')
        user = request.user
        core = authenticate(username=user.username,password=current_password)
        if core:
            if str(new_password).strip() == str(repeat_password).strip() and len(str(new_password).strip()) > 7:
                if re.match(r'[A-Za-z0-9@#$%^&+=]{8,}', str(new_password).strip()):
                    user.set_password(new_password)
                    res = 1
                    message = 'Succesfully Changed Password'
                else:
                    res = 0
                    message = 'Passwords Not Format Passwords'
            else:
                res = 0
                message = 'Passwords Not Format Passwords'
        else:
            res = 0
            message = 'Password is Incorrect' + current_password + ' - ' + user.password
        data = {"message":message,'res':res}
        return JsonResponse(data)
    else:
        raise Http404

def forgotPassword_ajax1(request):
    if request.user.is_authenticated():
        logout(request)

    if request.method == 'POST' and request.is_ajax():
        email = request.POST.get('reset-email')
        if '@' in email:
            try:
                user_email = User.objects.get(email=email)
            except:
                user_email = User.objects.get(username=email)
        else:
            try:
                user_email = User.objects.get(username=email)
            except:
                user_email = None
        if user_email:
            sendemail = 0
            random_string = str(random.random()).encode('utf8')

            salt = hashlib.sha1(random_string).hexdigest()[:5]

            salted = (salt + email).encode('utf8')

            activation_key = hashlib.sha1(salted).hexdigest()

            key_expires = datetime.datetime.today() + datetime.timedelta(1)

            #Get user by username
            # user=User.objects.get(username=username)

            # Create and save user profile
            try:
                userprofile = UserProfile.objects.get(user=user_email)
                userprofile.reset_token_key = activation_key
                userprofile.reset_key_expires = key_expires
                userprofile.save()
            except:
                new_profile = UserProfile(user=user_email, reset_token_key=activation_key,
                    reset_key_expires=key_expires)
                new_profile.save()

            # Send email with activation key
            from_mail = settings.EMAIL_HOST_USER
            to_mails = [user_email.email,]
            email_subject = 'Password Reset'
            email_body = "Hey %s, thanks for Reset Password. To Reset Password, click this link within \
            24hours <a href='http://127.0.0.1:8096/reset/password/change/%s/%s'>Reset</a>" % (user_email.username,user_email.username, activation_key)
            send_mail(
                email_subject,
                email_body,
                from_mail,
                to_mails,
                fail_silently=False,
                html_message=email_body,
            )
            message_code = 1
            message = 'Please confirm account with Email'
        else:
            message_code = 0
            message = 'Not confirmed'
        data = {'message_code':message_code,'message':message}
        return JsonResponse(data)
    else:
        raise Http404

def forgotPassword_changepassword(request, user_name,activation_key):
    #check if user is already logged in and if he is redirect him to some other url, e.g. home
    if request.user.is_authenticated():
        logout(request)


    try:
        user = User.objects.get(username=user_name)
    except:
        raise Http404
    user_profile = get_object_or_404(UserProfile,user=user, reset_token_key=activation_key)
    if user_profile.reset_key_expires > timezone.now():
        if request.method == 'POST':
            password = request.POST.get('new-password')
            password_repeat = request.POST.get('new-password-repeat')
            if str(password).strip() == str(password_repeat).strip() and len(str(password).strip()) > 7:
                if re.match(r'[A-Za-z0-9@#$%^&+=]{8,}', password):
                    user.set_password(password)
                    user.save()
                    user_profile.reset_token_key = ''
                    user_profile.save()
                    return HttpResponseRedirect('/')
                else:
                    message = 'Password Not Format'
            elif str(password).strip() != str(password_repeat).strip():
                message = 'Passwords not same '
            else:
                if len(str(password).strip()) > 7:
                    message = 'Password length not greater than 8'
                else:
                    message = 'Password Incorrect'
            context = {
                    'post_message':message,
                }
            return render_to_response('userprofile/forgot_change_password.html', context, context_instance=RequestContext(request))


            # return HttpResponse('post')
        return render_to_response('userprofile/forgot_change_password.html',context_instance=RequestContext(request))
    else:
        raise Http404

#**************************** RESET Password  &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
