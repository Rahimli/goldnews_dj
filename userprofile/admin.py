from django.contrib import admin
from .models import *
# Register your models here.
class UserProfileAdmin(admin.ModelAdmin):
    list_display = ["user","activation_key","key_expires"]
admin.site.register(UserProfile,UserProfileAdmin)
