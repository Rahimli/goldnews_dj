# -*- coding: utf-8 -*-
# Generated by Django 1.9.7 on 2017-04-09 09:16
from __future__ import unicode_literals

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('content', '0002_auto_20170409_0914'),
    ]

    operations = [
        migrations.AlterField(
            model_name='category',
            name='publish_start_date',
            field=models.DateTimeField(default=datetime.datetime(2017, 4, 9, 13, 16, 2, 957727), verbose_name='Yayımlanma Tarixi'),
        ),
        migrations.AlterField(
            model_name='multimedia',
            name='publish_start_date',
            field=models.DateTimeField(default=datetime.datetime(2017, 4, 9, 9, 16, 2, 988750, tzinfo=utc), verbose_name='Yayımlanma Tarixi'),
        ),
        migrations.AlterField(
            model_name='news',
            name='publish_start_date',
            field=models.DateTimeField(default=datetime.datetime(2017, 4, 9, 13, 16, 2, 959729), verbose_name='Yayımlanma Tarixi'),
        ),
        migrations.AlterField(
            model_name='newsview',
            name='date',
            field=models.DateTimeField(default=datetime.datetime(2017, 4, 9, 9, 16, 2, 963232, tzinfo=utc)),
        ),
    ]
