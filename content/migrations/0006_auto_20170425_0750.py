# -*- coding: utf-8 -*-
# Generated by Django 1.9.7 on 2017-04-25 03:50
from __future__ import unicode_literals

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('content', '0005_auto_20170425_0749'),
    ]

    operations = [
        migrations.AlterField(
            model_name='category',
            name='publish_start_date',
            field=models.DateTimeField(default=datetime.datetime(2017, 4, 25, 7, 50, 50, 545814), verbose_name='Yayımlanma Tarixi'),
        ),
        migrations.AlterField(
            model_name='multimedia',
            name='publish_start_date',
            field=models.DateTimeField(default=datetime.datetime(2017, 4, 25, 3, 50, 50, 570812, tzinfo=utc), verbose_name='Yayımlanma Tarixi'),
        ),
        migrations.AlterField(
            model_name='news',
            name='publish_start_date',
            field=models.DateTimeField(default=datetime.datetime(2017, 4, 25, 7, 50, 50, 547314), verbose_name='Yayımlanma Tarixi'),
        ),
        migrations.AlterField(
            model_name='newsview',
            name='date',
            field=models.DateTimeField(default=datetime.datetime(2017, 4, 25, 3, 50, 50, 550819, tzinfo=utc)),
        ),
    ]
