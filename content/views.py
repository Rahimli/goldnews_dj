import datetime

from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.db.models import Q
from django.http import Http404
from django.http import HttpResponse
from django.shortcuts import get_object_or_404, render_to_response, render

# Create your views here.
from django.template import RequestContext
from content.models import *
from django.utils import timezone

from home.models import *

# def videos(request):
#     return render_to_response('videos.html',
#                               {},
#                               context_instance=RequestContext(request))
# def video(request):
#     return render_to_response('video.html',
#                               {},
#                               context_instance=RequestContext(request))
def newsd(request):
    return render_to_response('newsa.html',
                              {},
                              context_instance=RequestContext(request))


def category(request, c_slug):
    now = timezone.now()
    daily_48 = now - timezone.timedelta(days=2)
    weekly = now - timezone.timedelta(days=7)
    monthly = now - timezone.timedelta(days=30)
    daily_48_list = []
    weekly_list = []
    monthly_list = []


    category = get_object_or_404(Category, slug=c_slug)
    newss_views = NewsView.objects.filter(news__categorys=category)
    base_daily_48_most_read_news_views = newss_views.filter(date__range=(daily_48, now)).values('news__id').annotate(
        count=Count('news__id')).order_by('-count')[:12]
    for base_daily_most_read_news in base_daily_48_most_read_news_views:
        daily_48_list.append(base_daily_most_read_news['news__id'])
    category_most_popular_newss = News.objects.filter(id__in=daily_48_list).filter(Q(categorys=category) | Q(categorys__parent=category)).filter(
        publish_start_date__lte=now).filter(is_active=True)[:10]
    news_list = News.objects.filter(Q(categorys=category) | Q(categorys__parent=category)).filter(publish_start_date__lte=now,is_active=True).order_by('-publish_start_date')
    most_read_news = News.objects.filter(Q(categorys=category) | Q(categorys__parent=category)).order_by(
        '-read_count').filter(publish_start_date__lte=now)
    paginator = Paginator(news_list, 24)  # Show 25 contacts per page

    page = request.GET.get('page')
    try:
        newss = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        newss = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        newss = paginator.page(paginator.num_pages)
    context = {
        'category': category,
        'category_most_popular_newss': category_most_popular_newss,
        'newss': newss,
        'most_read_news': most_read_news,
    }
    return render(request, 'content/category.html', context=context)


def allnewses(request):
    now = timezone.now()
    body_text = 'page-full-width'
    news_list = News.objects.filter(publish_start_date__lte=now).filter(is_active=True).order_by('-publish_start_date')
    paginator = Paginator(news_list, 66)  # Show 25 contacts per page

    page = request.GET.get('page')
    try:
        newss = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        newss = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        newss = paginator.page(paginator.num_pages)
    context = {
        'body_text': body_text,
        'newss': newss,
    }
    return render(request, 'content/all_newses.html', context=context)


def p_category(request, c_slug, pc_slug):
    category = get_object_or_404(Category, slug=c_slug)
    p_category = get_object_or_404(Category, slug=pc_slug, parent=category)
    most_read_news = News.objects.filter(categorys=category).order_by('-read_count')
    news_list = News.objects.filter(categorys=p_category)
    paginator = Paginator(news_list, 1)  # Show 25 contacts per page

    page = request.GET.get('page')
    try:
        newss = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        newss = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        newss = paginator.page(paginator.num_pages)
    context = {
        'category': category,
        'p_category': p_category,
        'most_read_news': most_read_news,
        'newss': newss
    }
    return render(request, 'content/p_category.html', context=context)


def news(request, a_id=None, a_slug=None):
    # category = get_object_or_404(Category,slug=c_slug)
    now = timezone.now()
    body_text = 'kp-single'
    # category = get_object_or_404(Category,slug=c_slug)
    news = get_object_or_404(News, id=str(a_id), slug=a_slug, publish_start_date__lte=now,is_active=True)
    # return HttpResponse(news.categorys.count())
    related_topics = News.objects.filter().exclude(id=a_id).filter(
        publish_start_date__lte=now).order_by('?')[:2]
    # if related_topics:
    #     return HttpResponse('he ')
    # else:
    #     return HttpResponse('yox ')
    work_deatil_path = True
    # now = timezone.now()
    # return HttpResponse(now.day  - news.create_date.day)
    news_page = True
    if not NewsView.objects.filter(news=news, session=request.session.session_key):
        if not request.session.exists(request.session.session_key):
            request.session.create()
        view = NewsView(
            news=news,
            ip=request.META['REMOTE_ADDR'],
            created=datetime.datetime.now(),
            session=request.session.session_key)
        # return HttpResponse(request.session.session_key)
        view.save()
        news.read_count += 1
        news.save()

    context = {
        # 'category':category,
        'body_text': body_text,
        'news': news,
        'work_deatil_path': work_deatil_path,
        'related_topics': related_topics,
        'news_page': news_page,
    }
    return render(request, 'content/news.html', context=context)


def search(request,contnet_slug):
    now = timezone.now()
    # tag = get_object_or_404(Tag,slug=t_slug)
    search = request.GET.get("search")
    if not search:

        context = {
            # 'search':search,
        }
        return render_to_response('content/search.html',
                                  context,
                                  context_instance=RequestContext(request))
    else:
        if contnet_slug == 'news':
            data_list = News.objects.filter(is_active=True).filter(publish_start_date__lte=now.today()).filter(
                Q(first_name__icontains=search) | Q(second_name__icontains=search)).filter(publish_start_date__lte=now)
            # return HttpResponse(data_list.count())
        elif contnet_slug == 'video':
            data_list = Multimedia.objects.all().filter(is_active=True).filter(publish_start_date__lte=now.today()).filter(
                Q(first_name__icontains=search) | Q(second_name__icontains=search)).filter(publish_start_date__lte=now)
        else:
            raise Http404
        data_count = data_list.count()
        search_obj = SearchWord(search_key=search, result_count=data_count, user_ip=request.META.get('REMOTE_ADDR'))
        search_obj.save()
        paginator = Paginator(data_list, 40)  # Show 25 contacts per page

        page = request.GET.get('page')
        try:
            datas = paginator.page(page)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            datas = paginator.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of results.
            datas = paginator.page(paginator.num_pages)
        # return HttpResponse(articles.count(articles))
        context = {
            'search': search,
            'datas': datas,
            'data_count': data_count,
        }
        return render(request, 'content/search.html', context=context)


def tag(request,contnet_slug, t_slug):
    now = timezone.now()
    body_text = 'kp-category-1'
    tag = Tag.objects.filter(slug=t_slug).first()
    if not tag:
        raise Http404
    if contnet_slug == 'news':
        data_list = News.objects.filter(tags=tag).filter(publish_start_date__lte=now)
    elif contnet_slug == 'video':
        data_list = Multimedia.objects.filter(type='video_news').filter(is_active=True).filter(tags=tag).filter(publish_start_date__lte=now)
    elif contnet_slug == 'gallery':
        data_list = Multimedia.objects.filter(type='gallery_news').filter(is_active=True).filter(tags=tag).filter(publish_start_date__lte=now)
    else:
        raise Http404
    paginator = Paginator(data_list, 15)  # Show 25 contacts per page

    page = request.GET.get('page')
    try:
        datas = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        datas = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        datas = paginator.page(paginator.num_pages)
    # return HttpResponse(body_text)
    context = {
        'tag': tag,
        'body_text': body_text,
        'datas': datas,
    }
    return render(request, 'content/tag.html', context=context)



def videos(request):
    now = timezone.now()
    data_list = Multimedia.objects.filter(type='video_news').filter(publish_start_date__lte=now).order_by('-publish_start_date')
    most_viewed_videos = Multimedia.objects.filter(type='video_news').filter(publish_start_date__lte=now).order_by('-read_count')
    slider_videos = Multimedia.objects.filter(type='video_news').filter(publish_start_date__lte=now).filter(show_slider=True).order_by('-publish_start_date')[:4]
    day_of_video = Multimedia.objects.filter(type='video_news').filter(publish_start_date__lte=now).order_by('-publish_start_date').filter(is_today=True).first()
    paginator = Paginator(data_list, 25)  # Show 25 contacts per page

    page = request.GET.get('page')
    try:
        datas = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        datas = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        datas = paginator.page(paginator.num_pages)
    # return HttpResponse(body_text)
    context = {
        'tag': tag,
        'videos': datas,
        'day_of_video': day_of_video,
        'most_viewed_videos': most_viewed_videos,
        'slider_videos': slider_videos,
    }
    return render(request, 'content/videos.html', context=context)


def galleries(request):
    now = timezone.now()
    data_list = Multimedia.objects.filter(type='gallery_news').filter(publish_start_date__lte=now).order_by('publish_start_date')
    most_viewed_galleries = Multimedia.objects.filter(type='gallery_news').filter(publish_start_date__lte=now).order_by('-read_count')
    slider_galleries = Multimedia.objects.filter(type='gallery_news').filter(publish_start_date__lte=now).filter(show_slider=True).order_by('-publish_start_date')[:4]
    day_of_gallery = Multimedia.objects.filter(type='gallery_news').filter(publish_start_date__lte=now).order_by('-publish_start_date').filter(is_today=True).first()
    paginator = Paginator(data_list, 25)  # Show 25 contacts per page

    page = request.GET.get('page')
    try:
        datas = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        datas = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        datas = paginator.page(paginator.num_pages)
    # return HttpResponse(body_text)
    context = {
        'tag': tag,
        'galleries': datas,
        'most_viewed_galleries': most_viewed_galleries,
        'slider_galleries': slider_galleries,
        'day_of_gallery': day_of_gallery,
    }
    return render(request, 'content/galleries.html', context=context)

def video(request, v_id=None, v_slug=None):
    # category = get_object_or_404(Category,slug=c_slug)
    now = timezone.now()
    video = get_object_or_404(Multimedia, id=str(v_id), slug=v_slug,type='video_news', publish_start_date__lte=now)
    # related_topics = News.objects.filter(categorys=news.categorys.all()).exclude(id=a_id).filter(
    #     publish_start_date__lte=now)
    # now = timezone.now()
    # return HttpResponse(now.day  - news.create_date.day)
    # news_page = True
    # if not NewsView.objects.filter(news=news, session=request.session.session_key):
    #     if not request.session.exists(request.session.session_key):
    #         request.session.create()
    #     view = NewsView(
    #         news=news,
    #         ip=request.META['REMOTE_ADDR'],
    #         created=datetime.datetime.now(),
    #         session=request.session.session_key)
    #     # return HttpResponse(request.session.session_key)
    #     view.save()
    #     news.read_count += 1
    #     news.save()

    context = {
        'video': video,
        # 'related_topics': related_topics,
        # 'news_page': news_page,
    }
    return render(request, 'content/video.html', context=context)


def gallery(request, g_id=None, g_slug=None):
    # category = get_object_or_404(Category,slug=c_slug)
    now = timezone.now()
    gallery = get_object_or_404(Multimedia, id=str(g_id), slug=g_slug,type='gallery_news', publish_start_date__lte=now)
    # related_topics = News.objects.filter(categorys=news.categorys.all()).exclude(id=a_id).filter(
    #     publish_start_date__lte=now)
    # now = timezone.now()
    # return HttpResponse(now.day  - news.create_date.day)
    # news_page = True
    # if not NewsView.objects.filter(news=news, session=request.session.session_key):
    #     if not request.session.exists(request.session.session_key):
    #         request.session.create()
    #     view = NewsView(
    #         news=news,
    #         ip=request.META['REMOTE_ADDR'],
    #         created=datetime.datetime.now(),
    #         session=request.session.session_key)
    #     # return HttpResponse(request.session.session_key)
    #     view.save()
    #     news.read_count += 1
    #     news.save()

    context = {
        'gallery': gallery,
        # 'related_topics': related_topics,
        # 'news_page': news_page,
    }
    return render(request, 'content/gallery.html', context=context)