from django.conf.urls import url, patterns
from django.conf.urls.i18n import i18n_patterns

from .views import *


urlpatterns = patterns('',
    url(r'^category/(?P<c_slug>[\w-]+)/view/$', category,name='category'),
    url(r'^galleries/$', galleries,name='galleries'),
    url(r'^galleries/(?P<g_id>[\w-]+)/(?P<g_slug>[\w-]+)/view/$', gallery, name='gallery'),
    url(r'^videos/$', videos,name='videos'),
    url(r'^videos/(?P<v_id>[\w-]+)/(?P<v_slug>[\w-]+)/view/$', video,name='video'),
    url(r'^newsd/$', newsd,name='newsd'),
    url(r'^video/$', video,name='video'),
    # url(r'^category/(?P<c_slug>[\w-]+)/(?P<pc_slug>[\w-]+)/$', p_category,name='p_category'),
    url(r'^news/(?P<a_id>[\w-]+)/(?P<a_slug>[\w-]+)/read/$', news,name='news'),
    url(r'^tag/(?P<contnet_slug>[\w-]+)/(?P<t_slug>[\w-]+)/$', tag ,name='tag'),
    url(r'^search/(?P<contnet_slug>[\w-]+)/$', search,name='search'),

    # url(r'^content/rate/$', rate_def,name='rate_def'),
)

