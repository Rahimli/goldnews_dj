import datetime
import uuid

from django.contrib.auth.models import User
from django.db import models
from ckeditor_uploader.fields import RichTextUploadingField
from django.core.exceptions import ValidationError
# Create your models here.
from django.db.models import Q
from django.utils import timezone
from django.core.exceptions import ValidationError

from django.utils.translation import ugettext as _
from eblog import settings
from django.db.models.aggregates import Count
from random import randint
# from django.utils import timezone

class Category(models.Model):
    parent = models.ForeignKey('self', null=True, blank=True,verbose_name="Üst Kateqoroiya")
    status = models.BooleanField(default=True,verbose_name="Yayımla")
    order_index = models.DecimalField(max_digits=19, decimal_places=10,verbose_name="Sırası")
    name = models.CharField(max_length=256, null=False,verbose_name="Adı")
    unique_name = models.CharField(max_length=256, unique=True, null=False,verbose_name="Unikal Adı")
    logo = models.ImageField(upload_to='category/logos',blank=True,null=True,verbose_name="Kateqoroiya Loqo")
    author = models.ForeignKey(settings.AUTH_USER_MODEL,related_name='+',editable=False,blank=True,null=True,verbose_name="Yazar")
    description = models.CharField(max_length=1024,blank=True,verbose_name="Təsvir")
    publish_start_date = models.DateTimeField(null=False,default=datetime.datetime.now(),verbose_name="Yayımlanma Tarixi")
    publish_end_date = models.DateTimeField(null=True,blank=True,verbose_name="Son Tarixi")
    news_background = models.CharField(max_length=255,blank=True,null=True,verbose_name="Kateqoroiya Fon rəngi")
    suffix_color = models.CharField(max_length=255,blank=True,null=True,verbose_name="Suffiks Rəngi")
    prefix_color = models.CharField(max_length=255,blank=True,null=True,verbose_name="Prefiks Rəngi")
    date = models.DateTimeField(auto_now_add=True,verbose_name="Tarix")
    news_count = models.PositiveIntegerField(default=0,editable=False,verbose_name="Xəbər Sayı")
    slug = models.SlugField(unique=True)
    class Meta:
        verbose_name_plural = "Kateqoriyalar"
        verbose_name = "Kateqoriya"
    # def clean(self):
    #
    #     if self.parent:
    #         pass
    #     else:
    #         if self.logo:
    #             pass
    #         else:
    #             raise ValidationError('please upload category logo')
    def __str__(self):
        if self.parent:
            data = '{} ----- {}'.format(self.name,self.parent.name)
        else:
            data = '{} -------------'.format(self.name)
        return data
    # def get_all_children(self):
    #     include_self = True
    #     r = []
    #     if include_self:
    #         r.append(self.id)
    #     for c in Category.objects.filter(parent=self):
    #         for k in c.get_all_children(include_self=True):
    #             r.append(k)
    #     categories = Category.objects.filter(id__in=r)
    #     return categories
    def get_categorys(self):
        categorys = Category.objects.filter(parent=self)
        return categorys

    def get_parent_category(self):
        # category = None
        parent = self.parent
        if not parent:
            parent = self
        else:
            while parent:
                parent = parent
                if not parent.parent:
                    break
                else:
                    parent = parent.parent

        return parent

    def get_recent_news(self):
        now = timezone.now()
        recent_news = News.objects.filter(Q(categorys=self) | Q(categorys__parent=self)).filter(publish_start_date__lte=now,is_active=True).order_by('-publish_start_date')[:10]
        return recent_news

# class RandomNewsManager(models.Manager):
#     def random(self):
#         count = self.aggregate(count=Count('id'))['count']
#         random_index = randint(0, count - 1)
#         return self.all()[random_index]

def news_upload_location(instance,filename):
    return "news/%s/%s" %(instance.slug,filename)

import os
from uuid import uuid4

def path_and_rename(path):
    def wrapper(instance, filename):
        try :
            ext = filename.split('.')[-1]
            ext = str(ext).lower()
        except:
            ext = filename.split('.')[-1]
        # get filename
        if instance.pk:
            filename = '{}.{}'.format(instance.pk, ext)
        else:
            # set filename as random string
            filename = '{}.{}'.format(uuid4().hex, ext)
        # return the whole path to the file
        return os.path.join(path, filename)
    return wrapper

class News(models.Model):
    NEWSTYPE_CHOICES = (
                  ('standart_news', "Standart News"),
                  ('video_news', "Video News"),
                  ('gallery_news', "Gallery News"),
                 )
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    categorys = models.ManyToManyField('Category',verbose_name="Kategoriyalar")
    is_active = models.BooleanField(default=False,editable=False,verbose_name="Yayımla")
    author = models.ForeignKey(settings.AUTH_USER_MODEL,related_name='+',editable=False,blank=True,null=True,verbose_name="Yazar")
    updated_by_user = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='+',editable=False,blank=True,null=True,verbose_name="Redaktə Edən")
    image = models.ImageField(max_length=500,upload_to=path_and_rename('news/'),blank=False,verbose_name="Şəkil")
    # news_type = models.CharField(choices=NEWSTYPE_CHOICES,default='standart_news',max_length=20)
    first_name = models.CharField(verbose_name='prefix',max_length=256, null=True,blank=True)
    second_name = models.CharField(verbose_name='suffix',max_length=256, null=True,blank=True)
    slug  = models.SlugField()
    show_slider = models.BooleanField(default=False,verbose_name="Sliderdə Olsun")
    short_text = models.CharField(max_length=256, null=True,blank=True,verbose_name="Qısa Tekst")
    full_text = RichTextUploadingField(config_name='awesome_ckeditor',verbose_name="Xəbər Mətni")
    multimedias = models.ManyToManyField('Multimedia',blank=True,verbose_name="Video və ya Foto xəbər")
    meta_keywords = models.CharField(max_length=10240, null=True,blank=True,verbose_name="Meta taq")
    meta_description = models.TextField(null=True,blank=True,verbose_name="Meta təsvir")
    # description = models.TextField(max_length=1024, blank=True,null=True)
    publish_start_date = models.DateTimeField(null=False,default=datetime.datetime.now(),verbose_name="Yayımlanma Tarixi")
    publish_end_date = models.DateTimeField(null=True,blank=True,verbose_name="Son Tarixi")
    read_count = models.PositiveIntegerField(default=0,editable=True,verbose_name="Oxunma Sayı")
    tags = models.ManyToManyField('Tag',blank=True,verbose_name="Açar Sözlər")
    date = models.DateTimeField(auto_now_add=True,verbose_name="Tarix")
    def __unicode__(self):
        return u"{0}".format(self.first_name + ' - ' + self.second_name)
    def __str__(self):
        return self.first_name + ' - ' + self.second_name

    categorys.short_description = 'Kategoriyalar'

    class Meta:
        ordering = ["-publish_start_date"]
        verbose_name_plural = "Xəbərlər"
        verbose_name = "Xəbər"

    def clean(self):
        if not (self.first_name or self.second_name):
            raise ValidationError('Please add prefix or suffix')

    # def get_images(self):
    #     images = NewsImage.objects.filter(gallery=self)
    #     return images
    # def get_videos(self):
    #     videos = NewsVideo.objects.filter(video=self)
    #     return videos
    # def get_url_videos(self):
    #     videos = NewsVideo.objects.filter(video=self).exclude(video_url=None)
    #     return videos

# class NewsType(models.Model):
#     id = models.UUIDField(primary_key=True, default=uuid.uuid4,editable=False)
#     name = models.CharField(max_length=256,null=False)
#     unique_name = models.CharField(max_length=256,unique=True,null=False)
#     # this field ( text ) translated
#     text = models.CharField(max_length=256,null=False)
#     description = models.CharField(max_length=1024,blank=True)


class NewsView(models.Model):
    news = models.ForeignKey('News', related_name='+')
    ip = models.CharField(max_length=40)
    session = models.CharField(max_length=40)
    created = models.DateTimeField(auto_now_add=True)
    date = models.DateTimeField(default=timezone.now())
    def __str__(self):
        return self.news.first_name
    class Meta:
        verbose_name_plural = "Xəbərlərin Baxışları"
        verbose_name = "Xəbərlərin Baxışı"

def multimedia_upload_location(instance,filename):
    return "news/%s/%s" %(instance.type,filename)
class Multimedia(models.Model):
    Multimedia_CHOICES = (
                  ('', "------ " + _('choose_type') + ' ------'),
                  ('video_news', "Video Xəbər"),
                  ('gallery_news', "Foto Xəbər"),
                 )
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    is_active = models.BooleanField(default=False,editable=False,verbose_name="Yayımla")
    author = models.ForeignKey(settings.AUTH_USER_MODEL,related_name='+',editable=False,blank=True,null=True,verbose_name='Yazar')
    updated_by_user = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='+',editable=False,blank=True,null=True,verbose_name='Redaktə Edən')
    image = models.ImageField(max_length=500,upload_to=news_upload_location,blank=False,verbose_name='Şəkil')
    type = models.CharField(choices=Multimedia_CHOICES,max_length=20,verbose_name='Xəbərin Tipi')
    first_name = models.CharField(verbose_name='prefix',max_length=256, null=True,blank=True)
    second_name = models.CharField(verbose_name='suffix',max_length=256, null=True,blank=True)
    slug  = models.SlugField()
    show_slider = models.BooleanField(default=False,verbose_name="Sliderdə Olsun")
    is_today = models.BooleanField(default=False,verbose_name="Günün Video və ya Foto Xəbəri")
    short_text = models.CharField(max_length=256, null=True,blank=True,verbose_name="Qısa Tekst")
    # full_text = RichTextUploadingField(config_name='awesome_ckeditor',blank=True,null=True)
    meta_keywords = models.CharField(max_length=10240, null=True,blank=True,verbose_name="Meta Açar Sözlər")
    meta_description = models.TextField(null=True,blank=True,verbose_name="Meta təsvir")
    # description = models.TextField(max_length=1024, blank=True,null=True)
    publish_start_date = models.DateTimeField(null=False,default=timezone.now(),verbose_name="Yayımlanma Tarixi")
    publish_end_date = models.DateTimeField(null=True,blank=True,verbose_name="Son Yayımlanma Tarixi")
    read_count = models.PositiveIntegerField(default=0,editable=True,verbose_name="Oxunma Sayı")
    tags = models.ManyToManyField('Tag',blank=True,verbose_name="Açar Sözlər")
    date = models.DateTimeField(auto_now_add=True,verbose_name="Tarix")
    def __unicode__(self):
        return u"{0}".format(self.first_name + ' - ' + self.second_name)
    def __str__(self):
        return self.first_name + ' - ' + self.second_name
    class Meta:
        ordering = ["-date"]
        verbose_name_plural = "Video və Qaleriyalar"
        verbose_name = "Video və Qaleriya"
    def get_images(self):
        images = MultimediaImage.objects.filter(multimedia=self)
        return images
    def get_videos(self):
        videos = MultimediaVideo.objects.filter(multimedia=self)
        return videos
    def get_url_videos(self):
        videos = MultimediaVideo.objects.filter(multimedia=self).exclude(video_url=None)
        return videos
# class NewsType(models.Model):
#     id = models.UUIDField(primary_key=True, default=uuid.uuid4,editable=False)
#     name = models.CharField(max_length=256,null=False)
#     unique_name = models.CharField(max_length=256,unique=True,null=False)
#     # this field ( text ) translated
#     text = models.CharField(max_length=256,null=False)
#     description = models.CharField(max_length=1024,blank=True)

class Tag(models.Model):
    title = models.CharField(max_length=255,verbose_name='Şəkil')
    date = models.DateTimeField(auto_now_add=True,verbose_name='Şəkil')
    slug = models.SlugField()
    def __str__(self):
        return self.title

    class Meta:
        verbose_name_plural = "Taqlar"
        verbose_name = "Taq"


# class NewsType(models.Model):
#     status = models.CharField(max_length=256, null=False)
#
#     name = models.CharField(max_length=256, null=False)
#
#     unique_name = models.CharField(max_length=256, unique=True, null=False)


def gallery_image_upload_location(instance,filename):
    return "gallery/%s/%s" %(instance.gallery.slug,filename)
# class NewsImage(models.Model):
#     gallery = models.ForeignKey('News')
#     image = models.FileField(upload_to=gallery_image_upload_location,blank=False)
#     # context = models.CharField(max_length=255,blank=True)
#     date = models.DateTimeField(auto_now_add=True)


def multimedia_image_upload_location(instance,filename):
    return "multimedia/galleries/%s/%s" %(instance.multimedia.slug,filename)
class MultimediaImage(models.Model):
    multimedia = models.ForeignKey('Multimedia')
    image = models.FileField(upload_to=multimedia_image_upload_location,blank=False,verbose_name='Şəkil')
    context = models.TextField(blank=True,verbose_name='Mətn')
    date = models.DateTimeField(auto_now_add=True,verbose_name='Tarix')
    class Meta:
        verbose_name_plural = "Foto Xəbər Şəkiləri"
        verbose_name = "Foto Xəbər Şəkli"

def video_image_upload_location(instance,filename):
    return "multimedia/videos/%s/%s" %(instance.multimedia.slug,filename)

def video_video_upload_location(instance,filename):
    return "multimedia/videos/%s/%s" %(instance.multimedia.slug,filename)
class MultimediaVideo(models.Model):
    multimedia = models.ForeignKey('Multimedia')
    video_file = models.FileField(upload_to=video_video_upload_location,blank=True,null=True,verbose_name='Video Fayl')
    video_url = models.URLField(blank=True,null=True,verbose_name='Video Linki,youtube,vimeo və.s')
    date = models.DateTimeField(auto_now_add=True,verbose_name='Tarix')
    class Meta:
        verbose_name_plural = "Video Xəbər Videoları"
        verbose_name = "Video Xəbər Videosu"

class Feedback(models.Model):
    user = models.ForeignKey(User,blank=True,null=True)
    content = models.TextField()
    date = models.DateTimeField(auto_now_add=True)