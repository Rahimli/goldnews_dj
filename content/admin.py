from django.contrib import admin
from .models import *
# Register your models here.

from django.contrib.admin.widgets import AdminFileWidget
from django.utils.safestring import mark_safe


class ImageWidget(AdminFileWidget):
    def render(self, name, value, attrs=None):
        output = []
        if value and hasattr(value, "url"):
            output.append(('<a target="_blank" href="%s">'
                           '<img src="%s" height="200" title="%s"/></a> '
                           % (value.url, value.url, value.name)))
        output.append(super(AdminFileWidget, self).render(name, value, attrs))
        return mark_safe(u''.join(output))

class TagAdmin(admin.ModelAdmin):
    list_display = ("title", "date")
    list_filter = ("title", "date")
    search_fields = ("title",)
    prepopulated_fields = {'slug': ('title',)}

admin.site.register(Tag, TagAdmin)


class CategoryAdmin(admin.ModelAdmin):
    # def save_model(self, request, instance, form, change):
    #     user = request.user
    #     instance = form.save(commit=False)
    #     if not change or not instance.user:
    #         instance.user = user
    #     instance.modified_by = user
    #     instance.save()
    #     form.save_m2m()
    #     return instance
    list_display = ("name",)
    list_filter = ("name",)
    search_fields = ("name",'unique_name','description',)
    prepopulated_fields = {'slug': ('unique_name',)}

admin.site.register(Category,CategoryAdmin)

# class NewsImageInlineAdmin(admin.TabularInline):
#     model = NewsImage
#     extra = 1
#     def get_extra(self, request, obj=None, **kwargs):
#         if obj:
#              return 1
#         return self.extra
#     def formfield_for_dbfield(self, db_field, **kwargs):
#         if db_field.name == 'image':
#             request = kwargs.pop("request", None)
#             kwargs['widget'] = ImageWidget
#             return db_field.formfield(**kwargs)
#         return super(NewsImageInlineAdmin, self).formfield_for_dbfield(db_field, **kwargs)
#
# class NewsVideoInlineAdmin(admin.TabularInline):
#     model = NewsVideo
#     extra = 1
#     def get_extra(self, request, obj=None, **kwargs):
#         if obj:
#              return 1
#         return self.extra

class NewsAdmin(admin.ModelAdmin):
    def save_model(self, request, instance, form, change):
        user = request.user
        instance = form.save(commit=False)
        if not change or not instance.author:
            instance.author = user
        if change and not instance.updated_by_user:
            instance.updated_by_user = user
        instance.modified_by = user
        instance.save()
        form.save_m2m()
        return instance
    list_display = ("first_name","read_count","author","updated_by_user","show_slider","publish_start_date","is_active")
    prepopulated_fields = {'slug': ('first_name','second_name',)}
    list_filter = ("publish_start_date","is_active","read_count","author","show_slider","date",)
    list_editable = ("show_slider",)
    search_fields = ('first_name','short_text','full_text','meta_description','meta_keywords','description','read_count',)
    # inlines = (NewsImageInlineAdmin,NewsVideoInlineAdmin,)
    # list_display = ['first_name', 'image_thumb']
    def formfield_for_dbfield(self, db_field, **kwargs):
        if db_field.name == 'image':
            request = kwargs.pop("request", None)
            kwargs['widget'] = ImageWidget
            return db_field.formfield(**kwargs)
        return super(NewsAdmin, self).formfield_for_dbfield(db_field, **kwargs)

admin.site.register(News,NewsAdmin)

# admin.site.register(NewsType)
class NewsViewAdmin(admin.ModelAdmin):
    search_fields = ('ip',)
admin.site.register(NewsView,NewsViewAdmin)



class MultimediaImageInlineAdmin(admin.TabularInline):
    model = MultimediaImage
    extra = 1
    def get_extra(self, request, obj=None, **kwargs):
        if obj:
             return 1
        return self.extra
    def formfield_for_dbfield(self, db_field, **kwargs):
        if db_field.name == 'image':
            request = kwargs.pop("request", None)
            kwargs['widget'] = ImageWidget
            return db_field.formfield(**kwargs)
        return super(MultimediaImageInlineAdmin, self).formfield_for_dbfield(db_field, **kwargs)


class MultimediaVideoInlineAdmin(admin.TabularInline):
    model = MultimediaVideo
    extra = 1
    def get_extra(self, request, obj=None, **kwargs):
        if obj:
             return 1
        return self.extra

class MultimediaAdmin(admin.ModelAdmin):
    def save_model(self, request, instance, form, change):
        user = request.user
        instance = form.save(commit=False)
        if not change or not instance.author:
            instance.author = user
        if change and not instance.updated_by_user:
            instance.updated_by_user = user
        instance.modified_by = user
        instance.save()
        form.save_m2m()
        return instance
    list_display = ("first_name","read_count","author","type","updated_by_user","show_slider",)
    prepopulated_fields = {'slug': ('first_name','second_name',)}
    list_filter = ("publish_start_date","is_active","type","read_count","author","show_slider","date",)
    list_editable = ("show_slider",)
    search_fields = ('first_name','short_text','meta_description','meta_keywords','description','read_count',)
    inlines = (MultimediaImageInlineAdmin,MultimediaVideoInlineAdmin)
    # list_display = ['first_name', 'image_thumb']
    def formfield_for_dbfield(self, db_field, **kwargs):
        if db_field.name == 'image':
            request = kwargs.pop("request", None)
            kwargs['widget'] = ImageWidget
            return db_field.formfield(**kwargs)
        return super(MultimediaAdmin, self).formfield_for_dbfield(db_field, **kwargs)

admin.site.register(Multimedia,MultimediaAdmin)